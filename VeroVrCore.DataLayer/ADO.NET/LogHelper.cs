﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using VeroVrCore.Common.Models;
using VeroVrCore.DataLayer.EntityFramework;

namespace VeroVrCore.DataLayer.ADO.NET
{
    public class LogHelper
    {
        private readonly DbProviderFactory _factory;
        private readonly DbType dbType;
        private string _connectionString;

        public LogHelper(string connectionStr, DbType dbType)
        {
            _connectionString = connectionStr;
            
                switch (dbType)
                {
                    case DbType.MsSql:
                    _factory = SqlClientFactory.Instance;
                        break;
                    case DbType.MySql:
                    _factory = MySqlClientFactory.Instance;
                        break;
                    default:
                    throw new NotSupportedException("This type of database wasn't define.");
                }
            this.dbType = dbType;
        }

        private bool ExecuteNonQuery(string commandStr, List<DbParameter> paramList)
        {
            bool result = false;
            using (var conn = _factory.CreateConnection())
            {
                conn.ConnectionString = _connectionString;

                if (conn.State != System.Data.ConnectionState.Open)
                {
                    conn.Open();
                }

                using (var command = _factory.CreateCommand())
                {
                    command.Connection = conn;
                    command.CommandText = commandStr;
                    command.Parameters.AddRange(paramList.ToArray());
                    int count = command.ExecuteNonQuery();
                    result = count > 0;
                }
            }
            return result;
        }

        public bool InsertLog(LogEvent log)
        {
            string command = $@"INSERT INTO LogEvents (EventID, LogLevel, Message, CreatedTime) VALUES (@EventID, @LogLevel, @Message, @CreatedTime)";

            switch (dbType)
            {
                case DbType.MsSql:
                    List<SqlParameter> paramListSql = new List<SqlParameter>();
                    paramListSql.Add(new SqlParameter("EventID", log.EventId));
                    paramListSql.Add(new SqlParameter("LogLevel", log.LogLevel));
                    paramListSql.Add(new SqlParameter("Message", log.Message));
                    paramListSql.Add(new SqlParameter("CreatedTime", log.CreatedTime));

                    return ExecuteNonQuery(command, paramListSql.Cast<DbParameter>().ToList());
                case DbType.MySql:
                    List<MySqlParameter> paramListMySql = new List<MySqlParameter>();
                    paramListMySql.Add(new MySqlParameter("EventID", log.EventId));
                    paramListMySql.Add(new MySqlParameter("LogLevel", log.LogLevel));
                    paramListMySql.Add(new MySqlParameter("Message", log.Message));
                    paramListMySql.Add(new MySqlParameter("CreatedTime", log.CreatedTime));

                    return ExecuteNonQuery(command, paramListMySql.Cast<DbParameter>().ToList());
                default:
                    throw new NotSupportedException("This type of database wasn't define.");
            }
        }
    }
}
