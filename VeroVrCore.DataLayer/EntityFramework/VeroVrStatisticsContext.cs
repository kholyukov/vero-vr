﻿namespace VeroVrCore.DataLayer.EntityFramework
{
    using global::VeroVrCore.Common.Models;
    using Microsoft.EntityFrameworkCore;

    namespace VeroVrCore.DataLayer.EntityFramework
    {
        public class VeroVrStatisticsContext : DbContext
        {
            public DbSet<LogEvent> LogEvents { get; set; }
            public DbSet<UserActivity> UserActivities { get; set; }
            public DbSet<UserClickLinkInDescriptionActivity> UserClickLinkInDescriptionActivities { get; set; }
            public DbSet<UserInArActivity> UserInArActivities { get; set; }
            public DbSet<UserInProjectDescriptionActivity> UserInProjectDescriptionActivities { get; set; }
            public DbSet<UserInRoom360Activity> UserInRoom360Activities { get; set; }
            public DbSet<UserInRoomVrActivity> UserInRoomVrActivities { get; set; }
            public DbSet<UserInYouTubeVideoActivity> UserInYouTubeVideoActivities { get; set; }
            public DbSet<UserLoginActivity> UserLoginActivities { get; set; }
            public DbSet<UserObjectActivity> UserObjectActivities { get; set; }
            public DbSet<UserProjectActivity> UserProjectActivities { get; set; }
            public DbSet<UserRoomImageActivity> UserRoomImageActivities { get; set; }
            public DbSet<UserShareProjectActivity> UserShareProjectActivities { get; set; }
            public DbSet<UserInPlansActivity> UserInPlansActivities { get; set; }

            public VeroVrStatisticsContext(DbContextOptions<VeroVrStatisticsContext> options) : base(options)
            {
            }
            protected override void OnModelCreating(ModelBuilder modelBuilder)
            {
                modelBuilder.Entity<LogEvent>(entity =>
                {
                    entity.HasKey(e => e.Id);
                    entity.Property(e => e.Id)
                    .IsRequired()
                    .HasColumnName("Id")
                    .HasColumnType("int")
                    .ValueGeneratedOnAdd();

                    entity.Property(e => e.LogLevel).HasMaxLength(50);

                    entity.Property(e => e.Message).HasMaxLength(4000);
                });

                modelBuilder.Entity<UserLoginActivity>(entity =>
                {
                    entity.HasKey(e => e.Id);
                    entity.Property(e => e.Id)
                    .IsRequired()
                    .HasColumnName("Id")
                    .HasColumnType("int")
                    .ValueGeneratedOnAdd();

                    entity.Property(e => e.UserId)
                       .HasColumnType("int")
                       .IsRequired();

                    entity.Property(e => e.EventDateStamp)
                       .HasDefaultValueSql("'0001-01-01 00:00:00'")
                       .IsRequired();
                    entity.HasIndex(e => e.EventDateStamp);
                });

                modelBuilder.Entity<UserActivity>(entity =>
                {
                    entity.HasKey(e => e.Id);
                    entity.Property(e => e.Id)
                    .IsRequired()
                    .HasColumnName("Id")
                    .HasColumnType("int")
                    .ValueGeneratedOnAdd();

                    entity.Property(e => e.UserId)
                      .HasColumnType("int")
                      .IsRequired();

                    entity.Property(e => e.EventDateStamp)
                       .HasDefaultValueSql("'0001-01-01 00:00:00'")
                       .IsRequired();
                    entity.HasIndex(e => e.EventDateStamp);
                });

                modelBuilder.Entity<UserShareProjectActivity>(entity =>
                {
                    entity.HasKey(e => e.Id);
                    entity.Property(e => e.Id)
                    .IsRequired()
                    .HasColumnName("Id")
                    .HasColumnType("int")
                    .ValueGeneratedOnAdd();

                    entity.Property(e => e.EventDateStamp)
                       .HasDefaultValueSql("'0001-01-01 00:00:00'")
                       .IsRequired();
                    entity.HasIndex(e => e.EventDateStamp);
                });

                modelBuilder.Entity<UserClickLinkInDescriptionActivity>(entity =>
                {
                    entity.HasKey(e => e.Id);
                    entity.Property(e => e.Id)
                    .IsRequired()
                    .HasColumnName("Id")
                    .HasColumnType("int")
                    .ValueGeneratedOnAdd();

                    entity.Property(e => e.EventDateStamp)
                       .HasDefaultValueSql("'0001-01-01 00:00:00'")
                       .IsRequired();
                    entity.HasIndex(e => e.EventDateStamp);
                });
            }
        }
    }

}
