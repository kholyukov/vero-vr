﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using VeroVrCore.Common.Infrastructure;
using VeroVrCore.Common.Models;
using VeroVrCore.Common.Security;

namespace VeroVrCore.DataLayer.EntityFramework
{
    public class VeroVrDataInitializer
    {
        public static void Initialize(VeroVrContext context, bool isUseMySql = false)
        {
            //context.Database.EnsureDeleted();

            var creator = context.Database.GetService<IDatabaseCreator>() as RelationalDatabaseCreator;

            bool isExistsDb = creator.Exists();

            if (isExistsDb == false)
            {
                creator.Create();

                if (isUseMySql)
                {
                    string databaseName = context.Database.GetDbConnection().Database;
                    context.Database.ExecuteSqlCommand(new RawSqlString($"ALTER DATABASE {databaseName} CHARACTER SET utf8 COLLATE utf8_general_ci;"));
                }
            }

            context.Database.Migrate();

            if (!context.Users.Any())
            {
                var roomTypeStreet = new RoomType { RoomTypeName = "Street" };
                var roomTypeLivingRoom = new RoomType { RoomTypeName = "Living room" };
                var bedRoom = new RoomType { RoomTypeName = "Bedroom" };
                var kitchen = new RoomType { RoomTypeName = "Kitchen" };
                var kidsRoom = new RoomType { RoomTypeName = "Kids room" };
                var defaultRoomType = new RoomType { RoomTypeName = InfoOptions.DefaultInfoOption };

                var dayTimeDay = new DayTime
                {
                    DayTimeName = "Day",
                };

                var nightTimeDay = new DayTime
                {
                    DayTimeName = "Night",
                };

                context.DayTimes.Add(dayTimeDay);
                context.DayTimes.Add(nightTimeDay);

                var firstPlanName = new PlanInfo { PlanName = "First plan name" };
                var secondPlanName = new PlanInfo { PlanName = "Second plan name" };
                var defaultPlanName = new PlanInfo { PlanName = InfoOptions.DefaultInfoOption };

                var boutiqueArObjectName = new ObjectArInfo { ObjectArName = "Boutique" };
                var millArObjectName = new ObjectArInfo { ObjectArName = "Mill" };
                var houseArObjectName = new ObjectArInfo { ObjectArName = "House" };
                var testtexArObjectName = new ObjectArInfo { ObjectArName = "testtex" };
                var testtex2ArObjectName = new ObjectArInfo { ObjectArName = "testtex2" };

                var defaultArObjectName = new ObjectArInfo { ObjectArName = InfoOptions.DefaultInfoOption };

                var interiorClassic = new Interior
                {
                    InteriorName = "Classic"
                };

                var interiorDefault = new Interior
                {
                    InteriorName = InfoOptions.DefaultInfoOption
                };

                var projectOne = new Project
                {
                    ProjectCode = "124BH31F",
                    ProjectName = "Project 1",
                    ProjectDescription = " [http://verovisuals.com/] - link name:(verovisuals) Description of Project 1",
                    ProjectImageUrl = "/content/1/8/2ff11cf4-4257-442f-bc0d-bb5def6da304.jpg",
                    RoomTypes = new List<RoomType>
                    {
                        roomTypeStreet,
                        roomTypeLivingRoom,
                        bedRoom,
                        kitchen,
                        kidsRoom,
                        defaultRoomType
                    },
                    PlanInfos = new List<PlanInfo>
                    {
                        firstPlanName,
                        secondPlanName,
                        defaultPlanName
                    },
                    Interiors = new List<Interior>
                    {
                        interiorClassic,
                        interiorDefault
                    },
                    ObjectArInfos = new List<ObjectArInfo>
                    {
                        boutiqueArObjectName,
                        millArObjectName,
                        houseArObjectName,
                        testtexArObjectName,
                        testtex2ArObjectName,
                        defaultArObjectName
                    }
                };

                context.Projects.Add(projectOne);

                context.SaveChanges();

                //var projectSecond = new Project
                //{
                //    DefaultRoomId = 1,
                //    ProjectCode = "TEST1234",
                //    ProjectName = "Project 1",
                //    ProjectDescription = "http://verovisuals.com/ Description of test activate project",
                //    ProjectImageUrl = "/content/1/1/8/2ff11cf4-4257-442f-bc0d-bb5def6da304.jpg",
                //    RoomTypes = new List<RoomType>
                //    {
                //        roomTypeLivingRoom,
                //        bedRoom,
                //        kitchen
                //    },
                //    PlanInfos = new List<PlanInfo>
                //    {
                //        firstPlanName,
                //        secondPlanName
                //    },
                //    Interiors = new List<Interior>
                //    {
                //        interiorClassic
                //    }
                //};

                //context.Projects.Add(projectSecond);

                //context.SaveChanges();

                //projectSecond.DefaultInteriorId = interiorClassic.InteriorId;
                //projectSecond.DefaultDayTimeId = dayTimeDay.DayTimeId;
                //projectSecond.DefaultPlanInfoId = firstPlanName.PlanInfoId;

                //context.SaveChanges();

                //var projectSecond = new Project
                //{
                //    ProjectName = "SecondProject",
                //    ProjectDescription = "This is 2 test project"
                //};

                //context.Projects.Add(projectSecond);

                const string testsalt = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9";

                var userSuperAdmin = new User
                {
                    UserAddress = "Test address 1",
                    UserAvatarUrl = "/content/user/avatar/0ca33b1c-a523-4a1e-aef7-fe9919435774.jpg",
                    UserDisplayName = "SuperAdmin User",
                    UserEmail = "superadmin@mail.com",
                    UserPhoneNumber = "+375256358364",
                    UserPassword = Hasher.Hash("123", testsalt),
                    Salt = testsalt,
                    UserRole = UserRole.SuperAdmin,
                    UserCompanyName = "Apple",
                    UserCountry = "Netherlands",
                    UserCity = "Groningen",
                    UserAgeRange = "35-44",
                    DateOfRegistration = DateTime.Now,
                    UserFacebook = "https://facebook.com/",
                    FacebookLikes = new[]
                    {
                        "https://facebook.com/test1",
                        "https://facebook.com/test2",
                        "https://facebook.com/test3"
                    }
                };

                userSuperAdmin.UserProjects = new List<UserProject>
                {
                    new UserProject
                    {
                        User = userSuperAdmin,
                        Project = projectOne
                    }
                };

                context.Users.Add(userSuperAdmin);

                var userSuperManager = new User
                {
                    UserAddress = "Test address 2",
                    UserAvatarUrl = "/content/user/avatar/0ca33b1c-a523-4a1e-aef7-fe9919435774.jpg",
                    UserDisplayName = "SuperManager User",
                    UserEmail = "supermanager@mail.com",
                    UserPhoneNumber = "+375446689618",
                    UserPassword = Hasher.Hash("123", testsalt),
                    Salt = testsalt,
                    UserRole = UserRole.SuperManager,
                    UserCompanyName = "Microsoft",
                    UserCountry = "Netherlands",
                    UserCity = "Middelburg",
                    UserAgeRange = "25-34",
                    DateOfRegistration = DateTime.Now,
                    DeviceAtLastLogin = "IPhone 8",
                    DeviceTypeAtLastLogin = "IOS Phone",
                    UserFacebook = "https://facebook.com/",
                    FacebookLikes = new[]
                    {
                        "https://facebook.com/test1",
                        "https://facebook.com/test2",
                        "https://facebook.com/test3"
                    }
                };

                userSuperManager.UserProjects = new List<UserProject>
                {
                    new UserProject
                    {
                        User = userSuperManager,
                        Project = projectOne
                    }
                };

                context.Users.Add(userSuperManager);

                var userSuperManagerWithScript = new User
                {
                    UserAddress = "Test address 3",
                    UserAvatarUrl = "/content/user/avatar/0ca33b1c-a523-4a1e-aef7-fe9919435774.jpg",
                    UserDisplayName = "SuperManagerWithScript User",
                    UserEmail = "supermanagerwithscript@mail.com",
                    UserPhoneNumber = "+375256312874",
                    UserPassword = Hasher.Hash("123", testsalt),
                    Salt = testsalt,
                    UserRole = UserRole.SuperManagerWithScript,
                    UserCompanyName = "Hp",
                    UserCountry = "Netherlands",
                    UserCity = "Middelburg",
                    UserAgeRange = "25-34",
                    DateOfRegistration = DateTime.Now,
                    DeviceAtLastLogin = "IPhone 8s",
                    DeviceTypeAtLastLogin = "IOS Phone",
                    UserFacebook = "https://facebook.com/test999profile",
                    FacebookLikes = new[]
                    {
                        "https://facebook.com/test4",
                        "https://facebook.com/test5",
                        "https://facebook.com/test6"
                    }
                };

                userSuperManagerWithScript.UserProjects = new List<UserProject>
                {
                    new UserProject
                    {
                        User = userSuperManagerWithScript,
                        Project = projectOne
                    }
                };

                context.Users.Add(userSuperManagerWithScript);

                var userManager = new User
                {
                    UserAddress = "1146 Pleasant St.",
                    UserAvatarUrl = "/content/user/avatar/0ca33b1c-a523-4a1e-aef7-fe9919435774.jpg",
                    UserDisplayName = "Manager User",
                    UserEmail = "manager@mail.com",
                    Occupation = "Manager",
                    UserAgeRange = "25-34",
                    UserCountry = "United States",
                    UserCity = "New York",
                    UserPhoneNumber = "+375256332597",
                    UserPassword = Hasher.Hash("123", testsalt),
                    Salt = testsalt,
                    UserRole = UserRole.Manager,
                    UserCompanyName = "Microsoft",
                    DateOfRegistration = DateTime.Now,
                    DeviceAtLastLogin = "IPad 5 mini",
                    DeviceTypeAtLastLogin = "IOS Tablet",
                    UserFacebook = "https://facebook.com/test1119profile",
                    FacebookLikes = new[]
                    {
                        "https://facebook.com/test14",
                        "https://facebook.com/test55",
                        "https://facebook.com/test36"
                    }
                };

                userManager.UserProjects = new List<UserProject>
                {
                    new UserProject
                    {
                        User = userManager,
                        Project = projectOne
                    }
                };

                context.Users.Add(userManager);

                var userCustomer = new User
                {
                    UserDisplayName = "John",
                    UserAvatarUrl = "/content/user/avatar/0ca33b1c-a523-4a1e-aef7-fe9919435774.jpg",
                    UserAddress = "9146 Pleasant St.",
                    UserEmail = "customer@mail.com",
                    UserPhoneNumber = "+375256321456",
                    Occupation = "businessman",
                    UserCountry = "Germany",
                    UserCity = "Schwerin",
                    UserPassword = Hasher.Hash("123", testsalt),
                    Salt = testsalt,
                    UserAgeRange = "25-34",
                    UserRole = UserRole.Customer,
                    DateOfRegistration = DateTime.Now.AddMonths(-1),
                    DeviceAtLastLogin = "IPad 5 mini",
                    DeviceTypeAtLastLogin = "IOS Tablet",
                    UserFacebook = "https://facebook.com/test232profile",
                    FacebookLikes = new[]
                    {
                        "https://facebook.com/test34",
                        "https://facebook.com/test55",
                        "https://facebook.com/test86"
                    }
                };

                userCustomer.UserProjects = new List<UserProject>
                {
                    new UserProject
                    {
                        User = userCustomer,
                        Project = projectOne
                    }
                };

                context.Users.Add(userCustomer);

                var userWithGmail = new User
                {
                    UserDisplayName = "Test User",
                    UserAddress = "9146 Domino St.",
                    UserEmail = "yrgbel2@gmail.com",
                    UserAvatarUrl = "/content/user/avatar/0ca33b1c-a523-4a1e-aef7-fe9919435774.jpg",
                    UserPhoneNumber = "+375256321731",
                    Occupation = "businessman",
                    UserCountry = "Germany",
                    UserCity = "Berlin",
                    UserPassword = Hasher.Hash("123", testsalt),
                    Salt = testsalt,
                    UserAgeRange = "25-34",
                    UserRole = UserRole.Customer,
                    DateOfRegistration = DateTime.Now,
                    DeviceAtLastLogin = "IPad 5 mini",
                    DeviceTypeAtLastLogin = "IOS Tablet",
                    UserFacebook = "https://facebook.com/test2442profile",
                    FacebookLikes = new[]
                    {
                        "https://facebook.com/test41",
                        "https://facebook.com/test35",
                        "https://facebook.com/test61"
                    }
                };

                userWithGmail.UserProjects = new List<UserProject>
                {
                    new UserProject
                    {
                        User = userWithGmail,
                        Project = projectOne
                    }
                };

                context.Users.Add(userWithGmail);

                var userWithGmail2 = new User
                {
                    UserDisplayName = "Test User Second",
                    UserAddress = "9146 Velos St.",
                    UserEmail = "yrgbel3@gmail.com",
                    UserAvatarUrl = "/content/user/avatar/0ca33b1c-a523-4a1e-aef7-fe9919435774.jpg",
                    UserPhoneNumber = "+375256329999",
                    Occupation = "businessman",
                    UserCountry = "Germany",
                    UserCity = "Berlin",
                    UserPassword = Hasher.Hash("123", testsalt),
                    Salt = testsalt,
                    UserAgeRange = "35-44",
                    UserRole = UserRole.Customer,
                    DateOfRegistration = DateTime.Now.AddMonths(-3),
                    DeviceAtLastLogin = "IPad 5 mini",
                    DeviceTypeAtLastLogin = "IOS Tablet",
                    UserFacebook = "https://facebook.com/test55profile",
                    FacebookLikes = new[]
                    {
                        "https://facebook.com/test44",
                        "https://facebook.com/test53",
                        "https://facebook.com/test64"
                    }
                };

                userWithGmail2.UserProjects = new List<UserProject>
                {
                    new UserProject
                    {
                        User = userWithGmail2,
                        Project = projectOne
                    }
                };

                context.Users.Add(userWithGmail2);

                var customerNetherlands1 = new User
                {
                    UserAddress = "Test address 2",
                    UserAvatarUrl = "/content/user/avatar/0ca33b1c-a523-4a1e-aef7-fe9919435774.jpg",
                    UserDisplayName = "Customer User One",
                    UserEmail = "customerNetherlands1@mail.com",
                    UserPhoneNumber = "+375446689999",
                    UserPassword = Hasher.Hash("123", testsalt),
                    Salt = testsalt,
                    UserRole = UserRole.Customer,
                    UserCompanyName = "Microsoft",
                    UserCountry = "Netherlands",
                    UserCity = "Amsterdam",
                    UserAgeRange = "35-44",
                    DateOfRegistration = DateTime.Now,
                    DeviceAtLastLogin = "IPhone 8",
                    DeviceTypeAtLastLogin = "IOS Phone"
                };

                customerNetherlands1.UserProjects = new List<UserProject>
                {
                    new UserProject
                    {
                        User = customerNetherlands1,
                        Project = projectOne
                    }
                };

                context.Users.Add(customerNetherlands1);

                var customerNetherlands2 = new User
                {
                    UserAddress = "Test address 3",
                    UserAvatarUrl = "/content/user/avatar/0ca33b1c-a523-4a1e-aef7-fe9919435774.jpg",
                    UserDisplayName = "Customer User Second",
                    UserEmail = "customerNetherlands2@mail.com",
                    UserPhoneNumber = "+375256312874",
                    UserPassword = Hasher.Hash("123", testsalt),
                    Salt = testsalt,
                    UserRole = UserRole.Customer,
                    UserCompanyName = "Hp",
                    UserCountry = "Netherlands",
                    UserCity = "Amsterdam",
                    UserAgeRange = "35-44",
                    DateOfRegistration = DateTime.Now.AddMonths(-1),
                    DeviceAtLastLogin = "IPhone 8",
                    DeviceTypeAtLastLogin = "IOS Phone"
                };

                customerNetherlands2.UserProjects = new List<UserProject>
                {
                    new UserProject
                    {
                        User = customerNetherlands2,
                        Project = projectOne
                    }
                };

                context.Users.Add(customerNetherlands2);

                var customerNetherlands4 = new User
                {
                    UserAddress = "1146 Pleasant St.",
                    UserAvatarUrl = "/content/user/avatar/0ca33b1c-a523-4a1e-aef7-fe9919435774.jpg",
                    UserDisplayName = "Customer User test",
                    UserEmail = "customerNetherlands4@mail.com",
                    Occupation = "Manager",
                    UserAgeRange = "35-44",
                    UserCountry = "Netherlands",
                    UserCity = "Middelburg",
                    UserPhoneNumber = "+375256332597",
                    UserPassword = Hasher.Hash("123", testsalt),
                    Salt = testsalt,
                    UserRole = UserRole.Customer,
                    UserCompanyName = "Microsoft",
                    DateOfRegistration = DateTime.Now,
                    DeviceAtLastLogin = "IPhone 8",
                    DeviceTypeAtLastLogin = "IOS Phone"
                };

                customerNetherlands4.UserProjects = new List<UserProject>
                {
                    new UserProject
                    {
                        User = customerNetherlands4,
                        Project = projectOne
                    }
                };

                context.Users.Add(customerNetherlands4);

                var customerUsa1 = new User
                {
                    UserAddress = "1146 Pleasant St.",
                    UserAvatarUrl = "/content/user/avatar/0ca33b1c-a523-4a1e-aef7-fe9919435774.jpg",
                    UserDisplayName = "Иванов Иван",
                    UserEmail = "customerNetherlands3@mail.com",
                    Occupation = "Manager",
                    UserAgeRange = "25-34",
                    UserCountry = "United States",
                    UserCity = "New York",
                    UserPhoneNumber = "+375256332597",
                    UserPassword = Hasher.Hash("123", testsalt),
                    Salt = testsalt,
                    UserRole = UserRole.Customer,
                    UserCompanyName = "Microsoft",
                    DateOfRegistration = DateTime.Now.AddMonths(-2),
                    DeviceAtLastLogin = "IPhone 8",
                    DeviceTypeAtLastLogin = "IOS Phone"
                };

                customerUsa1.UserProjects = new List<UserProject>
                {
                    new UserProject
                    {
                        User = customerUsa1,
                        Project = projectOne
                    }
                };

                context.Users.Add(customerUsa1);

                context.SaveChanges();

                //Plans
                var objectPlan1 = new ObjectPlan
                {
                    ObjectName = firstPlanName.PlanName,
                    PathFileOnServer = "/content/1/5/bd8a2462-a618-4eeb-9250-d230129bd3b8.jpg",
                    ObjectType = ObjectType.Plan,
                    Project = projectOne,
                    TransitionPoints = new List<TransitionPoint>
                        {
                            new TransitionPoint
                            {
                                PositionX = 0,
                                PositionY = 0,
                                PositionZ = 0,
                                RoomId = 1
                            },
                            new TransitionPoint
                            {
                                PositionX = 62.99415,
                                PositionY = -48.0411873,
                                PositionZ = 0,
                                RoomId = 2
                            },
                            new TransitionPoint
                            {
                                PositionX = 84.9902,
                                PositionY = 103.038132,
                                PositionZ = 0,
                                RoomId = 4
                            },
                            new TransitionPoint
                            {
                                PositionX = 225.002441,
                                PositionY = -39.1452255,
                                PositionZ = 0,
                                RoomId = 5
                            },
                            new TransitionPoint
                            {
                                PositionX = -69.59064,
                                PositionY = 109.360161,
                                PositionZ = 0,
                                RoomId = 3
                            },
                            new TransitionPoint
                            {
                                PositionX = 236.672028,
                                PositionY = 40.7684669,
                                PositionZ = 0,
                                RoomId = 6
                            }
                        },
                    FloorNumber = 1
                };

                context.Objects.Add(objectPlan1);

                var objectPlan2 = new ObjectPlan
                {
                    ObjectName = secondPlanName.PlanName,
                    PathFileOnServer = "/content/1/5/ae8a7462-d808-8ee9-1150-c330629bd4c8.jpg",
                    ObjectType = ObjectType.Plan,
                    Project = projectOne,
                    TransitionPoints = new List<TransitionPoint>
                        {
                            new TransitionPoint
                            {
                                PositionX = 0.0,
                                PositionY = 0.0,
                                PositionZ = 0.0,
                                RoomId = 15
                            }
                        },
                    FloorNumber = 2
                };

                context.Objects.Add(objectPlan2);
                context.SaveChanges();

                //Rooms
                var room1 = new ObjectRoom
                {
                    ObjectType = ObjectType.Room,
                    Project = projectOne,
                    ObjectName = roomTypeStreet.RoomTypeName,
                    TransitionPoints = new List<TransitionPoint>
                        {
                            new TransitionPoint
                            {
                                PositionX = -5.88245773,
                                PositionY = 1.1976943,
                                PositionZ = -10.1259928,
                                RoomId = 3
                            }
                        }
                };

                var roomImagesForRoom1 = new List<RoomImage>
                {
                    new RoomImage
                    {
                        ImageUrl = "/content/1/1/0614a4b4-18c9-4be1-bd20-13833cdf721c.jpg",
                        DayTime = dayTimeDay,
                        InteriorId = interiorClassic.InteriorId,
                        RoomTypeId = roomTypeStreet.RoomTypeId,
                        ObjectPlanId = objectPlan1.ObjectId,
                        Object = room1
                    },
                     new RoomImage
                    {
                        ImageUrl = "/content/1/1/cee113c9-197d-482f-a6a3-1f450d50681b.jpg",
                        DayTime = nightTimeDay,
                        InteriorId = interiorClassic.InteriorId,
                        RoomTypeId = roomTypeStreet.RoomTypeId,
                        ObjectPlanId = objectPlan1.ObjectId,
                        Object = room1
                    }
                };

                room1.RoomImages = roomImagesForRoom1;

                var room2 = new ObjectRoom
                {
                    ObjectType = ObjectType.Room,
                    Project = projectOne,
                    ObjectName = roomTypeLivingRoom.RoomTypeName,
                    TransitionPoints = new List<TransitionPoint>
                        {
                            new TransitionPoint
                            {
                                PositionX = 10.0024023,
                                PositionY = -1.49152064,
                                PositionZ = 1.07350731,
                                RoomId = 4
                            },
                            new TransitionPoint
                            {
                                PositionX = -2.276423,
                                PositionY = -1.07079887,
                                PositionZ = -9.629799,
                                RoomId = 2
                            }
                        }
                };

                var roomImagesForRoom2 = new List<RoomImage>
                {
                    new RoomImage
                    {
                        ImageUrl = "/content/1/1/9ca84b0d-a523-4a1e-aef9-fe9918735804.jpg",
                        DayTime = dayTimeDay,
                        InteriorId = interiorClassic.InteriorId,
                        RoomTypeId = roomTypeLivingRoom.RoomTypeId,
                        ObjectPlanId = objectPlan1.ObjectId,
                        Object = room2
                    }
                };

                room2.RoomImages = roomImagesForRoom2;

                var room3 = new ObjectRoom
                {
                    ObjectType = ObjectType.Room,
                    Project = projectOne,
                    ObjectName = kitchen.RoomTypeName,
                    TransitionPoints = new List<TransitionPoint>
                        {
                            new TransitionPoint
                            {
                                PositionX = 2.0128243,
                                PositionY = -3.6809,
                                PositionZ = -9.039434,
                                RoomId = 3
                            },
                            new TransitionPoint
                            {
                                PositionX = 3.13297677,
                                PositionY = 1.162096,
                                PositionZ = 10.1617861,
                                RoomId = 5
                            }
                        }
                };

                room3.RoomImages = new List<RoomImage>
                {
                    new RoomImage
                    {
                        ImageUrl = "/content/1/1/0614a4b3-18c9-4be1-bd20-13833cdf988b.jpg",
                        DayTime = dayTimeDay,
                        InteriorId = interiorClassic.InteriorId,
                        RoomTypeId = kitchen.RoomTypeId,
                        ObjectPlanId = objectPlan1.ObjectId,
                        Object = room3
                    }
                };

                var room4 = new ObjectRoom
                {
                    ObjectType = ObjectType.Room,
                    Project = projectOne,
                    ObjectName = bedRoom.RoomTypeName,
                    TransitionPoints = new List<TransitionPoint>
                        {
                            new TransitionPoint
                            {
                                PositionX = 0.380395,
                                PositionY = 1.01563668,
                                PositionZ = -10.1152182,
                                RoomId = 6
                            },
                            new TransitionPoint
                            {
                                PositionX = 3.91643572,
                                PositionY = 0.992079735,
                                PositionZ = -10.3182516,
                                RoomId = 4
                            }
                        }
                };

                room4.RoomImages = new List<RoomImage>
                {
                    new RoomImage
                    {
                        ImageUrl = "/content/1/1/07483d69-fb0b-42f0-a206-4ac0334310a2.jpg",
                        DayTime = dayTimeDay,
                        InteriorId = interiorClassic.InteriorId,
                        RoomTypeId = bedRoom.RoomTypeId,
                        ObjectPlanId = objectPlan1.ObjectId,
                        Object = room4
                    }
                };

                var room5 = new ObjectRoom
                {
                    ObjectType = ObjectType.Room,
                    Project = projectOne,
                    ObjectName = kidsRoom.RoomTypeName,
                    TransitionPoints = new List<TransitionPoint>
                        {
                            new TransitionPoint
                            {
                                PositionX = 9.17607,
                                PositionY = -5.9569397,
                                PositionZ = 2.81669068,
                                RoomId = 3
                            },
                            new TransitionPoint
                            {
                                PositionX = -0.8550862,
                                PositionY = 0.8279047,
                                PositionZ = -10.2613964,
                                RoomId = 5
                            },
                            new TransitionPoint
                            {
                                PositionX = -11.4090538,
                                PositionY = 0.654734,
                                PositionZ = -3.94938612,
                                RoomId = 3
                            }
                        }
                };

                room5.RoomImages = new List<RoomImage>
                {
                    new RoomImage
                    {
                        ImageUrl = "/content/1/1/aee838c9-197d-482f-86a3-1f450d50627b.jpg",
                        DayTime = dayTimeDay,
                        InteriorId = interiorClassic.InteriorId,
                        RoomTypeId = kidsRoom.RoomTypeId,
                        ObjectPlanId = objectPlan1.ObjectId,
                        Object = room5
                    }
                };

                context.Objects.Add(room1);
                context.Objects.Add(room2);
                context.Objects.Add(room3);
                context.Objects.Add(room4);
                context.Objects.Add(room5);

                context.SaveChanges();

                //Floor 2
                var room1Floor2 = new ObjectRoom
                {
                    ObjectType = ObjectType.Room,
                    Project = projectOne,
                    ObjectName = roomTypeStreet.RoomTypeName,
                    TransitionPoints = new List<TransitionPoint>
                        {
                            new TransitionPoint
                            {
                                PositionX = -5.88245773,
                                PositionY = 1.1976943,
                                PositionZ = -10.1259928,
                                RoomId = 16
                            }
                        }
                };

                var roomImagesForRoom1Floor2 = new List<RoomImage>
                {
                    new RoomImage
                    {
                        ImageUrl = "/content/1/1/2a1b1c53-452b-4dc1-b947-1edd3e1d0b89.jpg",
                        DayTime = dayTimeDay,
                        InteriorId = interiorClassic.InteriorId,
                        RoomTypeId = roomTypeStreet.RoomTypeId,
                        ObjectPlanId = objectPlan2.ObjectId,
                        Object = room1
                    },
                     new RoomImage
                    {
                        ImageUrl = "/content/1/1/0b392bd1-9ecd-43dc-9cae-cb6ac4e3f3f2.jpg",
                        DayTime = nightTimeDay,
                        InteriorId = interiorClassic.InteriorId,
                        RoomTypeId = roomTypeLivingRoom.RoomTypeId,
                        ObjectPlanId = objectPlan2.ObjectId,
                        Object = room1
                    }
                };

                room1Floor2.RoomImages = roomImagesForRoom1Floor2;

                var room2Floor2 = new ObjectRoom
                {
                    ObjectType = ObjectType.Room,
                    Project = projectOne,
                    ObjectName = roomTypeLivingRoom.RoomTypeName,
                    TransitionPoints = new List<TransitionPoint>
                        {
                            new TransitionPoint
                            {
                                PositionX = 10.0024023,
                                PositionY = -1.49152064,
                                PositionZ = 1.07350731,
                                RoomId = 4
                            },
                            new TransitionPoint
                            {
                                PositionX = -2.276423,
                                PositionY = -1.07079887,
                                PositionZ = -9.629799,
                                RoomId = 2
                            }
                        }
                };

                var roomImagesForRoom2Floor2 = new List<RoomImage>
                {
                    new RoomImage
                    {
                        ImageUrl = "/content/1/1/0d3f7fce-1fd3-4813-add2-25c110afe321.jpg",
                        DayTime = dayTimeDay,
                        InteriorId = interiorClassic.InteriorId,
                        RoomTypeId = roomTypeLivingRoom.RoomTypeId,
                        ObjectPlanId = objectPlan2.ObjectId,
                        Object = room2
                    }
                };

                room2Floor2.RoomImages = roomImagesForRoom2Floor2;

                var room3Floor2 = new ObjectRoom
                {
                    ObjectType = ObjectType.Room,
                    Project = projectOne,
                    ObjectName = bedRoom.RoomTypeName,
                    TransitionPoints = new List<TransitionPoint>
                        {
                            new TransitionPoint
                            {
                                PositionX = 2.0128243,
                                PositionY = -3.6809,
                                PositionZ = -9.039434,
                                RoomId = 3
                            },
                            new TransitionPoint
                            {
                                PositionX = 3.13297677,
                                PositionY = 1.162096,
                                PositionZ = 10.1617861,
                                RoomId = 5
                            }
                        }
                };

                room3Floor2.RoomImages = new List<RoomImage>
                {
                    new RoomImage
                    {
                        ImageUrl = "/content/1/1/8cc546bd-6bb7-461f-b6f8-88eacf584c68.jpg",
                        DayTime = dayTimeDay,
                        InteriorId = interiorClassic.InteriorId,
                        RoomTypeId = bedRoom.RoomTypeId,
                        ObjectPlanId = objectPlan2.ObjectId,
                        Object = room3
                    }
                };

                var room4Floor2 = new ObjectRoom
                {
                    ObjectType = ObjectType.Room,
                    Project = projectOne,
                    ObjectName = kitchen.RoomTypeName,
                    TransitionPoints = new List<TransitionPoint>
                        {
                            new TransitionPoint
                            {
                                PositionX = 0.380395,
                                PositionY = 1.01563668,
                                PositionZ = -10.1152182,
                                RoomId = 6
                            },
                            new TransitionPoint
                            {
                                PositionX = 3.91643572,
                                PositionY = 0.992079735,
                                PositionZ = -10.3182516,
                                RoomId = 4
                            }
                        }
                };

                room4Floor2.RoomImages = new List<RoomImage>
                {
                    new RoomImage
                    {
                        ImageUrl = "/content/1/1/c9be92b3-510e-4ffe-91af-cecb676fa3ae.jpg",
                        DayTime = dayTimeDay,
                        InteriorId = interiorClassic.InteriorId,
                        RoomTypeId = kitchen.RoomTypeId,
                        ObjectPlanId = objectPlan2.ObjectId,
                        Object = room4
                    }
                };

                var room5Floor2 = new ObjectRoom
                {
                    ObjectType = ObjectType.Room,
                    Project = projectOne,
                    ObjectName = kidsRoom.RoomTypeName,
                    TransitionPoints = new List<TransitionPoint>
                        {
                            new TransitionPoint
                            {
                                PositionX = 9.17607,
                                PositionY = -5.9569397,
                                PositionZ = 2.81669068,
                                RoomId = 3
                            },
                            new TransitionPoint
                            {
                                PositionX = -0.8550862,
                                PositionY = 0.8279047,
                                PositionZ = -10.2613964,
                                RoomId = 5
                            },
                            new TransitionPoint
                            {
                                PositionX = -11.4090538,
                                PositionY = 0.654734,
                                PositionZ = -3.94938612,
                                RoomId = 3
                            }
                        }
                };

                room5Floor2.RoomImages = new List<RoomImage>
                {
                    new RoomImage
                    {
                        ImageUrl = "/content/1/1/c0279587-9b71-4785-9cee-0a92bb08591c.jpg",
                        DayTime = dayTimeDay,
                        InteriorId = interiorClassic.InteriorId,
                        RoomTypeId = kidsRoom.RoomTypeId,
                        ObjectPlanId = objectPlan2.ObjectId,
                        Object = room5
                    }
                };

                context.Objects.Add(room1Floor2);
                context.Objects.Add(room2Floor2);
                context.Objects.Add(room3Floor2);
                context.Objects.Add(room4Floor2);
                context.Objects.Add(room5Floor2);

                context.SaveChanges();

                var firstObjectAr = new ObjectAr
                {
                    ObjectName = boutiqueArObjectName.ObjectArName,
                    PathFileOnServer = "/content/1/3/IximTJNdUEONLSTZlUVTFQBB/house.obj",
                    Additionals = new[]
                    {
                        "/content/1/3/IximTJNdUEONLSTZlUVTFQBB/house.mtl",
                        "/content/1/3/IximTJNdUEONLSTZlUVTFQBB/maps/111.png"
                    },
                    ObjectType = ObjectType.Ar,
                    Project = projectOne,
                    Size = 0.005,
                    TransitionPoints = null,
                    Version = Guid.NewGuid().ToString()
                };

                context.Objects.AddRange(
                   //Ar
                   firstObjectAr,

                    //new ObjectAr
                    //{
                    //    ObjectName = millArObjectName.ObjectArName,
                    //    PathFileOnServer = "/content/1/3/1oF91MUd8kaeVtCChj2YFQBB/low-poly-mill.obj",
                    //    Additionals = new [] { "/content/1/3/1oF91MUd8kaeVtCChj2YFQBB/low-poly-mill.mtl" },
                    //    ObjectType = ObjectType.Ar,
                    //    Project = projectOne,
                    //    Size = 0.005,
                    //    TransitionPoints = null
                    //    Version = Guid.NewGuid().ToString()
                    //},

                    new ObjectAr
                    {
                        ObjectName = testtexArObjectName.ObjectArName,
                        PathFileOnServer = "/content/1/3/2iiaeJNdUEONLSTZlUSTSST/modelhouse.obj",
                        Additionals = new[]
                        {
                            "/content/1/3/2iiaeJNdUEONLSTZlUSTSST/modelhouse.mtl",
                            "/content/1/3/2iiaeJNdUEONLSTZlUSTSST/maps/texturehouse_COLOR.png",
                            "/content/1/3/2iiaeJNdUEONLSTZlUSTSST/maps/texturehouse_NRM.png",
                            "/content/1/3/2iiaeJNdUEONLSTZlUSTSST/maps/texturehouse_Opac.png",
                            "/content/1/3/2iiaeJNdUEONLSTZlUSTSST/maps/texturehouse_SPEC2.png"
                        },
                        ObjectType = ObjectType.Ar,
                        Project = projectOne,
                        Size = 0.005,
                        TransitionPoints = null,
                        Version = Guid.NewGuid().ToString()
                    },

                    new ObjectAr
                    {
                        ObjectName = testtex2ArObjectName.ObjectArName,
                        PathFileOnServer = "/content/1/3/FximtJNdUEONLSTZlUSTSUU/modelhouse.obj",
                        Additionals = new[]
                       {
                            "/content/1/3/FximtJNdUEONLSTZlUSTSUU/modelhouse.mtl",
                            "/content/1/3/FximtJNdUEONLSTZlUSTSUU/maps/texturehouse_COLOR.png",
                            "/content/1/3/FximtJNdUEONLSTZlUSTSUU/maps/texturehouse_NRM.png",
                            "/content/1/3/FximtJNdUEONLSTZlUSTSUU/maps/texturehouse_Opac.png",
                            "/content/1/3/FximtJNdUEONLSTZlUSTSUU/maps/texturehouse_SPEC.png"
                        },
                        ObjectType = ObjectType.Ar,
                        Project = projectOne,
                        Size = 0.005,
                        TransitionPoints = null,
                        Version = Guid.NewGuid().ToString()
                    },

                    //new ObjectAr
                    //{
                    //    ObjectName = houseArObjectName.ObjectArName,
                    //    PathFileOnServer = "/content/1/3/2xi9TJUdUEONLSUZlUVTUQMU/modelhouse.obj",
                    //    Additionals = new[]
                    //    {
                    //        "/content/1/3/2xi9TJUdUEONLSUZlUVTUQMU/modelhouse.mtl",
                    //        "/content/1/3/2xi9TJUdUEONLSUZlUVTUQMU/maps/23764337.png"
                    //    },
                    //    ObjectType = ObjectType.Ar,
                    //    Project = projectOne,
                    //    Size = 0.005,
                    //    TransitionPoints = null
                    //    Version = Guid.NewGuid().ToString()
                    //},

                    //Photos
                    new ObjectPhoto
                    {
                        PathFileOnServer = "/content/1/4/ae6dffd0-cf27-406a-8e58-f9e2c33b73b2.jpg",
                        ObjectType = ObjectType.Photo,
                        Project = projectOne
                    },

                    new ObjectPhoto
                    {
                        PathFileOnServer = "/content/1/4/78f4600d-0e4d-4bda-9da0-3bc484878970.jpg",
                        ObjectType = ObjectType.Photo,
                        Project = projectOne
                    },

                    new ObjectPhoto
                    {
                        PathFileOnServer = "/content/1/4/086a8b7f-b851-4f73-860a-7d961de55fbc.jpg",
                        ObjectType = ObjectType.Photo,
                        Project = projectOne
                    },

                    new ObjectPhoto
                    {
                        PathFileOnServer = "/content/1/4/cfc70a21-394a-4fc1-b556-93aacd6454a2.jpg",
                        ObjectType = ObjectType.Photo,
                        Project = projectOne
                    },

                    //Video YouTube
                    new ObjectYouTube
                    {
                        ObjectDescription = "This is a test link video on youtube.com",
                        PathFileOnServer = "https://www.youtube.com/watch?v=PEp0_MCPsvA",
                        ObjectType = ObjectType.VideoYouTube,
                        Project = projectOne
                    }


                //Panorama script
                //new Object
                //{
                //    ObjectDescription = "This is panorama script test object",
                //    PathFileOnServer = "d:/test1/panoramaScript.js",
                //    ObjectType = ObjectType.PanoramaScript,
                //    Project = projectSecond
                //}
                );

                context.SaveChanges();
            }
        }
    }
}
