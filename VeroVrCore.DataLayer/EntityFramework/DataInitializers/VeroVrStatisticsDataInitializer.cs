﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Linq;
using VeroVrCore.Common.Models;
using VeroVrCore.DataLayer.EntityFramework.VeroVrCore.DataLayer.EntityFramework;

namespace VeroVrCore.DataLayer.EntityFramework
    {
        public class VeroVrStatisticsDataInitializer
        {
            public static void Initialize(VeroVrStatisticsContext statisticsContext,
                VeroVrContext veroVrContext, bool isUseMySql = false)
            {
                //statisticsContext.Database.EnsureDeleted();

                var creator = statisticsContext.Database.GetService<IDatabaseCreator>() as RelationalDatabaseCreator;

                bool isExistsDb = creator.Exists();

                if (isExistsDb == false)
                {
                    creator.Create();

                    if (isUseMySql)
                    {
                        string databaseName = statisticsContext.Database.GetDbConnection().Database;
                        statisticsContext.Database.ExecuteSqlCommand(new RawSqlString($"ALTER DATABASE {databaseName} CHARACTER SET utf8 COLLATE utf8_general_ci;"));
                    }
                }

                statisticsContext.Database.Migrate();

                if (!statisticsContext.UserActivities.Any())
                {
                    //Activities
                    Project testProject = veroVrContext
                              .Projects
                              .Include("Objects.RoomImages")
                              .Single(p => p.ProjectId == 1);

                    var roomImages = testProject.Objects.SelectMany(o => o.RoomImages).ToList();

                    User testCustomer = veroVrContext
                        .Users
                        .Include(e => e.UserProjects)
                        .FirstOrDefault(u =>
                        u.UserRole == UserRole.Customer && u.UserProjects
                        .Any(p => p.ProjectId == testProject.ProjectId));


                    DateTime firstDateOfRegistration =
                        veroVrContext
                        .Users
                        .Min(u => u.DateOfRegistration);

                    User testCustomerFirstRegistred = veroVrContext
                        .Users
                        .Include(e => e.UserProjects)
                        .AsNoTracking()
                        .FirstOrDefault(u =>
                        u.UserRole == UserRole.Customer && u.UserProjects
                        .Any(p => p.ProjectId == testProject.ProjectId
                        && u.DateOfRegistration == firstDateOfRegistration));

                    //User login activities

                    statisticsContext.UserLoginActivities.Add(
                        new UserLoginActivity
                        {
                            Device = "Iphone 8",
                            DeviceType = "IOS Phone",
                            EventDateStamp = DateTime.Now.AddMonths(-1),
                            UserId = testCustomer.UserId
                        });

                    statisticsContext.UserLoginActivities.Add(
                       new UserLoginActivity
                       {
                           Device = "Iphone 8",
                           DeviceType = "IOS Phone",
                           EventDateStamp = DateTime.Now.AddMonths(-1),
                           UserId = testCustomerFirstRegistred.UserId
                       });

                    statisticsContext.SaveChanges();

                    var testArObject = testProject.Objects.First(o => o.ObjectType == ObjectType.Ar);
                    var testPlanObject = testProject.Objects.First(o => o.ObjectType == ObjectType.Plan);

                    statisticsContext.UserInProjectDescriptionActivities.Add(
                        new UserInProjectDescriptionActivity
                        {
                            UserId = testCustomer.UserId,
                            ProjectId = testCustomer.UserProjects.FirstOrDefault().ProjectId,
                            EventDateStamp = DateTime.Now.AddMinutes(-6.5),
                            UserActivityType = UserActivityType.ProjectDescriptionView
                        });

                    statisticsContext.UserInProjectDescriptionActivities.Add(
                         new UserInProjectDescriptionActivity
                         {
                             UserId = testCustomer.UserId,
                             ProjectId = testProject.ProjectId,
                             EventDateStamp = DateTime.Now.AddMinutes(-2.5),
                             UserActivityType = UserActivityType.ProjectDescriptionView
                         });

                    statisticsContext.UserInYouTubeVideoActivities.Add(
                        new UserInYouTubeVideoActivity
                        {
                            UserId = testCustomer.UserId,
                            ProjectId = testProject.ProjectId,
                            EventDateStamp = DateTime.Now.AddMinutes(-5.5),
                            UserActivityType = UserActivityType.YouTubeVideoView
                        });

                    statisticsContext.UserInYouTubeVideoActivities.Add(
                      new UserInYouTubeVideoActivity
                      {
                          UserId = testCustomer.UserId,
                          ProjectId = testProject.ProjectId,
                          EventDateStamp = DateTime.Now.AddMinutes(-3.5),
                          UserActivityType = UserActivityType.YouTubeVideoView
                      });

                    statisticsContext.UserInArActivities.Add(
                        new UserInArActivity
                        {
                            UserId = testCustomer.UserId,
                            ObjectId = testArObject.ObjectId,
                            EventDateStamp = DateTime.Now.AddMinutes(-5.0),
                            UserActivityType = UserActivityType.ArModelView
                        });

                    statisticsContext.UserInArActivities.Add(
                        new UserInArActivity
                        {
                            UserId = testCustomer.UserId,
                            ObjectId = testArObject.ObjectId,
                            EventDateStamp = DateTime.Now.AddMinutes(-3.0),
                            UserActivityType = UserActivityType.ArModelView
                        });

                    statisticsContext.UserInArActivities.Add(
                      new UserInArActivity
                      {
                          UserId = testCustomer.UserId,
                          ObjectId = testArObject.ObjectId,
                          EventDateStamp = DateTime.Now.AddMinutes(-2.5),
                          UserActivityType = UserActivityType.ArModelView
                      });

                    statisticsContext.UserInRoom360Activities.Add(
                        new UserInRoom360Activity
                        {
                            UserId = testCustomer.UserId,
                            RoomImageId = roomImages[0].RoomImageId,
                            EventDateStamp = DateTime.Now.AddMinutes(-4.5),
                            UserActivityType = UserActivityType.Room360View
                        });

                    statisticsContext.UserInRoom360Activities.Add(
                       new UserInRoom360Activity
                       {
                           UserId = testCustomer.UserId,
                           RoomImageId = roomImages[0].RoomImageId,
                           EventDateStamp = DateTime.Now.AddMinutes(-3.2),
                           UserActivityType = UserActivityType.Room360View
                       });

                    statisticsContext.UserInRoomVrActivities.Add(
                        new UserInRoomVrActivity
                        {
                            UserId = testCustomer.UserId,
                            RoomImageId = roomImages[1].RoomImageId,
                            EventDateStamp = DateTime.Now.AddMinutes(-4.0),
                            UserActivityType = UserActivityType.VrModeView
                        });

                    statisticsContext.UserInRoomVrActivities.Add(
                    new UserInRoomVrActivity
                    {
                        UserId = testCustomer.UserId,
                        RoomImageId = roomImages[2].RoomImageId,
                        EventDateStamp = DateTime.Now.AddMinutes(-2.8),
                        UserActivityType = UserActivityType.VrModeView
                    });

                    statisticsContext.UserClickLinkInDescriptionActivities.Add(
                        new UserClickLinkInDescriptionActivity
                        {
                            UserId = testCustomer.UserId,
                            ProjectId = testProject.ProjectId,
                            Link = "https://facebook.com/",
                            EventDateStamp = DateTime.Now.AddMinutes(-3.0)
                        });

                    statisticsContext.UserClickLinkInDescriptionActivities.Add(
                     new UserClickLinkInDescriptionActivity
                     {
                         UserId = testCustomer.UserId,
                         ProjectId = testProject.ProjectId,
                         Link = "https://www.apple.com/ios/app-store/",
                         EventDateStamp = DateTime.Now.AddMinutes(-2.5)
                     });

                    statisticsContext.UserClickLinkInDescriptionActivities.Add(
                     new UserClickLinkInDescriptionActivity
                     {
                         UserId = testCustomer.UserId,
                         ProjectId = testProject.ProjectId,
                         Link = "https://www.apple.com/ios/app-store/",
                         EventDateStamp = DateTime.Now.AddMinutes(-2.1)
                     });

                    statisticsContext.UserInPlansActivities.Add(
                       new UserInPlansActivity
                       {
                           UserId = testCustomer.UserId,
                           ObjectId = testPlanObject.ObjectId,
                           EventDateStamp = DateTime.Now.AddMinutes(-4.2),
                           UserActivityType = UserActivityType.PlansView
                       });

                    statisticsContext.UserInPlansActivities.Add(
                       new UserInPlansActivity
                       {
                           UserId = testCustomer.UserId,
                           ObjectId = testPlanObject.ObjectId,
                           EventDateStamp = DateTime.Now.AddMinutes(-3.2),
                           UserActivityType = UserActivityType.PlansView
                       });

                    statisticsContext.SaveChanges();
                }
            }
        }
    }
