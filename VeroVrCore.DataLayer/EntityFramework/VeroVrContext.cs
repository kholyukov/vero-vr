﻿using Microsoft.EntityFrameworkCore;
using VeroVrCore.Common.Models;

namespace VeroVrCore.DataLayer.EntityFramework
{
    public class VeroVrContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<BaseObject> Objects { get; set; }

        public DbSet<ObjectAr> ObjectArs { get; set; }
        public DbSet<ObjectRoom> ObjectRooms { get; set; }
        public DbSet<ObjectYouTube> ObjectYouTubes { get; set; }
        public DbSet<ObjectPhoto> ObjectPhotos { get; set; }
        public DbSet<ObjectTransitionPoint> ObjectTransitionPoints { get; set; }

        public DbSet<TransitionPoint> TransitionPoints { get; set; }

        public DbSet<DayTime> DayTimes { get; set; }
        public DbSet<Interior> Interiors { get; set; }
        public DbSet<RoomType> RoomTypes { get; set; }
        public DbSet<PlanInfo> PlanInfos { get; set; }
        public DbSet<ObjectPlan> ObjectPlans { get; set; }
        public DbSet<RoomImage> RoomImages { get; set; }
        public DbSet<ObjectArInfo> ObjectArInfos { get; set; }

        public DbSet<UserOnlineActivity> UserOnlineActivities { get; set; }

        public VeroVrContext(DbContextOptions<VeroVrContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserProject>(entity =>
            {
                entity.HasKey(t => new { t.UserId, t.ProjectId });

                entity.Property(e => e.UserId)
                .HasColumnName("UserId")
                .HasColumnType("int");

                entity.Property(e => e.ProjectId)
               .HasColumnName("ProjectId")
               .HasColumnType("int");

                entity
                  .HasOne(e => e.User)
                  .WithMany(e => e.UserProjects)
                  .HasForeignKey(e => e.UserId);

                entity
                 .HasOne(e => e.Project)
                 .WithMany(e => e.UserProjects)
                 .HasForeignKey(e => e.ProjectId);

                entity.Ignore(e => e.UserRole);
            });

            modelBuilder.Entity<BaseObject>(entity =>
            {
                entity.HasKey(e => e.ObjectId);
                entity.ToTable("Objects");

                entity.Property(e => e.ObjectId)
                .IsRequired()
                .HasColumnName("ObjectId")
                .HasColumnType("int")
                .ValueGeneratedOnAdd();

                entity.HasOne(e => e.Project)
                  .WithMany(e => e.Objects)
                  .HasForeignKey(e => e.ProjectId);

                entity.HasMany(e => e.TransitionPoints)
                .WithOne(e => e.Object);

                entity.HasMany(e => e.RoomImages)
                .WithOne(e => e.Object);
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.HasKey(e => e.UserId);
                entity.Property(e => e.UserId)
                .IsRequired()
                .HasColumnName("UserId")
                .HasColumnType("int")
                .ValueGeneratedOnAdd();

                entity.HasMany(e => e.UserProjects)
                .WithOne(e => e.User);

                entity.Property(e => e.DateOfRegistration)
                .HasDefaultValueSql("'0001-01-01 00:00:00'")
                .IsRequired();
                                  
                entity.HasIndex(e => e.DateOfRegistration);

                entity.Ignore(e => e.FacebookLikes);
            });

            modelBuilder.Entity<Project>(entity =>
            {
                entity.HasKey(e => e.ProjectId);
                entity.Property(e => e.ProjectId)
                .IsRequired()
                .HasColumnName("ProjectId")
                .HasColumnType("int")
                .ValueGeneratedOnAdd();

                entity.HasMany(e => e.UserProjects)
                .WithOne(e => e.Project);

                entity.HasMany(e => e.Objects)
                .WithOne(e => e.Project);

                entity.HasMany(e => e.RoomTypes)
                .WithOne(e => e.Project)
                .HasForeignKey(e => e.ProjectId);

                entity.HasMany(e => e.PlanInfos)
               .WithOne(e => e.Project)
               .HasForeignKey(e => e.ProjectId);

                entity.HasMany(e => e.Interiors)
              .WithOne(e => e.Project)
              .HasForeignKey(e => e.ProjectId);

                entity.HasMany(e => e.ObjectArInfos)
              .WithOne(e => e.Project)
              .HasForeignKey(e => e.ProjectId);
            });

            modelBuilder.Entity<TransitionPoint>(entity =>
            {
                entity.HasKey(e => e.TransitionPointId);
                entity.Property(e => e.TransitionPointId)
                .IsRequired()
                .HasColumnName("TransitionPointId")
                .HasColumnType("int")
                .ValueGeneratedOnAdd();

                entity.HasOne(e => e.Object)
              .WithMany(e => e.TransitionPoints)
              .HasForeignKey(e => e.ObjectId);
            });

            modelBuilder.Entity<RoomType>(entity =>
            {
                entity.HasKey(e => e.RoomTypeId);
                entity.Property(e => e.RoomTypeId)
                .IsRequired()
                .HasColumnName("RoomTypeId")
                .HasColumnType("int")
                .ValueGeneratedOnAdd();

                entity.HasOne(e => e.Project)
               .WithMany(e => e.RoomTypes);
            });

            modelBuilder.Entity<PlanInfo>(entity =>
            {
                entity.HasKey(e => e.PlanInfoId);
                entity.Property(e => e.PlanInfoId)
                .IsRequired()
                .HasColumnName("PlanInfoId")
                .HasColumnType("int")
                .ValueGeneratedOnAdd();

                entity.HasOne(e => e.Project)
               .WithMany(e => e.PlanInfos);
            });

            modelBuilder.Entity<ObjectAr>(entity =>
            {
                entity.Ignore(e => e.Additionals);
            });

            modelBuilder.Entity<Interior>(entity =>
            {
                entity.HasKey(e => e.InteriorId);
                entity.Property(e => e.InteriorId)
                .IsRequired()
                .HasColumnName("InteriorId")
                .HasColumnType("int")
                .ValueGeneratedOnAdd();

                 entity.HasOne(e => e.Project)
                .WithMany(e => e.Interiors);
            });

            modelBuilder.Entity<DayTime>(entity =>
            {
                entity.HasKey(e => e.DayTimeId);
                entity.Property(e => e.DayTimeId)
                .IsRequired()
                .HasColumnName("DayTimeId")
                .HasColumnType("int")
                .ValueGeneratedOnAdd();

                entity.HasMany(e => e.RoomImages)
                .WithOne(e => e.DayTime)
                .HasForeignKey(e => e.DayTimeId);
            });

            modelBuilder.Entity<RoomImage>(entity =>
            {
                entity.HasKey(e => e.RoomImageId);
                entity.Property(e => e.RoomImageId)
                .IsRequired()
                .HasColumnName("RoomImageId")
                .HasColumnType("int")
                .ValueGeneratedOnAdd();

                entity.HasOne(e => e.DayTime)
                .WithMany(e => e.RoomImages)
                .HasForeignKey(e => e.DayTimeId);

                entity.HasOne(e => e.Object)
                .WithMany(e => e.RoomImages)
                .HasForeignKey(e => e.ObjectId);
            });

            modelBuilder.Entity<ObjectArInfo>(entity =>
            {
                entity.HasKey(e => e.ObjectArInfoId);
                entity.Property(e => e.ObjectArInfoId)
                .IsRequired()
                .HasColumnName("ObjectArInfoId")
                .HasColumnType("int")
                .ValueGeneratedOnAdd();

                entity.HasOne(e => e.Project)
               .WithMany(e => e.ObjectArInfos);
            });

            modelBuilder.Entity<UserOnlineActivity>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.Property(e => e.Id)
               .IsRequired()
               .HasColumnName("Id")
               .HasColumnType("int")
                .ValueGeneratedOnAdd();

                entity.Property(e => e.UserId)
                .IsRequired()
                .HasColumnName("UserId")
                .HasColumnType("int");

                entity.HasIndex(e => e.UserId).IsUnique();

                entity.Property(e => e.EventDateStamp)
                   .HasDefaultValueSql("'0001-01-01 00:00:00'")
                   .IsRequired();
                entity.HasIndex(e => e.EventDateStamp);
            });
        }
    }
}
