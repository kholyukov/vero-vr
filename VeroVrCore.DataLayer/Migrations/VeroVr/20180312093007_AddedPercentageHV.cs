﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VeroVrCore.DataLayer.Migrations
{
    public partial class AddedPercentageHV : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<double>(
                name: "PercentageH",
                table: "TransitionPoints",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "PercentageV",
                table: "TransitionPoints",
                nullable: false,
                defaultValue: 0.0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PercentageH",
                table: "TransitionPoints");

            migrationBuilder.DropColumn(
                name: "PercentageV",
                table: "TransitionPoints");
        }
    }
}
