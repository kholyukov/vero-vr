﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VeroVrCore.DataLayer.Migrations
{
    public partial class AddRelationProjectroomTypes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ProjectId",
                table: "RoomTypes",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_RoomTypes_ProjectId",
                table: "RoomTypes",
                column: "ProjectId");

            migrationBuilder.AddForeignKey(
                name: "FK_RoomTypes_Projects_ProjectId",
                table: "RoomTypes",
                column: "ProjectId",
                principalTable: "Projects",
                principalColumn: "ProjectId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_RoomTypes_Projects_ProjectId",
                table: "RoomTypes");

            migrationBuilder.DropIndex(
                name: "IX_RoomTypes_ProjectId",
                table: "RoomTypes");

            migrationBuilder.DropColumn(
                name: "ProjectId",
                table: "RoomTypes");
        }
    }
}
