﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VeroVrCore.DataLayer.Migrations
{
    public partial class AddUserInRoomAndProjectActivities : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "UserInProjectActivities",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    EventDateStamp = table.Column<DateTime>(nullable: false, defaultValueSql: "'0001-01-01 00:00:00'"),
                    ProjectId = table.Column<int>(type: "int", nullable: false),
                    UserId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserInProjectActivities", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UserInRoomActivities",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    EventDateStamp = table.Column<DateTime>(nullable: false, defaultValueSql: "'0001-01-01 00:00:00'"),
                    RoomImageId = table.Column<int>(type: "int", nullable: false),
                    UserId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserInRoomActivities", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_UserInProjectActivities_EventDateStamp",
                table: "UserInProjectActivities",
                column: "EventDateStamp");

            migrationBuilder.CreateIndex(
                name: "IX_UserInRoomActivities_EventDateStamp",
                table: "UserInRoomActivities",
                column: "EventDateStamp");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "UserInProjectActivities");

            migrationBuilder.DropTable(
                name: "UserInRoomActivities");
        }
    }
}
