﻿// <auto-generated />
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.EntityFrameworkCore.Storage.Internal;
using System;
using VeroVrCore.Common.Models;
using VeroVrCore.DataLayer.EntityFramework;

namespace VeroVrCore.DataLayer.Migrations
{
    [DbContext(typeof(VeroVrContext))]
    [Migration("20180117162259_AddedImageUrlForCompany")]
    partial class AddedImageUrlForCompany
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.0.1-rtm-125")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("VeroVrCore.Common.Models.Company", b =>
                {
                    b.Property<int>("CompanyId")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("CompanyId")
                        .HasColumnType("int");

                    b.Property<string>("CompanyAddress");

                    b.Property<string>("CompanyContacts");

                    b.Property<string>("CompanyName");

                    b.Property<string>("ImageUrl");

                    b.HasKey("CompanyId");

                    b.ToTable("Companies");
                });

            modelBuilder.Entity("VeroVrCore.Common.Models.Object", b =>
                {
                    b.Property<int>("ObjectId")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("ObjectId")
                        .HasColumnType("int");

                    b.Property<string>("ObjectDescription");

                    b.Property<int>("ObjectType");

                    b.Property<string>("PathFileOnServer");

                    b.Property<int>("ProjectId");

                    b.HasKey("ObjectId");

                    b.HasIndex("ProjectId");

                    b.ToTable("Objects");
                });

            modelBuilder.Entity("VeroVrCore.Common.Models.Project", b =>
                {
                    b.Property<int>("ProjectId")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("ProjectId")
                        .HasColumnType("int");

                    b.Property<string>("ProjectDescription");

                    b.Property<string>("ProjectName");

                    b.HasKey("ProjectId");

                    b.ToTable("Projects");
                });

            modelBuilder.Entity("VeroVrCore.Common.Models.User", b =>
                {
                    b.Property<int>("UserId")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("UserId")
                        .HasColumnType("int");

                    b.Property<int?>("CompanyId");

                    b.Property<string>("UserAddress");

                    b.Property<string>("UserAvatarUrl");

                    b.Property<string>("UserDisplayName");

                    b.Property<string>("UserEmail");

                    b.Property<string>("UserFacebook");

                    b.Property<string>("UserPassword");

                    b.Property<string>("UserPhoneNumber");

                    b.Property<int>("UserRole");

                    b.HasKey("UserId");

                    b.HasIndex("CompanyId");

                    b.ToTable("Users");
                });

            modelBuilder.Entity("VeroVrCore.Common.Models.UserProject", b =>
                {
                    b.Property<int>("UserId")
                        .HasColumnName("UserId")
                        .HasColumnType("int");

                    b.Property<int>("ProjectId")
                        .HasColumnName("ProjectId")
                        .HasColumnType("int");

                    b.HasKey("UserId", "ProjectId");

                    b.HasIndex("ProjectId");

                    b.ToTable("UserProject");
                });

            modelBuilder.Entity("VeroVrCore.Common.Models.Object", b =>
                {
                    b.HasOne("VeroVrCore.Common.Models.Project", "Project")
                        .WithMany("Objects")
                        .HasForeignKey("ProjectId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("VeroVrCore.Common.Models.User", b =>
                {
                    b.HasOne("VeroVrCore.Common.Models.Company", "Company")
                        .WithMany("Users")
                        .HasForeignKey("CompanyId");
                });

            modelBuilder.Entity("VeroVrCore.Common.Models.UserProject", b =>
                {
                    b.HasOne("VeroVrCore.Common.Models.Project", "Project")
                        .WithMany("UserProjects")
                        .HasForeignKey("ProjectId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("VeroVrCore.Common.Models.User", "User")
                        .WithMany("UserProjects")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
#pragma warning restore 612, 618
        }
    }
}
