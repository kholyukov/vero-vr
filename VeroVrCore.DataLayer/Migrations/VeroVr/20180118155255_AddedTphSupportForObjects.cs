﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VeroVrCore.DataLayer.Migrations
{
    public partial class AddedTphSupportForObjects : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ProjectImageUrl",
                table: "Projects",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Discriminator",
                table: "Objects",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<double>(
                name: "Size",
                table: "Objects",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "RoomName",
                table: "Objects",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "RoomNumber",
                table: "Objects",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TransitionPointsText",
                table: "Objects",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "TransitionPoints",
                columns: table => new
                {
                    TransitionPointId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ObjectId = table.Column<int>(nullable: false),
                    PositionX = table.Column<double>(nullable: false),
                    PositionY = table.Column<double>(nullable: false),
                    PositionZ = table.Column<double>(nullable: false),
                    RoomId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TransitionPoints", x => x.TransitionPointId);
                    table.ForeignKey(
                        name: "FK_TransitionPoints_Objects_ObjectId",
                        column: x => x.ObjectId,
                        principalTable: "Objects",
                        principalColumn: "ObjectId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TransitionPoints_ObjectId",
                table: "TransitionPoints",
                column: "ObjectId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TransitionPoints");

            migrationBuilder.DropColumn(
                name: "ProjectImageUrl",
                table: "Projects");

            migrationBuilder.DropColumn(
                name: "Discriminator",
                table: "Objects");

            migrationBuilder.DropColumn(
                name: "Size",
                table: "Objects");

            migrationBuilder.DropColumn(
                name: "RoomName",
                table: "Objects");

            migrationBuilder.DropColumn(
                name: "RoomNumber",
                table: "Objects");

            migrationBuilder.DropColumn(
                name: "TransitionPointsText",
                table: "Objects");
        }
    }
}
