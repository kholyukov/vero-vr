﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VeroVrCore.DataLayer.Migrations
{
    public partial class AddedDefaultPlanInfoIdToProject : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_RoomImage_DayTimes_DayTimeId",
                table: "RoomImage");

            migrationBuilder.DropForeignKey(
                name: "FK_RoomImage_Objects_ObjectId",
                table: "RoomImage");

            migrationBuilder.DropPrimaryKey(
                name: "PK_RoomImage",
                table: "RoomImage");

            migrationBuilder.RenameTable(
                name: "RoomImage",
                newName: "RoomImages");

            migrationBuilder.RenameIndex(
                name: "IX_RoomImage_ObjectId",
                table: "RoomImages",
                newName: "IX_RoomImages_ObjectId");

            migrationBuilder.RenameIndex(
                name: "IX_RoomImage_DayTimeId",
                table: "RoomImages",
                newName: "IX_RoomImages_DayTimeId");

            migrationBuilder.AddColumn<int>(
                name: "PlanInfoId",
                table: "RoomImages",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "RoomTypeId",
                table: "RoomImages",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "DefaultPlanInfoId",
                table: "Projects",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddPrimaryKey(
                name: "PK_RoomImages",
                table: "RoomImages",
                columns: new[] { "RoomImageId", "InteriorId", "DayTimeId" });

            migrationBuilder.AddForeignKey(
                name: "FK_RoomImages_DayTimes_DayTimeId",
                table: "RoomImages",
                column: "DayTimeId",
                principalTable: "DayTimes",
                principalColumn: "DayTimeId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_RoomImages_Objects_ObjectId",
                table: "RoomImages",
                column: "ObjectId",
                principalTable: "Objects",
                principalColumn: "ObjectId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_RoomImages_DayTimes_DayTimeId",
                table: "RoomImages");

            migrationBuilder.DropForeignKey(
                name: "FK_RoomImages_Objects_ObjectId",
                table: "RoomImages");

            migrationBuilder.DropPrimaryKey(
                name: "PK_RoomImages",
                table: "RoomImages");

            migrationBuilder.DropColumn(
                name: "PlanInfoId",
                table: "RoomImages");

            migrationBuilder.DropColumn(
                name: "RoomTypeId",
                table: "RoomImages");

            migrationBuilder.DropColumn(
                name: "DefaultPlanInfoId",
                table: "Projects");

            migrationBuilder.RenameTable(
                name: "RoomImages",
                newName: "RoomImage");

            migrationBuilder.RenameIndex(
                name: "IX_RoomImages_ObjectId",
                table: "RoomImage",
                newName: "IX_RoomImage_ObjectId");

            migrationBuilder.RenameIndex(
                name: "IX_RoomImages_DayTimeId",
                table: "RoomImage",
                newName: "IX_RoomImage_DayTimeId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_RoomImage",
                table: "RoomImage",
                columns: new[] { "RoomImageId", "InteriorId", "DayTimeId" });

            migrationBuilder.AddForeignKey(
                name: "FK_RoomImage_DayTimes_DayTimeId",
                table: "RoomImage",
                column: "DayTimeId",
                principalTable: "DayTimes",
                principalColumn: "DayTimeId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_RoomImage_Objects_ObjectId",
                table: "RoomImage",
                column: "ObjectId",
                principalTable: "Objects",
                principalColumn: "ObjectId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
