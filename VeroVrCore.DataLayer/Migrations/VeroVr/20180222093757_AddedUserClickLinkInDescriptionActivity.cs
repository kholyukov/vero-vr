﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VeroVrCore.DataLayer.Migrations
{
    public partial class AddedUserClickLinkInDescriptionActivity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_UserActivities",
                table: "UserActivities");

            migrationBuilder.DropColumn(
                name: "Discriminator",
                table: "UserActivities");

            migrationBuilder.DropColumn(
                name: "UserActivityType",
                table: "UserActivities");

            migrationBuilder.DropColumn(
                name: "ObjectId",
                table: "UserActivities");

            migrationBuilder.DropColumn(
                name: "RoomImageId",
                table: "UserActivities");

            migrationBuilder.RenameTable(
                name: "UserActivities",
                newName: "UserClickLinkInDescriptionActivities");

            migrationBuilder.RenameIndex(
                name: "IX_UserActivities_EventDateStamp",
                table: "UserClickLinkInDescriptionActivities",
                newName: "IX_UserClickLinkInDescriptionActivities_EventDateStamp");

            migrationBuilder.AlterColumn<int>(
                name: "ProjectId",
                table: "UserClickLinkInDescriptionActivities",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Link",
                table: "UserClickLinkInDescriptionActivities",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_UserClickLinkInDescriptionActivities",
                table: "UserClickLinkInDescriptionActivities",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "UserActivities",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Discriminator = table.Column<string>(nullable: false),
                    EventDateStamp = table.Column<DateTime>(nullable: false, defaultValueSql: "'0001-01-01 00:00:00'"),
                    UserActivityType = table.Column<int>(nullable: false),
                    UserId = table.Column<int>(type: "int", nullable: false),
                    ObjectId = table.Column<int>(nullable: true),
                    ProjectId = table.Column<int>(nullable: true),
                    RoomImageId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserActivities", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_UserActivities_EventDateStamp",
                table: "UserActivities",
                column: "EventDateStamp");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "UserActivities");

            migrationBuilder.DropPrimaryKey(
                name: "PK_UserClickLinkInDescriptionActivities",
                table: "UserClickLinkInDescriptionActivities");

            migrationBuilder.DropColumn(
                name: "Link",
                table: "UserClickLinkInDescriptionActivities");

            migrationBuilder.RenameTable(
                name: "UserClickLinkInDescriptionActivities",
                newName: "UserActivities");

            migrationBuilder.RenameIndex(
                name: "IX_UserClickLinkInDescriptionActivities_EventDateStamp",
                table: "UserActivities",
                newName: "IX_UserActivities_EventDateStamp");

            migrationBuilder.AlterColumn<int>(
                name: "ProjectId",
                table: "UserActivities",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<string>(
                name: "Discriminator",
                table: "UserActivities",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<int>(
                name: "UserActivityType",
                table: "UserActivities",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "ObjectId",
                table: "UserActivities",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "RoomImageId",
                table: "UserActivities",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_UserActivities",
                table: "UserActivities",
                column: "Id");
        }
    }
}
