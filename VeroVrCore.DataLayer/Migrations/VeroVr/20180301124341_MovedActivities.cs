﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VeroVrCore.DataLayer.Migrations
{
    public partial class MovedActivities : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "LogEvents");

            migrationBuilder.DropTable(
                name: "UserActivities");

            migrationBuilder.DropTable(
                name: "UserClickLinkInDescriptionActivities");

            migrationBuilder.DropTable(
                name: "UserLoginActivities");

            migrationBuilder.DropTable(
                name: "UserShareProjectActivities");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "LogEvents",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedTime = table.Column<DateTime>(nullable: false),
                    EventId = table.Column<int>(nullable: false),
                    LogLevel = table.Column<string>(maxLength: 50, nullable: true),
                    Message = table.Column<string>(maxLength: 4000, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LogEvents", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UserActivities",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Discriminator = table.Column<string>(nullable: false),
                    EventDateStamp = table.Column<DateTime>(nullable: false, defaultValueSql: "'0001-01-01 00:00:00'"),
                    UserActivityType = table.Column<int>(nullable: false),
                    UserId = table.Column<int>(type: "int", nullable: false),
                    ObjectId = table.Column<int>(nullable: true),
                    ProjectId = table.Column<int>(nullable: true),
                    RoomImageId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserActivities", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UserClickLinkInDescriptionActivities",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    EventDateStamp = table.Column<DateTime>(nullable: false, defaultValueSql: "'0001-01-01 00:00:00'"),
                    Link = table.Column<string>(nullable: true),
                    ProjectId = table.Column<int>(nullable: false),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserClickLinkInDescriptionActivities", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UserLoginActivities",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Device = table.Column<string>(nullable: true),
                    DeviceType = table.Column<string>(nullable: true),
                    EventDateStamp = table.Column<DateTime>(nullable: false, defaultValueSql: "'0001-01-01 00:00:00'"),
                    UserId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserLoginActivities", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UserShareProjectActivities",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    EventDateStamp = table.Column<DateTime>(nullable: false, defaultValueSql: "'0001-01-01 00:00:00'"),
                    ProjectId = table.Column<int>(nullable: false),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserShareProjectActivities", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_UserActivities_EventDateStamp",
                table: "UserActivities",
                column: "EventDateStamp");

            migrationBuilder.CreateIndex(
                name: "IX_UserClickLinkInDescriptionActivities_EventDateStamp",
                table: "UserClickLinkInDescriptionActivities",
                column: "EventDateStamp");

            migrationBuilder.CreateIndex(
                name: "IX_UserLoginActivities_EventDateStamp",
                table: "UserLoginActivities",
                column: "EventDateStamp");

            migrationBuilder.CreateIndex(
                name: "IX_UserShareProjectActivities_EventDateStamp",
                table: "UserShareProjectActivities",
                column: "EventDateStamp");
        }
    }
}
