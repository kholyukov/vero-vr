﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VeroVrCore.DataLayer.Migrations
{
    public partial class RenameColumnInProject : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "PlanInfoId",
                table: "RoomImages",
                newName: "ObjectPlanId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ObjectPlanId",
                table: "RoomImages",
                newName: "PlanInfoId");
        }
    }
}
