﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VeroVrCore.DataLayer.Migrations
{
    public partial class RemovedPlanName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PlanName",
                table: "Objects");

            migrationBuilder.RenameColumn(
                name: "RoomName",
                table: "Objects",
                newName: "ObjectName");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ObjectName",
                table: "Objects",
                newName: "RoomName");

            migrationBuilder.AddColumn<string>(
                name: "PlanName",
                table: "Objects",
                nullable: true);
        }
    }
}
