﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VeroVrCore.DataLayer.Migrations
{
    public partial class AddedDayTimeRoomImageAndInterior : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "PersonalTitle",
                table: "Users",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UserCity",
                table: "Users",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UserCountry",
                table: "Users",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserDateBirthdayDayTimeId",
                table: "Users",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MtlUrl",
                table: "Objects",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "DayTimes",
                columns: table => new
                {
                    DayTimeId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DayTimeName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DayTimes", x => x.DayTimeId);
                });

            migrationBuilder.CreateTable(
                name: "Interiors",
                columns: table => new
                {
                    InteriorId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    InteriorName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Interiors", x => x.InteriorId);
                });

            migrationBuilder.CreateTable(
                name: "RoomImage",
                columns: table => new
                {
                    RoomImageId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    InteriorId = table.Column<int>(nullable: false),
                    DayTimeId = table.Column<int>(nullable: false),
                    ImageUrl = table.Column<string>(nullable: true),
                    ObjectId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RoomImage", x => new { x.RoomImageId, x.InteriorId, x.DayTimeId });
                    table.ForeignKey(
                        name: "FK_RoomImage_DayTimes_DayTimeId",
                        column: x => x.DayTimeId,
                        principalTable: "DayTimes",
                        principalColumn: "DayTimeId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_RoomImage_Interiors_InteriorId",
                        column: x => x.InteriorId,
                        principalTable: "Interiors",
                        principalColumn: "InteriorId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_RoomImage_Objects_ObjectId",
                        column: x => x.ObjectId,
                        principalTable: "Objects",
                        principalColumn: "ObjectId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Users_UserDateBirthdayDayTimeId",
                table: "Users",
                column: "UserDateBirthdayDayTimeId");

            migrationBuilder.CreateIndex(
                name: "IX_RoomImage_DayTimeId",
                table: "RoomImage",
                column: "DayTimeId");

            migrationBuilder.CreateIndex(
                name: "IX_RoomImage_InteriorId",
                table: "RoomImage",
                column: "InteriorId");

            migrationBuilder.CreateIndex(
                name: "IX_RoomImage_ObjectId",
                table: "RoomImage",
                column: "ObjectId");

            migrationBuilder.AddForeignKey(
                name: "FK_Users_DayTimes_UserDateBirthdayDayTimeId",
                table: "Users",
                column: "UserDateBirthdayDayTimeId",
                principalTable: "DayTimes",
                principalColumn: "DayTimeId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Users_DayTimes_UserDateBirthdayDayTimeId",
                table: "Users");

            migrationBuilder.DropTable(
                name: "RoomImage");

            migrationBuilder.DropTable(
                name: "DayTimes");

            migrationBuilder.DropTable(
                name: "Interiors");

            migrationBuilder.DropIndex(
                name: "IX_Users_UserDateBirthdayDayTimeId",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "PersonalTitle",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "UserCity",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "UserCountry",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "UserDateBirthdayDayTimeId",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "MtlUrl",
                table: "Objects");
        }
    }
}
