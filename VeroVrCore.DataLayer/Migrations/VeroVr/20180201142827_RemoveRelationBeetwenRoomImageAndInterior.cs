﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VeroVrCore.DataLayer.Migrations
{
    public partial class RemoveRelationBeetwenRoomImageAndInterior : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_RoomImage_Interiors_InteriorId",
                table: "RoomImage");

            migrationBuilder.DropForeignKey(
                name: "FK_Users_DayTimes_UserDateBirthdayDayTimeId",
                table: "Users");

            migrationBuilder.DropIndex(
                name: "IX_Users_UserDateBirthdayDayTimeId",
                table: "Users");

            migrationBuilder.DropIndex(
                name: "IX_RoomImage_InteriorId",
                table: "RoomImage");

            migrationBuilder.DropColumn(
                name: "UserDateBirthdayDayTimeId",
                table: "Users");

            migrationBuilder.AddColumn<int>(
                name: "ProjectId",
                table: "Interiors",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Interiors_ProjectId",
                table: "Interiors",
                column: "ProjectId");

            migrationBuilder.AddForeignKey(
                name: "FK_Interiors_Projects_ProjectId",
                table: "Interiors",
                column: "ProjectId",
                principalTable: "Projects",
                principalColumn: "ProjectId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Interiors_Projects_ProjectId",
                table: "Interiors");

            migrationBuilder.DropIndex(
                name: "IX_Interiors_ProjectId",
                table: "Interiors");

            migrationBuilder.DropColumn(
                name: "ProjectId",
                table: "Interiors");

            migrationBuilder.AddColumn<int>(
                name: "UserDateBirthdayDayTimeId",
                table: "Users",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Users_UserDateBirthdayDayTimeId",
                table: "Users",
                column: "UserDateBirthdayDayTimeId");

            migrationBuilder.CreateIndex(
                name: "IX_RoomImage_InteriorId",
                table: "RoomImage",
                column: "InteriorId");

            migrationBuilder.AddForeignKey(
                name: "FK_RoomImage_Interiors_InteriorId",
                table: "RoomImage",
                column: "InteriorId",
                principalTable: "Interiors",
                principalColumn: "InteriorId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Users_DayTimes_UserDateBirthdayDayTimeId",
                table: "Users",
                column: "UserDateBirthdayDayTimeId",
                principalTable: "DayTimes",
                principalColumn: "DayTimeId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
