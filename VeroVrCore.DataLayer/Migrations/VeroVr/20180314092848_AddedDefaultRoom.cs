﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VeroVrCore.DataLayer.Migrations
{
    public partial class AddedDefaultRoom : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DefaultDayTimeId",
                table: "Projects");

            migrationBuilder.DropColumn(
                name: "DefaultInteriorId",
                table: "Projects");

            migrationBuilder.DropColumn(
                name: "DefaultPlanInfoId",
                table: "Projects");

            migrationBuilder.DropColumn(
                name: "DefaultRoomId",
                table: "Projects");

            migrationBuilder.AddColumn<int>(
                name: "DefaultRoom",
                table: "RoomImages",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DefaultRoom",
                table: "RoomImages");

            migrationBuilder.AddColumn<int>(
                name: "DefaultDayTimeId",
                table: "Projects",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "DefaultInteriorId",
                table: "Projects",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "DefaultPlanInfoId",
                table: "Projects",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "DefaultRoomId",
                table: "Projects",
                nullable: false,
                defaultValue: 0);
        }
    }
}
