﻿// <auto-generated />
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.EntityFrameworkCore.Storage.Internal;
using Microsoft.EntityFrameworkCore.ValueGeneration;
using System;
using VeroVrCore.Common.Models;
using VeroVrCore.DataLayer.EntityFramework;

namespace VeroVrCore.DataLayer.Migrations
{
    [DbContext(typeof(VeroVrContext))]
    [Migration("20180222093757_AddedUserClickLinkInDescriptionActivity")]
    partial class AddedUserClickLinkInDescriptionActivity
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.0.1-rtm-125")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("VeroVrCore.Common.Models.BaseObject", b =>
                {
                    b.Property<int>("ObjectId")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("ObjectId")
                        .HasColumnType("int");

                    b.Property<string>("Discriminator")
                        .IsRequired();

                    b.Property<string>("ObjectDescription");

                    b.Property<string>("ObjectName");

                    b.Property<int>("ObjectType");

                    b.Property<string>("PathFileOnServer");

                    b.Property<int>("ProjectId");

                    b.HasKey("ObjectId");

                    b.HasIndex("ProjectId");

                    b.ToTable("Objects");

                    b.HasDiscriminator<string>("Discriminator").HasValue("BaseObject");
                });

            modelBuilder.Entity("VeroVrCore.Common.Models.DayTime", b =>
                {
                    b.Property<int>("DayTimeId")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("DayTimeId")
                        .HasColumnType("int");

                    b.Property<string>("DayTimeName");

                    b.HasKey("DayTimeId");

                    b.ToTable("DayTimes");
                });

            modelBuilder.Entity("VeroVrCore.Common.Models.EventLog", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("Id")
                        .HasColumnType("int");

                    b.Property<DateTime>("CreatedTime");

                    b.Property<int>("EventId");

                    b.Property<string>("LogLevel")
                        .HasMaxLength(50);

                    b.Property<string>("Message")
                        .HasMaxLength(4000);

                    b.HasKey("Id");

                    b.ToTable("EventLogs");
                });

            modelBuilder.Entity("VeroVrCore.Common.Models.Interior", b =>
                {
                    b.Property<int>("InteriorId")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("InteriorId")
                        .HasColumnType("int");

                    b.Property<string>("InteriorName");

                    b.Property<int>("ProjectId");

                    b.HasKey("InteriorId");

                    b.HasIndex("ProjectId");

                    b.ToTable("Interiors");
                });

            modelBuilder.Entity("VeroVrCore.Common.Models.ObjectArInfo", b =>
                {
                    b.Property<int>("ObjectArInfoId")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("ObjectArInfoId")
                        .HasColumnType("int");

                    b.Property<string>("ObjectArName");

                    b.Property<int>("ProjectId");

                    b.HasKey("ObjectArInfoId");

                    b.HasIndex("ProjectId");

                    b.ToTable("ObjectArInfos");
                });

            modelBuilder.Entity("VeroVrCore.Common.Models.PlanInfo", b =>
                {
                    b.Property<int>("PlanInfoId")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("PlanInfoId")
                        .HasColumnType("int");

                    b.Property<string>("PlanName");

                    b.Property<int>("ProjectId");

                    b.HasKey("PlanInfoId");

                    b.HasIndex("ProjectId");

                    b.ToTable("PlanInfos");
                });

            modelBuilder.Entity("VeroVrCore.Common.Models.Project", b =>
                {
                    b.Property<int>("ProjectId")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("ProjectId")
                        .HasColumnType("int");

                    b.Property<int>("DefaultDayTimeId");

                    b.Property<int>("DefaultInteriorId");

                    b.Property<int>("DefaultPlanInfoId");

                    b.Property<int>("DefaultRoomId");

                    b.Property<string>("ProjectCode");

                    b.Property<string>("ProjectDescription");

                    b.Property<string>("ProjectImageUrl");

                    b.Property<string>("ProjectName");

                    b.Property<string>("ProjectToken");

                    b.HasKey("ProjectId");

                    b.ToTable("Projects");
                });

            modelBuilder.Entity("VeroVrCore.Common.Models.RoomImage", b =>
                {
                    b.Property<int>("RoomImageId")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("RoomImageId")
                        .HasColumnType("int");

                    b.Property<int>("DayTimeId");

                    b.Property<string>("ImageUrl");

                    b.Property<int>("InteriorId");

                    b.Property<int>("ObjectId");

                    b.Property<int>("ObjectPlanId");

                    b.Property<int>("RoomTypeId");

                    b.HasKey("RoomImageId");

                    b.HasIndex("DayTimeId");

                    b.HasIndex("ObjectId");

                    b.ToTable("RoomImages");
                });

            modelBuilder.Entity("VeroVrCore.Common.Models.RoomType", b =>
                {
                    b.Property<int>("RoomTypeId")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("RoomTypeId")
                        .HasColumnType("int");

                    b.Property<int>("ProjectId");

                    b.Property<string>("RoomTypeName");

                    b.HasKey("RoomTypeId");

                    b.HasIndex("ProjectId");

                    b.ToTable("RoomTypes");
                });

            modelBuilder.Entity("VeroVrCore.Common.Models.TransitionPoint", b =>
                {
                    b.Property<int>("TransitionPointId")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("TransitionPointId")
                        .HasColumnType("int");

                    b.Property<int>("ObjectId");

                    b.Property<double>("PositionX");

                    b.Property<double>("PositionY");

                    b.Property<double>("PositionZ");

                    b.Property<int>("RoomId");

                    b.HasKey("TransitionPointId");

                    b.HasIndex("ObjectId");

                    b.ToTable("TransitionPoints");
                });

            modelBuilder.Entity("VeroVrCore.Common.Models.User", b =>
                {
                    b.Property<int>("UserId")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("UserId")
                        .HasColumnType("int");

                    b.Property<DateTime>("DateOfRegistration")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValueSql("'0001-01-01 00:00:00'");

                    b.Property<string>("DeviceAtLastLogin");

                    b.Property<string>("Occupation");

                    b.Property<string>("UserAddress");

                    b.Property<int?>("UserAgeRange");

                    b.Property<string>("UserAvatarUrl");

                    b.Property<string>("UserCity");

                    b.Property<string>("UserCompanyName");

                    b.Property<string>("UserCountry");

                    b.Property<string>("UserDisplayName");

                    b.Property<string>("UserEmail");

                    b.Property<string>("UserFacebook");

                    b.Property<string>("UserPassword");

                    b.Property<string>("UserPhoneNumber");

                    b.Property<int>("UserRole");

                    b.HasKey("UserId");

                    b.HasIndex("DateOfRegistration");

                    b.ToTable("Users");
                });

            modelBuilder.Entity("VeroVrCore.Common.Models.UserActivity", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("Id")
                        .HasColumnType("int");

                    b.Property<string>("Discriminator")
                        .IsRequired();

                    b.Property<DateTime>("EventDateStamp")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValueSql("'0001-01-01 00:00:00'");

                    b.Property<int>("UserActivityType");

                    b.Property<int>("UserId")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("EventDateStamp");

                    b.ToTable("UserActivities");

                    b.HasDiscriminator<string>("Discriminator").HasValue("UserActivity");
                });

            modelBuilder.Entity("VeroVrCore.Common.Models.UserClickLinkInDescriptionActivity", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("Id")
                        .HasColumnType("int");

                    b.Property<DateTime>("EventDateStamp")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValueSql("'0001-01-01 00:00:00'");

                    b.Property<string>("Link");

                    b.Property<int>("ProjectId");

                    b.Property<int>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("EventDateStamp");

                    b.ToTable("UserClickLinkInDescriptionActivities");
                });

            modelBuilder.Entity("VeroVrCore.Common.Models.UserLoginActivity", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("Id")
                        .HasColumnType("int");

                    b.Property<string>("Device");

                    b.Property<string>("DeviceType");

                    b.Property<DateTime>("EventDateStamp")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValueSql("'0001-01-01 00:00:00'");

                    b.Property<int>("UserId")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("EventDateStamp");

                    b.ToTable("UserLoginActivities");
                });

            modelBuilder.Entity("VeroVrCore.Common.Models.UserOnlineActivity", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("Id")
                        .HasColumnType("int");

                    b.Property<DateTime>("EventDateStamp")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValueSql("'0001-01-01 00:00:00'");

                    b.Property<int>("UserId")
                        .HasColumnName("UserId")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("EventDateStamp");

                    b.HasIndex("UserId")
                        .IsUnique();

                    b.ToTable("UserOnlineActivities");
                });

            modelBuilder.Entity("VeroVrCore.Common.Models.UserProject", b =>
                {
                    b.Property<int>("UserId")
                        .HasColumnName("UserId")
                        .HasColumnType("int");

                    b.Property<int>("ProjectId")
                        .HasColumnName("ProjectId")
                        .HasColumnType("int");

                    b.HasKey("UserId", "ProjectId");

                    b.HasIndex("ProjectId");

                    b.ToTable("UserProject");
                });

            modelBuilder.Entity("VeroVrCore.Common.Models.UserShareProjectActivity", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("Id")
                        .HasColumnType("int");

                    b.Property<DateTime>("EventDateStamp")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValueSql("'0001-01-01 00:00:00'");

                    b.Property<int>("ProjectId");

                    b.Property<int>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("EventDateStamp");

                    b.ToTable("UserShareProjectActivities");
                });

            modelBuilder.Entity("VeroVrCore.Common.Models.ObjectPhoto", b =>
                {
                    b.HasBaseType("VeroVrCore.Common.Models.BaseObject");


                    b.ToTable("ObjectPhoto");

                    b.HasDiscriminator().HasValue("ObjectPhoto");
                });

            modelBuilder.Entity("VeroVrCore.Common.Models.ObjectTransitionPoint", b =>
                {
                    b.HasBaseType("VeroVrCore.Common.Models.BaseObject");

                    b.Property<string>("TransitionPointsText");

                    b.ToTable("ObjectTransitionPoint");

                    b.HasDiscriminator().HasValue("ObjectTransitionPoint");
                });

            modelBuilder.Entity("VeroVrCore.Common.Models.ObjectYouTube", b =>
                {
                    b.HasBaseType("VeroVrCore.Common.Models.BaseObject");


                    b.ToTable("ObjectYouTube");

                    b.HasDiscriminator().HasValue("ObjectYouTube");
                });

            modelBuilder.Entity("VeroVrCore.Common.Models.UserObjectActivity", b =>
                {
                    b.HasBaseType("VeroVrCore.Common.Models.UserActivity");

                    b.Property<int>("ObjectId");

                    b.ToTable("UserObjectActivity");

                    b.HasDiscriminator().HasValue("UserObjectActivity");
                });

            modelBuilder.Entity("VeroVrCore.Common.Models.UserProjectActivity", b =>
                {
                    b.HasBaseType("VeroVrCore.Common.Models.UserActivity");

                    b.Property<int>("ProjectId");

                    b.ToTable("UserProjectActivity");

                    b.HasDiscriminator().HasValue("UserProjectActivity");
                });

            modelBuilder.Entity("VeroVrCore.Common.Models.UserRoomImageActivity", b =>
                {
                    b.HasBaseType("VeroVrCore.Common.Models.UserActivity");

                    b.Property<int>("RoomImageId");

                    b.ToTable("UserRoomImageActivity");

                    b.HasDiscriminator().HasValue("UserRoomImageActivity");
                });

            modelBuilder.Entity("VeroVrCore.Common.Models.ObjectAr", b =>
                {
                    b.HasBaseType("VeroVrCore.Common.Models.ObjectTransitionPoint");

                    b.Property<string>("MtlUrl");

                    b.Property<double>("Size");

                    b.ToTable("ObjectAr");

                    b.HasDiscriminator().HasValue("ObjectAr");
                });

            modelBuilder.Entity("VeroVrCore.Common.Models.ObjectPlan", b =>
                {
                    b.HasBaseType("VeroVrCore.Common.Models.ObjectTransitionPoint");

                    b.Property<int>("FloorNumber");

                    b.ToTable("ObjectPlan");

                    b.HasDiscriminator().HasValue("ObjectPlan");
                });

            modelBuilder.Entity("VeroVrCore.Common.Models.ObjectRoom", b =>
                {
                    b.HasBaseType("VeroVrCore.Common.Models.ObjectTransitionPoint");


                    b.ToTable("ObjectRoom");

                    b.HasDiscriminator().HasValue("ObjectRoom");
                });

            modelBuilder.Entity("VeroVrCore.Common.Models.UserInArActivity", b =>
                {
                    b.HasBaseType("VeroVrCore.Common.Models.UserObjectActivity");


                    b.ToTable("UserInArActivity");

                    b.HasDiscriminator().HasValue("UserInArActivity");
                });

            modelBuilder.Entity("VeroVrCore.Common.Models.UserInYouTubeVideoActivity", b =>
                {
                    b.HasBaseType("VeroVrCore.Common.Models.UserObjectActivity");


                    b.ToTable("UserInYouTubeVideoActivity");

                    b.HasDiscriminator().HasValue("UserInYouTubeVideoActivity");
                });

            modelBuilder.Entity("VeroVrCore.Common.Models.UserInProjectDescriptionActivity", b =>
                {
                    b.HasBaseType("VeroVrCore.Common.Models.UserProjectActivity");


                    b.ToTable("UserInProjectDescriptionActivity");

                    b.HasDiscriminator().HasValue("UserInProjectDescriptionActivity");
                });

            modelBuilder.Entity("VeroVrCore.Common.Models.UserInRoom360Activity", b =>
                {
                    b.HasBaseType("VeroVrCore.Common.Models.UserRoomImageActivity");


                    b.ToTable("UserInRoom360Activity");

                    b.HasDiscriminator().HasValue("UserInRoom360Activity");
                });

            modelBuilder.Entity("VeroVrCore.Common.Models.UserInRoomVrActivity", b =>
                {
                    b.HasBaseType("VeroVrCore.Common.Models.UserRoomImageActivity");


                    b.ToTable("UserInRoomVrActivity");

                    b.HasDiscriminator().HasValue("UserInRoomVrActivity");
                });

            modelBuilder.Entity("VeroVrCore.Common.Models.BaseObject", b =>
                {
                    b.HasOne("VeroVrCore.Common.Models.Project", "Project")
                        .WithMany("Objects")
                        .HasForeignKey("ProjectId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("VeroVrCore.Common.Models.Interior", b =>
                {
                    b.HasOne("VeroVrCore.Common.Models.Project", "Project")
                        .WithMany("Interiors")
                        .HasForeignKey("ProjectId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("VeroVrCore.Common.Models.ObjectArInfo", b =>
                {
                    b.HasOne("VeroVrCore.Common.Models.Project", "Project")
                        .WithMany("ObjectArInfos")
                        .HasForeignKey("ProjectId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("VeroVrCore.Common.Models.PlanInfo", b =>
                {
                    b.HasOne("VeroVrCore.Common.Models.Project", "Project")
                        .WithMany("PlanInfos")
                        .HasForeignKey("ProjectId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("VeroVrCore.Common.Models.RoomImage", b =>
                {
                    b.HasOne("VeroVrCore.Common.Models.DayTime", "DayTime")
                        .WithMany("RoomImages")
                        .HasForeignKey("DayTimeId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("VeroVrCore.Common.Models.BaseObject", "Object")
                        .WithMany("RoomImages")
                        .HasForeignKey("ObjectId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("VeroVrCore.Common.Models.RoomType", b =>
                {
                    b.HasOne("VeroVrCore.Common.Models.Project", "Project")
                        .WithMany("RoomTypes")
                        .HasForeignKey("ProjectId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("VeroVrCore.Common.Models.TransitionPoint", b =>
                {
                    b.HasOne("VeroVrCore.Common.Models.BaseObject", "Object")
                        .WithMany("TransitionPoints")
                        .HasForeignKey("ObjectId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("VeroVrCore.Common.Models.UserProject", b =>
                {
                    b.HasOne("VeroVrCore.Common.Models.Project", "Project")
                        .WithMany("UserProjects")
                        .HasForeignKey("ProjectId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("VeroVrCore.Common.Models.User", "User")
                        .WithMany("UserProjects")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
#pragma warning restore 612, 618
        }
    }
}
