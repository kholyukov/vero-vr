﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VeroVrCore.DataLayer.Migrations
{
    public partial class AddedDeviceFieldForLoginStatistic : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Device",
                table: "LoginActivities",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Users_DateOfRegistration",
                table: "Users",
                column: "DateOfRegistration");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Users_DateOfRegistration",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "Device",
                table: "LoginActivities");
        }
    }
}
