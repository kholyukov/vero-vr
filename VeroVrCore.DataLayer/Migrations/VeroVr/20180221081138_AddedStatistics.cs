﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VeroVrCore.DataLayer.Migrations
{
    public partial class AddedStatistics : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "LoginActivities");

            migrationBuilder.DropTable(
                name: "UserInProjectActivities");

            migrationBuilder.DropTable(
                name: "UserInRoomActivities");

            migrationBuilder.DropColumn(
                name: "PersonalTitle",
                table: "Users");

            migrationBuilder.AddColumn<int>(
                name: "UserAgeRange",
                table: "Users",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "UserActivities",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Discriminator = table.Column<string>(nullable: false),
                    EventDateStamp = table.Column<DateTime>(nullable: false, defaultValueSql: "'0001-01-01 00:00:00'"),
                    UserActivityType = table.Column<int>(nullable: false),
                    UserId = table.Column<int>(type: "int", nullable: false),
                    ObjectId = table.Column<int>(nullable: true),
                    ProjectId = table.Column<int>(nullable: true),
                    RoomImageId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserActivities", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UserLoginActivities",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Device = table.Column<string>(nullable: true),
                    EventDateStamp = table.Column<DateTime>(nullable: false, defaultValueSql: "'0001-01-01 00:00:00'"),
                    UserId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserLoginActivities", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UserOnlineActivities",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    EventDateStamp = table.Column<DateTime>(nullable: false, defaultValueSql: "'0001-01-01 00:00:00'"),
                    UserId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserOnlineActivities", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UserShareProjectActivities",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    EventDateStamp = table.Column<DateTime>(nullable: false, defaultValueSql: "'0001-01-01 00:00:00'"),
                    ProjectId = table.Column<int>(nullable: false),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserShareProjectActivities", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_UserActivities_EventDateStamp",
                table: "UserActivities",
                column: "EventDateStamp");

            migrationBuilder.CreateIndex(
                name: "IX_UserLoginActivities_EventDateStamp",
                table: "UserLoginActivities",
                column: "EventDateStamp");

            migrationBuilder.CreateIndex(
                name: "IX_UserOnlineActivities_EventDateStamp",
                table: "UserOnlineActivities",
                column: "EventDateStamp");

            migrationBuilder.CreateIndex(
                name: "IX_UserOnlineActivities_UserId",
                table: "UserOnlineActivities",
                column: "UserId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_UserShareProjectActivities_EventDateStamp",
                table: "UserShareProjectActivities",
                column: "EventDateStamp");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "UserActivities");

            migrationBuilder.DropTable(
                name: "UserLoginActivities");

            migrationBuilder.DropTable(
                name: "UserOnlineActivities");

            migrationBuilder.DropTable(
                name: "UserShareProjectActivities");

            migrationBuilder.DropColumn(
                name: "UserAgeRange",
                table: "Users");

            migrationBuilder.AddColumn<string>(
                name: "PersonalTitle",
                table: "Users",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "LoginActivities",
                columns: table => new
                {
                    LoginActivityId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DateOfLogin = table.Column<DateTime>(nullable: false, defaultValueSql: "'0001-01-01 00:00:00'"),
                    Device = table.Column<string>(nullable: true),
                    UserId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LoginActivities", x => x.LoginActivityId);
                });

            migrationBuilder.CreateTable(
                name: "UserInProjectActivities",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    EventDateStamp = table.Column<DateTime>(nullable: false, defaultValueSql: "'0001-01-01 00:00:00'"),
                    ProjectId = table.Column<int>(type: "int", nullable: false),
                    UserId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserInProjectActivities", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UserInRoomActivities",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    EventDateStamp = table.Column<DateTime>(nullable: false, defaultValueSql: "'0001-01-01 00:00:00'"),
                    RoomImageId = table.Column<int>(type: "int", nullable: false),
                    UserId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserInRoomActivities", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_LoginActivities_DateOfLogin",
                table: "LoginActivities",
                column: "DateOfLogin");

            migrationBuilder.CreateIndex(
                name: "IX_UserInProjectActivities_EventDateStamp",
                table: "UserInProjectActivities",
                column: "EventDateStamp");

            migrationBuilder.CreateIndex(
                name: "IX_UserInRoomActivities_EventDateStamp",
                table: "UserInRoomActivities",
                column: "EventDateStamp");
        }
    }
}
