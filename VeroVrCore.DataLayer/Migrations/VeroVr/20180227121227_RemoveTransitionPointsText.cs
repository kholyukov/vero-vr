﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VeroVrCore.DataLayer.Migrations
{
    public partial class RemoveTransitionPointsText : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TransitionPointsText",
                table: "Objects");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "TransitionPointsText",
                table: "Objects",
                nullable: true);
        }
    }
}
