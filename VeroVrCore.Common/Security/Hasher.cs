﻿using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using System;
using System.Security.Cryptography;

namespace VeroVrCore.Common.Security
{
    public static class Hasher
    {
        public static string Hash(string textPlain, string salt = null)
        {
            byte[] saltGenerated = new byte[0];

            saltGenerated = String.IsNullOrEmpty(salt) ?
                Convert.FromBase64String(GetSalt()) : Convert.FromBase64String(salt);

            // derive a 256-bit subkey (use HMACSHA256 with 10,000 iterations)
            string hashed = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: textPlain,
                salt: saltGenerated,
                prf: KeyDerivationPrf.HMACSHA256,
                iterationCount: 10000,
                numBytesRequested: 256 / 8));

            return hashed;
        }

        public static string GetSalt()
        {
            // generate a 128-bit salt using a secure PRNG
            byte[] salt = new byte[128 / 8];

            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(salt);
            }

            return Convert.ToBase64String(salt);
        }
    }
}
