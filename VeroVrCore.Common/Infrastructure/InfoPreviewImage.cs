﻿namespace VeroVrCore.Common.Infrastructure
{
    public class InfoPreviewImage
    {
        public const string PreviewRoomPostfix = "_previewRoom";
        public const string PreviewPlanPostfix = "_previewPlan";
    }
}
