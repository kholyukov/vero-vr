﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace VeroVrCore.Common.Infrastructure
{
    public static class LinkParser
    {
        public static string GerParsedDescription(string descr)
        {
            Regex regex = new Regex(@"([(http|ftp|https):\/\/[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?]) - (link name:.*?)");

            MatchCollection splitLinks = regex.Matches(descr);

            //if there is at least 1 match
            if (splitLinks.Count > 0)
            {
                foreach (Match match in splitLinks)
                {
                    //looking for link name
                    Match linkNameMatch = Regex.Match(match.Value, @"(.*)");
                    //looking for link itself
                    Match urlMatch = Regex.Match(match.Value, @"([(http|ftp|https):\/\/[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?])");
                    //if both matches are successful - do the rest of the work
                    if (linkNameMatch.Success && urlMatch.Success)
                    {
                        descr = descr.Replace(urlMatch.Value,
                            string.Format("{0}", urlMatch.Value.Trim('[', ']'))); //removing square brackets
                    }
                }
            }

            return Regex.Replace(descr, @"( - link name:.*?\))", "").Trim();
        }
    }
}
