﻿namespace VeroVrCore.Common.Models
{
    public class TransitionPoint
    {
        public int TransitionPointId { get; set; }
        public double PositionX { get; set; }
        public double PositionY { get; set; }
        public double PositionZ { get; set; }
        public double PercentageH { get; set; }
        public double PercentageV { get; set; }
        public int RoomId { get; set; }

        public int ObjectId { get; set; }
        public BaseObject Object { get; set; }
    }
}
