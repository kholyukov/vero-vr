﻿using System.Collections.Generic;

namespace VeroVrCore.Common.Models
{
    public class BaseObject
    {
        public int ObjectId { get; set; }
        public string ObjectDescription { get; set; }
        public string ObjectName { get; set; }
        public string PathFileOnServer { get; set; }
        public ObjectType ObjectType { get; set; }
        public Project Project { get; set; }
        public int ProjectId { get; set; }

        public virtual ICollection<TransitionPoint> TransitionPoints { get; set; }
        public virtual ICollection<RoomImage> RoomImages { get; set; }
        public BaseObject()
        {
            TransitionPoints = new List<TransitionPoint>();
            RoomImages = new List<RoomImage>();
        }
    }
}
