﻿namespace VeroVrCore.Common.Models
{
    public class RoomImage
    {
        public int RoomImageId { get; set; }
        public string ImageUrl { get; set; }
        public DayTime DayTime { get; set; }
        public BaseObject Object { get; set; }
        public int InteriorId { get; set; }
        public int RoomTypeId { get; set; }
        public int ObjectPlanId { get; set; }
        public int DayTimeId { get; set; }
        public int ObjectId { get; set; }
        public DefaultRoom DefaultRoom { get; set; }
    }
}
