﻿using System.ComponentModel;

namespace VeroVrCore.Common.Models
{
    public enum UserRole
    {
        SuperAdmin,
        SuperManager,
        SuperManagerWithScript,
        Manager,
        Customer
    }
}
