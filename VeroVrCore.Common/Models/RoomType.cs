﻿namespace VeroVrCore.Common.Models
{
    public class RoomType
    {
        public int RoomTypeId { get; set; }
        public string RoomTypeName { get; set; }
        public Project Project { get; set; }
        public int ProjectId { get; set; }
    }
}
