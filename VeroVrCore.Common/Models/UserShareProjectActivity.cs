﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VeroVrCore.Common.Models
{
    public class UserShareProjectActivity
    {
        public int Id { get; set; }
        public DateTime EventDateStamp { get; set; }
        public int UserId { get; set; }
        public int ProjectId { get; set; }
    }
}
