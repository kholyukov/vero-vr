﻿using System.Collections.Generic;

namespace VeroVrCore.Common.Models
{
    public class ObjectTransitionPoint : BaseObject
    {
        public override ICollection<TransitionPoint> TransitionPoints { get; set; }
        public ObjectTransitionPoint()
        {
            TransitionPoints = new List<TransitionPoint>();
        }
    }
}
