﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace VeroVrCore.Common.Models
{
    public class User
    {
        public int UserId { get; set; }
        public string UserAvatarUrl { get; set; }
        public string UserDisplayName { get; set; }
        public string UserCompanyName { get; set; }
        public string Occupation { get; set; }
        public string UserPhoneNumber { get; set; }
        public string UserCountry { get; set; }
        public string UserCity { get; set; }
        public string UserAgeRange { get; set; }
        public string UserEmail { get; set; }
        public string UserFacebook { get; set; }
        public string UserAddress { get; set; }
        public string UserPassword { get; set; }
        public string Salt { get; set; }
        public UserRole UserRole { get; set; }
        public DateTime DateOfRegistration { get; set; }
        public string DeviceAtLastLogin { get; set; }
        public string DeviceTypeAtLastLogin { get; set; }
        public virtual ICollection<UserProject> UserProjects { get; set; }
        public string FacebookLikesJson { get; set; }
        //Interests is not to be mapped
        public string[] FacebookLikes
        {
            get { return FacebookLikesJson == null ? null : JsonConvert.DeserializeObject<string[]>(FacebookLikesJson); }
            set { FacebookLikesJson = JsonConvert.SerializeObject(value); }
        }
        public virtual IEnumerable<Project> Projects
        {
            get { return UserProjects?.Select(up => up.Project); }
        }

        public User()
        {
            UserProjects = new List<UserProject>();
        }
    }
}
