﻿using System.Collections.Generic;

namespace VeroVrCore.Common.Models
{
    public class ObjectRoom : ObjectTransitionPoint
    {
        public override ICollection<RoomImage> RoomImages { get; set; }
        public ObjectRoom()
        {
            RoomImages = new List<RoomImage>();
        }
    }
}
