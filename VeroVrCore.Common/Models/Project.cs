﻿using System.Collections.Generic;
using System.Linq;

namespace VeroVrCore.Common.Models
{
    public class Project
    {
        public int ProjectId { get; set; }
        public string ProjectName { get; set; }
        public string ProjectDescription { get; set; }
        public string ProjectImageUrl { get; set; }
        public string ProjectPdfUrl { get; set; }
        public string ProjectCode { get; set; }
        public string ProjectToken { get; set; }

        public virtual ICollection<RoomType> RoomTypes { get; set; }
        public virtual ICollection<PlanInfo> PlanInfos { get; set; }
        public virtual ICollection<Interior> Interiors { get; set; }
        public virtual ICollection<UserProject> UserProjects { get; set; }
        public virtual ICollection<ObjectArInfo> ObjectArInfos { get; set; }
        public decimal? ProjectOriginalSizeBytes { get; set; }

        public virtual IEnumerable<User> Customers
        {
            get
            {
                return UserProjects?
                    .Select(up => up.User)
                    .Where(u => u.UserRole == UserRole.Customer);
            }
        }

        public virtual User Manager
        {
            get
            {
                return UserProjects?
                    .Select(up => up.User)
                    .FirstOrDefault(u => u.UserRole == UserRole.Manager);
            }
        }

        public virtual ICollection<BaseObject> Objects { get; set; }

        public Project()
        {
            UserProjects = new List<UserProject>();
            Objects = new List<BaseObject>();
            RoomTypes = new List<RoomType>();
            PlanInfos = new List<PlanInfo>();
            Interiors = new List<Interior>();
            ObjectArInfos = new List<ObjectArInfo>();
        }
    }
}
