﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VeroVrCore.Common.Models
{
    public class UserRoomImageActivity : UserActivity
    {
        public int RoomImageId { get; set; }
    }
}
