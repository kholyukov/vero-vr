﻿namespace VeroVrCore.Common.Models
{
    public class Interior
    {
        public int InteriorId { get; set; }
        public string InteriorName { get; set; }
        public Project Project { get; set; }
        public int ProjectId { get; set; }
    }
}
