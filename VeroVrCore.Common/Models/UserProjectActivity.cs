﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VeroVrCore.Common.Models
{
    public class UserProjectActivity : UserActivity
    {
        public int ProjectId { get; set; }
    }
}
