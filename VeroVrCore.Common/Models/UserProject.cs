﻿namespace VeroVrCore.Common.Models
{
    public class UserProject
    {
        public int UserId { get; set; }
        public virtual User User { get; set; }

        public int ProjectId { get; set; }
        public virtual Project Project { get; set; }

        public UserRole UserRole { get { return User.UserRole;  } }
    }
}
