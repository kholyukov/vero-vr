﻿using System.Collections.Generic;

namespace VeroVrCore.Common.Models
{
    public class DayTime
    {
        public int DayTimeId { get; set; }
        public string DayTimeName { get; set; }
        public ICollection<RoomImage> RoomImages { get; set; }

        public DayTime()
        {
            RoomImages = new List<RoomImage>();
        }
    }
}
