﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VeroVrCore.Common.Models
{
    public class PlanInfo
    {
        public int PlanInfoId { get; set; }
        public string PlanName { get; set; }
        public Project Project { get; set; }
        public int ProjectId { get; set; }
    }
}
