﻿using System;

namespace VeroVrCore.Common.Models
{
    public class UserOnlineActivity
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public DateTime EventDateStamp { get; set; }
    }
}
