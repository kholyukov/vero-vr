﻿namespace VeroVrCore.Common.Models
{
    public enum ObjectType
    {
        Room = 1,
        Ar = 3,
        Photo,
        Plan,
        VideoYouTube,
        PanoramaScript,
        ProjectIcon,
        UserAvatar,
        Pdf
    }
}
