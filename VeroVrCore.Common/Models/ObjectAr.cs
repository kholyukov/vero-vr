﻿using Newtonsoft.Json;

namespace VeroVrCore.Common.Models
{
    public class ObjectAr : ObjectTransitionPoint
    {
        public string AdditionalJsonUrls { get; set; }
        //Additionals is not to be mapped
        public string[] Additionals
        {
            get { return AdditionalJsonUrls == null ? null : JsonConvert.DeserializeObject<string[]>(AdditionalJsonUrls); }
            set { AdditionalJsonUrls = JsonConvert.SerializeObject(value); }
        }
        public double Size { get; set; }
        public string Version { get; set; }
    }
}
