﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VeroVrCore.Common.Models
{
    public class ObjectArInfo
    {
        public int ObjectArInfoId { get; set; }
        public string ObjectArName { get; set; }
        public Project Project { get; set; }
        public int ProjectId { get; set; }
    }
}
