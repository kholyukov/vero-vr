﻿namespace VeroVrCore.Common.Models
{
    public enum UserActivityType
    {
        ArModelView,
        ProjectDescriptionView,
        VrModeView,
        Room360View,
        YouTubeVideoView,
        ListOfProjects,
        PlansView
    }
}
