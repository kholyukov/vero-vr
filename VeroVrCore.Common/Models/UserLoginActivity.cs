﻿using System;

namespace VeroVrCore.Common.Models
{
    public class UserLoginActivity
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Device { get; set; }
        public string DeviceType { get; set; }
        public DateTime EventDateStamp { get; set; }
    }
}
