﻿using System;

namespace VeroVrCore.Common.Models
{
    public class LogEvent
    {
        public int Id { get; set; }
        public int EventId { get; set; }
        public string LogLevel { get; set; }
        public string Message { get; set; }
        public DateTime CreatedTime { get; set; }
    }
}
