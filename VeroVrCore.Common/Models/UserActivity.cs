﻿using System;

namespace VeroVrCore.Common.Models
{
    public class UserActivity
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public UserActivityType UserActivityType { get; set; }
        public DateTime EventDateStamp { get; set; }
    }
}
