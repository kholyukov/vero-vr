﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using VeroVrCore.Common.Infrastructure;
using VeroVrCore.Common.Models;
using VeroVrCore.DataLayer.EntityFramework;
using VeroVrCore.WebApi.Services;

namespace VeroVrCore.WebApi
{
    public class FileManagerService
    {
        private VeroVrContext _context;
        private IHostingEnvironment _appEnvironment;
        private ImageService _imageService;
        private bool _isOriginalLevel;

        public string GetContentPath => Path.DirectorySeparatorChar + "content" + Path.DirectorySeparatorChar;

        public FileManagerService
            (VeroVrContext context,
            IHostingEnvironment appEnvironment,
            ImageService imageService)
        {
            _context = context;
            _appEnvironment = appEnvironment;
            _imageService = imageService;
        }

        public async Task SaveFileForAddOperation(byte[] file, ObjectType objectType, string projectId)
        {
            if (file != null)
            {
                int projectNumberId;

                bool isExistProjectId = int.TryParse(projectId, out projectNumberId);

                if (isExistProjectId == false)
                {
                    throw new ArgumentException("ProjectId is not number");
                }

                var project = _context.Projects
                    .SingleOrDefault(p => p.ProjectId == projectNumberId);

                if (project == null)
                {
                    throw new ArgumentException($"Project with ProjectId={projectNumberId} doesn't exists");
                }

                char pathSep = System.IO.Path.DirectorySeparatorChar;

                // путь к папке
                string relativePath = $"{pathSep}content{pathSep}{projectId}{pathSep}{(int)objectType}{pathSep}";
                string path = _appEnvironment.WebRootPath + relativePath;
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                string extImage = ".jpg";

                string uniqueName = System.Guid.NewGuid().ToString();
                string newFullPath = path + uniqueName + extImage;
                string relativePathForAccess = (relativePath + uniqueName + extImage);

                // сохраняем файл в папку в каталоге wwwroot
                using (var imageFile = new FileStream(newFullPath, FileMode.Create))
                {
                    imageFile.Write(file, 0, file.Length);
                    imageFile.Flush();
                }

                //сохраняем информацию об объекте в базу данных

                if (objectType == ObjectType.ProjectIcon)
                {
                    project.ProjectImageUrl = relativePathForAccess;
                }
                else if (objectType == ObjectType.Photo)
                {
                    ObjectPhoto @object = new ObjectPhoto
                    {
                        PathFileOnServer = relativePathForAccess,
                        ObjectType = objectType,
                        Project = project
                    };

                    _context.Objects.Add(@object);
                }
                else if (objectType == ObjectType.Plan)
                {
                    ObjectPlan @object = new ObjectPlan
                    {
                        PathFileOnServer = relativePathForAccess,
                        ObjectType = objectType,
                        Project = project
                    };

                    _context.Objects.Add(@object);
                }
                else
                {
                    BaseObject @object = new BaseObject
                    {
                        PathFileOnServer = relativePathForAccess,
                        ObjectType = objectType,
                        Project = project
                    };

                    _context.Objects.Add(@object);
                }

                project.ProjectOriginalSizeBytes = await GetFullSizeProjectByProjectIdAsync(project.ProjectId);

                await _context.SaveChangesAsync();
            }
        }

        public async Task<string> SaveUserAvatar(byte[] file, int userId)
        {
            char pathSep = System.IO.Path.DirectorySeparatorChar;

            if (file == null)
            {
                throw new ArgumentNullException("Profile image file is not specified");
            }

            // путь к папке
            string relativePath = $"{pathSep}content{pathSep}user{pathSep}avatar{pathSep}{userId}{pathSep}";
            string path = _appEnvironment.WebRootPath + relativePath;

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            string extImage = ".jpg";

            string uniqueName = System.Guid.NewGuid().ToString();
            string newFullPath = path + uniqueName + extImage;
            string relativePathForAccess = (relativePath + uniqueName + extImage);

            // сохраняем файл в папку в каталоге wwwroot
            using (var imageFile = new FileStream(newFullPath, FileMode.Create))
            {
                await imageFile.WriteAsync(file, 0, file.Length);
                imageFile.Flush();
            }

            return relativePathForAccess;
        }

        public async Task SaveFilesForAddOperation(IFormFileCollection files,
            ObjectType objectType,
            string projectId,
            IFormFileCollection mapsFiles = null,
            RoomImage roomImage = null,
            int? planInfoId = null,
            int? floorNumber = null,
            int? arObjectInfoId = null,
            double? sizeAr = null)
        {
            Console.WriteLine("!!! START SaveFilesForAddOperation");
            if (files != null && files.Count > 0
                && !string.IsNullOrEmpty(projectId))
            {
                int projectNumberId;

                bool isExistProjectId = int.TryParse(projectId, out projectNumberId);

                if (isExistProjectId == false)
                {
                    throw new ArgumentException("ProjectId is not number");
                }

                Project project = _context
                    .Projects
                    .Include("Objects.RoomImages")
                    .SingleOrDefault(p => p.ProjectId == projectNumberId);

                if (project == null)
                {
                    throw new ArgumentException($"Project with ProjectId={projectNumberId} doesn't exists");
                }

                char pathSep = System.IO.Path.DirectorySeparatorChar;

                // путь к папке
                string uniqFolderNamebase64Guid = 
                    Convert.ToBase64String(Guid.NewGuid().ToByteArray())
                    .Replace("/", "F")
                    .Replace("+", "S")
                    .Replace("=", "B");

                string relativePath = $"{pathSep}content{pathSep}{projectId}{pathSep}{(int)objectType}{pathSep}";
                string relativeArPath = relativePath + uniqFolderNamebase64Guid + pathSep;
                string path = _appEnvironment.WebRootPath + relativePath;

                if (objectType == ObjectType.Ar)
                {
                    string pathAr = _appEnvironment.WebRootPath + relativeArPath;

                    string objPath = "";
                    List<string> additionalPaths = new List<string>();

                    if (!Directory.Exists(pathAr))
                    {
                        Directory.CreateDirectory(pathAr);
                    }

                    foreach (IFormFile uploadedFile in files)
                    {
                        string newFullPath = pathAr + uploadedFile.FileName;
                        string relativePathForAccess = (relativeArPath + uploadedFile.FileName);
                        string extension = Path.GetExtension(uploadedFile.FileName);

                        if (extension.ToLower() == ".obj")
                        {
                            objPath = relativePathForAccess;
                        }
                        else
                        {
                            additionalPaths.Add(relativePathForAccess);
                        }

                        // сохраняем файл в папку в каталоге wwwroot
                        using (var fileStream = new FileStream(newFullPath, FileMode.Create))
                        {
                            await uploadedFile.CopyToAsync(fileStream);
                        }
                    }

                    string mapPath = pathAr + $"maps{pathSep}";

                    if (!Directory.Exists(mapPath))
                    {
                        Directory.CreateDirectory(mapPath);
                    }

                    //Maps files
                    if (mapsFiles != null)
                    {
                        foreach (IFormFile mapFile in mapsFiles)
                        {
                            string extension = Path.GetExtension(mapFile.FileName).ToLower();
                            string newFullPath = mapPath + mapFile.FileName;
                            string relativePathForAccess = (relativeArPath + $"maps{pathSep}" + mapFile.FileName);

                            additionalPaths.Add(relativePathForAccess);

                            // сохраняем файл в папку в каталоге wwwroot
                            using (var fileStream = new FileStream(newFullPath, FileMode.Create))
                            {
                                await mapFile.CopyToAsync(fileStream);
                            }

                            if (extension == ".png" || extension == ".jpg" ||
                                    extension == ".jpeg" || extension == ".tif" ||
                                    extension == ".tiff" || extension == ".tga")
                            {
                                _imageService.ResizeImageByHeight(newFullPath, newFullPath, size: 256);
                            }
                        }
                    }

                    var arObjectInfo = await _context
                        .ObjectArInfos
                        .SingleOrDefaultAsync(a => a.ObjectArInfoId == arObjectInfoId);

                    ObjectAr @object = new ObjectAr
                    {
                        ObjectName = arObjectInfo.ObjectArName,
                        Size = sizeAr.HasValue ? sizeAr.Value : 0.0,
                        PathFileOnServer = objPath,
                        Additionals = additionalPaths.ToArray(),
                        ObjectType = objectType,
                        Project = project,
                        Version = Guid.NewGuid().ToString()
                    };

                    _context.Objects.Add(@object);
                }
                else
                {
                    foreach (IFormFile uploadedFile in files)
                    {
                        //Console.WriteLine("!!! START SAVING");

                        try
                        {
                            Console.WriteLine($"!!! {path} {Directory.Exists(path)}");
                            if (!Directory.Exists(path))
                            {
                                Directory.CreateDirectory(path);
                            }

                            string uniqueName = System.Guid.NewGuid().ToString();
                            string extension = Path.GetExtension(uploadedFile.FileName);

                            if((objectType == ObjectType.Room || objectType == ObjectType.Photo ||
                                objectType == ObjectType.Plan || objectType == ObjectType.ProjectIcon)
                                && String.IsNullOrEmpty(extension))
                            {
                                extension = ".jpg";
                            }

                            string newFullPath = path + uniqueName + extension;
                            string relativePathForAccess = (relativePath + uniqueName + extension);

                            // сохраняем файл в папку в каталоге wwwroot
                            using (var fileStream = new FileStream(newFullPath, FileMode.Create))
                            {
                                await uploadedFile.CopyToAsync(fileStream);
                            }

                            //сохраняем информацию об объекте в базу данных
                            if (objectType == ObjectType.ProjectIcon)
                            {
                                project.ProjectImageUrl = relativePathForAccess;
                            }
                            else if (objectType == ObjectType.Photo)
                            {
                                ObjectPhoto @object = new ObjectPhoto
                                {
                                    PathFileOnServer = relativePathForAccess,
                                    ObjectType = objectType,
                                    Project = project
                                };

                                _context.Objects.Add(@object);
                            }
                            else if (objectType == ObjectType.Plan)
                            {
                                var plan = _context.PlanInfos
                                    .SingleOrDefault(p => p.PlanInfoId == planInfoId);

                                ObjectPlan @object = new ObjectPlan
                                {
                                    ObjectName = plan?.PlanName,
                                    PathFileOnServer = relativePathForAccess,
                                    ObjectType = objectType,
                                    Project = project,
                                    FloorNumber = floorNumber.Value
                                };

                                _context.Objects.Add(@object);
                            }
                            else if (objectType == ObjectType.Room)
                            {
                                if (roomImage != null)
                                {

                                    var roomType = await _context
                                        .RoomTypes
                                        .SingleOrDefaultAsync(r => r.RoomTypeId == roomImage.RoomTypeId);

                                    roomImage.ImageUrl = relativePathForAccess;

                                    string roomName = roomType.RoomTypeName;

                                    BaseObject objectRoomWithSamePlanAndName =
                                        project.Objects
                                        .FirstOrDefault(o => o.ObjectType == ObjectType.Room
                                        //&& String.Equals(roomName.Trim(), o.ObjectName,
                                        //StringComparison.OrdinalIgnoreCase)
                                        && o.RoomImages
                                        .Select(ri => ri.RoomTypeId)
                                        .Contains(roomType.RoomTypeId)
                                        && o.RoomImages
                                        .Select(ri => ri.ObjectPlanId)
                                        .Contains(roomImage.ObjectPlanId));

                                    if (objectRoomWithSamePlanAndName != null)
                                    {
                                        roomImage.Object = objectRoomWithSamePlanAndName;
                                        _context.RoomImages.Add(roomImage);
                                    }
                                    else
                                    {
                                        var dayTime = await _context
                                       .DayTimes
                                       .SingleOrDefaultAsync(r => r.DayTimeId == roomImage.DayTimeId);

                                        var objectRoom = new ObjectRoom
                                        {
                                            ObjectType = objectType,
                                            Project = project,
                                            ObjectName = roomType.RoomTypeName
                                        };

                                        roomImage.Object = objectRoom;
                                        roomImage.DayTime = dayTime;

                                        objectRoom.RoomImages = new[]
                                        {
                                        roomImage
                                    };

                                        _context.Objects.Add(objectRoom);
                                    }
                                }
                            }
                            else
                            {
                                BaseObject @object = new BaseObject
                                {
                                    PathFileOnServer = relativePathForAccess,
                                    ObjectType = objectType,
                                    Project = project
                                };

                                _context.Objects.Add(@object);
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex);
                        }


                    }
                }

                project.ProjectOriginalSizeBytes = await GetFullSizeProjectByProjectIdAsync(project.ProjectId);

                await _context.SaveChangesAsync();
            }
        }

        public async Task SaveLinks(string[] links, ObjectType objectType, string projectId)
        {
            if (links != null && links.Length > 0 && !string.IsNullOrEmpty(projectId))
            {
                foreach (string link in links)
                {
                    //сохраняем информацию об объекте в базу данных
                    ObjectYouTube @object = new ObjectYouTube
                    {
                        PathFileOnServer = link,
                        ObjectType = objectType,
                        Project = _context.Projects
                        .FirstOrDefault(p => p.ProjectId == int.Parse(projectId))
                    };

                    _context.Objects.Add(@object);
                }

                await _context.SaveChangesAsync();
            }
        }

        public async Task<bool> IsFileExistsAsync(string relativePath)
        {
            return await Task.Factory.StartNew(() => File.Exists(_appEnvironment.WebRootPath + relativePath));
        }

        public async Task<bool> IsDirectoryExistsAsync(string relativePath)
        {
            return await Task.Factory.StartNew(() => Directory.Exists(_appEnvironment.WebRootPath + relativePath));
        }

        public async Task<DateTime> GetLastModifiedFile(string relativePath)
        {
            return await Task.Factory.StartNew(() =>
            File.GetLastWriteTime(_appEnvironment.WebRootPath + relativePath));
        }

        //Get full size in bytes
        public async Task<ulong> GetFullSizeProjectByProjectIdAsync(int id)
        {
            _isOriginalLevel = false;
            char pathSep = Path.DirectorySeparatorChar;
            DirectoryInfo di = new DirectoryInfo(_appEnvironment.WebRootPath + $"{pathSep}content{pathSep}{id}");

            return await Task.Factory.StartNew(() => DirSize(di));
        }

        //Get full size in bytes
        public ulong GetFullSizeProjectByProjectId(int id)
        {
            _isOriginalLevel = false;
            char pathSep = Path.DirectorySeparatorChar;
            DirectoryInfo di = new DirectoryInfo(_appEnvironment.WebRootPath + $"{pathSep}content{pathSep}{id}");

            return DirSize(di);
        }

        private ulong DirSize(DirectoryInfo d)
        {
            ulong size = 0;
            // Add file sizes.
            FileInfo[] fis = d.GetFiles();
            foreach (FileInfo fi in fis)
            {
                size += (ulong)fi.Length;
            }

            if(_isOriginalLevel == true)
            {
                return size;
            }

            // Add subdirectory sizes.
            DirectoryInfo[] dis = d.GetDirectories();

            foreach (DirectoryInfo di in dis)
            {
                char sep = Path.DirectorySeparatorChar;
                string pattern = $@"\/content/\d/[{(int)ObjectType.Room}{(int)ObjectType.Photo}{(int)ObjectType.Plan}{(int)ObjectType.ProjectIcon}]+$";
                Uri uri = new Uri(di.FullName);
                string convertedUri = uri.AbsoluteUri;

                _isOriginalLevel = Regex.IsMatch(convertedUri, pattern);

                size += DirSize(di);
            }
            return size;
        }

        public string NormalizePath(string path)
        {
            return Path.GetFullPath(new Uri(path).LocalPath)
                       .TrimEnd(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar)
                       .ToUpperInvariant();
        }

        public async Task DeleteFileAsync(string relativePath, bool deletePreview = false)
        {
            bool isFileExists = await IsFileExistsAsync(relativePath);

            if (isFileExists)
            {
                await Task.Factory.StartNew(() => File.Delete(_appEnvironment.WebRootPath + relativePath));

                if(deletePreview)
                {
                    await DeletePreviewFiles(relativePath);
                }
            }
        }

        private async Task DeletePreviewFiles(string relativePath)
        {
            string extension = Path.GetExtension(relativePath);

            string planPreviewRelativePath = Path.ChangeExtension(relativePath, null)
                + InfoPreviewImage.PreviewPlanPostfix + extension;

            bool isExistPlanPreview = await IsFileExistsAsync(planPreviewRelativePath);

            if (isExistPlanPreview)
            {
                await Task.Factory.StartNew(() => File.Delete(_appEnvironment.WebRootPath + planPreviewRelativePath));
            }

            string roomPreviewRelativePath = Path.ChangeExtension(relativePath, null)
              + InfoPreviewImage.PreviewRoomPostfix + extension;

            bool isExistRoomPreview = await IsFileExistsAsync(roomPreviewRelativePath);

            if (isExistRoomPreview)
            {
                await Task.Factory.StartNew(() => File.Delete(_appEnvironment.WebRootPath + roomPreviewRelativePath));
            }
        }

        public async Task DeleteDirectoryAsync(string relativePath)
        {
            bool IsDirectoryExists = await IsDirectoryExistsAsync(relativePath);

            if (IsDirectoryExists)
            {
                await Task.Factory.StartNew(() => Directory.Delete(_appEnvironment.WebRootPath + relativePath, true));
            }
        }

        public async Task DeleteFilesAsync(IEnumerable<string> relativePaths, bool deletePreview = false)
        {
            foreach (var path in relativePaths)
            {
                await DeleteFileAsync(path, deletePreview);
            }
        }

        public async Task<IEnumerable<string>> GetAllSameFilesAsync(string relativeFilePath)
        {
            string relativeDirectory = Path.GetDirectoryName(relativeFilePath);
            bool IsDirectoryExists = await IsDirectoryExistsAsync(relativeDirectory);

            string path = _appEnvironment.WebRootPath + relativeFilePath;

            string fullDirectoryPath = Path.GetDirectoryName(path);
            string fileName = Path.GetFileName(path);
            
            if (IsDirectoryExists)
            {
                return await Task
                    .Factory
                    .StartNew(() => Directory.GetFiles(fullDirectoryPath, "*" + fileName, SearchOption.AllDirectories)
                    .Select(p =>
                    {
                        int pos = p.IndexOf("content");
                        return Path.DirectorySeparatorChar + p.Remove(0, pos);
                    }));
            }

            return new string[] { };
        }

        public async Task UpdatePlan(IFormFile plan,
            string projectId,
            int planId)
        {
            ObjectPlan planInDb = _context
                .ObjectPlans
                .SingleOrDefault(i => i.ObjectId == planId);

            char pathSep = Path.DirectorySeparatorChar;

            if (planInDb == null)
            {
                throw new ArgumentException("Plan was not found");
            }

            // путь к папке
            string relativePath = $"{pathSep}content{pathSep}{projectId}{pathSep}{(int)ObjectType.Plan}{pathSep}";
            string path = _appEnvironment.WebRootPath + relativePath;

            if (!Directory.Exists(path))
            {
                throw new DirectoryNotFoundException($"The path {relativePath} was not found");
            }

            List<string> deletePaths = new List<string>();

            var optimizeImagesPaths = await GetAllSameFilesAsync(planInDb.PathFileOnServer);

            if (optimizeImagesPaths.Count() > 0)
            {
                deletePaths.AddRange(optimizeImagesPaths);
            }

            //delete plan from file system
            await DeleteFilesAsync(deletePaths, deletePreview: true);
            string uniqueName = Guid.NewGuid().ToString();

            string extension = Path.GetExtension(plan.FileName);

            if (String.IsNullOrEmpty(extension))
            {
                extension = ".jpg";
            }

            string newFullPath = path + uniqueName + extension;
            string relativePathForAccess = (relativePath + uniqueName + extension);

            // сохраняем файл в папку в каталоге wwwroot
            using (var fileStream = new FileStream(newFullPath, FileMode.Create))
            {
                await plan.CopyToAsync(fileStream);
            }

            //сохраняем информацию об объекте в базу данных
            planInDb.PathFileOnServer = relativePathForAccess;

            Project project = await _context
                .Projects
                .SingleOrDefaultAsync(p => p.ProjectId == planInDb.ProjectId);

            if(project != null)
            {
                project.ProjectOriginalSizeBytes = await GetFullSizeProjectByProjectIdAsync(project.ProjectId);
            }

            await _context.SaveChangesAsync();
        }

        public async Task UpdateRoom(IFormFile room, string projectId, int roomImageId)
        {
            RoomImage roomImageInDb = _context
                .RoomImages
                .Include(e => e.DayTime)
                .Include(e => e.Object)
                .SingleOrDefault(i => i.RoomImageId == roomImageId);

            char pathSep = Path.DirectorySeparatorChar;

            if (roomImageInDb == null)
            {
                throw new ArgumentException("Room was not found");
            }

            // путь к папке
            string relativePath = $"{pathSep}content{pathSep}{projectId}{pathSep}{(int)ObjectType.Room}{pathSep}";
            string path = _appEnvironment.WebRootPath + relativePath;

            if (!Directory.Exists(path))
            {
                throw new DirectoryNotFoundException($"The path {relativePath} was not found");
            }

            List<string> deletePaths = new List<string>();

            var optimizeImagesPaths = await GetAllSameFilesAsync(roomImageInDb.ImageUrl);

            if (optimizeImagesPaths.Count() > 0)
            {
                deletePaths.AddRange(optimizeImagesPaths);
            }

            //delete room images 360 from file system
            await DeleteFilesAsync(deletePaths, deletePreview: true);

            string uniqueName = Guid.NewGuid().ToString();
            string extension = Path.GetExtension(room.FileName);

            if (String.IsNullOrEmpty(extension))
            {
                extension = ".jpg";
            }

            string newFullPath = path + uniqueName + extension;
            string relativePathForAccess = (relativePath + uniqueName + extension);

            // сохраняем файл в папку в каталоге wwwroot
            using (var fileStream = new FileStream(newFullPath, FileMode.Create))
            {
                await room.CopyToAsync(fileStream);
            }

            //сохраняем информацию об объекте в базу данных
            roomImageInDb.ImageUrl = relativePathForAccess;

            Project project = await _context
              .Projects
              .SingleOrDefaultAsync(p => p.ProjectId == roomImageInDb.Object.ProjectId);

            if (project != null)
            {
                project.ProjectOriginalSizeBytes = await GetFullSizeProjectByProjectIdAsync(project.ProjectId);
            }

            await _context.SaveChangesAsync();
        }
    }
}
