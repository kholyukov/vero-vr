﻿using System.Net;

namespace VeroVrCore.WebApi.Services
{
    public class YouTubeService
    {
        public bool IsYoutubeLink(string link)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(link);
                request.Method = "HEAD";

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    return response.ResponseUri.ToString().Contains("youtube.com");
                }
            }
            catch
            {
                return false;
            }
        }
    }
}
