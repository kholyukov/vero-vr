﻿using FreeImageAPI;

namespace VeroVrCore.WebApi.Services
{
    public class ImageService
    {
        public void ResizeImageByHeight(string inputPath, string outputPath, int size)
        {
            using (var original = FreeImageBitmap.FromFile(inputPath))
            {
                Resize(inputPath, outputPath, size, original);
            }
        }

        public void ResizeImageByHeight(FreeImageBitmap image, string inputPath, string outputPath, int size)
        {
            Resize(inputPath, outputPath, size, image);
        }

        private void Resize(string inputPath, string outputPath, int size, FreeImageBitmap original)
        {
            if(original.Height > size || original.Width > size)
            {
                int width, height;

                if (original.Width < original.Height)
                {
                    width = size;
                    height = original.Height * size / original.Width;
                }
                else
                {
                    width = original.Width * size / original.Height;
                    height = size;
                }

                using (var resized = new FreeImageBitmap(original, width, height))
                {
                    if (inputPath == outputPath)
                    {
                        original.Dispose();
                    }

                    resized.Save(outputPath);
                }
            }
        }

        public FreeImageBitmap GetImage(string inputPath)
        {
            return FreeImageBitmap.FromFile(inputPath);
        }

        public void GetSize(string inputPath, out int height, out int width)
        {
            using (var original = FreeImageBitmap.FromFile(inputPath))
            {
                height = original.Height;
                width = original.Width;
            }
        }
    }
}
