﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using VeroVrCore.Common.Security;

namespace VeroVrCore.WebApi.Services
{
    public class NotificationService
    {
        private IConfiguration _configuration;

        public NotificationService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public async Task<bool> NotifyAsync(string title, string text,
          string[] emails)
        {
            try
            {
                // Get the server key from FCM console
                string serverKey = _configuration["Firebase:ServerKey"].ToString();
                string sendToAllValue = _configuration["Firebase:SendAllValue"].ToString();
                string notificationUrl = _configuration["Firebase:NotificationUrl"].ToString();

                if (String.IsNullOrEmpty(serverKey))
                {
                    throw new JsonException("Server key for firebase wasn't found.");
                }

                if (String.IsNullOrEmpty(sendToAllValue))
                {
                    throw new JsonException("Topic value for send notification all users in firebase wasn't found.");
                }

                if (String.IsNullOrEmpty(notificationUrl))
                {
                    throw new JsonException("Notification url value for send notification via firebase wasn't found.");
                }

                if(!Uri.IsWellFormedUriString(notificationUrl, UriKind.Absolute))
                {
                    throw new JsonException("Notification url is not valid.");
                }

                // Get the sender id from FCM console
                //var senderId = string.Format("id={0}", "Your sender id - use app config");

                object data = new object();

                if (emails == null || emails.Length == 0)
                {
                    data = new
                    {
                        notification = new { title, text },
                        to = sendToAllValue
                    };
                }
                else
                {
                    StringBuilder conditionValue = new StringBuilder();
                    string operatorOr = " || ";
                    string quoteMark = "'";
                    string inTopics = " in topics";

                    int iteration = emails.Length - 1;

                    for (int i = 0; i < iteration; i++)
                    {
                        string encryptedEmail = Encryption.Sha256Hash(emails[i], realy: true);
                        conditionValue.Append(quoteMark);
                        conditionValue.Append(encryptedEmail);
                        conditionValue.Append(quoteMark);
                        conditionValue.Append(inTopics);
                        conditionValue.Append(operatorOr);
                    }

                    string lastEmail = emails[emails.Length - 1];
                    string encryptedlastEmail = Encryption.Sha256Hash(lastEmail, realy: true);

                    conditionValue.Append(quoteMark);
                    conditionValue.Append(encryptedlastEmail);
                    conditionValue.Append(quoteMark);
                    conditionValue.Append(inTopics);

                    string prepareCondition = conditionValue.ToString();

                    data = new
                    {
                        notification = new { title, text },
                        condition = prepareCondition // Recipient device token
                    };
                }

                // Using Newtonsoft.Json
                var jsonBody = JsonConvert.SerializeObject(data);

                using (var httpRequest = new HttpRequestMessage(HttpMethod.Post, notificationUrl))
                {
                    httpRequest.Headers.TryAddWithoutValidation("Authorization", serverKey);
                    //httpRequest.Headers.TryAddWithoutValidation("Sender", senderId);
                    httpRequest.Content = new StringContent(jsonBody, Encoding.UTF8, "application/json");

                    using (var httpClient = new HttpClient())
                    {
                        var result = await httpClient.SendAsync(httpRequest);

                        if (result.IsSuccessStatusCode)
                        {
                            return true;
                        }
                        else
                        {
                            // Use result.StatusCode to handle failure
                            // Your custom error handler here
                            //_logger.LogError($"Error sending notification. Status Code: {result.StatusCode}");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //_logger.LogError($"Exception thrown in Notify Service: {ex}");
            }

            return false;
        }
    }
}
