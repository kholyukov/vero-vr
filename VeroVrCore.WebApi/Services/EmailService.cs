﻿using MailKit;
using MailKit.Net.Imap;
using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using MimeKit;
using MimeKit.Utils;
using System;
using System.Threading.Tasks;
using VeroVrCore.WebApi.LogProvider;
using VeroVrCore.WebApi.LogProviders;

namespace VeroVrCore.WebApi.Services
{
    public class EmailService
    {
        private IConfiguration _configuration;
        private readonly ILogger<EmailService> _logger;

        public EmailService(IConfiguration configuration, ILogger<EmailService> logger)
        {
            _configuration = configuration;
            _logger = logger;
        }

        public async Task SendEmailAsync(string email, string subject, string message)
        {
            string emailAdmin = _configuration["EmailAdministration:Email"].ToString();
            string passwordAdmin = _configuration["EmailAdministration:Password"].ToString();
            string name = _configuration["EmailAdministration:Name"].ToString();

            var emailMessage = new MimeMessage();
            var mailFrom = new MailboxAddress(name, emailAdmin);

            emailMessage.From.Add(mailFrom);
            emailMessage.To.Add(new MailboxAddress("", email));
            emailMessage.Subject = subject;
            emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html)
            {
                Text = message
            };

            string host = _configuration["SmtpClientSettings:Host"].ToString();
            int port = Int32.Parse(_configuration["SmtpClientSettings:Port"].ToString());
            bool isUseSsl = Boolean.Parse(_configuration["SmtpClientSettings:UseSsl"].ToString());

            if (!host.Contains("gmail"))
            {
                emailMessage.ResentReplyTo.Add(mailFrom);
                emailMessage.ResentMessageId = MimeUtils.GenerateMessageId();
                emailMessage.ResentDate = DateTimeOffset.Now;
            }

            using (var client = new SmtpClient())
            {
                await client.ConnectAsync(host: host, port: port, useSsl: isUseSsl);
                // Note: since we don't have an OAuth2 token, disable
                // the XOAUTH2 authentication mechanism.
                // Must be removed for Gmail SMTP
                if (host.Contains("gmail"))
                {
                    client.AuthenticationMechanisms.Remove("XOAUTH2");
                }

                await client.AuthenticateAsync(emailAdmin, passwordAdmin);
                await client.SendAsync(emailMessage);

                await client.DisconnectAsync(true);
            }

            if (!host.Contains("gmail"))
            {
                string hostImap = _configuration["ImapClientSettings:Host"].ToString();
                int portImap = Int32.Parse(_configuration["ImapClientSettings:Port"].ToString());
                bool isUseSslImap = Boolean.Parse(_configuration["ImapClientSettings:UseSsl"].ToString());
                string sentMessagesFolderName = _configuration["ImapClientSettings:SentMessagesFolder"].ToString();

                using (var Imapclient = new ImapClient())
                {
                    try
                    {
                        SecureSocketOptions secureImap = isUseSslImap ? SecureSocketOptions.Auto : SecureSocketOptions.None;

                        await Imapclient.ConnectAsync(hostImap, portImap, secureImap);

                        // Note: since we don't have an OAuth2 token, disable
                        // the XOAUTH2 authentication mechanism.
                        Imapclient.AuthenticationMechanisms.Remove("XOAUTH2");

                        // MailKit uses by default ntlm authentication
                        await Imapclient.AuthenticateAsync(emailAdmin, passwordAdmin);

                        var personal = Imapclient.GetFolder(Imapclient.PersonalNamespaces[0]);

                        var folders = personal.GetSubfolders(false);

                        foreach (var folder in folders)
                        {
                            if (folder.Name.Trim().ToLower() == sentMessagesFolderName.Trim().ToLower())
                            {
                                folder.Open(FolderAccess.ReadWrite);
                                folder.Append(emailMessage, MessageFlags.None, DateTimeOffset.Now);
                                break;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError((int)LoggingEvents.ERROR,
                            LogMessageFormatter.GetErrorMessage("IMAPException has occured: ", ex));
                    }
                    finally
                    {
                        Imapclient?.Disconnect(true);
                    }
                }
            }
        }
    }
}
