﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VeroVrCore.WebApi
{
    public class AuthOptions
    {
        public const string ISSUER = "VeroVR"; // издатель токена

        public const string AUDIENCE = "https://veroreality.app"; // потребитель токена

        public const int LIFETIME = 720; // время жизни токена - 720 минут (12 часов)

        const string KEY = "{CDD4B999-EE70-4017-90AB-2A5EBCAA5F3E}";   // ключ для шифрации

        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(KEY));
        }
    }
}
