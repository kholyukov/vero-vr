﻿using System;

namespace VeroVrCore.WebApi.LogProviders
{
    public static class LogMessageFormatter
    {
        public static string GetErrorMessage(string message, Exception ex)
        {
            string logMessage = $"{message}\n Exception message: {ex.Message}";

            if (!String.IsNullOrEmpty(ex?.InnerException?.Message))
            {
                logMessage += $"\n Inner exception: {ex.InnerException.Message}";
            }

            if (!String.IsNullOrEmpty(ex.StackTrace))
            {
                logMessage += $"\n Stack trace: {ex.StackTrace}\n";
            }

            return logMessage;
        }
    }
}
