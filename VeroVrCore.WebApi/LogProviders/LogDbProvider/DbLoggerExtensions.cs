﻿using Microsoft.Extensions.Logging;
using System;
using VeroVrCore.DataLayer.EntityFramework;

namespace VeroVrCore.WebApi.LogProvider
{
    public static class DBLoggerExtensions
    {
        public static ILoggerFactory AddContext(this ILoggerFactory factory, DbType dbType,
      Func<string, LogLevel, bool> filter = null, string connectionStr = null)
        {
            factory.AddProvider(new DBLoggerProvider(filter, connectionStr, dbType));
            return factory;
        }

        public static ILoggerFactory AddContext(this ILoggerFactory factory, LogLevel minLevel, DbType dbType, string connectionStr)
        {
            return AddContext(
                factory, dbType,
                (_, logLevel) => logLevel >= minLevel, connectionStr);
        }
    }
}
