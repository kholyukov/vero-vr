﻿using Microsoft.Extensions.Logging;
using System;
using VeroVrCore.Common.Models;
using VeroVrCore.DataLayer.ADO.NET;
using VeroVrCore.DataLayer.EntityFramework;

namespace VeroVrCore.WebApi.LogProvider
{
    public class DBLogger : ILogger
    {
        private string _categoryName;
        private readonly Func<string, LogLevel, bool> _filter;
        private int MessageMaxLength = 4000;
        private LogHelper _helper;

        public DBLogger(string categoryName, Func<string, LogLevel, bool> filter, string connectionString, DbType dbType)
        {
            _categoryName = categoryName;
            this._filter = filter;
            _helper = new LogHelper(connectionString, dbType);
        }

        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state,
            Exception exception, Func<TState, Exception, string> formatter)
        {
            if (!IsEnabled(logLevel))
            {
                return;
            }
            if (formatter == null)
            {
                throw new ArgumentNullException(nameof(formatter));
            }

            var message = formatter(state, exception);

            if (string.IsNullOrEmpty(message))
            {
                return;
            }

            if (exception != null)
            {
                message += "\n" + exception.ToString();
            }

            message = message.Length > MessageMaxLength ? message.Substring(0, MessageMaxLength) : message;

            LogEvent logEvent = new LogEvent
            {
                Message = message,
                EventId = eventId.Id,
                LogLevel = logLevel.ToString(),
                CreatedTime = DateTime.Now
            };

           _helper.InsertLog(logEvent);
        }

        public bool IsEnabled(LogLevel logLevel)
        {
            return (_filter == null || _filter(_categoryName, logLevel));
        }

        public IDisposable BeginScope<TState>(TState state)
        {
            return null;
        }
    }
}
