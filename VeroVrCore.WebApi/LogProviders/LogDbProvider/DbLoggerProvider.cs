﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using VeroVrCore.Common.Models;
using VeroVrCore.DataLayer.EntityFramework;

namespace VeroVrCore.WebApi.LogProvider
{
    public class DBLoggerProvider : ILoggerProvider
    {
        private readonly Func<string, LogLevel, bool> _filter;
        private string _connectionString;
        private readonly DbType dbType;

        public DBLoggerProvider(Func<string, LogLevel, bool> filter, string connectionStr, DbType dbType)
        {
            this._filter = filter;
            _connectionString = connectionStr;
            this.dbType = dbType;
        }

        public ILogger CreateLogger(string categoryName)
        {
            return new DBLogger(categoryName, _filter, _connectionString, dbType);
        }

        public void Dispose()
        {

        }
    }
}
