﻿namespace VeroVrCore.WebApi.LogProvider
{
    // 
    // Summary: 
    // Defines logging severity levels. 
    public enum LoggingEvents
    {
        ERROR = 1000,
        GENERATE_ITEMS = 1001,
        GET_LIST_ITEMS,
        GET_ITEM,
        INSERT_ITEM,
        UPDATE_ITEM,
        DELETE_ITEM
    }
}
