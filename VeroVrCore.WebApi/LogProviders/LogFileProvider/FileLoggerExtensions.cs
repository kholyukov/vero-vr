﻿using Microsoft.Extensions.Logging;
using System;

namespace VeroVrCore.WebApi.LogProviders.LogFileProvider
{
    public static class FileLoggerExtensions
    {
        public static ILoggerFactory AddFile(this ILoggerFactory factory, string path, Func<string, LogLevel, bool> filter = null)
        {
            factory.AddProvider(new FileLoggerProvider(path, filter));
            return factory;
        }

        public static ILoggerFactory AddFile(this ILoggerFactory factory, LogLevel minLevel, string path)
        {
            return AddFile(
                factory, path,
                (_, logLevel) => logLevel >= minLevel);
        }
    }
}
