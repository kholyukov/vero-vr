﻿using Microsoft.Extensions.Logging;
using System;

namespace VeroVrCore.WebApi.LogProviders.LogFileProvider
{
    public class FileLoggerProvider : ILoggerProvider
    {
        private string path;
        private readonly Func<string, LogLevel, bool> _filter;

        public FileLoggerProvider(string _path, Func<string, LogLevel, bool> filter)
        {
            path = _path;
            this._filter = filter;
        }
        public ILogger CreateLogger(string categoryName)
        {
            return new FileLogger(categoryName, path, _filter);
        }

        public void Dispose()
        {
        }
    }
}
