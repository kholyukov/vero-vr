﻿using Microsoft.Extensions.Logging;
using System;
using System.IO;

namespace VeroVrCore.WebApi.LogProviders.LogFileProvider
{
    public class FileLogger : ILogger
    {
        private readonly string _categoryName;
        private string filePath;
        private readonly Func<string, LogLevel, bool> _filter;
        private object _lock = new object();
        public FileLogger(string categoryName, string path, Func<string, LogLevel, bool> filter)
        {
            this._categoryName = categoryName;
            filePath = path;
            this._filter = filter;
        }
        public IDisposable BeginScope<TState>(TState state)
        {
            return null;
        }

        public bool IsEnabled(LogLevel logLevel)
        {
            return (_filter == null || _filter(_categoryName, logLevel));
        }

        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
        {
            if (!IsEnabled(logLevel))
            {
                return;
            }

            if (formatter == null)
            {
                throw new ArgumentNullException(nameof(formatter));
            }

            var message = formatter(state, exception);

            if (string.IsNullOrEmpty(message))
            {
                return;
            }

            if (formatter != null)
            {
                lock (_lock)
                {
                    string logMessage = $"Error: " + formatter(state, exception) + $"Created time:{DateTime.Now}" + Environment.NewLine + Environment.NewLine;
                    File.AppendAllText(filePath, logMessage);
                }
            }
        }
    }
}
