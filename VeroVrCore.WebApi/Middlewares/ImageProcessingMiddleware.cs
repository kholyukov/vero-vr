﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;
using System;
using System.IO;
using System.Threading.Tasks;
using VeroVrCore.Common.Infrastructure;
using VeroVrCore.Common.Security;
using VeroVrCore.WebApi.Services;

namespace VeroVrCore.WebApi.Middlewares
{
    public class ImageProcessingMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ImageService _imageService;
        private readonly IHostingEnvironment _environment;
        private object _lock = new object();

        public ImageProcessingMiddleware(RequestDelegate next,
            ImageService imageService,
            IHostingEnvironment environment)
        {
            this._next = next;
            _imageService = imageService;
            this._environment = environment;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            StringValues token;
            bool isExistTokenParam = context.Request.Query.TryGetValue("token", out token);

            string routeWithoutToken = $"{context.Request.Scheme}://{context.Request.Host.Value}{context.Request.Path.Value}";

            Uri uriWithoutToken = new Uri(routeWithoutToken);
            string localPath = uriWithoutToken.LocalPath;
            string pathFolderWithoutToken = Path.GetDirectoryName(localPath);

            string extension = Path.GetExtension(localPath);

            bool isImage = String.Equals(extension, ".jpg", StringComparison.OrdinalIgnoreCase)
                || String.Equals(extension, ".jpeg", StringComparison.OrdinalIgnoreCase)
                || String.Equals(extension, ".png", StringComparison.OrdinalIgnoreCase);

            bool isNotFound = false;

            if (isImage)
            {
                bool isDefaultAvatar = pathFolderWithoutToken.EndsWith("avatar");

                char sep = Path.DirectorySeparatorChar;

                bool isCustomAvatar = pathFolderWithoutToken.Contains($"{sep}content{sep}user{sep}avatar{sep}");

                bool isRoomPreview = localPath.EndsWith(InfoPreviewImage.PreviewRoomPostfix + extension);
                bool isPlanPreview = localPath.EndsWith(InfoPreviewImage.PreviewPlanPostfix + extension);

                if (isExistTokenParam && isDefaultAvatar == false)
                {
                    string tokenDecrypted = "";

                    try
                    {
                        tokenDecrypted = Crypto.ActionDecrypt(token);
                        //Console.WriteLine("Height:" + tokenDecrypted);
                    }
                    catch
                    {
                        isNotFound = true;
                    }

                    if (!String.IsNullOrEmpty(tokenDecrypted))
                    {
                        int heightParam;
                        bool isNumber = int.TryParse(tokenDecrypted, out heightParam);

                        if (isNumber)
                        {
                            string route = $"{context.Request.Scheme}://{context.Request.Host.Value}{context.Request.Path.Value}?token={token}";

                            Uri uri = new Uri(route);

                            string path = _environment.WebRootPath + uri.LocalPath;

                            string pathWithoutQueryParams = uri.LocalPath;

                            string pathFolder = Path.GetDirectoryName(path);

                            string directoryWithImages = pathFolder + sep + heightParam;

                            string imagePath = directoryWithImages + sep + Path.GetFileName(path);

                            const int minHeight = 16;

                            if (isImage)
                            {
                                if (isPlanPreview)
                                {
                                    string originalPathPlan = path.Replace(InfoPreviewImage.PreviewPlanPostfix + extension, extension);

                                    bool isMake = false;

                                    lock (_lock)
                                    {
                                        isMake = TryMakePreview(originalPathPlan, heightParam, path, minHeight);
                                    }

                                    if(isMake == false)
                                    {
                                        byte[] imageFile;

                                        lock (_lock)
                                        {
                                            imageFile = File.ReadAllBytes(originalPathPlan);
                                        }

                                        context.Response.StatusCode = 200;
                                        context.Response.ContentType = MimeTypeMap.GetMimeType(extension);
                                        await context.Response.Body.WriteAsync(imageFile, 0, imageFile.Length);
                                        return;
                                    }
                                }
                                else if (isRoomPreview)
                                {
                                    string originalPathRoom = path.Replace(InfoPreviewImage.PreviewRoomPostfix + extension, extension);

                                    bool isMake = false;

                                    lock (_lock)
                                    {
                                        isMake = TryMakePreview(originalPathRoom, heightParam, path, minHeight);
                                    }
                                        
                                    if (isMake == false)
                                    {
                                        byte[] imageFile;

                                        lock (_lock)
                                        {
                                            imageFile = File.ReadAllBytes(originalPathRoom);
                                        }

                                        context.Response.StatusCode = 200;
                                        context.Response.ContentType = MimeTypeMap.GetMimeType(extension);
                                        await context.Response.Body.WriteAsync(imageFile, 0, imageFile.Length);
                                        return;
                                    }
                                }
                                else if (File.Exists(path))
                                {
                                    bool isExistImage = File.Exists(imagePath);

                                    if (!isExistImage)
                                    {
                                        lock (_lock)
                                        {
                                            using (var image = _imageService.GetImage(path))
                                            {
                                                if (heightParam >= minHeight && image.Height > heightParam)
                                                {
                                                    bool isExistDirectory = Directory.Exists(directoryWithImages);

                                                    if (isCustomAvatar && isExistDirectory)
                                                    {
                                                        Directory.Delete(directoryWithImages, recursive: true);
                                                        Directory.CreateDirectory(directoryWithImages);
                                                    }
                                                    else if (!isExistDirectory)
                                                    {
                                                        Directory.CreateDirectory(directoryWithImages);
                                                    }

                                                    _imageService.ResizeImageByHeight(path, imagePath, heightParam);
                                                }
                                            }
                                        }
                                    }

                                    if (File.Exists(imagePath))
                                    {
                                        byte[] imageFile;

                                        lock (_lock)
                                        {
                                            imageFile = File.ReadAllBytes(imagePath);
                                        }

                                        context.Response.StatusCode = 200;
                                        context.Response.ContentType = MimeTypeMap.GetMimeType(extension);
                                        await context.Response.Body.WriteAsync(imageFile, 0, imageFile.Length);
                                        return;
                                    }
                                    else if (File.Exists(path))
                                    {
                                        byte[] imageFile;

                                        lock (_lock)
                                        {
                                            imageFile = File.ReadAllBytes(path);
                                        }

                                        context.Response.StatusCode = 200;
                                        context.Response.ContentType = MimeTypeMap.GetMimeType(extension);
                                        await context.Response.Body.WriteAsync(imageFile, 0, imageFile.Length);
                                        return;
                                    }
                                    else
                                    {
                                        isNotFound = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if(isNotFound)
            {
                context.Response.StatusCode = 404;
            }

            await _next.Invoke(context);
        }

        private bool TryMakePreview(string originalPathRoom, int heightParam, string path, int minHeight)
        {
            if (File.Exists(originalPathRoom))
            {
                using (var image = _imageService.GetImage(originalPathRoom))
                {
                    if (heightParam >= minHeight && image.Height > heightParam)
                    {
                        bool isExistPreviewRoom = File.Exists(path);

                        if (isExistPreviewRoom == false)
                        {
                            _imageService.ResizeImageByHeight(originalPathRoom, path, heightParam);
                        }

                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            else
            {
                return false;
            }
        }
    }
    public static class ImageProcessingMiddlewareExtensions
    {
        public static IApplicationBuilder UseImageProcessing(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<ImageProcessingMiddleware>();
        }
    }
}
