﻿using System;
using System.IO;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using VeroVrCore.DataLayer.EntityFramework;
using VeroVrCore.DataLayer.EntityFramework.VeroVrCore.DataLayer.EntityFramework;

namespace VeroVrCore.WebApi
{
    public class Program
    {
        public static IConfigurationRoot Configuration { get; set; }

        public static void Main(string[] args)
        {
            var host = BuildWebHost(args);

            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
           .AddJsonFile("appsettings.json");

            Configuration = builder.Build();

            using (IServiceScope scope = host.Services.CreateScope())
            {
                IServiceProvider services = scope.ServiceProvider;

                try
                {
                    bool isUseMySql = false;

                    string dbTypeParam = Configuration["DbTypeSettings:DbType"].ToString();
                    DbType dbType = Enum.Parse<DbType>(dbTypeParam.Trim(), ignoreCase: true);

                    if(dbType == DbType.MySql)
                    {
                        isUseMySql = true;
                    }

                    VeroVrContext context = services.GetRequiredService<VeroVrContext>();
                    VeroVrDataInitializer.Initialize(context, isUseMySql);

                    VeroVrStatisticsContext statisticsContext = services.GetRequiredService<VeroVrStatisticsContext>();
                    VeroVrStatisticsDataInitializer.Initialize(statisticsContext, context, isUseMySql);
                }
                catch (Exception ex)
                {
                    ILogger<Program> logger = services.GetRequiredService<ILogger<Program>>();
                    logger.LogError(ex, "An error occurred while seeding the database.");
                }
            }

            host.Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .ConfigureLogging(logging => logging.SetMinimumLevel(LogLevel.Error))
                .UseKestrel(options =>
                {
                    options.Limits.MaxRequestBodySize = null;
                })
                .Build();
    }
}
