﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using VeroVrCore.Common.Models;
using VeroVrCore.DataLayer.EntityFramework;
using VeroVrCore.DataLayer.EntityFramework.VeroVrCore.DataLayer.EntityFramework;

namespace VeroVrCore.WebApi.Controllers
{
    public class StatisticsController : Controller
    {
        private VeroVrContext _veroVrContext;
        private VeroVrStatisticsContext _statisticsContext;
        private IHostingEnvironment _hostingEnvironment;
        private IConfiguration _configuration;
        private ILogger<StatisticsController> _logger;

        public StatisticsController(VeroVrContext veroVrContext,
            VeroVrStatisticsContext statisticsContext,
            IHostingEnvironment hostingEnvironment,
            IConfiguration configuration,
            ILogger<StatisticsController> logger)
        {
            _veroVrContext = veroVrContext;
            _statisticsContext = statisticsContext;
            _hostingEnvironment = hostingEnvironment;
            _configuration = configuration;
            _logger = logger;
        }

        [Authorize]
        [HttpPost("api/LogUserActivity")]
        public async Task LogUserActivity(string eventType, int? id)
        {
            if (String.IsNullOrEmpty(eventType))
            {
                Response.StatusCode = 400;
                var responseError = new
                {
                    error = "The event type was not specified.",
                };

                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonConvert.SerializeObject(responseError, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                return;
            }

            string userId = User.FindFirst("UserId")?.Value;

            if (userId == null)
            {
                Response.StatusCode = 404;
                var responseError = new
                {
                    error = "The user was not found."
                };

                // сериализация ответа
                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonConvert.SerializeObject(responseError, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                return;
            }

            int userIdNumber = int.Parse(userId);

            var user = await _veroVrContext
                .Users
                .AsNoTracking()
                .SingleOrDefaultAsync(u => u.UserId == userIdNumber);

            if (user == null)
            {
                Response.StatusCode = 404;
                var responseError = new
                {
                    error = "The user was not found."
                };

                // сериализация ответа
                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonConvert.SerializeObject(responseError, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                return;
            }

            if (user.UserRole != UserRole.Customer)
            {
                Response.StatusCode = 200;

                var responseForNotCustomer = new
                {
                    succeeded = true,
                    error = string.Empty,
                };

                // сериализация ответа
                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonConvert.SerializeObject(responseForNotCustomer, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                return;
            }

            UserActivityType type;
            bool isParsed = Enum.TryParse(eventType, out type);

            if (isParsed == false)
            {
                Response.StatusCode = 400;
                var responseError = new
                {
                    error = "Unknown type of user activity.",
                };

                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonConvert.SerializeObject(responseError, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                return;
            }

            if (type == UserActivityType.ProjectDescriptionView)
            {
                if (id == null)
                {
                    Response.StatusCode = 400;
                    var responseError = new
                    {
                        error = "The projectId was not specified.",
                    };

                    Response.ContentType = "application/json";
                    await Response.WriteAsync(JsonConvert.SerializeObject(responseError, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                    return;
                }
            }

            if (type == UserActivityType.Room360View ||
                type == UserActivityType.VrModeView)
            {
                if (id == null)
                {
                    Response.StatusCode = 400;
                    var responseError = new
                    {
                        error = "The roomImageId was not specified.",
                    };

                    Response.ContentType = "application/json";
                    await Response.WriteAsync(JsonConvert.SerializeObject(responseError, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                    return;
                }
            }

            if (type == UserActivityType.ArModelView ||
                type == UserActivityType.YouTubeVideoView ||
                type == UserActivityType.PlansView)
            {
                if (id == null)
                {
                    Response.StatusCode = 400;
                    var responseError = new
                    {
                        error = "The objectId was not specified.",
                    };

                    Response.ContentType = "application/json";
                    await Response.WriteAsync(JsonConvert.SerializeObject(responseError, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                    return;
                }
            }

            switch (type)
            {
                case UserActivityType.ArModelView:
                    _statisticsContext.UserInArActivities.Add(new UserInArActivity
                    {
                        ObjectId = id.Value,
                        EventDateStamp = DateTime.Now,
                        UserId = userIdNumber,
                        UserActivityType = UserActivityType.ArModelView
                    });
                    break;

                case UserActivityType.YouTubeVideoView:
                    _statisticsContext.UserInYouTubeVideoActivities.Add(new UserInYouTubeVideoActivity
                    {
                        ProjectId = id.Value,
                        EventDateStamp = DateTime.Now,
                        UserId = userIdNumber,
                        UserActivityType = UserActivityType.YouTubeVideoView
                    });
                    break;

                case UserActivityType.ProjectDescriptionView:
                    _statisticsContext.UserInProjectDescriptionActivities.Add(new UserInProjectDescriptionActivity
                    {
                        ProjectId = id.Value,
                        EventDateStamp = DateTime.Now,
                        UserId = userIdNumber,
                        UserActivityType = UserActivityType.ProjectDescriptionView
                    });

                    break;

                case UserActivityType.Room360View:
                    _statisticsContext.UserInRoom360Activities.Add(new UserInRoom360Activity
                    {
                        RoomImageId = id.Value,
                        EventDateStamp = DateTime.Now,
                        UserId = userIdNumber,
                        UserActivityType = UserActivityType.Room360View
                    });
                    break;

                case UserActivityType.VrModeView:
                    _statisticsContext.UserInRoomVrActivities.Add(new UserInRoomVrActivity
                    {
                        RoomImageId = id.Value,
                        EventDateStamp = DateTime.Now,
                        UserId = userIdNumber,
                        UserActivityType = UserActivityType.VrModeView
                    });
                    break;

                case UserActivityType.ListOfProjects:
                    _statisticsContext.UserActivities.Add(new UserActivity
                    {
                        EventDateStamp = DateTime.Now,
                        UserId = userIdNumber,
                        UserActivityType = UserActivityType.ListOfProjects
                    });
                    break;

                case UserActivityType.PlansView:
                    _statisticsContext.UserActivities.Add(new UserInPlansActivity
                    {
                        EventDateStamp = DateTime.Now,
                        UserId = userIdNumber,
                        ObjectId = id.Value,
                        UserActivityType = UserActivityType.PlansView
                    });
                    break;
            }

            await _statisticsContext.SaveChangesAsync();

            Response.StatusCode = 200;

            var response = new
            {
                succeeded = true,
                error = string.Empty,
            };

            // сериализация ответа
            Response.ContentType = "application/json";
            await Response.WriteAsync(JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented }));
        }

        [Authorize]
        [HttpPost("api/LogUserClickLinkDescriptionActivity")]
        public async Task LogUserClickLinkDescriptionActivity(int? projectId, string link)
        {
            if (projectId == null)
            {
                Response.StatusCode = 400;
                var responseError = new
                {
                    error = "The projectId was not specified"
                };

                // сериализация ответа
                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonConvert.SerializeObject(responseError, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                return;
            }

            if (String.IsNullOrEmpty(link))
            {
                Response.StatusCode = 400;
                var responseError = new
                {
                    error = "The link was not specified"
                };

                // сериализация ответа
                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonConvert.SerializeObject(responseError, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                return;
            }

            string userId = User.FindFirst("UserId")?.Value;

            int userIdValue = int.Parse(userId);

            var userIndb = await
            _veroVrContext
            .Users
            .SingleOrDefaultAsync(u => u.UserId == userIdValue);

            if (userIndb == null)
            {
                Response.StatusCode = 404;
                var responseError = new
                {
                    error = "The user was not found."
                };

                // сериализация ответа
                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonConvert.SerializeObject(responseError, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                return;
            }

            if (userIndb.UserRole == UserRole.Customer)
            {
                var user = await _veroVrContext
               .Users
               .AsNoTracking()
               .SingleOrDefaultAsync(u => u.UserId == userIdValue);

                if (user == null)
                {
                    Response.StatusCode = 404;
                    var responseError = new
                    {
                        error = "The user was not found."
                    };

                    // сериализация ответа
                    Response.ContentType = "application/json";
                    await Response.WriteAsync(JsonConvert.SerializeObject(responseError, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                    return;
                }

                if (user.UserRole != UserRole.Customer)
                {
                    Response.StatusCode = 200;

                    var responseCustomer = new
                    {
                        succeeded = true,
                        error = string.Empty,
                    };

                    // сериализация ответа
                    Response.ContentType = "application/json";
                    await Response.WriteAsync(JsonConvert.SerializeObject(responseCustomer, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                    return;
                }

                _statisticsContext
                .UserClickLinkInDescriptionActivities
                .Add(new UserClickLinkInDescriptionActivity
                {
                    UserId = userIdValue,
                    ProjectId = projectId.Value,
                    Link = link,
                    EventDateStamp = DateTime.Now
                });

                await _statisticsContext.SaveChangesAsync();
            }
            
            Response.StatusCode = 200;

            var response = new
            {
                succeeded = true,
                error = string.Empty,
            };

            // сериализация ответа
            Response.ContentType = "application/json";
            await Response.WriteAsync(JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented }));
            return;
        }

        [Authorize]
        [HttpPost("api/UpdateUserOnlineStatus")]
        public async Task UpdateUserOnlineStatus()
        {
            string userId = User.FindFirst("UserId")?.Value;

            int userIdValue = int.Parse(userId);

            var userIndb = await
              _veroVrContext
              .Users
              .SingleOrDefaultAsync(u => u.UserId == userIdValue);

            if(userIndb == null)
            {
                Response.StatusCode = 404;
                var responseError = new
                {
                    error = "The user was not found."
                };

                // сериализация ответа
                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonConvert.SerializeObject(responseError, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                return;
            }

            if(userIndb.UserRole == UserRole.Customer)
            {
                var user = await
                _veroVrContext
                .UserOnlineActivities
                .SingleOrDefaultAsync(u => u.UserId == userIdValue);


                if (user == null)
                {
                    _veroVrContext
                    .UserOnlineActivities
                    .Add(new UserOnlineActivity { UserId = userIdValue, EventDateStamp = DateTime.Now });
                }
                else
                {
                    user.EventDateStamp = DateTime.Now;
                }

                await _veroVrContext.SaveChangesAsync();
            }

            Response.StatusCode = 200;

            var response = new
            {
                succeeded = true,
                error = string.Empty,
            };

            // сериализация ответа
            Response.ContentType = "application/json";
            await Response.WriteAsync(JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented }));
        }

        [Authorize(Roles = "SuperAdmin, SuperManager, Manager, SuperManagerWithScript")]
        [HttpGet("api/GetUsersForMap")]
        public async Task GetUsersForMap([FromQuery] string filter)
        {
            if (String.IsNullOrEmpty(filter))
            {
                Response.StatusCode = 400;
                var responseError = new
                {
                    error = "The value for the filter was not specified",
                };

                // сериализация ответа
                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonConvert.SerializeObject(responseError, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                return;
            }

            Response.StatusCode = 200;

            var response = new
            {
                usersForMap = GetUsersForMapJson(filter),
                error = string.Empty,
            };

            // сериализация ответа
            Response.ContentType = "application/json";
            await Response.WriteAsync(JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented }));
        }

        [Authorize(Roles = "SuperAdmin, SuperManager, Manager, SuperManagerWithScript")]
        [HttpGet("api/GetNumberOfActiveUsersPerMonth")]
        public async Task GetNumberOfActiveUsersPerMonth([FromQuery] int? month,
            [FromQuery] int? year)
        {
            if (month == null || year == null)
            {
                Response.StatusCode = 400;
                var responseError = new
                {
                    error = "The month or year were not specified",
                };

                // сериализация ответа
                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonConvert.SerializeObject(responseError, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                return;
            }

            var monthForQuery = month.Value;
            var yearForQuery = year.Value;

            int count = await _statisticsContext
                      .UserLoginActivities
                      .AsNoTracking()
                      .Where(ua => ua.EventDateStamp.Month == monthForQuery
                      && ua.EventDateStamp.Year == yearForQuery)
                      .Select(u => u.UserId)
                      .Distinct()
                      .CountAsync();

            Response.StatusCode = 200;

            var response = new
            {
                numberOfActiveUsersPerMonth = count,
                error = string.Empty,
            };

            // сериализация ответа
            Response.ContentType = "application/json";
            await Response.WriteAsync(JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented }));
        }

        [Authorize(Roles = "SuperAdmin, SuperManager, Manager, SuperManagerWithScript")]
        [HttpGet("api/GetNumberOfReturnedUsersPerMonth")]
        public async Task GetNumberOfReturnedUsersPerMonth([FromQuery] int? month,
            [FromQuery] int? year)
        {
            if (month == null || year == null)
            {
                Response.StatusCode = 400;
                var responseError = new
                {
                    error = "The month or year were not specified",
                };

                // сериализация ответа
                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonConvert.SerializeObject(responseError, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                return;
            }

            DateTime date;
            try
            {
                date = new DateTime(year.Value, month.Value, 1);
            }
            catch
            {
                Response.StatusCode = 400;
                var responseError = new
                {
                    error = "Month or year has an incorrect format.",
                };

                // сериализация ответа
                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonConvert.SerializeObject(responseError, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                return;
            }

            var monthForQuery = month.Value;
            var yearForQuery = year.Value;

            var pastDate = date.AddMonths(-1);

            var userIdsReturned =
                 _statisticsContext
                      .UserLoginActivities
                      .AsNoTracking()
                      .Where(u =>
                      // не был в прошлом месяце
                      ((u.EventDateStamp.Month != pastDate.Month
                      && u.EventDateStamp.Year != pastDate.Year)
                      // или раньше прошлого месяца
                      || u.EventDateStamp < pastDate) &&
                      // но был зарегистрирован раньше заданного месяца
                     _veroVrContext
                                .Users
                                .AsNoTracking()
                                .Any(d => d.DateOfRegistration < date && d.UserId == u.UserId)
                     // и появился в заданном месяце
                     && u.EventDateStamp.Month == monthForQuery
                      && u.EventDateStamp.Year == yearForQuery)
                      .Distinct();

            int count = await userIdsReturned.CountAsync();

            Response.StatusCode = 200;

            var response = new
            {
                numberOfActiveUsersPerMonth = count,
                error = string.Empty,
            };

            // сериализация ответа
            Response.ContentType = "application/json";
            await Response.WriteAsync(JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented }));
        }

        [Authorize(Roles = "SuperAdmin, SuperManager, Manager, SuperManagerWithScript")]
        [HttpGet("api/ExportToExcelUsersWithProjects")]
        public async Task<IActionResult> ExportToExcelUsersWithProjects([FromQuery]int? projectId)
        {
            string sFileName = _configuration["ExportData:PathToTemplateExcelFile"];

            if (String.IsNullOrEmpty(sFileName))
            {
                var responseError = new
                {
                    error = "No path to the excel file",
                };

                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonConvert.SerializeObject(responseError, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                return NotFound();
            }

            string Url = string.Format("{0}://{1}/{2}", Request.Scheme, Request.Host, sFileName);
            string sWebRootFolder = _hostingEnvironment.WebRootPath;
            string pathToFile = sWebRootFolder + sFileName;

            if (System.IO.File.Exists(pathToFile) == false)
            {
                var responseError = new
                {
                    error = "Excel template file was not found.",
                };

                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonConvert.SerializeObject(responseError, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                return NotFound();
            }


            FileInfo file = new FileInfo(pathToFile);
            var memory = new MemoryStream();
            using (var fs = new FileStream(pathToFile, FileMode.Create, FileAccess.Write))
            {
                IWorkbook workbook;
                workbook = new XSSFWorkbook();
                ISheet excelSheet = workbook.CreateSheet("Clients and their projects");

                int rowCounter = 0;
                IRow row = excelSheet.CreateRow(rowCounter);

                row.CreateCell(0).SetCellValue("Name");
                row.CreateCell(1).SetCellValue("Email");
                row.CreateCell(2).SetCellValue("Phone number");
                row.CreateCell(3).SetCellValue("Facebook link");
                row.CreateCell(4).SetCellValue("Facebook likes");
                row.CreateCell(5).SetCellValue("Age range");
                row.CreateCell(6).SetCellValue("Occupation");
                row.CreateCell(7).SetCellValue("City");
                row.CreateCell(8).SetCellValue("Country");
                row.CreateCell(9).SetCellValue("Address");
                row.CreateCell(10).SetCellValue("Date of registration");
                row.CreateCell(11).SetCellValue("Device");
                row.CreateCell(12).SetCellValue("Device type");
                row.CreateCell(13).SetCellValue("Project name");
                row.CreateCell(14).SetCellValue("Project description");
                row.CreateCell(15).SetCellValue("Project code");

                IEnumerable<User> users = new List<User>();

                if (projectId != null)
                {
                    var projectAndRelatedUser =
                         await _veroVrContext
                         .Projects
                         .Include("UserProjects.User")
                         .AsNoTracking()
                         .SingleOrDefaultAsync(p => p.ProjectId == projectId);

                    if(projectAndRelatedUser != null)
                    {
                        users = projectAndRelatedUser
                                .UserProjects
                                .Select(u => u.User)
                                .Where(u => u.UserRole == UserRole.Customer);
                    }
                }
                else
                {
                    users =
                       _veroVrContext
                       .Users
                       .Where(u => u.UserRole == UserRole.Customer)
                       .Include("UserProjects.Project")
                       .AsNoTracking();
                }

                foreach (var user in users)
                {
                    var projects = user.UserProjects.Select(u => u.Project);

                    foreach (var project in projects)
                    {
                        string[] facebookLikes = user.FacebookLikes;

                        string facebookLikesEnumeration = "";

                        if(facebookLikes != null && facebookLikes.Length > 0)
                        {
                            if (facebookLikes.Length > 1)
                            {
                                facebookLikesEnumeration = String.Join(", ", facebookLikes);
                            }
                            else if (facebookLikes.Length == 1)
                            {
                                facebookLikesEnumeration = facebookLikes[0];
                            }
                        }
                       
                        row = excelSheet.CreateRow(++rowCounter);
                        row.CreateCell(0).SetCellValue(user.UserDisplayName);
                        row.CreateCell(1).SetCellValue(user.UserEmail);
                        row.CreateCell(2).SetCellValue(user.UserPhoneNumber);
                        row.CreateCell(3).SetCellValue(user.UserFacebook);
                        row.CreateCell(4).SetCellValue(facebookLikesEnumeration);
                        row.CreateCell(5).SetCellValue(user.UserAgeRange);
                        row.CreateCell(6).SetCellValue(user.Occupation);
                        row.CreateCell(7).SetCellValue(user.UserCity);
                        row.CreateCell(8).SetCellValue(user.UserCountry);
                        row.CreateCell(9).SetCellValue(user.UserAddress);
                        row.CreateCell(10).SetCellValue(user.DateOfRegistration.ToShortDateString());
                        row.CreateCell(11).SetCellValue(user.DeviceAtLastLogin);
                        row.CreateCell(12).SetCellValue(user.DeviceTypeAtLastLogin);
                        row.CreateCell(13).SetCellValue(project.ProjectName);
                        row.CreateCell(14).SetCellValue(project.ProjectDescription);
                        row.CreateCell(15).SetCellValue(project.ProjectCode);
                    }
                }

                workbook.Write(fs);
            }
            using (var stream = new FileStream(pathToFile, FileMode.Open))
            {
                await stream.CopyToAsync(memory);
            }
            memory.Position = 0;
            string fileNameToSave = _configuration["ExportData:FileNameExcelFile"];

            if (!String.IsNullOrEmpty(fileNameToSave))
            {
                fileNameToSave = $"{fileNameToSave}_{DateTime.Now.ToFileTime()}.xlsx";
            }
            else
            {
                fileNameToSave = $"[VeroVr] Users and projects assigned to them_{DateTime.Now.ToFileTime()}.xlsx";
            }

            ContentDisposition cd = new ContentDisposition
            {
                FileName = fileNameToSave
            };

            Response.Headers.Add("Content-Disposition", cd.ToString());
            //Response.Headers.Add("Access-Control-Expose-Headers", "Content-Disposition");

            return File(memory, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        }

        [Authorize(Roles = "SuperAdmin, SuperManager, Manager, SuperManagerWithScript")]
        [HttpGet("/api/GetCountManagers")]
        public async Task GetCountManagers()
        {
            int countManagers = await _veroVrContext
                .Users
                .AsNoTracking()
                .Where(u => u.UserRole == UserRole.Manager)
                .CountAsync();

            var response = new
            {
                count = countManagers,
                error = string.Empty
            };

            Response.ContentType = "application/json";
            await Response.WriteAsync(JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented }));
        }

        [Authorize(Roles = "SuperAdmin, SuperManager, Manager, SuperManagerWithScript")]
        [HttpGet("/api/GetCountSuperManagers")]
        public async Task GetCountSuperManagers()
        {
            int countSuperManagers = await _veroVrContext
                .Users
                .AsNoTracking()
                .Where(u => u.UserRole == UserRole.SuperManager)
                .CountAsync();

            var response = new
            {
                count = countSuperManagers,
                error = string.Empty
            };

            Response.ContentType = "application/json";
            await Response.WriteAsync(JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented }));
        }

        [Authorize(Roles = "SuperAdmin, SuperManager, Manager, SuperManagerWithScript")]
        [HttpGet("/api/GetCountCustomers")]
        public async Task GetCountCustomers()
        {
            int countCustomrers = await _veroVrContext
                .Users
                .AsNoTracking()
                .Where(u => u.UserRole == UserRole.Customer)
                .CountAsync();

            var response = new
            {
                count = countCustomrers,
                error = string.Empty
            };

            Response.ContentType = "application/json";
            await Response.WriteAsync(JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented }));
        }

        [Authorize(Roles = "SuperAdmin, SuperManager, Manager, SuperManagerWithScript")]
        [HttpGet("api/GetNumberOfRegisteredUsersPerMonth")]
        public async Task GetNumberOfRegisteredUsersPerMonth([FromQuery] int? month,
          [FromQuery] int? year)
        {
            int monthForQuery = DateTime.Now.Month;
            int yearForQuery = DateTime.Now.Year;

            if (month.HasValue && year.HasValue)
            {
                monthForQuery = month.Value;
                yearForQuery = year.Value;
            }

            int count = await _veroVrContext
                      .Users
                      .AsNoTracking()
                      .Where(u => u.DateOfRegistration.Month == monthForQuery
                      && u.DateOfRegistration.Year == yearForQuery
                      && u.UserRole == UserRole.Customer)
                      .CountAsync();

            Response.StatusCode = 200;

            var response = new
            {
                numberOfRegisteredUsersPerMonth = count,
                error = string.Empty,
            };

            // сериализация ответа
            Response.ContentType = "application/json";
            await Response.WriteAsync(JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented }));
        }

        [Authorize(Roles = "SuperAdmin, SuperManager, Manager, SuperManagerWithScript")]
        [HttpGet("api/GetNumberOfRegisteredUsersPerWeek")]
        public async Task GetNumberOfRegisteredUsersPerWeek()
        {
            var date = DateTime.Now.AddDays(-7);

            int count = await _veroVrContext
                      .Users
                      .AsNoTracking()
                      .Where(u => u.DateOfRegistration >= date
                      && u.UserRole == UserRole.Customer)
                      .CountAsync();

            Response.StatusCode = 200;

            var response = new
            {
                numberOfRegisteredUsersPerWeek = count,
                error = string.Empty,
            };

            // сериализация ответа
            Response.ContentType = "application/json";
            await Response.WriteAsync(JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented }));
        }

        [Authorize(Roles = "SuperAdmin, SuperManager, Manager, SuperManagerWithScript")]
        [HttpGet("api/GetNewUsersPerDay")]
        public async Task GetNewUsersPerDay([FromQuery] int? day,
         [FromQuery] int? month,
         [FromQuery] int? year)
        {
            int dayForQuery = DateTime.Now.Day;
            int monthForQuery = DateTime.Now.Month;
            int yearForQuery = DateTime.Now.Year;

            if (day.HasValue && month.HasValue && year.HasValue)
            {
                dayForQuery = day.Value;
                monthForQuery = month.Value;
                yearForQuery = year.Value;
            }

            var usersPerDay = _veroVrContext
                      .Users
                      .Include("UserProjects.Project")
                      .AsNoTracking()
                      .Where(u => u.DateOfRegistration.Day == dayForQuery
                      && u.DateOfRegistration.Month == monthForQuery
                      && u.DateOfRegistration.Year == yearForQuery
                      && u.UserRole == UserRole.Customer)
                      .ToList();

            Response.StatusCode = 200;

            var response = new
            {
                newUsersPerDay = GetUsersPerDayJson(usersPerDay),
                error = string.Empty,
            };

            // сериализация ответа
            Response.ContentType = "application/json";
            await Response.WriteAsync(JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented }));
        }

        [Authorize(Roles = "SuperAdmin, SuperManager, Manager, SuperManagerWithScript")]
        [HttpGet("api/GetNewUsersPerDayWithPagination")]
        public async Task GetNewUsersPerDayWithPagination([FromQuery] int? day,
         [FromQuery] int? month,
         [FromQuery] int? year,
         [FromQuery]int? pageSize,
         [FromQuery] int? page)
        {
            if (!pageSize.HasValue || !page.HasValue)
            {
                Response.StatusCode = 400;
                var responseError = new
                {
                    succeeded = false,
                    error = "Not all parameters have been defined."
                };

                // сериализация ответа
                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonConvert.SerializeObject(responseError, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                return;
            }

            int dayForQuery = DateTime.Now.Day;
            int monthForQuery = DateTime.Now.Month;
            int yearForQuery = DateTime.Now.Year;

            if (day.HasValue && month.HasValue && year.HasValue)
            {
                dayForQuery = day.Value;
                monthForQuery = month.Value;
                yearForQuery = year.Value;
            }

            var usersPerDay = _veroVrContext
                      .Users
                      .Include("UserProjects.Project")
                      .AsNoTracking()
                      .Where(u => u.DateOfRegistration.Day == dayForQuery
                      && u.DateOfRegistration.Month == monthForQuery
                      && u.DateOfRegistration.Year == yearForQuery
                      && u.UserRole == UserRole.Customer);

            int countNewUsers = usersPerDay.Count();

            usersPerDay = usersPerDay
                      .OrderBy(u => u.UserId)
                      .Skip((page.Value - 1) * pageSize.Value)
                      .Take(pageSize.Value);

            Response.StatusCode = 200;

            var response = new
            {
                countNewUsers = countNewUsers,
                newUsersPerDay = GetUsersPerDayJson(usersPerDay),
                error = string.Empty,
            };

            // сериализация ответа
            Response.ContentType = "application/json";
            await Response.WriteAsync(JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented }));
        }

        [Authorize(Roles = "SuperAdmin, SuperManager, Manager, SuperManagerWithScript")]
        [HttpGet("api/GetNewUsersPerLast24hours")]
        public async Task GetNewUsersPerLast24hours()
        {
            DateTime dateFrom = DateTime.Now.AddHours(-24);
            DateTime dateTo = DateTime.Now;

            var usersPerDay = _veroVrContext
                      .Users
                      .Include("UserProjects.Project")
                      .AsNoTracking()
                      .Where(u => u.DateOfRegistration >= dateFrom
                      && u.DateOfRegistration <= dateTo
                      && u.UserRole == UserRole.Customer)
                      .ToList();

            Response.StatusCode = 200;

            var response = new
            {
                //TODO:
                newUsersPerDay = GetUsersPerDayJson(usersPerDay),
                error = string.Empty,
            };

            // сериализация ответа
            Response.ContentType = "application/json";
            await Response.WriteAsync(JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented }));
        }

        [Authorize(Roles = "SuperAdmin, SuperManager, Manager, SuperManagerWithScript")]
        [HttpGet("api/GetNewUsersPerLast24hoursWithPagination")]
        public async Task GetNewUsersPerLast24hoursWithPagination([FromQuery]int? pageSize,
        [FromQuery] int? page)
        {
            if (!pageSize.HasValue || !page.HasValue)
            {
                Response.StatusCode = 400;
                var responseError = new
                {
                    succeeded = false,
                    error = "Not all parameters have been defined."
                };

                // сериализация ответа
                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonConvert.SerializeObject(responseError, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                return;
            }

            DateTime dateFrom = DateTime.Now.AddHours(-24);
            DateTime dateTo = DateTime.Now;

            var usersPerDay = _veroVrContext
                      .Users
                      .Include("UserProjects.Project")
                      .AsNoTracking()
                      .Where(u => u.DateOfRegistration >= dateFrom
                      && u.DateOfRegistration <= dateTo
                      && u.UserRole == UserRole.Customer);

            int countNewUsers = usersPerDay.Count();

            usersPerDay = usersPerDay
                      .OrderBy(u => u.UserId)
                      .Skip((page.Value - 1) * pageSize.Value)
                      .Take(pageSize.Value);

            Response.StatusCode = 200;

            var response = new
            {
                countNewUsers = countNewUsers,
                newUsersPerDay = GetUsersPerDayJson(usersPerDay),
                error = string.Empty,
            };

            // сериализация ответа
            Response.ContentType = "application/json";
            await Response.WriteAsync(JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented }));
        }

        [Authorize(Roles = "SuperAdmin, SuperManager, Manager, SuperManagerWithScript")]
        [HttpGet("api/GetAllUsersDevices")]
        public async Task GetAllUsersDevices()
        {
            IEnumerable<string> devices = await _statisticsContext
                                     .UserLoginActivities
                                     .AsNoTracking()
                                     .Select(u => u.Device)
                                     .Distinct()
                                     .ToListAsync();

            var response = new
            {
                userDevices = devices
            };

            Response.StatusCode = 200;

            // сериализация ответа
            Response.ContentType = "application/json";
            await Response.WriteAsync(JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented }));
        }

        [Authorize(Roles = "SuperAdmin, SuperManager, Manager, SuperManagerWithScript")]
        [HttpGet("api/GetListOfUsersOnline")]
        public async Task GetListOfUsersOnline()
        {
            DateTime dateOnline = DateTime.Now.AddSeconds(-25);
            DateTime maxDateForViewOneScene = DateTime.Now.AddHours(-1);

            const string placeListOfProjects = "List of projects";

            var usersOnline = _veroVrContext
                .Users
                .Include("UserProjects.Project")
                .AsNoTracking()
                .Where(u => _veroVrContext
                .UserOnlineActivities
                .AsNoTracking()
                .Any(e => e.EventDateStamp >= dateOnline
                && e.UserId == u.UserId) && u.UserRole == UserRole.Customer)
                .ToList();

            ICollection<object> usersOnlineJson = new List<object>();

            if (usersOnline.Count > 0)
            {
                var usersListOfProjectsActivities = _statisticsContext
                              .UserActivities
                              .AsNoTracking()
                              .Where(e => e.UserActivityType == UserActivityType.ListOfProjects &&
                               e.EventDateStamp >= maxDateForViewOneScene)
                              .ToList();

                var usersProjectDescriptionActivities = _statisticsContext
                       .UserInProjectDescriptionActivities
                       .AsNoTracking()
                       .Where(e => e.EventDateStamp >= maxDateForViewOneScene)
                       .ToList();

                //Trace.WriteLine(usersProjectDescriptionActivities.ToString());

                var usersInRoom360Activities = _statisticsContext
                     .UserInRoom360Activities
                     .AsNoTracking()
                     .Where(e => e.EventDateStamp >= maxDateForViewOneScene)
                     .ToList();

                var userInRoomVrActivities = _statisticsContext
                    .UserInRoomVrActivities
                    .AsNoTracking()
                    .Where(e => e.EventDateStamp >= maxDateForViewOneScene)
                    .ToList();

                var userInYouTubeVideoActivities = _statisticsContext
                   .UserInYouTubeVideoActivities
                   .AsNoTracking()
                   .Where(e => e.EventDateStamp >= maxDateForViewOneScene)
                   .ToList();

                var userInArActivities = _statisticsContext
                  .UserInArActivities
                  .AsNoTracking()
                  .Where(e => e.EventDateStamp >= maxDateForViewOneScene)
                  .ToList();

                var userInPlansActivities = _statisticsContext
                 .UserInPlansActivities
                 .AsNoTracking()
                 .Where(e => e.EventDateStamp >= maxDateForViewOneScene)
                 .ToList();

            foreach (var userOnline in usersOnline)
            {
                List<dynamic> placeActivities = new List<dynamic>();

                if (usersListOfProjectsActivities
                   .Select(u => u.UserId)
                   .Contains(userOnline.UserId))
                {
                    var prListOfProjectActivity =
                        usersListOfProjectsActivities
                        .OrderByDescending(u => u.EventDateStamp)
                        .First(u => u.UserId == userOnline.UserId);

                    placeActivities.Add(new
                    {
                        ProjectId = 0,
                        DateStampActivity = prListOfProjectActivity.EventDateStamp,
                        Place = placeListOfProjects
                    });
                }

                if (usersProjectDescriptionActivities
                    .Select(u => u.UserId)
                    .Contains(userOnline.UserId))
                {
                    var prDescriptionActivity =
                        usersProjectDescriptionActivities
                        .OrderByDescending(u => u.EventDateStamp)
                        .First(u => u.UserId == userOnline.UserId);

                    placeActivities.Add(new
                    {
                        ProjectId = prDescriptionActivity.ProjectId,
                        DateStampActivity = prDescriptionActivity.EventDateStamp,
                        Place = "Project description"
                    });
                }

                if (usersInRoom360Activities
                    .Select(u => u.UserId)
                    .Contains(userOnline.UserId))
                {
                    var activity360 = usersInRoom360Activities
                        .OrderByDescending(u => u.EventDateStamp)
                        .First(u => u.UserId == userOnline.UserId);

                    var roomImageIn360Activity = await _veroVrContext
                        .RoomImages
                        .Include(e => e.Object)
                        .AsNoTracking()
                        .SingleOrDefaultAsync(r => r.RoomImageId == activity360.RoomImageId);

                    if (roomImageIn360Activity != null)
                    {
                        placeActivities.Add(new
                        {
                            ProjectId = roomImageIn360Activity.Object.ProjectId,
                            DateStampActivity = activity360.EventDateStamp,
                            Place = roomImageIn360Activity.Object.ObjectName + " (Room 360)"
                        });
                    }
                }

                if (userInRoomVrActivities
                    .Select(u => u.UserId)
                    .Contains(userOnline.UserId))
                {
                    var activityVr = userInRoomVrActivities
                        .OrderByDescending(u => u.EventDateStamp)
                        .First(u => u.UserId == userOnline.UserId);

                    var roomImageInVrActivity = await _veroVrContext
                       .RoomImages
                       .Include(e => e.Object)
                       .AsNoTracking()
                       .SingleOrDefaultAsync(r => r.RoomImageId == activityVr.RoomImageId);

                    if (roomImageInVrActivity != null)
                    {
                        placeActivities.Add(new
                        {
                            ProjectId = roomImageInVrActivity.Object.ProjectId,
                            DateStampActivity = activityVr.EventDateStamp,
                            Place = roomImageInVrActivity.Object.ObjectName + " (Room VR)"
                        });
                    }
                }

                if (userInYouTubeVideoActivities
                    .Select(u => u.UserId)
                    .Contains(userOnline.UserId))
                {
                    var activityVideoActivity = userInYouTubeVideoActivities
                        .OrderByDescending(u => u.EventDateStamp)
                        .First(u => u.UserId == userOnline.UserId);

                    placeActivities.Add(new
                    {
                        ProjectId = activityVideoActivity.ProjectId,
                        DateStampActivity = activityVideoActivity.EventDateStamp,
                        Place = "YouTube video"
                    });
                }

                if (userInArActivities
                    .Select(u => u.UserId)
                    .Contains(userOnline.UserId))
                {
                    var activityAr = userInArActivities
                        .OrderByDescending(u => u.EventDateStamp)
                        .First(u => u.UserId == userOnline.UserId);

                    var objectAr = await _veroVrContext
                     .ObjectArs
                     .AsNoTracking()
                     .SingleOrDefaultAsync(r => r.ObjectId == activityAr.ObjectId);

                    if (objectAr != null)
                    {
                        placeActivities.Add(new
                        {
                            ProjectId = objectAr.ProjectId,
                            DateStampActivity = activityAr.EventDateStamp,
                            Place = objectAr.ObjectName + " (AR)"
                        });
                    }
                }

                if (userInPlansActivities
                  .Select(u => u.UserId)
                  .Contains(userOnline.UserId))
                {
                    var activityPlan = userInPlansActivities
                        .OrderByDescending(u => u.EventDateStamp)
                        .First(u => u.UserId == userOnline.UserId);

                    var objectPlan = await _veroVrContext
                     .ObjectPlans
                     .AsNoTracking()
                     .SingleOrDefaultAsync(r => r.ObjectId == activityPlan.ObjectId);

                    if(objectPlan != null)
                    {
                        placeActivities.Add(new
                        {
                            ProjectId = objectPlan.ProjectId,
                            DateStampActivity = activityPlan.EventDateStamp,
                            Place = objectPlan.ObjectName + " (Plan)"
                        });
                    }
                }

                int? projectId = null;
                string projectName = null;
                string projectPlace = null;

                var lastPlace = placeActivities
                    .OrderByDescending(e => e.DateStampActivity)
                    .FirstOrDefault();

                if(lastPlace != null)
                {
                    int? lastProjectId = lastPlace.ProjectId;

                    if (lastProjectId.HasValue && lastProjectId.Value != 0)
                    {
                        var onlineProject =
                            userOnline
                            .UserProjects
                            .SingleOrDefault(u => u.ProjectId == lastProjectId);

                            projectId = onlineProject?.Project?.ProjectId;
                            projectName = onlineProject?.Project?.ProjectName;
                    }

                    projectPlace = lastPlace?.Place;
                }
                else
                {
                    projectPlace = placeListOfProjects;
                }

                usersOnlineJson.Add(new
                {
                    id = userOnline.UserId,
                    name = userOnline.UserDisplayName,
                    ocupation = userOnline.Occupation,
                    mail = userOnline.UserEmail,
                    phone = userOnline.UserPhoneNumber,
                    avatarUrl = userOnline.UserAvatarUrl,
                    facebookLink = userOnline.UserFacebook,
                    facebookLikes = userOnline.FacebookLikes,
                    country = userOnline.UserCountry,
                    city = userOnline.UserCity,
                    address = userOnline.UserCountry,
                    ageRange = userOnline.UserAgeRange,
                    project = new
                    {
                        projectId = projectId,
                        projectName = projectName,
                        projectPlace = projectPlace
                    }
                });
            }
            }
            var response = new
            {
                onlineUsers = usersOnlineJson,
                error = string.Empty,
            };

            // сериализация ответа
            Response.ContentType = "application/json";
            await Response.WriteAsync(JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented }));
        }

        [Authorize(Roles = "SuperAdmin, SuperManager, Manager, SuperManagerWithScript")]
        [HttpGet("api/GetListOfUsersOnlineWithPagination")]
        public async Task GetListOfUsersOnlineWithPagination([FromQuery]int? pageSize,
            [FromQuery] int? page, [FromQuery] string userName)
        {
            if(!pageSize.HasValue || !page.HasValue)
            {
                Response.StatusCode = 400;
                var responseError = new
                {
                    succeeded = false,
                    error = "Not all parameters have been defined."
                };

                // сериализация ответа
                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonConvert.SerializeObject(responseError, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                return;
            }

            DateTime dateOnline = DateTime.Now.AddSeconds(-25);
            DateTime maxDateForViewOneScene = DateTime.Now.AddHours(-1);

            const string placeListOfProjects = "List of projects";

            var usersOnline = _veroVrContext
                .Users
                .Include("UserProjects.Project")
                .AsNoTracking()
                .Where(u => (_veroVrContext
                .UserOnlineActivities
                .AsNoTracking()
                .Any(e => e.EventDateStamp >= dateOnline
                && e.UserId == u.UserId) && u.UserRole == UserRole.Customer)
                && (userName == null || u.UserDisplayName.ToLower().Contains(userName.ToLower().Trim())));

            int countUsersOnline = usersOnline.Count();

            usersOnline = usersOnline.OrderBy(u => u.UserId)
                .Skip((page.Value - 1) * pageSize.Value)
                .Take(pageSize.Value);

            // Get the generated SQL
            //_logger.LogError(usersOnline.ToSql());

            var usersOnlineList = await usersOnline.ToListAsync();

            ICollection<object> usersOnlineJson = new List<object>();

            if (usersOnlineList.Count > 0)
            {
                var usersListOfProjectsActivities = _statisticsContext
                              .UserActivities
                              .AsNoTracking()
                              .Where(e => e.UserActivityType == UserActivityType.ListOfProjects &&
                               e.EventDateStamp >= maxDateForViewOneScene)
                              .ToList();

                var usersProjectDescriptionActivities = _statisticsContext
                       .UserInProjectDescriptionActivities
                       .AsNoTracking()
                       .Where(e => e.EventDateStamp >= maxDateForViewOneScene)
                       .ToList();

                //Trace.WriteLine(usersProjectDescriptionActivities.ToString());

                var usersInRoom360Activities = _statisticsContext
                     .UserInRoom360Activities
                     .AsNoTracking()
                     .Where(e => e.EventDateStamp >= maxDateForViewOneScene)
                     .ToList();

                var userInRoomVrActivities = _statisticsContext
                    .UserInRoomVrActivities
                    .AsNoTracking()
                    .Where(e => e.EventDateStamp >= maxDateForViewOneScene)
                    .ToList();

                var userInYouTubeVideoActivities = _statisticsContext
                   .UserInYouTubeVideoActivities
                   .AsNoTracking()
                   .Where(e => e.EventDateStamp >= maxDateForViewOneScene)
                   .ToList();

                var userInArActivities = _statisticsContext
                  .UserInArActivities
                  .AsNoTracking()
                  .Where(e => e.EventDateStamp >= maxDateForViewOneScene)
                  .ToList();

                var userInPlansActivities = _statisticsContext
                 .UserInPlansActivities
                 .AsNoTracking()
                 .Where(e => e.EventDateStamp >= maxDateForViewOneScene)
                 .ToList();

                foreach (var userOnline in usersOnlineList)
                {
                    List<dynamic> placeActivities = new List<dynamic>();

                    if (usersListOfProjectsActivities
                       .Select(u => u.UserId)
                       .Contains(userOnline.UserId))
                    {
                        var prListOfProjectActivity =
                            usersListOfProjectsActivities
                            .OrderByDescending(u => u.EventDateStamp)
                            .First(u => u.UserId == userOnline.UserId);

                        placeActivities.Add(new
                        {
                            ProjectId = 0,
                            DateStampActivity = prListOfProjectActivity.EventDateStamp,
                            Place = placeListOfProjects
                        });
                    }

                    if (usersProjectDescriptionActivities
                        .Select(u => u.UserId)
                        .Contains(userOnline.UserId))
                    {
                        var prDescriptionActivity =
                            usersProjectDescriptionActivities
                            .OrderByDescending(u => u.EventDateStamp)
                            .First(u => u.UserId == userOnline.UserId);

                        placeActivities.Add(new
                        {
                            ProjectId = prDescriptionActivity.ProjectId,
                            DateStampActivity = prDescriptionActivity.EventDateStamp,
                            Place = "Project description"
                        });
                    }

                    if (usersInRoom360Activities
                        .Select(u => u.UserId)
                        .Contains(userOnline.UserId))
                    {
                        var activity360 = usersInRoom360Activities
                            .OrderByDescending(u => u.EventDateStamp)
                            .First(u => u.UserId == userOnline.UserId);

                        var roomImageIn360Activity = await _veroVrContext
                            .RoomImages
                            .Include(e => e.Object)
                            .AsNoTracking()
                            .SingleOrDefaultAsync(r => r.RoomImageId == activity360.RoomImageId);

                        if (roomImageIn360Activity != null)
                        {
                            placeActivities.Add(new
                            {
                                ProjectId = roomImageIn360Activity.Object.ProjectId,
                                DateStampActivity = activity360.EventDateStamp,
                                Place = roomImageIn360Activity.Object.ObjectName + " (Room 360)"
                            });
                        }
                    }

                    if (userInRoomVrActivities
                        .Select(u => u.UserId)
                        .Contains(userOnline.UserId))
                    {
                        var activityVr = userInRoomVrActivities
                            .OrderByDescending(u => u.EventDateStamp)
                            .First(u => u.UserId == userOnline.UserId);

                        var roomImageInVrActivity = await _veroVrContext
                           .RoomImages
                           .Include(e => e.Object)
                           .AsNoTracking()
                           .SingleOrDefaultAsync(r => r.RoomImageId == activityVr.RoomImageId);

                        if (roomImageInVrActivity != null)
                        {
                            placeActivities.Add(new
                            {
                                ProjectId = roomImageInVrActivity.Object.ProjectId,
                                DateStampActivity = activityVr.EventDateStamp,
                                Place = roomImageInVrActivity.Object.ObjectName + " (Room VR)"
                            });
                        }
                    }

                    if (userInYouTubeVideoActivities
                        .Select(u => u.UserId)
                        .Contains(userOnline.UserId))
                    {
                        var activityVideoActivity = userInYouTubeVideoActivities
                            .OrderByDescending(u => u.EventDateStamp)
                            .First(u => u.UserId == userOnline.UserId);

                        placeActivities.Add(new
                        {
                            ProjectId = activityVideoActivity.ProjectId,
                            DateStampActivity = activityVideoActivity.EventDateStamp,
                            Place = "YouTube video"
                        });
                    }

                    if (userInArActivities
                        .Select(u => u.UserId)
                        .Contains(userOnline.UserId))
                    {
                        var activityAr = userInArActivities
                            .OrderByDescending(u => u.EventDateStamp)
                            .First(u => u.UserId == userOnline.UserId);

                        var objectAr = await _veroVrContext
                         .ObjectArs
                         .AsNoTracking()
                         .SingleOrDefaultAsync(r => r.ObjectId == activityAr.ObjectId);

                        if (objectAr != null)
                        {
                            placeActivities.Add(new
                            {
                                ProjectId = objectAr.ProjectId,
                                DateStampActivity = activityAr.EventDateStamp,
                                Place = objectAr.ObjectName + " (AR)"
                            });
                        }
                    }

                    if (userInPlansActivities
                      .Select(u => u.UserId)
                      .Contains(userOnline.UserId))
                    {
                        var activityPlan = userInPlansActivities
                            .OrderByDescending(u => u.EventDateStamp)
                            .First(u => u.UserId == userOnline.UserId);

                        var objectPlan = await _veroVrContext
                         .ObjectPlans
                         .AsNoTracking()
                         .SingleOrDefaultAsync(r => r.ObjectId == activityPlan.ObjectId);

                        if (objectPlan != null)
                        {
                            placeActivities.Add(new
                            {
                                ProjectId = objectPlan.ProjectId,
                                DateStampActivity = activityPlan.EventDateStamp,
                                Place = objectPlan.ObjectName + " (Plan)"
                            });
                        }
                    }

                    int? projectId = null;
                    string projectName = null;
                    string projectPlace = null;

                    var lastPlace = placeActivities
                        .OrderByDescending(e => e.DateStampActivity)
                        .FirstOrDefault();

                    if (lastPlace != null)
                    {
                        int? lastProjectId = lastPlace.ProjectId;

                        if (lastProjectId.HasValue && lastProjectId.Value != 0)
                        {
                            var onlineProject =
                                userOnline
                                .UserProjects
                                .SingleOrDefault(u => u.ProjectId == lastProjectId);

                            projectId = onlineProject?.Project?.ProjectId;
                            projectName = onlineProject?.Project?.ProjectName;
                        }

                        projectPlace = lastPlace?.Place;
                    }
                    else
                    {
                        projectPlace = placeListOfProjects;
                    }

                    usersOnlineJson.Add(new
                    {
                        id = userOnline.UserId,
                        name = userOnline.UserDisplayName,
                        ocupation = userOnline.Occupation,
                        mail = userOnline.UserEmail,
                        phone = userOnline.UserPhoneNumber,
                        avatarUrl = userOnline.UserAvatarUrl,
                        facebookLink = userOnline.UserFacebook,
                        facebookLikes = userOnline.FacebookLikes,
                        country = userOnline.UserCountry,
                        city = userOnline.UserCity,
                        address = userOnline.UserCountry,
                        ageRange = userOnline.UserAgeRange,
                        project = new
                        {
                            projectId = projectId,
                            projectName = projectName,
                            projectPlace = projectPlace
                        }
                    });
                }
            }
            var response = new
            {
                countOnlineUsers = countUsersOnline,
                onlineUsers = usersOnlineJson,
                error = string.Empty,
            };

            // сериализация ответа
            Response.ContentType = "application/json";
            await Response.WriteAsync(JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented }));
        }

        [Authorize(Roles = "SuperAdmin, SuperManager, Manager, SuperManagerWithScript")]
        [HttpGet("api/GetAgeRangeUserInfo")]
        public async Task GetAgeRangeUserInfo()
        {
            var users = _veroVrContext
                        .Users
                        .Include("UserProjects.Project")
                        .AsNoTracking()
                        .Where(u => u.UserRole == UserRole.Customer);

            var usersJson = new List<object>();

            foreach (var user in users)
            {
                usersJson.Add(new
                {
                    id = user.UserId,
                    name = user.UserDisplayName,
                    ageRange = user.UserAgeRange,
                    facebookLink = user.UserFacebook,
                    facebookLikes = user.FacebookLikes,
                    phone = user.UserPhoneNumber,
                    mail = user.UserEmail,
                    device = user.DeviceAtLastLogin,
                    projects = user.UserProjects.Select(u => new { id = u.Project.ProjectId, name = u.Project.ProjectName })
                });
            }

            var response = new
            {
                users = usersJson,
                error = string.Empty,
            };

            // сериализация ответа
            Response.ContentType = "application/json";
            await Response.WriteAsync(JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented }));
        }

        [Authorize(Roles = "SuperAdmin, SuperManager, Manager, SuperManagerWithScript")]
        [HttpGet("api/GetProjectAndUsersInfo")]
        public async Task GetProjectAndUsersInfo()
        {
            int userId = int.Parse(User.FindFirst("UserId")?.Value);

            var user = _veroVrContext
                .Users
                .Include("UserProjects")
                .AsNoTracking()
                .SingleOrDefault(u => u.UserId == userId);

            if(user == null)
            {
                Response.StatusCode = 404;
                var responseError = new
                {
                    error = "Current user was not found."
                };

                // сериализация ответа
                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonConvert.SerializeObject(responseError, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                return;
            }

            var projectIds = user
                            .UserProjects
                            .Select(up => up.ProjectId);
            //TODO: Optimization
            var projects = _veroVrContext
                        .Projects
                        .Include(o => o.Objects)
                        .Include("UserProjects.User")
                        .AsNoTracking()
                        .ToList()
                        .Where(p => p.UserProjects.Any(up => projectIds.Contains(up.ProjectId)));

            var pojectsJson = new List<object>();

            foreach (var project in projects)
            {
                pojectsJson.Add(new
                {
                    id = project.ProjectId,
                    name = project.ProjectName,
                    description = project.ProjectDescription,
                    code = project.ProjectCode,
                    videoUrl = project.Objects.FirstOrDefault(o => o.ObjectType == ObjectType.VideoYouTube)?.PathFileOnServer,
                    miniImageUrl = project.ProjectImageUrl,
                    users = project
                            .UserProjects
                            .Where(u => u.UserRole == UserRole.Customer)
                            .Select(u => new
                            {
                                id = u.User.UserId,
                                name = u.User.UserDisplayName,
                                ageRange = u.User.UserAgeRange,
                                facebookLink = u.User.UserFacebook,
                                facebookLikes = u.User.FacebookLikes,
                                phone = u.User.UserPhoneNumber,
                                mail = u.User.UserEmail,
                                device = u.User.DeviceAtLastLogin,
                                deviceType = u.User.DeviceTypeAtLastLogin
                            })
                });
            }

            var response = new
            {
                users = pojectsJson,
                error = string.Empty,
            };

            // сериализация ответа
            Response.ContentType = "application/json";
            await Response.WriteAsync(JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented }));
        }

        [Authorize(Roles = "SuperAdmin, SuperManager, Manager, SuperManagerWithScript")]
        [HttpGet("api/GetProjectAndUsersInfoWithPagination")]
        public async Task GetProjectAndUsersInfoWithPagination([FromQuery]int? pageSize,
                [FromQuery] int? page)
        {
            if (!pageSize.HasValue || !page.HasValue)
            {
                Response.StatusCode = 400;
                var responseError = new
                {
                    succeeded = false,
                    error = "Not all parameters have been defined."
                };

                // сериализация ответа
                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonConvert.SerializeObject(responseError, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                return;
            }

            int userId = int.Parse(User.FindFirst("UserId")?.Value);

            var user = _veroVrContext
                .Users
                .Include("UserProjects")
                .AsNoTracking()
                .SingleOrDefault(u => u.UserId == userId);

            if (user == null)
            {
                Response.StatusCode = 404;
                var responseError = new
                {
                    error = "Current user was not found."
                };

                // сериализация ответа
                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonConvert.SerializeObject(responseError, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                return;
            }

            var projectIds = user
                            .UserProjects
                            .Select(up => up.ProjectId);
            
            var projects = _veroVrContext
                        .Projects
                        .Include(o => o.Objects)
                        .Include("UserProjects.User")
                        .AsNoTracking()
                        .OrderBy(p => p.ProjectId)
                        .Skip((page.Value - 1) * pageSize.Value)
                        .Take(pageSize.Value)
                        .ToList()
                        .Where(p => p.UserProjects.Any(up => projectIds.Contains(up.ProjectId)));

            int countProjects = _veroVrContext
                            .Projects
                            .Include(e => e.UserProjects)
                            .AsNoTracking()
                            .ToList()
                            .Where(p => p.UserProjects.Any(up => projectIds.Contains(up.ProjectId)))
                            .Count();

            var pojectsJson = new List<object>();

            foreach (var project in projects)
            {
                pojectsJson.Add(new
                {
                    id = project.ProjectId,
                    name = project.ProjectName,
                    description = project.ProjectDescription,
                    code = project.ProjectCode,
                    videoUrl = project.Objects.FirstOrDefault(o => o.ObjectType == ObjectType.VideoYouTube)?.PathFileOnServer,
                    miniImageUrl = project.ProjectImageUrl,
                    users = project
                            .UserProjects
                            .Where(u => u.UserRole == UserRole.Customer)
                            .Select(u => new
                            {
                                id = u.User.UserId,
                                name = u.User.UserDisplayName,
                                ageRange = u.User.UserAgeRange,
                                facebookLink = u.User.UserFacebook,
                                facebookLikes = u.User.FacebookLikes,
                                phone = u.User.UserPhoneNumber,
                                mail = u.User.UserEmail,
                                device = u.User.DeviceAtLastLogin,
                                deviceType = u.User.DeviceTypeAtLastLogin
                            })
                });
            }

            var response = new
            {
                countProjects = countProjects,
                users = pojectsJson,
                error = string.Empty,
            };

            // сериализация ответа
            Response.ContentType = "application/json";
            await Response.WriteAsync(JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented }));
        }

        [Authorize(Roles = "SuperAdmin, SuperManager, Manager, SuperManagerWithScript")]
        [HttpGet("api/GetUsersHistoryByPeriod")]
        public async Task GetUserHistoryByPeriod(
             [FromQuery] int? fromMonth,
             [FromQuery] int? fromYear,
             [FromQuery] int? toMonth,
             [FromQuery] int? toYear)
        {
            if (fromMonth == null || fromYear == null
                || toMonth == null || toYear == null)
            {
                Response.StatusCode = 400;
                var responseError = new
                {
                    error = "Not all parameters have been defined."
                };

                // сериализация ответа
                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonConvert.SerializeObject(responseError, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                return;
            }

            DateTime beginningOfPeriod = new DateTime(fromYear.Value, fromMonth.Value, 1);
            DateTime endOfPeriod =
                new DateTime(toYear.Value, toMonth.Value, DateTime.DaysInMonth(toYear.Value, toMonth.Value))
                .AddMonths(1);


            var listMonths = GetMonthsBetween(beginningOfPeriod, endOfPeriod);

            List<dynamic> objs = new List<dynamic>();

            var listMonthsPrepared = new List<DateTime>();

            foreach (var item in listMonths)
            {
                if (!listMonthsPrepared.Contains(new DateTime(item.Year, item.Month, 1)))
                {
                    listMonthsPrepared.Add(new DateTime(item.Year, item.Month, 1));
                }
            }

            foreach (var date in listMonthsPrepared)
            {
                int numberOfActiveUsers = await _statisticsContext
                    .UserLoginActivities
                    .AsNoTracking()
                    .Where(ua => ua.EventDateStamp.Month == date.Month
                    && ua.EventDateStamp.Year == date.Year)
                    .Select(u => u.UserId)
                    .Distinct()
                    .CountAsync();

                int numberOfNewUsers = await _veroVrContext
                      .Users
                      .AsNoTracking()
                      .Where(u => u.DateOfRegistration.Month == date.Month
                      && u.DateOfRegistration.Year == date.Year
                      && u.UserRole == UserRole.Customer)
                      .CountAsync();

                var pastDate = date.AddMonths(-1);

                IEnumerable<int> usersRegistredIds = _veroVrContext
                                 .Users
                                 .AsNoTracking()
                                 .Where(d => d.DateOfRegistration < date)
                                 .Select(u => u.UserId)
                                 .ToList();

                var userIdsReturned =
                     _statisticsContext
                          .UserLoginActivities
                          .AsNoTracking()
                          .Where(u =>
                          // не был в прошлом месяце
                          ((u.EventDateStamp.Month != pastDate.Month
                          && u.EventDateStamp.Year != pastDate.Year)
                          // или раньше прошлого месяца
                          || u.EventDateStamp < pastDate)
                         // и появился в заданном месяце
                         && (u.EventDateStamp.Month == date.Month
                          && u.EventDateStamp.Year == date.Year))
                          .Select(u => u.UserId)
                          .Distinct()
                          .ToList();

                IEnumerable<int> userIds = userIdsReturned.Where(id =>
                          // но был зарегистрирован раньше заданного месяца
                          usersRegistredIds.Contains(id));

                int count = userIds.Count();

                objs.Add(new
                {
                    month = date.Month,
                    year = date.Year,
                    numberOfActiveUsers = numberOfActiveUsers,
                    numberOfNewUsers = numberOfNewUsers,
                    numberOfReturnedUsers = count
                });


            }

            var response = new
            {
                userStatistics = objs,
                error = ""
            };

            // сериализация ответа
            Response.ContentType = "application/json";
            await Response.WriteAsync(JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented }));
            return;

        }

        [Authorize(Roles = "SuperAdmin, SuperManager, Manager, SuperManagerWithScript")]
        [HttpGet("api/GetTypesOfUserDevices")]
        public async Task GetTypesOfUserDevices()
        {
           var devicetypes = _veroVrContext
                .Users
                .AsNoTracking()
                .Where(u => u.UserRole == UserRole.Customer)
                .GroupBy(l => l.DeviceTypeAtLastLogin)
                .Select(cl =>
                           new
                             {
                                  deviceType = cl.Key,
                                  number = cl.Count()
                             })
                             ;

           // string sql = devicetypes.ToSql();

            var listDevicetypes = await devicetypes.ToListAsync();

            var response = new
            {
                devicetypes = listDevicetypes
            };

            Response.StatusCode = 200;
            // сериализация ответа
            Response.ContentType = "application/json";
            await Response.WriteAsync(JsonConvert
                .SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented }));
            return;
        }

        [Authorize(Roles = "SuperAdmin, SuperManager, Manager, SuperManagerWithScript")]
        [HttpGet("api/GetUserHistoryOfTimeSpent")]
        public async Task GetUserHistoryOfTimeSpent(
             [FromQuery]int? id,
             [FromQuery] int? fromDay,
             [FromQuery] int? fromMonth,
             [FromQuery] int? fromYear,
             [FromQuery] int? toDay,
             [FromQuery] int? toMonth,
             [FromQuery] int? toYear)
        {
            if(fromDay == null || id == null || fromMonth == null || fromYear == null
                || toDay == null || toMonth == null || toYear == null)
            {
                Response.StatusCode = 400;
                var responseError = new
                {
                    error = "Not all parameters have been defined."
                };

                // сериализация ответа
                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonConvert.SerializeObject(responseError, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                return;
            }

            User user =
                        await _veroVrContext
                        .Users
                        .Include(e => e.UserProjects)
                        .AsNoTracking()
                        .SingleOrDefaultAsync(u => u.UserId == id);

            if (user == null)
            {
                Response.StatusCode = 404;
                var responseError = new
                {
                    error = "User wasn't found."
                };

                // сериализация ответа
                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonConvert.SerializeObject(responseError, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                return;
            }

            DateTime beginningOfPeriod;
            DateTime endOfPeriod;

            try
            {
                beginningOfPeriod = new DateTime(fromYear.Value, fromMonth.Value, 1);
                endOfPeriod = new DateTime(toYear.Value, toMonth.Value, DateTime.DaysInMonth(toYear.Value, toMonth.Value));
            }
            catch (Exception)
            {
                Response.StatusCode = 400;
                var responseError = new
                {
                    error = "The period parameters are incorrectly set."
                };

                // сериализация ответа
                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonConvert.SerializeObject(responseError, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                return;
            }

            if (beginningOfPeriod > endOfPeriod)
            {
                Response.StatusCode = 400;
                var responseError = new
                {
                    error = "The beginning of the period can not be more than the end of the period."
                };

                // сериализация ответа
                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonConvert.SerializeObject(responseError, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                return;
            }

            var userActivity = _statisticsContext
                      .UserActivities
                      .AsNoTracking()
                      .Where(a => a.EventDateStamp.Date >= beginningOfPeriod &&
                      a.EventDateStamp.Date <= endOfPeriod &&
                      a.UserId == user.UserId).ToList();

            var userInProjectDescriptionActivity = userActivity
                        .Where(u => u.UserActivityType == UserActivityType.ProjectDescriptionView)
                        .Cast<UserInProjectDescriptionActivity>();

            var userInRooms360Activity = userActivity
                .Where(u => u.UserActivityType == UserActivityType.Room360View)
                .Cast<UserInRoom360Activity>();

            var userInRoomsVrActivity = userActivity
                .Where(u => u.UserActivityType == UserActivityType.VrModeView)
                .Cast<UserInRoomVrActivity>();

            var userInVideoActivity = userActivity
                .Where(u => u.UserActivityType == UserActivityType.YouTubeVideoView)
                .Cast<UserInYouTubeVideoActivity>();

            var userInArActivity = userActivity
                .Where(u => u.UserActivityType == UserActivityType.ArModelView)
                .Cast<UserInArActivity>();

            var userInPlansActivity = _statisticsContext
           .UserInPlansActivities
           .AsNoTracking()
           .Where(a => a.EventDateStamp.Date >= beginningOfPeriod &&
           a.EventDateStamp.Date <= endOfPeriod &&
           a.UserId == user.UserId).ToList();

            var userClickLinkInDescriptionActivity = _statisticsContext
            .UserClickLinkInDescriptionActivities
            .AsNoTracking()
            .Where(a => a.EventDateStamp.Date >= beginningOfPeriod &&
            a.EventDateStamp.Date <= endOfPeriod &&
            a.UserId == user.UserId).ToList();

          

            var response = new
            {
               userHistory = new
               {
                   id = user.UserId,
                   name = user.UserDisplayName,
                   ocupation = user.Occupation,
                   mail = user.UserEmail,
                   phone = user.UserPhoneNumber,
                   avatarUrl = user.UserAvatarUrl,
                   facebookLink = user.UserFacebook,
                   facebookLikes = user.FacebookLikes,
                   country = user.UserCountry,
                   city = user.UserCity,
                   address = user.UserCountry,
                   ageRange = user.UserAgeRange,
                   projects = GetViewedProject(user,
                   userInProjectDescriptionActivity,
                   userInRooms360Activity,
                   userInRoomsVrActivity,
                   userInArActivity,
                   userInPlansActivity,
                   userInVideoActivity,
                   userClickLinkInDescriptionActivity,
                   userActivity)
               }
            };

            Response.StatusCode = 200;
            // сериализация ответа
            Response.ContentType = "application/json";
            await Response.WriteAsync(JsonConvert
                .SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented }));
            return;
        }

        #region Helper methods

        private IEnumerable<dynamic> GetViewedProject(User user,
            IEnumerable<UserInProjectDescriptionActivity> userInProjectDescriptionActivity,
            IEnumerable<UserInRoom360Activity> userInRoom360Activity,
            IEnumerable<UserInRoomVrActivity> userInRoomVrActivity,
            IEnumerable<UserInArActivity> userInArActivity,
            IEnumerable<UserInPlansActivity> userInPlansActivity,
            IEnumerable<UserInYouTubeVideoActivity> userInYouTubeVideoActivity,
            IEnumerable<UserClickLinkInDescriptionActivity> userClickLinkInDescriptionActivity,
            IEnumerable<UserActivity> userActivities)
        {
            ICollection<dynamic> viewedProjectsJson = new List<dynamic>();

            var projectIdsInWhichUserWas = new List<int>();

            //check description
            var descriptionProjectIds = userInProjectDescriptionActivity
                                      .Select(a => a.ProjectId)
                                      .Distinct();

            projectIdsInWhichUserWas.AddRange(descriptionProjectIds);

            //check room360 and VR
            var roomImages360Ids = userInRoom360Activity
                                      .Select(a => a.RoomImageId)
                                      .Distinct();

            var roomImagesVrIds = userInRoomVrActivity
                                    .Select(a => a.RoomImageId)
                                    .Distinct();

            var roomImagesIds = roomImages360Ids.Concat(roomImagesVrIds).Distinct();

            var roomImages360Vr = new List<RoomImage>();

            foreach (var roomImagesId in roomImagesIds)
            {
                var roomImage360VrProject = _veroVrContext
                   .RoomImages
                   .Include(o => o.Object)
                   .AsNoTracking()
                   .SingleOrDefault(ri => ri.RoomImageId == roomImagesId);

                if (roomImage360VrProject != null)
                {
                    roomImages360Vr.Add(roomImage360VrProject);
                }
            }

            var roomImages360VrProjectIds = roomImages360Vr.Select(r => r.Object.ProjectId).Distinct();

            projectIdsInWhichUserWas.AddRange(roomImages360VrProjectIds);

            //check Ar objects
            var objectArIds = userInArActivity
                                      .Select(a => a.ObjectId)
                                      .Distinct();

            var objectsAr = new List<ObjectAr>();

            foreach (var objectArId in objectArIds)
            {
                var objectAr = _veroVrContext
                   .ObjectArs
                   .AsNoTracking()
                   .SingleOrDefault(ri => ri.ObjectId == objectArId);

                if (objectAr != null)
                {
                    objectsAr.Add(objectAr);
                }
            }

            var objectArProjectIds = objectsAr.Select(r => r.ProjectId).Distinct();

            projectIdsInWhichUserWas.AddRange(objectArProjectIds);

            //check plans
            var objectPlanIds = userInPlansActivity
                                      .Select(a => a.ObjectId)
                                      .Distinct();

            var objectsPlan = new List<ObjectPlan>();

            foreach (var objectPlanId in objectPlanIds)
            {
                var objectAr = _veroVrContext
                   .ObjectPlans
                   .AsNoTracking()
                   .SingleOrDefault(ri => ri.ObjectId == objectPlanId);

                if (objectAr != null)
                {
                    objectsPlan.Add(objectAr);
                }
            }

            var objectsPlanIds = objectsPlan.Select(r => r.ProjectId).Distinct();

            projectIdsInWhichUserWas.AddRange(objectsPlanIds);

            //check youtube activity
            var youTubeProjectIds = userInYouTubeVideoActivity
                                      .Select(a => a.ProjectId)
                                      .Distinct();

            projectIdsInWhichUserWas.AddRange(youTubeProjectIds);

            //check to click link in project description
            var userClickLinkInDescriptionProjectIds = userClickLinkInDescriptionActivity
                                      .Select(a => a.ProjectId)
                                      .Distinct();

            projectIdsInWhichUserWas.AddRange(userClickLinkInDescriptionProjectIds);

            projectIdsInWhichUserWas = projectIdsInWhichUserWas.Distinct().ToList();

            var projectsInWhichUserWas = _veroVrContext
                                    .Projects
                                    .AsNoTracking()
                                    .Where(p => projectIdsInWhichUserWas.Contains(p.ProjectId));

            var roomImageIds360InWhichUserWas = userInRoom360Activity
                                      .Select(a => a.RoomImageId)
                                      .Distinct();

            var roomImages360InWhichUserWas = _veroVrContext
                                    .RoomImages
                                    .AsNoTracking()
                                    .Where(r => roomImageIds360InWhichUserWas.Contains(r.RoomImageId));

                foreach (var project in projectsInWhichUserWas)
                {
                    viewedProjectsJson.Add(new
                    {
                        id = project.ProjectId,
                        name = project.ProjectName,
                        description = project.ProjectDescription,
                        clickLinkInfo = GetInfoClickLinkInDescription(userClickLinkInDescriptionActivity, project.ProjectId),
                        places = GetTimeSpentInPlaces(project.ProjectId, userActivities)
                    });
                }


            return viewedProjectsJson;
        }

        private IEnumerable<object> GetTimeSpentInPlaces(int projectId,
            IEnumerable<UserActivity> userActivities)
        {
            var listOfTimes = new List<Tuple<string, TimeSpan>>();

            TimeSpan timeInProjectDescription = GetTimeInProjectActivity(userActivities,
                    UserActivityType.ProjectDescriptionView, null, null, projectId);

            string placeNameProjectDescription = "Project description";

            listOfTimes.Add(new Tuple<string, TimeSpan>(placeNameProjectDescription, timeInProjectDescription));

            TimeSpan timeInYouTubeVideo = GetTimeInProjectActivity(userActivities,
                    UserActivityType.YouTubeVideoView, null, null, projectId);

            string placeNameYouTubeVideo = "YouTube video";

                listOfTimes.Add(new Tuple<string, TimeSpan>(placeNameYouTubeVideo, timeInYouTubeVideo));

            IEnumerable<int> roomImages360Ids = userActivities
                .Where(u => u.UserActivityType == UserActivityType.Room360View)
                .Cast<UserInRoom360Activity>()
                .Select(r => r.RoomImageId)
                .Distinct();

            var roomImages = _veroVrContext
                .RoomImages
                .Include("Object.Project")
                .AsNoTracking()
                .Where(r => r.Object.ProjectId == projectId)
                .ToList()
                .Where(r => roomImages360Ids.Contains(r.RoomImageId));

            foreach (var roomImage360 in roomImages)
            {
                TimeSpan timeInRoom360Image = 
                    GetTimeInProjectActivity(userActivities,
                    UserActivityType.Room360View,
                    roomImage360.RoomImageId);

                string placeName = roomImage360.Object.ObjectName + " (360 mode)";

                listOfTimes.Add(new Tuple<string, TimeSpan>(placeName, timeInRoom360Image));
            }

            IEnumerable<int> roomImagesVrIds = userActivities
                .Where(u => u.UserActivityType == UserActivityType.VrModeView)
                .Cast<UserInRoomVrActivity>()
                .Select(r => r.RoomImageId)
                .Distinct();
            
            var roomImagesVr = _veroVrContext
                .RoomImages
                .Include("Object.Project")
                .AsNoTracking()
                .Where(r => r.Object.ProjectId == projectId)
                .ToList()
                .Where(r => roomImagesVrIds.Contains(r.RoomImageId));

            foreach (var roomImageVr in roomImagesVr)
            {
                TimeSpan timeInRoomVrImage = 
                    GetTimeInProjectActivity(userActivities,
                    UserActivityType.VrModeView,
                    roomImageVr.RoomImageId);

                string placeName = roomImageVr.Object.ObjectName + " (VR mode)";

                listOfTimes.Add(new Tuple<string, TimeSpan>(placeName, timeInRoomVrImage));
            }

            IEnumerable<int> objectsArIds = userActivities
                .Where(u => u.UserActivityType == UserActivityType.ArModelView)
                .Cast<UserInArActivity>()
                .Select(r => r.ObjectId)
                .Distinct();
            
            var objectsAr = _veroVrContext
                .ObjectArs
                .AsNoTracking()
                .Where(r => r.ProjectId == projectId)
                .ToList()
                .Where(r => objectsArIds.Contains(r.ObjectId));

            foreach (var objectAr in objectsAr)
            {
                TimeSpan timeInObjectAr = GetTimeInProjectActivity(userActivities,
                    UserActivityType.ArModelView, null, objectAr.ObjectId);

                string placeName = objectAr.ObjectName + " (AR mode)";

                listOfTimes.Add(new Tuple<string, TimeSpan>(placeName, timeInObjectAr));
            }

            IEnumerable<int> objectsPlansIds = userActivities
                .Where(u => u.UserActivityType == UserActivityType.PlansView)
                .Cast<UserInPlansActivity>()
                .Select(r => r.ObjectId)
                .Distinct();
            
            var objectsPlans = _veroVrContext
              .ObjectPlans
              .AsNoTracking()
              .Where(r => r.ProjectId == projectId)
              .ToList()
              .Where(r => objectsPlansIds.Contains(r.ObjectId));

            foreach (var objectPlan in objectsPlans)
            {
                TimeSpan timeInObjectAr = GetTimeInProjectActivity(userActivities,
                    UserActivityType.PlansView, null, objectPlan.ObjectId);

                string placeName = objectPlan.ObjectName + " (Plan)";

                listOfTimes.Add(new Tuple<string, TimeSpan>(placeName, timeInObjectAr));
            }

            var groupedTimes = listOfTimes.GroupBy(p => p.Item1)
                                        .Select(cl =>
                                            new
                                            {
                                                NamePlace = cl.Key,
                                                TotalTimeTicks = cl.Sum(c => c.Item2.Ticks)
                                            })
                                            .ToList();

            var totalList = new List<dynamic>();

            var listOfPlacesAndTimeTotal = new List<object>();

            foreach (var time in groupedTimes)
            {
                var timeParam = TimeSpan.FromTicks(time.TotalTimeTicks);

                listOfPlacesAndTimeTotal.Add(new
                {
                    name = time.NamePlace,
                    timeHours = timeParam.Hours,
                    timeMinutes = timeParam.Minutes,
                    timeSeconds = timeParam.Seconds,
                    timeDays = timeParam.Days
                });
            }

            return listOfPlacesAndTimeTotal;
        }

        private IEnumerable<object> GetInfoClickLinkInDescription(IEnumerable<UserClickLinkInDescriptionActivity> userClickLinkInDescriptionActivity,
            int projectId)
        {
            var infoActivity = new List<object>();

            var statisticClicks = userClickLinkInDescriptionActivity
                                        .Where(u => u.ProjectId == projectId)
                                        .GroupBy(l => l.Link)
                                        .Select(cl =>
                                            new
                                            {
                                                link = cl.Key,
                                                numberOfTimes = cl.Count()
                                            })
                                            .ToList();

            foreach (var statisticClick in statisticClicks)
            {
                infoActivity.Add(new
                {
                    link = statisticClick.link,
                    numberOfTimes = statisticClick.numberOfTimes
                });
            }

            return infoActivity;
        }

        private TimeSpan GetTimeInProjectActivity(IEnumerable<UserActivity> userActivities,
            UserActivityType currentActivityType, int? roomImageId = null, int? objectId = null, int? projectId = null)
        {
            List<TimeSpan> timeOnProjects = new List<TimeSpan>();

            int countLogs = userActivities.Count();

            int sevenMinInSeconds = 420;

            TimeSpan timeDefault = new TimeSpan(0, 7, 0);

            var userActivitiesList = userActivities
                .OrderBy(a => a.EventDateStamp)
                .ToList();

            DateTime nextUserActivity = DateTime.Now;

            int countActivities = userActivities.Count(a => a.UserActivityType == currentActivityType);

            if(countActivities > 0)
            {
                for (int i = 0; i < countLogs; i++)
                {
                    nextUserActivity = i < countLogs - 1 ?
                        userActivitiesList[i + 1].EventDateStamp : DateTime.Now;

                    if (i > 0 && userActivitiesList[i].UserActivityType == currentActivityType)
                    {
                        if (currentActivityType == UserActivityType.Room360View ||
                            currentActivityType == UserActivityType.VrModeView)
                        {
                            var userInRoomActivity = userActivitiesList[i] as UserRoomImageActivity;

                            if (userInRoomActivity.RoomImageId == roomImageId)
                            {
                                AddTime(timeOnProjects, sevenMinInSeconds, timeDefault,
                                    userActivitiesList[i].EventDateStamp,
                                    nextUserActivity);
                            }
                        }
                        else if (currentActivityType == UserActivityType.ArModelView ||
                           currentActivityType == UserActivityType.PlansView)
                        {
                            var userInObjectActivity = userActivitiesList[i] as UserObjectActivity;

                            if (userInObjectActivity.ObjectId == objectId)
                            {
                                AddTime(timeOnProjects, sevenMinInSeconds, timeDefault,
                                    userActivitiesList[i].EventDateStamp,
                                    nextUserActivity);
                            }
                        }
                        else if (currentActivityType == UserActivityType.YouTubeVideoView ||
                           currentActivityType == UserActivityType.ProjectDescriptionView)
                        {
                            var userInProjectActivity = userActivitiesList[i] as UserProjectActivity;

                            if (userInProjectActivity.ProjectId == projectId)
                            {
                                AddTime(timeOnProjects, sevenMinInSeconds, timeDefault,
                                    userActivitiesList[i].EventDateStamp,
                                    nextUserActivity);
                            }
                        }
                    }
                    else if (i == 0 && countActivities == 1)
                    {
                        TimeSpan time = (DateTime.Now - userActivitiesList[i].EventDateStamp);
                        bool isCorrectEndActivity = time.TotalSeconds <= sevenMinInSeconds;

                        if (isCorrectEndActivity)
                        {
                            timeOnProjects.Add(time);
                        }
                        else
                        {
                            timeOnProjects.Add(timeDefault);
                        }
                    }
                }
            }
            
            var resultTime = TimeSpan.FromTicks(timeOnProjects.Sum(c => c.Ticks));

            return resultTime;
        }

        private static void AddTime(List<TimeSpan> timeOnProjects,
            int sevenMinInSeconds,
            TimeSpan timeDefault,
            DateTime currentUserActivity,
            DateTime nextUserActivity)
        {
            var time = (nextUserActivity - currentUserActivity);

            bool isCorrectEndActivity = time.TotalSeconds <= sevenMinInSeconds;

            if (isCorrectEndActivity)
            {
                timeOnProjects.Add(time);
            }
            else
            {
                timeOnProjects.Add(timeDefault);
            }
        }

        private IEnumerable<dynamic> GetIdsWithTotalTimeObjectActivity(IEnumerable<UserObjectActivity> userObjectActivity)
        {
            var sortedObjects = userObjectActivity
                               .OrderByDescending(a => a.EventDateStamp)
                               .ToList();

            List<Tuple<int, TimeSpan>> timeOnProjects = new List<Tuple<int, TimeSpan>>();

            int countLogs = sortedObjects.Count();

            int sevenMinInSeconds = 420;

            TimeSpan timeDefault = new TimeSpan(0, 7, 0);

            for (int i = 0; i < countLogs; i++)
            {
                if (i > 0)
                {
                    var time = (sortedObjects[i - 1].EventDateStamp - sortedObjects[i].EventDateStamp);

                    bool isCorrectEndActivity = time.Seconds <= sevenMinInSeconds;

                    if (isCorrectEndActivity)
                    {
                        timeOnProjects.Add(new Tuple<int, TimeSpan>(sortedObjects[i].ObjectId, time));
                    }
                    else
                    {
                        timeOnProjects.Add(new Tuple<int, TimeSpan>(sortedObjects[i].ObjectId, timeDefault));
                    }
                }
                else if (i == 0 && countLogs == 1)
                {
                    TimeSpan time = (DateTime.Now - sortedObjects[i].EventDateStamp);
                    bool isCorrectEndActivity = time.Seconds <= sevenMinInSeconds;

                    if (isCorrectEndActivity)
                    {
                        timeOnProjects.Add(new Tuple<int, TimeSpan>(sortedObjects[i].ObjectId, time));
                    }
                    else
                    {
                        timeOnProjects.Add(new Tuple<int, TimeSpan>(sortedObjects[i].ObjectId, timeDefault));
                    }
                }
            }

            var objectIdsWithTotalTime = timeOnProjects
                                    .GroupBy(l => l.Item1)
                                    .Select(cl =>
                                        new
                                        {
                                            Id = cl.Key,
                                            Time = TimeSpan.FromTicks(cl.Sum(c => c.Item2.Ticks))
                                        })
                                        .ToList();

            return objectIdsWithTotalTime;
        }

        private IEnumerable<dynamic> GetUsersForMapJson(string filter)
        {
            string jsonString = Encoding.UTF8.GetString(Properties.Resources.worldCitiesBasic);
            IEnumerable<dynamic> countries =
                JsonConvert.DeserializeObject<IEnumerable<dynamic>>(jsonString,
                new JsonSerializerSettings { Formatting = Formatting.Indented });

            const string allCountries = "All";

            IQueryable<User> users = null;

            string preparedFilterCountry = filter.Trim().ToLower();

            if (String.Equals(preparedFilterCountry, allCountries, StringComparison.OrdinalIgnoreCase))
            {
                users = _veroVrContext
                            .Users
                            .Include("UserProjects.Project")
                            .AsNoTracking()
                            .Where(u => u.UserRole == UserRole.Customer);
            }
            else
            {
                countries = countries.Where(c => c.country?.ToString()
                ?.StartsWith(preparedFilterCountry, StringComparison.OrdinalIgnoreCase));
                users = _veroVrContext
                            .Users
                            .Include("UserProjects.Project")
                            .AsNoTracking()
                            .Where(u => u.UserCountry.ToLower() == preparedFilterCountry
                            && u.UserRole == UserRole.Customer);
            }

            ICollection<dynamic> jsonForSend = new List<dynamic>();

            IEnumerable<dynamic> userCountriesAndCities = users
                                    .Where(u => !String.IsNullOrEmpty(u.UserCountry) && !String.IsNullOrEmpty(u.UserCity))
                                    .Select(u => new { u.UserCountry, u.UserCity })
                                    .GroupBy(u => new { u.UserCountry, u.UserCity })
                                    .Select(g => g.First())
                                    .ToList();


            foreach (var userCountryAndCity in userCountriesAndCities)
            {
                var locationInfo = countries
                    .FirstOrDefault(c => c.city?.ToString()
                    ?.StartsWith(userCountryAndCity.UserCity, StringComparison.OrdinalIgnoreCase)
                && c.country?.ToString()?.StartsWith(userCountryAndCity.UserCountry, StringComparison.OrdinalIgnoreCase));

                if(locationInfo != null)
                {
                    jsonForSend.Add(
                   new
                   {
                       country = locationInfo.country,
                       city = locationInfo.city,
                       lat = locationInfo.lat,
                       lng = locationInfo.lng,
                       users = GetUsersByCountryAndCity(users, userCountryAndCity.UserCountry, userCountryAndCity.UserCity)
                   });
                }
            }

            return jsonForSend;
        }

        private IEnumerable<dynamic> GetUsersByCountryAndCity(IEnumerable<User> users, string userCountry, string userCity)
        {
            var usersForCountryAndCity = users
                .Where(u => String.Equals(u.UserCountry, userCountry, StringComparison.OrdinalIgnoreCase) &&
                            String.Equals(u.UserCity, userCity, StringComparison.OrdinalIgnoreCase)).ToList();

            ICollection<dynamic> usersJson = new List<dynamic>();

            foreach (var user in usersForCountryAndCity)
            {
                usersJson.Add(new
                {
                    id = user.UserId,
                    displayName = user.UserDisplayName,
                    email = user.UserEmail,
                    projects = GetProjectsForUser(user)
                });
            }

            return usersJson;
        }

        private IEnumerable<dynamic> GetProjectsForUser(User user)
        {
            ICollection<dynamic> projectsJson = new List<dynamic>();
            var projects = user.UserProjects.Select(p => p.Project);

            foreach (var project in projects)
            {
                projectsJson.Add(new
                {
                    id = project.ProjectId,
                    name = project.ProjectName,
                    description = project.ProjectDescription,
                    logo = project.ProjectImageUrl
                });
            }

            return projectsJson;
        }

        private IEnumerable<dynamic> GetUsersPerDayJson(IEnumerable<User> usersPerDay)
        {
            ICollection<dynamic> usersPerDayJson = new List<dynamic>();

            foreach (var user in usersPerDay)
            {
                usersPerDayJson.Add(new
                {
                    id = user.UserId,
                    displayName = user.UserDisplayName,
                    email = user.UserEmail,
                    projects = GetProjectsForUser(user)
                });
            }

            return usersPerDayJson;
        }

        public static List<DateTime> GetMonthsBetween(DateTime from, DateTime to)
        {
            if (from > to) return GetMonthsBetween(to, from);

            var monthDiff = Math.Abs((to.Year * 12 + (to.Month - 1)) - (from.Year * 12 + (from.Month - 1)));

            if (from.AddMonths(monthDiff) > to || to.Day < from.Day)
            {
                monthDiff -= 1;
            }

            List<DateTime> results = new List<DateTime>();
            for (int i = monthDiff; i >= 1; i--)
            {
                results.Add(to.AddMonths(-i));
            }

            return results;
        }

        #endregion
    }
}