﻿using HtmlAgilityPack;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using VeroVrCore.Common.Models;
using VeroVrCore.Common.Security;
using VeroVrCore.DataLayer.EntityFramework;
using VeroVrCore.DataLayer.EntityFramework.VeroVrCore.DataLayer.EntityFramework;
using VeroVrCore.WebApi.LogProvider;
using VeroVrCore.WebApi.LogProviders;
using VeroVrCore.WebApi.Services;

namespace VeroVrCore.WebApi.Controllers
{
    public class AccountController : Controller
    {
        private readonly ILogger<AccountController> _logger;
        private VeroVrContext _veroVrContext;
        private VeroVrStatisticsContext _statisticsContext;
        private FileManagerService _fileManager;
        private EmailService _emailService;
        private IHttpContextAccessor _httpContextAccessor;
        private IConfiguration _configuration;
        private readonly IMemoryCache _memoryCache;

        public AccountController(VeroVrContext veroVrContext,
            VeroVrStatisticsContext statisticsContext,
            FileManagerService fileManager, EmailService emailService,
            IHttpContextAccessor httpContextAccessor,
            IConfiguration configuration,
            IMemoryCache memoryCache,
            ILogger<AccountController> logger)
        {
            _configuration = configuration;
            _veroVrContext = veroVrContext;
            _statisticsContext = statisticsContext;
            _fileManager = fileManager;
            _emailService = emailService;
            _httpContextAccessor = httpContextAccessor;
            _memoryCache = memoryCache;
            _logger = logger;
        }

        [AllowAnonymous]
        [HttpPost("/api/login")]
        public async Task Login()
        {
            string email = null;
            string password = null;
            string device = null;
            string deviceType = null;

            try
            {
                try
                {
                    if (Request.HasFormContentType)
                    {
                        email = Request.Form["email"];
                        password = Request.Form["password"];
                        device = Request.Form["device"];
                        deviceType = Request.Form["deviceType"];
                    }
                    else
                    {
                        dynamic userInfo;

                        using (StreamReader stream = new StreamReader(Request.Body))
                        {
                            using (JsonReader reader = new JsonTextReader(stream))
                            {
                                JsonSerializer serializer = new JsonSerializer();
                                userInfo = serializer.Deserialize<dynamic>(reader);
                            }
                        }

                        email = userInfo.email;
                        password = userInfo.password;
                        device = userInfo?.device;
                        deviceType = userInfo?.deviceType;
                    }
                }
                catch(Exception ex)
                {
                    _logger.LogError((int)LoggingEvents.ERROR,
                        LogMessageFormatter.GetErrorMessage("An error occurred while retrieving the parameters when calling /api/login", ex));
                }

                Response.ContentType = "application/json";

                if (email == null || password == null)
                {
                    Response.StatusCode = 400;
                    var errorObj = new
                    {
                        error = "Invalid request format."
                    };
                    await Response.WriteAsync(JsonConvert.SerializeObject(errorObj, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                    return;
                }

                string preparedEmail = email.ToLower().Trim();

                User user = await _veroVrContext
                    .Users
                    .SingleOrDefaultAsync(u => u.UserEmail == preparedEmail);

                if (user == null)
                {
                    Response.StatusCode = 403;
                    var errorObj = new
                    {
                        error = "Invalid email or password."
                    };
                    await Response.WriteAsync(JsonConvert.SerializeObject(errorObj, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                    return;
                }

                string hashedPassword = Hasher.Hash(password, user.Salt);

                if(user == null || user.UserPassword != hashedPassword)
                {
                    Response.StatusCode = 403;
                    var errorObj = new
                    {
                        error = "Invalid email or password."
                    };
                    await Response.WriteAsync(JsonConvert.SerializeObject(errorObj, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                    return;
                }
                
                var identity = GetIdentity(user);
                if (identity == null || user == null)
                {
                    Response.StatusCode = 403;
                    var errorObj = new
                    {
                        error = "Invalid email or password."
                    };
                    await Response.WriteAsync(JsonConvert.SerializeObject(errorObj, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                    return;
                }

                // сериализация ответа
                string response = await GetAuthResponse(identity, user);

                if(!String.IsNullOrEmpty(device))
                {
                    user.DeviceAtLastLogin = device;
                    await _veroVrContext.SaveChangesAsync();
                }

                string[] supportedPlatforms =
                    _configuration.GetSection("MobileDevices:SupportedPlatforms").Get<string[]>();

                if (!String.IsNullOrEmpty(deviceType) && supportedPlatforms != null
                    && supportedPlatforms.Length > 0)
                {
                    string[] devicePlatform = deviceType.Split(" ");

                    if (devicePlatform.Length >= 2)
                    {
                        string deviceOS = devicePlatform[0];

                        if (supportedPlatforms.Select(s => s.ToLower()).Contains(deviceOS.ToLower()))
                        {
                            user.DeviceTypeAtLastLogin = deviceType;
                            await _veroVrContext.SaveChangesAsync();
                        }
                    }
                }
                else
                {
                    user.DeviceTypeAtLastLogin = null;
                    await _veroVrContext.SaveChangesAsync();
                }

                if (user.UserRole == UserRole.Customer)
                {
                    _statisticsContext
                        .UserLoginActivities
                        .Add(new UserLoginActivity
                        {
                            UserId = user.UserId,
                            Device = device,
                            DeviceType = deviceType,
                            EventDateStamp = DateTime.Now
                        });

                    await _statisticsContext.SaveChangesAsync();
                }

                await Response.WriteAsync(response);
            }
            catch (Exception ex)
            {
                _logger.LogError((int)LoggingEvents.ERROR,
                       LogMessageFormatter.GetErrorMessage("An error occurred when calling /api/login", ex));

                Response.StatusCode = 500;
                await Response.WriteAsync(ex.ToString());
            }
        }

        private async Task<string> GetAuthResponse(ClaimsIdentity identity, User user)
        {
            var now = DateTime.UtcNow;
            // создаем JWT-токен
            var jwt = new JwtSecurityToken(
                    issuer: AuthOptions.ISSUER,
                    audience: AuthOptions.AUDIENCE,
                    notBefore: now,
                    claims: identity.Claims,
                    expires: now.Add(TimeSpan.FromMinutes(AuthOptions.LIFETIME)),
                    signingCredentials: new SigningCredentials(AuthOptions.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            var response = new
            {
                token = encodedJwt,
                id = user.UserId,
                mail = user.UserEmail,
                role = (int)user.UserRole,
                roleName = user.UserRole.ToString(),
                companyName = user?.UserCompanyName,
                profile = new
                {
                    name = identity.Name,
                    avatarUrl = user?.UserAvatarUrl,
                    phone = user?.UserPhoneNumber,
                    facebookLink = user?.UserFacebook,
                    ocupation = user?.Occupation,
                    country = user?.UserCountry,
                    city = user?.UserCity,
                    address = user?.UserAddress,
                    ageRange = user?.UserAgeRange
                },
                countriesInfo = new
                {
                    pathFile = "/handbooks/countries.min.json",
                    dateOfChange = (await _fileManager
                    .GetLastModifiedFile("/handbooks/countries.min.json"))
                    .Ticks
                },
                error = string.Empty,
            };

            return JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented });
        }

        private async Task<User> GetUser(string email, string password)
        {
            // tcp:192.168.1.103,1433
            User user =
               await _veroVrContext
               .Users
               .SingleOrDefaultAsync(u => u.UserEmail == email && u.UserPassword == password);
            return user;
        }

        private ClaimsIdentity GetIdentity(User user)
        {
            //var users = await _veroVrContext.Users.Include(e => e.UserProjects);
            //user = users.FirstOrDefault(u => u.UserEmail == email && u.UserPassword == password);
            if (user != null)
            {
                var claims = new List<Claim>
                {
                    new Claim(ClaimsIdentity.DefaultNameClaimType, user.UserDisplayName),
                    new Claim(ClaimsIdentity.DefaultRoleClaimType, user.UserRole.ToString()),
                    new Claim("UserId", user.UserId.ToString())
                };
                ClaimsIdentity claimsIdentity = new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType,
                    ClaimsIdentity.DefaultRoleClaimType);
                return claimsIdentity;
            }

            // если пользователя не найдено
            return null;
        }

        [AllowAnonymous]
        [HttpPost("/api/SubmitResetPassword")]
        public async Task SubmitResetPassword()
        {
            try
            {
                string password = null;
                string token = null;

                try
                {
                    if (Request.HasFormContentType)
                    {
                        password = Request.Form["password"];
                        token = Request.Form["token"];
                    }
                    else
                    {
                        dynamic userInfo;

                        using (StreamReader stream = new StreamReader(Request.Body))
                        {
                            using (JsonReader reader = new JsonTextReader(stream))
                            {
                                JsonSerializer serializer = new JsonSerializer();
                                userInfo = serializer.Deserialize<dynamic>(reader);
                            }
                        }

                        password = userInfo.password;
                        token = userInfo.token;
                    }
                }
                catch { }

                if(String.IsNullOrEmpty(token) || String.IsNullOrEmpty(password))
                {
                    Response.StatusCode = 400;

                    var errorObj = new
                    {
                        error = "No token or password was sent."
                    };
                    await Response.WriteAsync(JsonConvert.SerializeObject(errorObj, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                    return;
                }

                string emailInMemory;

                bool isExistTokenInMemory =
                    _memoryCache.TryGetValue(token, out emailInMemory);

                Response.ContentType = "application/json";

                if (isExistTokenInMemory)
                {
                    var user = 
                        await _veroVrContext.Users.SingleOrDefaultAsync(u => u.UserEmail == emailInMemory);

                    if (user == null)
                    {
                        Response.StatusCode = 400;

                        var errorObj = new
                        {
                            error = "User with that e-mail is not registered in the system."
                        };
                        await Response.WriteAsync(JsonConvert.SerializeObject(errorObj, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                        return;
                    }

                    string salt = Hasher.GetSalt();

                    user.Salt = salt;
                    user.UserPassword = Hasher.Hash(password, salt);

                    await _veroVrContext.SaveChangesAsync();

                    //удаляем из кэша токен и метку e-mail
                    _memoryCache.Remove(emailInMemory);
                    _memoryCache.Remove(token);

                    string currentDomainUrl = _httpContextAccessor.HttpContext?.Request?.GetDisplayUrl();
                    Uri uri = new Uri(currentDomainUrl);
                    string domainUrl = uri.Scheme + Uri.SchemeDelimiter + uri.Host;

                    HttpClient hc = new HttpClient();
                    HttpResponseMessage result =
                        await
                        hc.GetAsync($"{domainUrl}/account/resetPasswordComplete.html");

                    Stream htmlStream = await result.Content.ReadAsStreamAsync();

                    HtmlDocument doc = new HtmlDocument();

                    doc.Load(htmlStream);

                    HtmlNode userNameSpanNode = doc.DocumentNode.Descendants("span")
                        .FirstOrDefault(e => e.Id == "userName");

                    if (userNameSpanNode == null)
                    {
                        throw new Exception("Element for inserting a user name wasn't found on page.");
                    }

                    // create a <userDisplayNameNode> element
                    var userDisplayNameTextNode = HtmlNode.CreateNode(HtmlDocument.HtmlEncode(user.UserDisplayName));
                    // append <title> to <head>
                    userNameSpanNode.AppendChild(userDisplayNameTextNode);

                    string sendHtml = null;

                    MemoryStream memStream = new MemoryStream();
                    doc.Save(memStream);

                    //convert memory stream to string
                    memStream.Position = 0;
                    using (StreamReader streamReader = new StreamReader(memStream))
                    {
                        sendHtml = streamReader.ReadToEnd();
                    }

                    await _emailService.SendEmailAsync(emailInMemory, "Password reset", sendHtml);

                    var response = new
                    {
                        message = "Password was changed successfully.",
                        succeeded = true,
                        error = string.Empty
                    };

                    await Response.WriteAsync(JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                }
                else
                {
                    var response = new
                    {
                        message = "This token isn't valid",
                        succeeded = false,
                        error = string.Empty
                    };
                    await Response
                    .WriteAsync(JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                }
            }
            catch (Exception ex)
            {
                _logger.LogError((int)LoggingEvents.ERROR,
                LogMessageFormatter.GetErrorMessage("An error occurred when calling /api/SubmitResetPassword", ex));

                Response.StatusCode = 500;
                await Response.WriteAsync(ex.ToString());
            }
        }

        [AllowAnonymous]
        [HttpPost("/api/CheckoutResetPassword")]
        public async Task CheckoutPasswordReset()
        {
            try
            {
                string token = null;

                try
                {
                    if (Request.HasFormContentType)
                    {
                        token = Request.Form["token"];
                    }
                    else
                    {
                        dynamic userInfo;

                        using (StreamReader stream = new StreamReader(Request.Body))
                        {
                            using (JsonReader reader = new JsonTextReader(stream))
                            {
                                JsonSerializer serializer = new JsonSerializer();
                                userInfo = serializer.Deserialize<dynamic>(reader);
                            }
                        }

                        token = userInfo.token;
                    }
                }
                catch { }

                if (String.IsNullOrEmpty(token) == false)
                {
                    string email;

                    bool isExistToken = _memoryCache.TryGetValue(token, out email);

                    Response.ContentType = "application/json";

                    if (isExistToken)
                    {
                        var response = new
                        {
                            message = "This token is valid",
                            succeeded = true,
                            error = string.Empty
                        };
                        await Response
                        .WriteAsync(JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                    }
                    else
                    {
                        var response = new
                        {
                            message = "This token isn't valid",
                            succeeded = false,
                            error = string.Empty
                        };
                        await Response
                        .WriteAsync(JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError((int)LoggingEvents.ERROR,
                LogMessageFormatter.GetErrorMessage("An error occurred when calling /api/CheckoutResetPassword", ex));

                Response.StatusCode = 500;
                await Response.WriteAsync(ex.ToString());
            }
        }

        [AllowAnonymous]
        [HttpPost("/api/ResetPassword")]
        public async Task ResetPassword()
        {
            try
            {
                string email = null;

                try
                {
                    if (Request.HasFormContentType)
                    {
                        email = Request.Form["email"];
                    }
                    else
                    {
                        dynamic userInfo;

                        using (StreamReader stream = new StreamReader(Request.Body))
                        {
                            using (JsonReader reader = new JsonTextReader(stream))
                            {
                                JsonSerializer serializer = new JsonSerializer();
                                userInfo = serializer.Deserialize<dynamic>(reader);
                            }
                        }

                        email = userInfo.email;
                    }
                }
                catch { }

                Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){1,10})+)$");
                Match match = regex.Match(email);

                bool isEmail = match.Success;

                if (isEmail == false)
                {
                    Response.StatusCode = 400;
                    var errorObj = new
                    {
                        error = "Invalid email format."
                    };
                    await Response.WriteAsync(JsonConvert.SerializeObject(errorObj, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                    return;
                }

                string emailPrepared = email.ToLower().Trim();

                var user = await _veroVrContext.Users.SingleOrDefaultAsync(u => u.UserEmail == emailPrepared);

                if (user == null)
                {
                    Response.StatusCode = 400;

                    var errorObj = new
                    {
                        error = "User with that e-mail is not registered in the system."
                    };
                    await Response.WriteAsync(JsonConvert.SerializeObject(errorObj, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                    return;
                }

                string currentDomainUrl = _httpContextAccessor.HttpContext?.Request?.GetDisplayUrl();
                Uri uri = new Uri(currentDomainUrl);
                string domainUrl = uri.Scheme + Uri.SchemeDelimiter + uri.Host;

                string token = CodeGenerator.GetCode(length: 128);

                string recoveryPasswordLink = $"{domainUrl}/auth/reset-password?token={token}";

                //string recoveryPasswordLink = $"http://localhost:4200/auth/reset-password?token=" + token;
                string tokenInMemory;

                bool isExistTokenForEmailInMemory =
                    _memoryCache.TryGetValue(user.UserEmail, out tokenInMemory);

                //если пользователь уже запрашивал сброс пароля и токен ещё действителен
                if(isExistTokenForEmailInMemory)
                {
                    //удаляем из кэша токен и метку e-mail
                    _memoryCache.Remove(user.UserEmail);
                    _memoryCache.Remove(tokenInMemory);
                }
                else
                {
                    string experationTime = 
                        _configuration["AccountSettings:ExpirationTimeForPasswordResetLinkMin"]
                        .ToString();

                    int countExpMin;
                    bool isSetExpTime = int.TryParse(experationTime, out countExpMin);

                    TimeSpan timeExp;

                    if (isSetExpTime)
                    {
                        timeExp = TimeSpan.FromMinutes(countExpMin);
                    }
                    else
                    {
                        timeExp = TimeSpan.FromHours(3);
                    }

                    // сохраняем в кэше токен и метку e-mail
                    _memoryCache.Set(token, user.UserEmail,
                  new MemoryCacheEntryOptions().SetAbsoluteExpiration(timeExp));

                    _memoryCache.Set(user.UserEmail, token,
                   new MemoryCacheEntryOptions().SetAbsoluteExpiration(timeExp));
                }

                HttpClient hc = new HttpClient();
                HttpResponseMessage result =
                    await
                    hc.GetAsync($"{domainUrl}/account/resetPassword.html");

                Stream htmlStream = await result.Content.ReadAsStreamAsync();

                HtmlDocument doc = new HtmlDocument();

                doc.Load(htmlStream);

                HtmlNode userName = doc.DocumentNode.Descendants("span")
                   .FirstOrDefault(e => e.Id == "userName");

                if (userName == null)
                {
                    throw new Exception("Element for inserting a user name wasn't found on page.");
                }

                HtmlNode resetLink = doc.DocumentNode.Descendants("a")
                    .FirstOrDefault(e => e.Id == "resetLink");

                if (resetLink == null)
                {
                    throw new Exception("Reset link wasn't found on page.");
                }

                // create a <userDisplayNameNode> element
                var userDisplayNameNode = HtmlNode.CreateNode(HtmlDocument.HtmlEncode(user.UserDisplayName));
                // append <title> to <head>
                userName.AppendChild(userDisplayNameNode);

                String encodedLink = HtmlDocument.HtmlEncode(recoveryPasswordLink);
                resetLink.SetAttributeValue("href", encodedLink);
                resetLink.AppendChild(HtmlNode.CreateNode(encodedLink));

                string sendHtml = null;

                MemoryStream memStream = new MemoryStream();
                doc.Save(memStream);

                //convert memory stream to string
                memStream.Position = 0;
                using (StreamReader streamReader = new StreamReader(memStream))
                {
                    sendHtml = streamReader.ReadToEnd();
                }

                await _emailService.SendEmailAsync(email, "Password reset", sendHtml);

                Response.ContentType = "application/json";
                var response = new
                {
                    succeeded = true,
                    error = string.Empty
                };

                await Response.WriteAsync(JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented }));
            }
            catch (Exception ex)
            {
                _logger.LogError((int)LoggingEvents.ERROR,
                LogMessageFormatter.GetErrorMessage("An error occurred when calling /api/ResetPassword", ex));

                Response.StatusCode = 500;
                await Response.WriteAsync(ex.ToString());
            }
        }

        [AllowAnonymous]
        [HttpPost("/api/CreateUserProfile")]
        public async Task CreateUserProfile()
        {
            try
            {
                dynamic userProfileInfo;

                using (StreamReader stream = new StreamReader(Request.Body))
                {
                    using (JsonReader reader = new JsonTextReader(stream))
                    {
                        JsonSerializer serializer = new JsonSerializer();
                        userProfileInfo = serializer.Deserialize<dynamic>(reader);
                    }
                }

                User newCustomer = new User();

                newCustomer.UserEmail = userProfileInfo.mail?.ToString()?.Trim();
                newCustomer.UserDisplayName = userProfileInfo.profile.name?.ToString()?.Trim();
                newCustomer.UserPhoneNumber = userProfileInfo.profile.phone?.ToString()?.Trim();
                newCustomer.UserFacebook = userProfileInfo.profile.facebookLink?.ToString()?.Trim();
                newCustomer.Occupation = userProfileInfo.profile.ocupation?.ToString()?.Trim();
                newCustomer.UserCountry = userProfileInfo.profile.country?.ToString()?.Trim();
                newCustomer.UserCity = userProfileInfo.profile.city?.ToString()?.Trim();
                newCustomer.UserAddress = userProfileInfo.profile.address?.ToString()?.Trim();
                newCustomer.UserAgeRange = userProfileInfo.profile?.ageRange?.ToString()?.Trim();
                newCustomer.UserRole = UserRole.Customer;
                newCustomer.DateOfRegistration = DateTime.Now;

                string deviceType = userProfileInfo.profile?.deviceType?.ToString()?.Trim();

                string[] supportedPlatforms = 
                    _configuration.GetSection("MobileDevices:SupportedPlatforms").Get<string[]>();

                if (!String.IsNullOrEmpty(deviceType) && supportedPlatforms != null
                    && supportedPlatforms.Length > 0)
                {
                    string[] devicePlatform = deviceType.Split(" ");

                    if(devicePlatform.Length >= 2)
                    {
                        string deviceOS = devicePlatform[0];

                        if(supportedPlatforms.Select(s => s.ToLower()).Contains(deviceOS.ToLower()))
                        {
                            newCustomer.DeviceTypeAtLastLogin = deviceType;
                        }
                    }
                }

                newCustomer.DeviceAtLastLogin = userProfileInfo.profile?.device;
                newCustomer.FacebookLikes = userProfileInfo.profile?.facebookLikes?.ToObject<string[]>();
                newCustomer.DateOfRegistration = DateTime.Now;

                if (String.IsNullOrEmpty(newCustomer.UserDisplayName))
                {
                    Response.StatusCode = 400;
                    var errorObj = new
                    {
                        error = "Username must be specified."
                    };
                    await Response.WriteAsync(JsonConvert.SerializeObject(errorObj, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                    return;
                }

                string password = userProfileInfo.token?.ToString()?.Trim();

                if (String.IsNullOrEmpty(password))
                {
                    Response.StatusCode = 400;
                    var errorObj = new
                    {
                        error = "Password must be specified."
                    };
                    await Response.WriteAsync(JsonConvert.SerializeObject(errorObj, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                    return;
                }

                string salt = Hasher.GetSalt();

                newCustomer.Salt = salt;
                newCustomer.UserPassword = Hasher.Hash(password, salt);

                if (String.IsNullOrEmpty(newCustomer.UserPhoneNumber))
                {
                    Response.StatusCode = 400;
                    var errorObj = new
                    {
                        error = "Phone number must be specified."
                    };
                    await Response.WriteAsync(JsonConvert.SerializeObject(errorObj, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                    return;
                }

                newCustomer.UserEmail = newCustomer.UserEmail
                    .Trim().ToLower();

                Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){1,10})+)$");
                Match match = regex.Match(newCustomer.UserEmail);

                bool isEmail = match.Success;

                if (isEmail == false)
                {
                    Response.StatusCode = 400;
                    var errorObj = new
                    {
                        error = "Invalid email format."
                    };
                    await Response.WriteAsync(JsonConvert.SerializeObject(errorObj, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                    return;
                }

                string preparedEmail = newCustomer.UserEmail.ToLower().Trim();

                bool isExistUserWithInputEmail =
                    _veroVrContext
                    .Users
                    .AsNoTracking()
                    .Any(u => u.UserEmail == preparedEmail);

                if (isExistUserWithInputEmail)
                {
                    Response.StatusCode = 400;
                    var errorObj = new
                    {
                        error = "User with this email already registered in the system."
                    };
                    await Response.WriteAsync(JsonConvert.SerializeObject(errorObj, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                    return;
                }

                _veroVrContext.Users.Add(newCustomer);

                await _veroVrContext.SaveChangesAsync();

                string avatarUrl = userProfileInfo.profile.avatarUrl;

                if (String.IsNullOrEmpty(avatarUrl))
                {
                    string pathDefaultAvatar = "/content/user/avatar/0ca33b1c-a523-4a1e-aef7-fe9919435774.jpg";
                    //сохраняем информацию об объекте в базу данных
                    newCustomer.UserAvatarUrl = pathDefaultAvatar;

                    await _veroVrContext.SaveChangesAsync();
                }
                else if (avatarUrl.Contains("base64"))
                {
                    string search = "base64,";

                    int pos = avatarUrl.IndexOf(search);
                    int count = pos + search.Length;
                    userProfileInfo.profile.avatarUrl = avatarUrl.Remove(0, count);
                    string base64Img = userProfileInfo.profile.avatarUrl;
                    byte[] userIcon = Convert.FromBase64String(base64Img);
                    string relativePathForAccess = await _fileManager.SaveUserAvatar(userIcon, newCustomer.UserId);

                    //сохраняем информацию об объекте в базу данных
                    newCustomer.UserAvatarUrl = relativePathForAccess;
                    newCustomer.DateOfRegistration = DateTime.Now;

                    await _veroVrContext.SaveChangesAsync();
                }
                

                User user = await GetUser(newCustomer.UserEmail,
                    newCustomer.UserPassword);

                var identity = GetIdentity(user);
                if (identity == null || user == null)
                {
                    Response.StatusCode = 403;
                    var errorObj = new
                    {
                        error = "Invalid email or password."
                    };
                    await Response.WriteAsync(JsonConvert.SerializeObject(errorObj, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                    return;
                }

                HttpClient hc = new HttpClient();
                HttpResponseMessage result =
                    await
                    hc.GetAsync($"http://{HttpContext.Request.Host}/account/createCustomerProfile.html");

                Stream htmlStream = await result.Content.ReadAsStreamAsync();

                HtmlDocument doc = new HtmlDocument();

                doc.Load(htmlStream);

                HtmlNode userNameNode = doc.DocumentNode.Descendants("span")
                    .FirstOrDefault(e => e.Id == "userName");

                if(userNameNode == null)
                {
                    throw new Exception("Element for inserting a user name wasn't found on page.");
                }

                userNameNode.InnerHtml = user.UserDisplayName;

                string sendHtml = doc.DocumentNode.OuterHtml;

                await _emailService.SendEmailAsync(user.UserEmail,
               "Your profile was created successfully", sendHtml);

                // сериализация ответа
                Response.ContentType = "application/json";
                string response = await GetAuthResponse(identity, user);
                await Response.WriteAsync(response);
            }
            catch (Exception ex)
            {
                _logger.LogError((int)LoggingEvents.ERROR,
                LogMessageFormatter.GetErrorMessage("An error occurred when calling /api/CreateUserProfile", ex));

                Response.StatusCode = 500;
                await Response.WriteAsync(ex.ToString());
            }
        }
    }
}
