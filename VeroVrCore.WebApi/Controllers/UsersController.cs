﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using VeroVrCore.Common.Models;
using VeroVrCore.Common.Security;
using VeroVrCore.DataLayer.EntityFramework;
using VeroVrCore.WebApi.Services;
using HtmlAgilityPack;
using VeroVrCore.Common.Infrastructure;
using System.Net.Mime;
using Microsoft.AspNetCore.Hosting;
using VeroVrCore.DataLayer.EntityFramework.VeroVrCore.DataLayer.EntityFramework;
using Microsoft.AspNetCore.Http.Extensions;

namespace VeroVrCore.WebApi.Controllers
{
    public class UsersController : Controller
    {
        private VeroVrContext _veroVrContext;
        private VeroVrStatisticsContext _veroVrStatisticsContext;
        private FileManagerService _fileManager;
        private EmailService _emailService;
        private NotificationService _notificationService;
        private IConfiguration _configuration;
        private IHostingEnvironment _hostingEnvironment;
        private IHttpContextAccessor _httpContextAccessor;

        public UsersController(VeroVrContext veroVrContext,
            FileManagerService fileManager,
            EmailService emailService,
            NotificationService notificationService,
            IConfiguration configuration,
            IHostingEnvironment hostingEnvironment,
            VeroVrStatisticsContext veroVrStatisticsContext,
            IHttpContextAccessor httpContextAccessor)
        {
            _veroVrContext = veroVrContext;
            _fileManager = fileManager;
            _emailService = emailService;
            _notificationService = notificationService;
            _configuration = configuration;
            _hostingEnvironment = hostingEnvironment;
            _veroVrStatisticsContext = veroVrStatisticsContext;
            _httpContextAccessor = httpContextAccessor;
        }

        [Route("/privacy-policy")]
        public IActionResult GetPrivacyPolicy()
        {
            string path = _hostingEnvironment.WebRootPath +
                _configuration["PrivacyPolicy:PathToFile"].ToString();

            string color = _configuration["PrivacyPolicy:BackgroundColor"]?.ToString();

            if (String.IsNullOrEmpty(color))
            {
                color = "#fff";
            }

            if (System.IO.File.Exists(path))
            {
                string doc = System.IO.File.ReadAllText(path);

                doc = @"<!DOCTYPE html>
                        <html>
                        <head>
                        <link rel='icon' type='image/x-icon' href='../icon.png'>
                        <meta charset='utf-8' />
                        <title>Privacy policy</title>
                        </head>
                        <body bgcolor='" + color + "'>" + doc + "</body></html>";

                string fileNameToSave = "Privacy policy.html";

                string mimeType = "text/html";

                ContentDisposition cd = new ContentDisposition
                {
                    FileName = fileNameToSave,
                    Inline = true
                };

                Response.Headers.Add("Content-Disposition", cd.ToString());

                byte[] bytes = Encoding.ASCII.GetBytes(doc);

                return File(bytes, mimeType);
            }

            return new EmptyResult();
        }

        [Route("/terms-of-use")]
        public IActionResult GetTermsOfUse()
        {
            string path = _hostingEnvironment.WebRootPath +
                _configuration["TermsOfUse:PathToFile"].ToString();

            string color = _configuration["TermsOfUse:BackgroundColor"]?.ToString();

            if (String.IsNullOrEmpty(color))
            {
                color = "#fff";
            }

            if (System.IO.File.Exists(path))
            {
                string doc = System.IO.File.ReadAllText(path);

                doc = @"<!DOCTYPE html>
                        <html>
                        <head>
                        <link rel='icon' type='image/x-icon' href='../icon.png'>
                        <meta charset='utf-8' />
                        <title>Terms of use</title>
                        </head>
                        <body bgcolor='" + color + "'>" + doc + "</body></html>";

                string fileNameToSave = "Terms of use.html";

                string mimeType = "text/html";

                ContentDisposition cd = new ContentDisposition
                {
                    FileName = fileNameToSave,
                    Inline = true
                };

                Response.Headers.Add("Content-Disposition", cd.ToString());

                byte[] bytes = Encoding.ASCII.GetBytes(doc);

                return File(bytes, mimeType);
            }

            return new EmptyResult();
        }

        [Authorize]
        [HttpPost("/api/UpdateUserProfile")]
        public async Task UpdateUserProfile()
        {
            try
            {
                string userID = User.FindFirst("UserId")?.Value;

                User user =
                    await _veroVrContext
                    .Users
                    .SingleOrDefaultAsync(u => u.UserId == int.Parse(userID));

                if (user == null)
                {
                    Response.StatusCode = 404;

                    var responseError = new
                    {
                        succeeded = false,
                        error = "User wasn't found in database.",
                    };

                    // сериализация ответа
                    Response.ContentType = "application/json";
                    await Response.WriteAsync(JsonConvert.SerializeObject(responseError, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                    return;
                }

                dynamic userProfileInfo;

                using (StreamReader stream = new StreamReader(Request.Body))
                {
                    using (JsonReader reader = new JsonTextReader(stream))
                    {
                        JsonSerializer serializer = new JsonSerializer();
                        userProfileInfo = serializer.Deserialize<dynamic>(reader);
                    }
                }

                user.UserDisplayName = userProfileInfo.name?.ToString()?.Trim();
                user.UserPhoneNumber = userProfileInfo.phone?.ToString()?.Trim();
                user.UserFacebook = userProfileInfo.facebookLink?.ToString()?.Trim();
                user.Occupation = userProfileInfo.ocupation?.ToString()?.Trim();
                user.UserCountry = userProfileInfo.country?.ToString()?.Trim();
                user.UserCity = userProfileInfo.city?.ToString()?.Trim();
                user.UserAddress = userProfileInfo.address?.ToString()?.Trim();
                user.UserAgeRange = userProfileInfo?.ageRange?.ToString()?.Trim();

                await _veroVrContext.SaveChangesAsync();

                string userAvatarUrl = userProfileInfo.avatarUrl;

                string pathDefaultAvatar = "/content/user/avatar/0ca33b1c-a523-4a1e-aef7-fe9919435774.jpg";

                if (String.IsNullOrEmpty(userAvatarUrl))
                {
                    //сохраняем информацию об объекте в базу данных
                    user.UserAvatarUrl = pathDefaultAvatar;

                    await _veroVrContext.SaveChangesAsync();
                }
                else if (userAvatarUrl.Contains("base64"))
                {
                    if (user.UserAvatarUrl != pathDefaultAvatar)
                    {
                        // путь к папке
                        bool isExistminiImage =
                            await _fileManager.IsFileExistsAsync(user.UserAvatarUrl);

                        if (isExistminiImage)
                        {
                            await _fileManager.DeleteFileAsync(user.UserAvatarUrl);
                        }
                    }

                    string search = "base64,";
                    int pos = userAvatarUrl.IndexOf(search);
                    int count = pos + search.Length;
                    userProfileInfo.avatarUrl = userAvatarUrl.Remove(0, count);
                    string base64Img = userProfileInfo.avatarUrl;
                    byte[] userIcon = Convert.FromBase64String(base64Img);

                    string relativePathForAccess = await _fileManager.SaveUserAvatar(userIcon, user.UserId);

                    //сохраняем информацию об объекте в базу данных
                    user.UserAvatarUrl = relativePathForAccess;

                    await _veroVrContext.SaveChangesAsync();
                }

                User savedCustomer = await _veroVrContext
                    .Users
                    .AsNoTracking()
                    .SingleAsync(c => c.UserId == user.UserId);

                Response.StatusCode = 200;

                var response =
                    new
                    {
                        name = savedCustomer.UserDisplayName,
                        avatarUrl = savedCustomer.UserAvatarUrl,
                        phone = savedCustomer.UserPhoneNumber,
                        facebookLink = savedCustomer.UserFacebook,
                        ocupation = savedCustomer.Occupation,
                        country = savedCustomer.UserCountry,
                        city = savedCustomer.UserCity,
                        address = savedCustomer.UserAddress,
                        ageRange = savedCustomer.UserAgeRange
                    };

                // сериализация ответа
                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented }));
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                await Response.WriteAsync(ex.ToString());
            }
        }

        [Authorize]
        [HttpPost("/api/ActivateProjectCode")]
        public async Task<IActionResult> ActivateProjectCode()
        {
            try
            {
                string userID = User.FindFirst("UserId")?.Value;

                User user =
                    await _veroVrContext
                    .Users
                    .Include(u => u.UserProjects)
                    .SingleOrDefaultAsync(u => u.UserId == int.Parse(userID));

                if (user == null)
                {
                    return NotFound();
                }

                string code = Request.Headers["activateProjectCode"];

                if (String.IsNullOrEmpty(code))
                {
                    return BadRequest();
                }

                var project = await _veroVrContext
                    .Projects
                    .SingleOrDefaultAsync(p => p.ProjectCode == code);

                if (project == null)
                {
                    Response.ContentType = "application/json";
                    var responseError = new
                    {
                        succeeded = false,
                        error = "Project with the specified code was not found."
                    };

                    return BadRequest(responseError);
                }

                var userProject =
                    user.UserProjects
                    .SingleOrDefault(up => up.ProjectId == project.ProjectId);

                if (userProject != null)
                {
                    Response.ContentType = "application/json";
                    var responseError = new
                    {
                        succeeded = false,
                        error = "The user already has the requested project."
                    };

                    return BadRequest(responseError);
                }

                user.UserProjects.Add(new UserProject
                {
                    User = user,
                    Project = project
                });

                await _veroVrContext.SaveChangesAsync();

                return RedirectToAction("GetAllProjectsOptimizedByCurrentUser",
                    "Projects");
            }
            catch (Exception ex)
            {
                return new StatusCodeResult(500);
            }
        }

        [Authorize(Roles = "SuperAdmin, SuperManager, Manager, SuperManagerWithScript")]
        [HttpPost("/api/SendNotification")]
        public async Task SendNotification(string[] emails, string title, string message)
        {
            try
            {
                string userID = User.FindFirst("UserId")?.Value;

                User user =
                    await _veroVrContext
                    .Users
                    .Include(u => u.UserProjects)
                    .SingleOrDefaultAsync(u => u.UserId == int.Parse(userID));

                if (user == null)
                {
                    Response.ContentType = "application/json";
                    Response.StatusCode = 404;
                    var errorObj = new
                    {
                        error = "User wasn't found."
                    };
                    await Response.WriteAsync(JsonConvert.SerializeObject(errorObj, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                    return;
                }

                if (emails == null || emails.Length == 0)
                {
                    await _notificationService.NotifyAsync(title, message, emails);
                }
                else
                {
                    IEnumerable<IEnumerable<string>> emailsParts = emails.Split(size: 5);

                    foreach (var emailPart in emailsParts)
                    {
                        await _notificationService.NotifyAsync(title, message, emailPart.ToArray());
                    }
                }

                Response.ContentType = "application/json";

                var response = new
                {
                    succeeded = true,
                    error = string.Empty
                };

                await Response.WriteAsync(JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                return;
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                await Response.WriteAsync(ex.ToString());
            }
        }

        [Authorize(Roles = "SuperAdmin, SuperManager, Manager, SuperManagerWithScript")]
        [HttpGet("/api/GetAllUsers")]
        public async Task GetAllUsers([FromQuery] string filter)
        {
            string userID = User.FindFirst("UserId")?.Value;

            User user =
                await _veroVrContext
                .Users
                .Include(u => u.UserProjects)
                .AsNoTracking()
                .SingleOrDefaultAsync(u => u.UserId == int.Parse(userID));

            if (user == null)
            {
                Response.ContentType = "application/json";
                Response.StatusCode = 404;
                var errorObj = new
                {
                    succeded = false,
                    error = "User wasn't found."
                };
                await Response.WriteAsync(JsonConvert.SerializeObject(errorObj, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                return;
            }

            var allUsers = _veroVrContext
                .Users
                .Include(e => e.UserProjects)
                .AsNoTracking()
                .Where(u => u.UserRole != UserRole.SuperAdmin);

            if (!String.IsNullOrEmpty(filter))
            {
                allUsers = allUsers.Where(u => u.UserRole == Enum.Parse<UserRole>(filter));
            }

            if (user.UserRole == UserRole.Manager)
            {
                allUsers = allUsers.Where(u => u.UserRole != UserRole.SuperManager
                && u.UserRole != UserRole.Manager
                && u.UserRole != UserRole.SuperManagerWithScript);
            }
            else if (user.UserRole == UserRole.SuperManager)
            {
                allUsers = allUsers.Where(u => u.UserRole != UserRole.SuperManager
                && u.UserRole != UserRole.SuperManagerWithScript);
            }

            var usersForResponse = allUsers.ToList().Select(u =>
                new
                {
                    userId = u.UserId,
                    name = u.UserDisplayName,
                    role = u.UserRole.ToString(),
                    ageRange = u.UserAgeRange,
                    ocupation = u.Occupation,
                    email = u.UserEmail,
                    phone = u.UserPhoneNumber,
                    facebookLink = u.UserFacebook,
                    country = u.UserCountry,
                    city = u.UserCity,
                    address = u.UserAddress,
                    companyName = u.UserCompanyName,
                    projectIds = u.UserProjects.Select(up => up.ProjectId)
                });

            var response = new
            {
                userList = usersForResponse,
                error = string.Empty
            };

            Response.ContentType = "application/json";
            await Response.WriteAsync(JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented }));
        }

        [Authorize(Roles = "SuperAdmin, SuperManager, Manager, SuperManagerWithScript")]
        [HttpGet("/api/GetAllUsersWithPagination")]
        public async Task GetAllUsersWithPagination([FromQuery] string filter, [FromQuery]int? pageSize,
                [FromQuery] int? page, [FromQuery] string userName)
        {
            if (!pageSize.HasValue || !page.HasValue)
            {
                Response.StatusCode = 400;
                var responseError = new
                {
                    succeeded = false,
                    error = "Not all parameters have been defined."
                };

                // сериализация ответа
                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonConvert.SerializeObject(responseError, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                return;
            }

            string userID = User.FindFirst("UserId")?.Value;

            User user =
                await _veroVrContext
                .Users
                .Include(u => u.UserProjects)
                .AsNoTracking()
                .SingleOrDefaultAsync(u => u.UserId == int.Parse(userID));

            if (user == null)
            {
                Response.ContentType = "application/json";
                Response.StatusCode = 404;
                var errorObj = new
                {
                    succeded = false,
                    error = "User wasn't found."
                };
                await Response.WriteAsync(JsonConvert.SerializeObject(errorObj, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                return;
            }

            var allUsers = _veroVrContext
                .Users
                .Include(e => e.UserProjects)
                .AsNoTracking()
                .Where(u => u.UserRole != UserRole.SuperAdmin);

            if (!String.IsNullOrEmpty(filter))
            {
                allUsers = allUsers.Where(u => u.UserRole == Enum.Parse<UserRole>(filter));
            }

            if (user.UserRole == UserRole.Manager)
            {
                allUsers = allUsers.Where(u => u.UserRole != UserRole.SuperManager
                && u.UserRole != UserRole.Manager
                && u.UserRole != UserRole.SuperManagerWithScript);
            }
            else if (user.UserRole == UserRole.SuperManager)
            {
                allUsers = allUsers.Where(u => u.UserRole != UserRole.SuperManager
                && u.UserRole != UserRole.SuperManagerWithScript);
            }

            allUsers = allUsers
                .Where(u => userName == null || u.UserDisplayName.ToLower().Contains(userName.ToLower().Trim()));

            int countUsers = allUsers.Count();

            allUsers = allUsers.OrderBy(u => u.UserId)
                .Skip((page.Value - 1) * pageSize.Value)
                .Take(pageSize.Value);

            var usersForResponse = allUsers.ToList().Select(u =>
                new
                {
                    userId = u.UserId,
                    name = u.UserDisplayName,
                    role = u.UserRole.ToString(),
                    ageRange = u.UserAgeRange,
                    ocupation = u.Occupation,
                    email = u.UserEmail,
                    phone = u.UserPhoneNumber,
                    facebookLink = u.UserFacebook,
                    country = u.UserCountry,
                    city = u.UserCity,
                    address = u.UserAddress,
                    companyName = u.UserCompanyName,
                    projectIds = u.UserProjects.Select(up => up.ProjectId)
                });

            var response = new
            {
                countUsers = countUsers,
                userList = usersForResponse,
                error = string.Empty
            };

            Response.ContentType = "application/json";
            await Response.WriteAsync(JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented }));
        }

        [Authorize(Roles = "SuperAdmin, SuperManager, Manager, SuperManagerWithScript")]
        [HttpGet("/api/GetUserById")]
        public async Task GetUserById([FromQuery] int? id)
        {
            if (id == null)
            {
                Response.StatusCode = 400;

                var responseError = new
                {
                    succeeded = false,
                    error = "Id parameter was not specified.",
                };

                // сериализация ответа
                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonConvert.SerializeObject(responseError, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                return;
            }

            var user = await _veroVrContext
                .Users
                .Include(e => e.UserProjects)
                .AsNoTracking()
                .SingleOrDefaultAsync(u => u.UserId == id);

            if (user == null)
            {
                Response.StatusCode = 404;

                var responseError = new
                {
                    succeeded = false,
                    error = "User wasn't found in database.",
                };

                // сериализация ответа
                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonConvert.SerializeObject(responseError, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                return;
            }

            var usersForResponse =
                new
                {
                    userId = user.UserId,
                    name = user.UserDisplayName,
                    email = user.UserEmail,
                    phone = user.UserPhoneNumber,
                    projectIds = user.UserProjects.Select(up => up.ProjectId)
                };

            var response = new
            {
                user = usersForResponse,
                error = string.Empty
            };

            Response.ContentType = "application/json";
            await Response.WriteAsync(JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented }));
        }

        [Authorize(Roles = "SuperAdmin, SuperManager, Manager, SuperManagerWithScript")]
        [HttpGet("/api/GetAllUserRoles")]
        public async Task GetAllUserRoles()
        {
            var response = new
            {
                userRolesList = Enum.GetNames(typeof(UserRole))
                .ToList()
                .Except(new[] { UserRole.SuperAdmin.ToString() }),
                error = string.Empty
            };

            Response.ContentType = "application/json";
            await Response.WriteAsync(JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented }));
        }

        [Authorize]
        [HttpGet("/api/GetShortUserInfo"), HttpPost("/api/GetShortUserInfo")]
        public async Task GetShortUserInfo()
        {
            string userID = User.FindFirst("UserId")?.Value;

            User user =
                await _veroVrContext
                .Users
                .AsNoTracking()
                .SingleOrDefaultAsync(u => u.UserId == int.Parse(userID));

            if (user == null)
            {
                Response.ContentType = "application/json";
                Response.StatusCode = 404;
                var errorObj = new
                {
                    error = "User wasn't found."
                };
                await Response.WriteAsync(JsonConvert.SerializeObject(errorObj, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                return;
            }

            var response = new
            {
                user = new
                {
                    name = user.UserDisplayName,
                    mail = user.UserEmail,
                    phone = user.UserPhoneNumber
                },
                error = string.Empty
            };

            Response.ContentType = "application/json";
            await Response.WriteAsync(JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented }));
        }

        [Authorize(Roles = "SuperAdmin, SuperManager, Manager, SuperManagerWithScript")]
        [HttpPost("/api/AddUser")]
        public async Task AddUser()
        {
            try
            {
                dynamic userProfileInfo;

                using (StreamReader stream = new StreamReader(Request.Body))
                {
                    using (JsonReader reader = new JsonTextReader(stream))
                    {
                        JsonSerializer serializer = new JsonSerializer();
                        userProfileInfo = serializer.Deserialize<dynamic>(reader);
                    }
                }

                User newCustomer = new User();

                newCustomer.UserEmail = userProfileInfo.profile.mail?.ToString()?.Trim()?.ToLower();
                newCustomer.UserDisplayName = userProfileInfo.profile.name?.ToString()?.Trim();
                newCustomer.UserPhoneNumber = userProfileInfo.profile.phone?.ToString()?.Trim();
                newCustomer.UserFacebook = userProfileInfo.profile.facebookLink?.ToString()?.Trim();
                newCustomer.Occupation = userProfileInfo.profile.ocupation?.ToString()?.Trim();
                newCustomer.UserCountry = userProfileInfo.profile.country?.ToString()?.Trim();
                newCustomer.UserCity = userProfileInfo.profile.city?.ToString()?.Trim();
                newCustomer.UserAddress = userProfileInfo.profile.address?.ToString()?.Trim();
                newCustomer.UserAgeRange = userProfileInfo.profile?.ageRange?.ToString()?.Trim();
                newCustomer.UserCompanyName = userProfileInfo.profile.companyName?.ToString()?.Trim();
                newCustomer.DateOfRegistration = DateTime.Now;

                string role = userProfileInfo.profile.role?.ToString()?.Trim();
                newCustomer.UserRole = Enum.Parse<UserRole>(role, ignoreCase: true);

                if (String.IsNullOrEmpty(newCustomer.UserDisplayName))
                {
                    Response.StatusCode = 400;
                    var errorObj = new
                    {
                        error = "Username must be specified."
                    };
                    await Response.WriteAsync(JsonConvert.SerializeObject(errorObj, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                    return;
                }

                if (String.IsNullOrEmpty(newCustomer.UserPhoneNumber))
                {
                    Response.StatusCode = 400;
                    var errorObj = new
                    {
                        error = "Phone number must be specified."
                    };
                    await Response.WriteAsync(JsonConvert.SerializeObject(errorObj, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                    return;
                }

                if (String.IsNullOrEmpty(newCustomer.UserEmail))
                {
                    Response.StatusCode = 400;
                    var errorObj = new
                    {
                        error = "Email must be specified."
                    };
                    await Response.WriteAsync(JsonConvert.SerializeObject(errorObj, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                    return;
                }

                Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){1,10})+)$");
                Match match = regex.Match(newCustomer.UserEmail);

                bool isEmail = match.Success;

                if (isEmail == false)
                {
                    Response.StatusCode = 400;
                    var errorObj = new
                    {
                        error = "Invalid email format."
                    };
                    await Response.WriteAsync(JsonConvert.SerializeObject(errorObj, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                    return;
                }

                string preparedEmail = newCustomer.UserEmail.ToLower().Trim();

                bool isExistUserWithInputEmail =
                    _veroVrContext
                    .Users
                    .AsNoTracking()
                    .Any(u => u.UserEmail == preparedEmail);

                if (isExistUserWithInputEmail)
                {
                    Response.StatusCode = 400;

                    var errorObj = new
                    {
                        error = "User with this email already registered in the system."
                    };

                    await Response.WriteAsync(JsonConvert.SerializeObject(errorObj, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                    return;
                }

                string salt = Hasher.GetSalt();
                string generatedPassword = CodeGenerator.GetCode(length: 5);

                newCustomer.Salt = salt;
                newCustomer.UserPassword = Hasher.Hash(generatedPassword, salt);

                _veroVrContext.Users.Add(newCustomer);
                await _veroVrContext.SaveChangesAsync();

                IEnumerable<int> idsProjects =
                    userProfileInfo
                    .profile
                    ?.projectIds
                    ?.ToObject<IEnumerable<int>>();

                if (idsProjects != null && idsProjects.Count() > 0)
                {
                    foreach (int projectId in idsProjects)
                    {
                        Project project =
                            await _veroVrContext
                            .Projects
                            .SingleOrDefaultAsync(u => u.ProjectId == projectId);

                        if (project == null)
                        {
                            Response.ContentType = "application/json";
                            Response.StatusCode = 404;
                            var errorObj = new
                            {
                                error = "Project wasn't found."
                            };
                            await Response.WriteAsync(JsonConvert.SerializeObject(errorObj, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                            return;
                        }

                        newCustomer.UserProjects.Add(
                        new UserProject
                        {
                            User = newCustomer,
                            Project = project
                        });

                        await _veroVrContext.SaveChangesAsync();
                    }
                }

                string userAvatarUrl = userProfileInfo.profile.avatarUrl;

                if (String.IsNullOrEmpty(userAvatarUrl))
                {
                    string pathDefaultAvatar = "/content/user/avatar/0ca33b1c-a523-4a1e-aef7-fe9919435774.jpg";
                    //сохраняем информацию об объекте в базу данных
                    newCustomer.UserAvatarUrl = pathDefaultAvatar;

                    await _veroVrContext.SaveChangesAsync();
                }
                else if (userAvatarUrl.Contains("base64"))
                {
                    // путь к папке
                    bool isExistminiImage =
                        await _fileManager.IsFileExistsAsync(newCustomer.UserAvatarUrl);

                    if (isExistminiImage)
                    {
                        await _fileManager.DeleteFileAsync(newCustomer.UserAvatarUrl);
                    }

                    string search = "base64,";
                    int pos = userAvatarUrl.IndexOf(search);
                    int count = pos + search.Length;
                    userProfileInfo.avatarUrl = userAvatarUrl.Remove(0, count);
                    string base64Img = userProfileInfo.avatarUrl;
                    byte[] userIcon = Convert.FromBase64String(base64Img);

                    string relativePathForAccess = await _fileManager.SaveUserAvatar(userIcon, newCustomer.UserId);

                    //сохраняем информацию об объекте в базу данных
                    newCustomer.UserAvatarUrl = relativePathForAccess;

                    await _veroVrContext.SaveChangesAsync();
                }

                HttpClient hc = new HttpClient();
                HttpResponseMessage result =
                    await
                    hc.GetAsync($"http://{HttpContext.Request.Host}/account/createUser.html");

                Stream htmlStream = await result.Content.ReadAsStreamAsync();

                HtmlDocument doc = new HtmlDocument();

                doc.Load(htmlStream);

                HtmlNode userNameNode = doc.DocumentNode.Descendants("span")
                    .FirstOrDefault(e => e.Id == "userName");

                if (userNameNode == null)
                {
                    throw new Exception("Element for inserting a user name wasn't found on page.");
                }

                userNameNode.InnerHtml = newCustomer.UserDisplayName;

                HtmlNode loginNode = doc.DocumentNode.Descendants("span")
                   .FirstOrDefault(e => e.Id == "login");

                if (loginNode == null)
                {
                    throw new Exception("Element for inserting a login wasn't found on page.");
                }

                loginNode.InnerHtml = newCustomer.UserEmail;

                HtmlNode password = doc.DocumentNode.Descendants("span")
                   .FirstOrDefault(e => e.Id == "password");

                if (password == null)
                {
                    throw new Exception("Element for inserting a password wasn't found on page.");
                }

                password.InnerHtml = generatedPassword;

                string sendHtml = doc.DocumentNode.OuterHtml;

                await _emailService.SendEmailAsync(newCustomer.UserEmail,
               "Your acount was created successfully", sendHtml);

                // сериализация ответа
                Response.ContentType = "application/json";
                var response = new
                {
                    profile = new
                    {
                        id = newCustomer.UserId,
                        name = newCustomer.UserDisplayName,
                        ocupation = newCustomer.Occupation,
                        mail = newCustomer.UserEmail,
                        phone = newCustomer.UserPhoneNumber,
                        avatarUrl = newCustomer.UserAvatarUrl,
                        facebookLink = newCustomer.UserFacebook,
                        country = newCustomer.UserCountry,
                        city = newCustomer.UserCity,
                        address = newCustomer.UserAddress,
                        ageRange = newCustomer.UserAgeRange,
                        companyName = newCustomer.UserCompanyName,
                        role = newCustomer.UserRole.ToString(),
                        projectIds = idsProjects
                    },
                    error = string.Empty
                };

                string jsonResponse = JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented });

                await Response.WriteAsync(jsonResponse);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                await Response.WriteAsync(ex.ToString());
            }
        }

        [Authorize]
        [HttpPost("/api/AddUserToProjects")]
        public async Task AddUserToProjects(int? userId, int[] projectIds)
        {
            if (userId == null)
            {
                Response.StatusCode = 400;
                var errorObj = new
                {
                    error = "Request parameters are incorrect."
                };
                await Response.WriteAsync(JsonConvert.SerializeObject(errorObj, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                return;
            }

            User user = await _veroVrContext
                                .Users
                                .Include(u => u.UserProjects)
                                .SingleOrDefaultAsync(u => u.UserId == userId);

            if (user == null)
            {
                Response.StatusCode = 400;
                var errorObj = new
                {
                    error = $"User with id={userId} wasn't found."
                };
                await Response.WriteAsync(JsonConvert.SerializeObject(errorObj, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                return;
            }

            if (projectIds != null && projectIds.Length > 0)
            {
                var projects = await _veroVrContext
                           .Projects
                           .Where(p => projectIds.Contains(p.ProjectId))
                           .ToListAsync();

                if (projects.Count == 0)
                {
                    Response.StatusCode = 400;
                    var errorObj = new
                    {
                        error = $"The projects were not found."
                    };
                    await Response.WriteAsync(JsonConvert.SerializeObject(errorObj, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                    return;
                }

                var oldIds = user.UserProjects.Select(up => up.ProjectId);

                var newIds = projects.Select(p => p.ProjectId).Except(oldIds);

                user.UserProjects = new List<UserProject> { };
                await _veroVrContext.SaveChangesAsync();

                foreach (var project in projects)
                {
                    user.UserProjects.Add(
                    new UserProject
                    {
                        User = user,
                        Project = project
                    });
                }

                await _veroVrContext.SaveChangesAsync();

                var newProjectsFiltered = projects.Where(p => newIds.Contains(p.ProjectId)).ToList();

                await SendNotificationAddUserToProjects(user, newProjectsFiltered);
            }
            else if(projectIds == null || projectIds.Length == 0)
            {
                user.UserProjects = new List<UserProject> { };
                await _veroVrContext.SaveChangesAsync();
            }

            Response.StatusCode = 200;

            var response = new
            {
                succeeded = true,
                error = string.Empty,
            };

            // сериализация ответа
            Response.ContentType = "application/json";
            await Response.WriteAsync(JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented }));
        }

        [Authorize(Roles = "SuperAdmin, SuperManager, Manager, SuperManagerWithScript")]
        [HttpDelete("/api/DeleteUser")]
        public async Task DeleteUser([FromQuery] int? id)
        {
            if (id == null)
            {
                Response.StatusCode = 400;
                return;
            }

            try
            {
                var user = await _veroVrContext
                    .Users
                    .SingleOrDefaultAsync(a => a.UserId == id);

                if (user == null)
                {
                    Response.StatusCode = 404;

                    var responseError = new
                    {
                        succeeded = false,
                        error = "User wasn't found in database.",
                    };

                    // сериализация ответа
                    Response.ContentType = "application/json";
                    await Response.WriteAsync(JsonConvert.SerializeObject(responseError, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                    return;
                }

                _veroVrContext.Users.Remove(user);

                //Remove avatar
                await RemoveUserAvatar(user);

                await _veroVrContext.SaveChangesAsync();
                await RemoveAllRelatedUserDataAsync(id.Value);

                HttpClient hc = new HttpClient();
                HttpResponseMessage result =
                    await
                    hc.GetAsync($"http://{HttpContext.Request.Host}/account/deleteCustomerProfile.html");

                Stream htmlStream = await result.Content.ReadAsStreamAsync();

                HtmlDocument doc = new HtmlDocument();

                doc.Load(htmlStream);

                HtmlNode userNameNode = doc.DocumentNode.Descendants("span")
                    .FirstOrDefault(e => e.Id == "userName");

                if (userNameNode == null)
                {
                    throw new Exception("Element for inserting a user name wasn't found on page.");
                }

                userNameNode.InnerHtml = user.UserDisplayName;

                string sendHtml = doc.DocumentNode.OuterHtml;

                await _emailService.SendEmailAsync(user.UserEmail,
               "Your profile was deleted successfully", sendHtml);

                Response.StatusCode = 200;

                var response = new
                {
                    succeeded = true,
                    error = string.Empty,
                };

                // сериализация ответа
                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented }));
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                await Response.WriteAsync(ex.ToString());
            }

            return;
        }

        [Authorize]
        [HttpPost("/api/DeleteUserProfile")]
        public async Task DeleteUserProfile()
        {
            string userID = User.FindFirst("UserId")?.Value;

            try
            {
                User currentUser =
                await _veroVrContext
                .Users
                .SingleOrDefaultAsync(u => u.UserId == int.Parse(userID));

                if (currentUser == null)
                {
                    Response.ContentType = "application/json";
                    Response.StatusCode = 404;
                    var errorObj = new
                    {
                        succeeded = false,
                        error = "Current user wasn't found."
                    };
                    await Response.WriteAsync(JsonConvert.SerializeObject(errorObj,
                        new JsonSerializerSettings { Formatting = Formatting.Indented }));
                    return;
                }

                int id = currentUser.UserId;

                _veroVrContext.Users.Remove(currentUser);
                await _veroVrContext.SaveChangesAsync();

                await RemoveAllRelatedUserDataAsync(id);

                //Remove avatar
                await RemoveUserAvatar(currentUser);

                string currentDomainUrl = _httpContextAccessor.HttpContext?.Request?.GetDisplayUrl();
                Uri uri = new Uri(currentDomainUrl);
                string domainUrl = uri.Scheme + Uri.SchemeDelimiter + uri.Host;

                HttpClient hc = new HttpClient();
                HttpResponseMessage result =
                    await
                    hc.GetAsync($"http://{HttpContext.Request.Host}/account/deleteCustomerProfile.html");

                Stream htmlStream = await result.Content.ReadAsStreamAsync();

                HtmlDocument doc = new HtmlDocument();

                doc.Load(htmlStream);

                HtmlNode userNameNode = doc.DocumentNode.Descendants("span")
                    .FirstOrDefault(e => e.Id == "userName");

                if (userNameNode == null)
                {
                    throw new Exception("Element for inserting a user name wasn't found on page.");
                }

                userNameNode.InnerHtml = currentUser.UserDisplayName;

                string sendHtml = doc.DocumentNode.OuterHtml;

                await _emailService.SendEmailAsync(currentUser.UserEmail,
               "Your profile was deleted successfully", sendHtml);

                Response.StatusCode = 200;

                var response = new
                {
                    succeeded = true,
                    error = string.Empty,
                };

                // сериализация ответа
                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented }));
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                await Response.WriteAsync(ex.ToString());
            }

            return;
        }

        private async Task RemoveUserAvatar(User user)
        {
            if (!String.IsNullOrWhiteSpace(user.UserAvatarUrl))
            {
                string avatarFolder = new DirectoryInfo(user.UserAvatarUrl).Parent.Name;
                bool userHasDefaultAvatar = avatarFolder.ToLower() == "avatar";

                if (!userHasDefaultAvatar)
                {
                    List<string> deletePaths = new List<string>();

                    deletePaths.Add(user.UserAvatarUrl);

                    var optimizeImagesPaths =
                        await _fileManager.GetAllSameFilesAsync(user.UserAvatarUrl);

                    if (optimizeImagesPaths.Count() > 0)
                    {
                        deletePaths.AddRange(optimizeImagesPaths);
                    }

                    //delete from file system
                    await _fileManager.DeleteFilesAsync(deletePaths, deletePreview: true);
                }
            }
        }


        [Authorize(Roles = "SuperAdmin, SuperManager, Manager, SuperManagerWithScript")]
        [HttpPut("/api/UpdateUserAcount")]
        public async Task UpdateUserAcount()
        {
            try
            {
                dynamic userProfileInfo;

                using (StreamReader stream = new StreamReader(Request.Body))
                {
                    using (JsonReader reader = new JsonTextReader(stream))
                    {
                        JsonSerializer serializer = new JsonSerializer();
                        userProfileInfo = serializer.Deserialize<dynamic>(reader);
                    }
                }

                int? id = userProfileInfo?.profile?.id;

                if (id == null)
                {
                    Response.StatusCode = 400;
                    return;
                }

                var user = await _veroVrContext
                    .Users
                    .Include(up => up.UserProjects)
                    .SingleOrDefaultAsync(u => u.UserId == id);

                if (user == null)
                {
                    Response.StatusCode = 404;

                    var responseError = new
                    {
                        succeeded = false,
                        error = "User wasn't found in database.",
                    };

                    // сериализация ответа
                    Response.ContentType = "application/json";
                    await Response.WriteAsync(JsonConvert.SerializeObject(responseError, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                    return;
                }

                user.UserDisplayName = userProfileInfo.profile.name?.ToString()?.Trim();
                user.UserPhoneNumber = userProfileInfo.profile.phone?.ToString()?.Trim();
                user.UserFacebook = userProfileInfo.profile.facebookLink?.ToString()?.Trim();
                user.Occupation = userProfileInfo.profile.ocupation?.ToString()?.Trim();
                user.UserCountry = userProfileInfo.profile.country?.ToString()?.Trim();
                user.UserCity = userProfileInfo.profile.city?.ToString()?.Trim();
                user.UserAddress = userProfileInfo.profile.address?.ToString()?.Trim();
                user.UserAgeRange = userProfileInfo.profile?.ageRange?.ToString()?.Trim();
                user.UserCompanyName = userProfileInfo.profile.companyName?.ToString()?.Trim();

                string password = userProfileInfo.profile.password;

                if (!String.IsNullOrEmpty(password))
                {
                    user.UserPassword = password;
                }

                if (String.IsNullOrEmpty(user.UserDisplayName))
                {
                    Response.StatusCode = 400;
                    var errorObj = new
                    {
                        error = "User name must be specified."
                    };
                    await Response.WriteAsync(JsonConvert.SerializeObject(errorObj, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                    return;
                }

                if (String.IsNullOrEmpty(user.UserPhoneNumber))
                {
                    Response.StatusCode = 400;
                    var errorObj = new
                    {
                        error = "Phone number must be specified."
                    };
                    await Response.WriteAsync(JsonConvert.SerializeObject(errorObj, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                    return;
                }

                string userEmail = userProfileInfo.profile.mail?.ToString()?.ToLower()?.Trim();

                if (String.IsNullOrEmpty(userEmail))
                {
                    Response.StatusCode = 400;
                    var errorObj = new
                    {
                        error = "Email must be specified."
                    };
                    await Response.WriteAsync(JsonConvert.SerializeObject(errorObj, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                    return;
                }
                else
                {
                    bool isExistUserWithInputEmail =
                         _veroVrContext
                         .Users
                         .AsNoTracking()
                         .Any(u => u.UserEmail == userEmail
                         && u.UserEmail != user.UserEmail);

                    if (isExistUserWithInputEmail)
                    {
                        Response.StatusCode = 400;

                        var errorObj = new
                        {
                            error = "User with this email already registered in the system."
                        };

                        await Response.WriteAsync(JsonConvert.SerializeObject(errorObj, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                        return;
                    }
                    else
                    {
                        user.UserEmail = userEmail;
                    }
                }

                await _veroVrContext.SaveChangesAsync();

                IEnumerable<int> projectIds = userProfileInfo.profile?.projectIds?.ToObject<IEnumerable<int>>();
                ICollection<Project> newProjects = new List<Project>();

                if (projectIds != null)
                {
                    if (projectIds.Count() == 0)
                    {
                        user.UserProjects = new List<UserProject> { };

                        await _veroVrContext.SaveChangesAsync();
                    }
                    else
                    {
                        var currentProjectIds = user.UserProjects.Select(up => up.ProjectId);

                        var userProjectsForDelete = new List<UserProject>();

                        foreach (var currentProjectId in currentProjectIds)
                        {
                            if (!projectIds.Contains(currentProjectId))
                            {
                                var projectForDelete = user.UserProjects.Single(up => up.ProjectId == currentProjectId);
                                userProjectsForDelete.Add(projectForDelete);
                            }
                        }

                        foreach (var userProjectForDelete in userProjectsForDelete)
                        {
                            user.UserProjects.Remove(userProjectForDelete);
                        }

                        var userProjectIdsAfterFilter = user.UserProjects.Select(up => up.ProjectId);

                        var newProjectsIds = projectIds.Except(userProjectIdsAfterFilter);

                        var arObjects = new List<Project>();

                        foreach (var newProjectsId in newProjectsIds)
                        {
                            var project = await _veroVrContext
                                         .Projects
                                         .SingleOrDefaultAsync(o => o.ProjectId == newProjectsId);

                            if (project != null)
                            {
                                newProjects.Add(project);
                            }
                        }

                        foreach (var project in newProjects)
                        {
                            var newUserProject = new UserProject { Project = project, User = user };
                            user.UserProjects.Add(newUserProject);
                        }
                    }
                }

                await _veroVrContext.SaveChangesAsync();

                await SendNotificationAddUserToProjects(user, newProjects);

                string userAvatarUrl = userProfileInfo.profile?.avatarUrl;

                if (!String.IsNullOrEmpty(userAvatarUrl) && userAvatarUrl.Contains("base64"))
                {
                    // путь к папке
                    bool isExistminiImage =
                        await _fileManager.IsFileExistsAsync(user.UserAvatarUrl);

                    if (isExistminiImage)
                    {
                        await _fileManager.DeleteFileAsync(user.UserAvatarUrl);
                    }

                    string search = "base64,";
                    int pos = userAvatarUrl.IndexOf(search);
                    int count = pos + search.Length;
                    userProfileInfo.avatarUrl = userAvatarUrl.Remove(0, count);
                    string base64Img = userProfileInfo.avatarUrl;
                    byte[] userIcon = Convert.FromBase64String(base64Img);

                    string relativePathForAccess = await _fileManager.SaveUserAvatar(userIcon, user.UserId);

                    //сохраняем информацию об объекте в базу данных
                    user.UserAvatarUrl = relativePathForAccess;

                    await _veroVrContext.SaveChangesAsync();
                }

                Response.ContentType = "application/json";

                var response = new
                {
                    profile = new
                    {
                        id = user.UserId,
                        name = user.UserDisplayName,
                        ocupation = user.Occupation,
                        mail = user.UserEmail,
                        phone = user.UserPhoneNumber,
                        avatarUrl = user.UserAvatarUrl,
                        facebookLink = user.UserFacebook,
                        country = user.UserCountry,
                        city = user.UserCity,
                        address = user.UserAddress,
                        ageRange = user.UserAgeRange,
                        companyName = user.UserCompanyName,
                        role = user.UserRole.ToString(),
                        projectIds = user.UserProjects.Select(up => up.ProjectId)
                    },
                    error = string.Empty
                };

                await Response.WriteAsync(JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented }));
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                await Response.WriteAsync(ex.ToString());
            }
        }

        private async Task SendNotificationAddUserToProjects(User user, ICollection<Project> newProjects)
        {
            const string markForRefreshProjectList3Whitespace = "   ";
            string title = _configuration["NotificationMessages:TittleForAddProjectToUser"]?.ToString()
                + markForRefreshProjectList3Whitespace;
            string message = "";

            if (newProjects.Count == 1)
            {
                message = _configuration["NotificationMessages:MessageForAddProjectToUser"]?.ToString();
                string wildCardNameOfProject = _configuration["NotificationMessages:Wildcards:NameOfProject"]?.ToString();
                var newProject = newProjects.Single();
                message = message.Replace(wildCardNameOfProject, newProject.ProjectName).Trim();
                message = $"Dear {user.UserDisplayName}, {message}";
                await _notificationService.NotifyAsync(title, message, new[] { user.UserEmail });
            }
            else if (newProjects.Count > 1)
            {
                message = _configuration["NotificationMessages:MessageForAddProjectsToUser"]?.ToString();
                string wildCardListOfProjectNames = _configuration["NotificationMessages:Wildcards:ListOfProjectNames"]?.ToString();

                StringBuilder listOfProjectNames = new StringBuilder();
                const string commaWithWhiteSpace = ", ";

                foreach (var newProject in newProjects)
                {
                    listOfProjectNames.Append(newProject.ProjectName);
                    listOfProjectNames.Append(commaWithWhiteSpace);
                }
                listOfProjectNames
                    .Remove(listOfProjectNames.Length - commaWithWhiteSpace.Length, commaWithWhiteSpace.Length);

                message = message.Replace(wildCardListOfProjectNames, listOfProjectNames.ToString()).Trim();
                message = $"Dear {user.UserDisplayName}, {message}";
                await _notificationService.NotifyAsync(title, message, new[] { user.UserEmail });
            }
        }

        [Authorize(Roles = "SuperAdmin, SuperManager, Manager, SuperManagerWithScript")]
        [HttpPut("/api/UpdateCompanySettings")]
        public async Task UpdateCompanySettings()
        {
            try
            {
                string userID = User.FindFirst("UserId")?.Value;

                User user =
                    await _veroVrContext
                    .Users
                    .SingleOrDefaultAsync(u => u.UserId == int.Parse(userID));

                if (user == null)
                {
                    Response.ContentType = "application/json";
                    Response.StatusCode = 404;
                    var errorObj = new
                    {
                        error = "User wasn't found."
                    };
                    await Response.WriteAsync(JsonConvert.SerializeObject(errorObj, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                    return;
                }

                dynamic companyInfo;

                using (StreamReader stream = new StreamReader(Request.Body))
                {
                    using (JsonReader reader = new JsonTextReader(stream))
                    {
                        JsonSerializer serializer = new JsonSerializer();
                        companyInfo = serializer.Deserialize<dynamic>(reader);
                    }
                }

                user.UserCompanyName = companyInfo.profile.companyName;
                user.UserAddress = companyInfo.profile.companyAddress;
                user.UserDisplayName = companyInfo.profile.contactName;
                user.UserPhoneNumber = companyInfo.profile.contactPhone;

                string password = companyInfo?.profile?.password?.ToString();

                if (!String.IsNullOrEmpty(password))
                {
                    string salt = Hasher.GetSalt();

                    user.Salt = salt;
                    user.UserPassword = Hasher.Hash(password, salt);
                }

                if (String.IsNullOrEmpty(user.UserDisplayName))
                {
                    Response.StatusCode = 400;
                    var errorObj = new
                    {
                        error = "User name must be specified."
                    };
                    await Response.WriteAsync(JsonConvert.SerializeObject(errorObj, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                    return;
                }

                if (String.IsNullOrEmpty(user.UserPhoneNumber))
                {
                    Response.StatusCode = 400;
                    var errorObj = new
                    {
                        error = "Phone number must be specified."
                    };
                    await Response.WriteAsync(JsonConvert.SerializeObject(errorObj, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                    return;
                }

                string email = companyInfo?.profile?.email?.ToString();

                if (String.IsNullOrEmpty(email))
                {
                    Response.StatusCode = 400;
                    var errorObj = new
                    {
                        error = "Email must be specified."
                    };
                    await Response.WriteAsync(JsonConvert.SerializeObject(errorObj, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                    return;
                }
                else
                {
                   string preparedEmail = email.ToLower().Trim();

                   bool isExistUserWithInputEmail =
                        _veroVrContext
                        .Users
                        .AsNoTracking()
                        .Any(u => u.UserEmail == preparedEmail
                        && u.UserEmail != user.UserEmail);

                    if (isExistUserWithInputEmail)
                    {
                        Response.StatusCode = 400;

                        var errorObj = new
                        {
                            error = "User with this email already registered in the system."
                        };

                        await Response.WriteAsync(JsonConvert.SerializeObject(errorObj, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                        return;
                    }
                    else
                    {
                        user.UserEmail = preparedEmail;
                    }
                }

                await _veroVrContext.SaveChangesAsync();

                Response.ContentType = "application/json";

                var response = new
                {
                    profile = new
                    {
                        companyName = user.UserCompanyName,
                        companyAddress = user.UserAddress,
                        email = user.UserEmail,
                        contactName = user.UserDisplayName,
                        contactPhone = user.UserPhoneNumber
                    },
                    error = string.Empty
                };

                await Response.WriteAsync(JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented }));
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                await Response.WriteAsync(ex.ToString());
            }
        }

        [Authorize(Roles = "SuperAdmin, SuperManager, Manager, SuperManagerWithScript")]
        [HttpGet("/api/GetCompanySettings")]
        public async Task GetCompanySettings()
        {
            try
            {
                string userID = User.FindFirst("UserId")?.Value;

                User user =
                    await _veroVrContext
                    .Users
                    .AsNoTracking()
                    .SingleOrDefaultAsync(u => u.UserId == int.Parse(userID));

                if (user == null)
                {
                    Response.ContentType = "application/json";
                    Response.StatusCode = 404;
                    var errorObj = new
                    {
                        error = "User wasn't found."
                    };
                    await Response.WriteAsync(JsonConvert.SerializeObject(errorObj, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                    return;
                }

                Response.ContentType = "application/json";

                var response = new
                {
                    profile = new
                    {
                        companyName = user.UserCompanyName,
                        companyAddress = user.UserAddress,
                        email = user.UserEmail,
                        contactName = user.UserDisplayName,
                        contactPhone = user.UserPhoneNumber
                    },
                    error = string.Empty
                };

                await Response.WriteAsync(JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented }));
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                await Response.WriteAsync(ex.ToString());
            }
        }

        [Authorize(Roles = "SuperAdmin, SuperManager, Manager, SuperManagerWithScript")]
        [HttpPost("/api/AddProjectToUser")]
        public async Task AddProjectToUser(int[] userIds, int? projectId, int[] userUncheckedIds)
        {
            string userID = User.FindFirst("UserId")?.Value;

            User currentUser =
                await _veroVrContext
                .Users
                .AsNoTracking()
                .SingleOrDefaultAsync(u => u.UserId == int.Parse(userID));

            if (currentUser == null)
            {
                Response.ContentType = "application/json";
                Response.StatusCode = 404;
                var errorObj = new
                {
                    error = "Current user wasn't found."
                };
                await Response.WriteAsync(JsonConvert.SerializeObject(errorObj, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                return;
            }

            if (projectId == null)
            {
                Response.StatusCode = 400;
                return;
            }

            try
            {
                Project project =
                    await _veroVrContext
                    .Projects
                    .Include(u => u.UserProjects)
                    .SingleOrDefaultAsync(u => u.ProjectId == projectId);

                if (project == null)
                {
                    Response.ContentType = "application/json";
                    Response.StatusCode = 404;
                    var errorObj = new
                    {
                        error = "Project wasn't found."
                    };
                    await Response.WriteAsync(JsonConvert.SerializeObject(errorObj, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                    return;
                }

                var users = new List<User>();

                foreach (var userId in userIds)
                {
                    var user = await _veroVrContext
                                 .Users
                                 .Include(u => u.UserProjects)
                                 .SingleOrDefaultAsync(u => u.UserId == userId);

                    if (user != null)
                    {
                        users.Add(user);
                    }
                }

                List<User> usersWithAddedNewProject = new List<User>();

                foreach (var user in users)
                {
                    var isProjectAssignToUserAlready =
                                    user
                                   .UserProjects
                                   .Any(up => up.ProjectId == projectId);

                    if (isProjectAssignToUserAlready == false)
                    {
                        usersWithAddedNewProject.Add(user);

                        user.UserProjects.Add(
                        new UserProject
                        {
                            User = user,
                            Project = project
                        });
                    }
                }

                List<User> usersForUnbinding = new List<User>();

                //bool isCancelUsers = userIds == null || userIds.Length == 0;

                if (userUncheckedIds != null && userUncheckedIds.Length > 0)
                {
                    if (currentUser.UserRole == UserRole.SuperAdmin)
                    {
                        foreach (int userUncheckedId in userUncheckedIds)
                        {
                            User user = await _veroVrContext
                                            .Users
                                            .Include(e => e.UserProjects)
                                            .SingleOrDefaultAsync(u => u.UserId == userUncheckedId
                                            && u.UserRole != UserRole.SuperAdmin);

                            if (user != null)
                            {
                                usersForUnbinding.Add(user);
                            }
                        }
                    }
                    else if (currentUser.UserRole == UserRole.SuperManager)
                    {
                        foreach (int userUncheckedId in userUncheckedIds)
                        {
                            User user = await _veroVrContext
                                            .Users
                                            .Include(e => e.UserProjects)
                                            .SingleOrDefaultAsync(u => u.UserId == userUncheckedId
                                            && u.UserRole != UserRole.SuperAdmin
                                       && u.UserRole != UserRole.SuperManager);

                            if (user != null)
                            {
                                usersForUnbinding.Add(user);
                            }
                        }
                    }
                    else if (currentUser.UserRole == UserRole.Manager)
                    {
                        foreach (int userUncheckedId in userUncheckedIds)
                        {
                            User user = await _veroVrContext
                                            .Users
                                            .Include(e => e.UserProjects)
                                            .SingleOrDefaultAsync(u => u.UserId == userUncheckedId
                                             && u.UserRole != UserRole.SuperAdmin
                                             && u.UserRole == UserRole.Customer);

                            if (user != null)
                            {
                                usersForUnbinding.Add(user);
                            }
                        }
                    }

                    foreach (var userForUnbinding in usersForUnbinding)
                    {
                        if (userForUnbinding.UserProjects.Select(up => up.ProjectId).Contains(projectId.Value))
                        {
                            var userProject = userForUnbinding
                                .UserProjects
                                .FirstOrDefault(up => up.ProjectId == projectId.Value);

                            if(userProject != null)
                            {
                                userForUnbinding.UserProjects.Remove(userProject);
                            }
                        }
                    }

                }

                await _veroVrContext.SaveChangesAsync();

                string title = _configuration["NotificationMessages:TittleForAddProjectToUser"]?.ToString();

                string wildCardNameOfProject = _configuration["NotificationMessages:Wildcards:NameOfProject"]?.ToString();

                string message = "";

                List<bool> sendNotificationStatuses = new List<bool>();

                message = _configuration["NotificationMessages:MessageForAddProjectToUser"]?.ToString();
                message = message.Replace(wildCardNameOfProject, project.ProjectName).Trim();

                const string markForRefreshProjectList3Whitespace = "   ";

                foreach (var userWithAddedNewProject in usersWithAddedNewProject)
                {
                    string messageForSend = $"Dear {userWithAddedNewProject.UserDisplayName}, {message}";

                    bool wasSent = await _notificationService.NotifyAsync(title + markForRefreshProjectList3Whitespace, messageForSend, new[] { userWithAddedNewProject.UserEmail });

                    sendNotificationStatuses.Add(wasSent);
                }

                if (sendNotificationStatuses.Any(s => s == false))
                {
                    Response.ContentType = "application/json";

                    var responseError = new
                    {
                        error = "Users were added to the project but the notification was not sent to all users."
                    };

                    await Response.WriteAsync(JsonConvert.SerializeObject(responseError, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                    return;
                }

                Response.StatusCode = 200;
                Response.ContentType = "application/json";

                var response = new
                {
                    succeeded = true,
                    error = string.Empty
                };

                await Response.WriteAsync(JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented }));
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                await Response.WriteAsync(ex.ToString());
            }
        }

        private async Task RemoveAllRelatedUserDataAsync(int userId)
        {
            var userActivities = _veroVrStatisticsContext
                .UserActivities
                .Where(ua => ua.UserId == userId);

            if (userActivities.Count() > 0)
            {
                _veroVrStatisticsContext.UserActivities.RemoveRange(userActivities);
            }

            var userClickLinkInDescriptionActivities = _veroVrStatisticsContext
               .UserClickLinkInDescriptionActivities
               .Where(ua => ua.UserId == userId);

            if (userClickLinkInDescriptionActivities.Count() > 0)
            {
                _veroVrStatisticsContext
                    .UserClickLinkInDescriptionActivities
                    .RemoveRange(userClickLinkInDescriptionActivities);
            }

            var userLoginActivities = _veroVrStatisticsContext
              .UserLoginActivities
              .Where(ua => ua.UserId == userId);

            if (userLoginActivities.Count() > 0)
            {
                _veroVrStatisticsContext
                    .UserLoginActivities
                    .RemoveRange(userLoginActivities);
            }

            var userShareProjectActivities = _veroVrStatisticsContext
             .UserShareProjectActivities
             .Where(ua => ua.UserId == userId);

            if (userLoginActivities.Count() > 0)
            {
                _veroVrStatisticsContext
                    .UserShareProjectActivities
                    .RemoveRange(userShareProjectActivities);
            }

            await _veroVrStatisticsContext.SaveChangesAsync();
        }
    }
}