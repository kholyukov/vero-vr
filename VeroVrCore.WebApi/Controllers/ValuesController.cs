﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace VeroVrCore.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        // GET api/values      
        [HttpGet]
        public string Get()
        {
            string s = "";
            var response = new
            {
                result = new Dictionary<int, string> { { 1, "a" }, { 2, "b" } },
                error = string.Empty,
            };
            s = JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented });
            return s;
        }
    }
}
