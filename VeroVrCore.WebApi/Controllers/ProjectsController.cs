﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Primitives;
using Newtonsoft.Json;
using VeroVrCore.Common.Infrastructure;
using VeroVrCore.Common.Models;
using VeroVrCore.Common.Security;
using VeroVrCore.DataLayer.EntityFramework;
using VeroVrCore.DataLayer.EntityFramework.VeroVrCore.DataLayer.EntityFramework;
using VeroVrCore.WebApi.LogProvider;
using VeroVrCore.WebApi.LogProviders;
using VeroVrCore.WebApi.Services;

namespace VeroVrCore.WebApi.Controllers
{
    public class ProjectsController : Controller
    {
        private VeroVrContext _veroVrContext;
        private VeroVrStatisticsContext _statisticsContext;
        private FileManagerService _fileManager;
        private IHttpContextAccessor _httpContextAccessor;
        private IConfiguration _configuration;
        private IHostingEnvironment _env;
        private ILogger<AccountController> _logger;
        private YouTubeService _youTubeService;
        private IHostingEnvironment _appEnvironment;

        public ProjectsController(VeroVrContext veroVrContext,
             VeroVrStatisticsContext statisticsContext,
            FileManagerService fileUpload,
            IHttpContextAccessor httpContextAccessor,
            IConfiguration configuration,
            IHostingEnvironment env,
            ILogger<AccountController> logger,
            YouTubeService youTubeService,
            IHostingEnvironment appEnvironment)
        {
            _veroVrContext = veroVrContext;
            this._statisticsContext = statisticsContext;
            _fileManager = fileUpload;
            _httpContextAccessor = httpContextAccessor;
            _configuration = configuration;
            _env = env;
            _logger = logger;
            _youTubeService = youTubeService;
            _appEnvironment = appEnvironment;
        }

        [AllowAnonymous]
        [HttpGet("sharing")]
        public async Task ProcessingSharingLink([FromQuery] string project)
        {
            if (String.IsNullOrEmpty(project))
            {
                Response.StatusCode = 404;
                return;
            }

            int projectId;

            try
            {
                projectId = int.Parse(Crypto.ActionDecrypt(project));
            }
            catch
            {
                Response.StatusCode = 404;
                return;
            }

            var projectInDb = _veroVrContext
                   .Projects
                   .AsNoTracking()
                   .SingleOrDefault(e => e.ProjectId == projectId);

            if (projectInDb == null)
            {
                Response.StatusCode = 404;
                return;
            }

            Response.StatusCode = 200;
            Response.ContentType = "text/html";

            string path = Path.Combine(_env.WebRootPath, "index.html");
            await Response.SendFileAsync(path);
            return;
        }

        [Authorize]
        [HttpGet("api/GetLinkForShareProject")]
        public async Task GetLinkForShareProject([FromQuery] int? id)
        {
            string userID = User.FindFirst("UserId")?.Value;

            if (userID == null)
            {
                Response.StatusCode = 404;
                return;
            }

            if (id == null)
            {
                Response.StatusCode = 400;
                var responseError = new
                {
                    error = "The parameter id wasn't specified."
                };

                // сериализация ответа
                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonConvert.SerializeObject(responseError, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                return;
            }

            var project = await _veroVrContext
                    .Projects
                    .AsNoTracking()
                    .SingleOrDefaultAsync(p => p.ProjectId == id);

            if (project == null)
            {
                Response.StatusCode = 404;
                var responseError = new
                {
                    error = "The project wasn't found."
                };

                // сериализация ответа
                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonConvert.SerializeObject(responseError, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                return;
            }

            string currentDomainUrl = _httpContextAccessor.HttpContext?.Request?.GetDisplayUrl();
            Uri uri = new Uri(currentDomainUrl);
            string domainUrl = uri.Scheme + Uri.SchemeDelimiter + uri.Host;

            string userEncrypt = Crypto.ActionEncrypt(userID);
            string projectEncrypt = Crypto.ActionEncrypt(project.ProjectId.ToString());

            string linkForShare = $"{domainUrl}/view/sharing?project={projectEncrypt}&user={userEncrypt}";
            string linkForSharePdf = $"{domainUrl}/view/sharing-pdf?project={projectEncrypt}&user={userEncrypt}";

            Response.StatusCode = 200;

            var response = new
            {
                shareLink = linkForShare,
                shareLinkPdf = linkForSharePdf,
                error = ""
            };

            // сериализация ответа
            Response.ContentType = "application/json";
            await Response.WriteAsync(JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented }));
            return;
        }

        [Authorize]
        [HttpGet("/api/getProjectList"), HttpPost("/api/getProjectList")]
        public async Task GetAllProjectsByCurrentUser()
        {
            string userID = User.FindFirst("UserId")?.Value;

            User user =
                await _veroVrContext
                .Users
                .Include(e => e.UserProjects)
                .AsNoTracking()
                .SingleOrDefaultAsync(u => u.UserId == int.Parse(userID));

            if (user == null)
            {
                Response.StatusCode = 404;
                return;
            }

            IEnumerable<Project> projects =
                _veroVrContext
                .Projects
                .Include("UserProjects.User")
                .Include("Objects.TransitionPoints")
                .Include("Objects.RoomImages.DayTime")
                .Include(e => e.ObjectArInfos)
                .Include(e => e.Objects)
                .Include(e => e.RoomTypes)
                .Include(e => e.PlanInfos)
                .Include(e => e.Interiors)
                .AsNoTracking()
                .Where(p => user.UserProjects.Select(up => up.ProjectId).Contains(p.ProjectId))
                .ToList();

            var response = new
            {
                projectList = GetProjectJson(projects),
                error = string.Empty
            };

            Response.ContentType = "application/json";
            await Response.WriteAsync(JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented }));
        }

        [Authorize]
        [HttpGet("/api/getProjectListOptimized"), HttpPost("/api/getProjectListOptimized")]
        public async Task GetAllProjectsOptimizedByCurrentUser()
        {
            string userID = User.FindFirst("UserId")?.Value;

            User user =
                await _veroVrContext
                .Users
                .Include(e => e.UserProjects)
                .AsNoTracking()
                .SingleOrDefaultAsync(u => u.UserId == int.Parse(userID));

            if (user == null)
            {
                Response.StatusCode = 404;
                return;
            }

            IEnumerable<Project> projects =
                _veroVrContext
                .Projects
                .Include("UserProjects.User")
                .Include("Objects.TransitionPoints")
                .Include("Objects.RoomImages.DayTime")
                .Include(e => e.ObjectArInfos)
                .Include(e => e.Objects)
                .Include(e => e.RoomTypes)
                .Include(e => e.PlanInfos)
                .Include(e => e.Interiors)
                .AsNoTracking()
                .Where(p => user.UserProjects.Select(up => up.ProjectId).Contains(p.ProjectId))
                .ToList();

            var response = new
            {
                projectList = GetProjectJsonOptimized(projects),
                error = string.Empty
            };

            Response.ContentType = "application/json";

            string serializedResponse = JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented });

            await Response.WriteAsync(serializedResponse);
        }

        [Authorize(Roles = "SuperAdmin, SuperManager, Manager, Customer, SuperManagerWithScript")]
        [HttpGet("/api/getSizeProject")]
        public async Task GetSizeProject([FromQuery] int? id)
        {
            if(id == null)
            {
                Response.StatusCode = 400;
                var responseError = new
                {
                    error = $"Project id wasn't specified."
                };
                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonConvert.SerializeObject(responseError, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                return;
            }

            Project project = await _veroVrContext
                    .Projects
                    .SingleOrDefaultAsync(p => p.ProjectId == id);

            if(project == null)
            {
                Response.StatusCode = 404;
                var responseError = new
                {
                    error = $"Project wasn't found."
                };
                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonConvert.SerializeObject(responseError, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                return;
            }

            ulong projectSizeBytes;

            if (project.ProjectOriginalSizeBytes.HasValue &&
                project.ProjectOriginalSizeBytes.Value > 0m)
            {
                projectSizeBytes = (ulong)project.ProjectOriginalSizeBytes.Value;
            }
            else
            {
                ulong projectSize = 
                        await _fileManager.GetFullSizeProjectByProjectIdAsync(project.ProjectId);

                project.ProjectOriginalSizeBytes = projectSize;
                _veroVrContext.SaveChanges();

                projectSizeBytes = projectSize;
            }

            var response = new
            {
                sizeBytes = projectSizeBytes
            };

            Response.StatusCode = 200;
            Response.ContentType = "application/json";
            await Response.WriteAsync(JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented }));
            return;
        }

        [Authorize]
        [HttpGet("/api/getProject"), HttpPost("/api/getProject")]
        public async Task GetProject([FromQuery] int? id, string[] interiorsInfo = null,
            string[] roomsInfo = null, string[] plansInfo = null, string[] arObjectsInfo = null,
            bool? isAddProject = null)
        {
            if (id == null)
            {
                Response.StatusCode = 400;
                return;
            }

            Project project = await _veroVrContext
                .Projects
                .Include("UserProjects.User")
                .Include("Objects.TransitionPoints")
                .Include("Objects.RoomImages.DayTime")
                .Include(e => e.Objects)
                .Include(e => e.RoomTypes)
                .Include(e => e.PlanInfos)
                .Include(e => e.Interiors)
                .Include(e => e.ObjectArInfos)
                .AsNoTracking()
                .SingleOrDefaultAsync(p => p.ProjectId == id.Value);

            if (project == null)
            {
                Response.StatusCode = 404;
                var responseError = new
                {
                    error = $"Project with id={id} wasn't found"
                };
                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonConvert.SerializeObject(responseError, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                return;
            }

            var response = new
            {
                project = GetProjectJson(new[] { project }, interiorsInfo,
                roomsInfo, plansInfo, arObjectsInfo,
                isAddProject: isAddProject.HasValue && isAddProject.Value).Single(),
                error = string.Empty,
            };
            Response.ContentType = "application/json";
            await Response.WriteAsync(JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented }));
        }

        [AllowAnonymous]
        [HttpGet("/api/getProjectOptimized"), HttpPost("/api/getProjectOptimized")]
        public async Task GetProjectOptimized([FromQuery] string id, int? decryptedUserId)
        {
            if (String.IsNullOrEmpty(id))
            {
                Response.StatusCode = 400;
                return;
            }

            int projectId = int.Parse(id);

            Project project = await _veroVrContext
                .Projects
                .Include("UserProjects.User")
                .Include("Objects.TransitionPoints")
                .Include("Objects.RoomImages.DayTime")
                .Include(e => e.RoomTypes)
                .Include(e => e.PlanInfos)
                .Include(e => e.Interiors)
                .Include(e => e.ObjectArInfos)
                .AsNoTracking()
                .SingleOrDefaultAsync(p => p.ProjectId == projectId);

            if (project == null)
            {
                Response.StatusCode = 404;
                var responseError = new
                {
                    error = $"Project with id={id} wasn't found"
                };
                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonConvert.SerializeObject(responseError, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                return;
            }

            if (decryptedUserId != null)
            {
                _statisticsContext.UserShareProjectActivities.Add(new UserShareProjectActivity
                {
                    EventDateStamp = DateTime.Now,
                    UserId = decryptedUserId.Value,
                    ProjectId = projectId
                });

                await _statisticsContext.SaveChangesAsync();
            }
            else
            {
                StringValues str;
                bool isExistHeader = Request.Headers.TryGetValue("UserId", out str);

                if (isExistHeader)
                {
                    _statisticsContext.UserShareProjectActivities.Add(new UserShareProjectActivity
                    {
                        EventDateStamp = DateTime.Now,
                        UserId = int.Parse(str),
                        ProjectId = projectId
                    });

                    await _statisticsContext.SaveChangesAsync();
                }
            }

            var response = new
            {
                projectList = GetProjectJsonOptimized(new[] { project }),
                error = string.Empty,
            };
            Response.ContentType = "application/json";
            await Response.WriteAsync(JsonConvert.SerializeObject(response,
                new JsonSerializerSettings { Formatting = Formatting.Indented }));
        }

        [AllowAnonymous]
        [HttpGet("/api/getProjectOptimizedAFrame"), HttpPost("/api/getProjectOptimizedAFrame")]
        public async Task GetProjectOptimizedAFrame([FromQuery] string id)
        {
            if (String.IsNullOrEmpty(id))
            {
                Response.StatusCode = 404;
                return;
            }

            int decryptedProjectId;

            try
            {
                decryptedProjectId = int.Parse(Crypto.ActionDecrypt(id));
            }
            catch
            {
                Response.StatusCode = 404;
                return;
            }

            StringValues str;
            bool isExistHeader = Request.Headers.TryGetValue("UserId", out str);

            int? decryptedUserId = null;

            if (isExistHeader)
            {
                try
                {
                    decryptedUserId = int.Parse(Crypto.ActionDecrypt(str));
                }
                catch
                {
                    Response.StatusCode = 404;
                    return;
                }
            }

            Project project = await _veroVrContext
                .Projects
                .Include("UserProjects.User")
                .Include("Objects.TransitionPoints")
                .Include("Objects.RoomImages.DayTime")
                .Include(e => e.Objects)
                .Include(e => e.RoomTypes)
                .Include(e => e.PlanInfos)
                .Include(e => e.Interiors)
                .Include(e => e.ObjectArInfos)
                .AsNoTracking()
                .SingleOrDefaultAsync(p => p.ProjectId == decryptedProjectId);

            if (project == null)
            {
                Response.StatusCode = 404;
                var responseError = new
                {
                    error = $"Project with id={id} wasn't found"
                };
                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonConvert.SerializeObject(responseError, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                return;
            }


            if (isExistHeader)
            {
                _statisticsContext.UserShareProjectActivities.Add(new UserShareProjectActivity
                {
                    EventDateStamp = DateTime.Now,
                    UserId = decryptedUserId.Value,
                    ProjectId = decryptedProjectId
                });

                await _veroVrContext.SaveChangesAsync();
            }

            var response = new
            {
                projectList = GetProjectJsonOptimized(new[] { project }),
                error = string.Empty,
            };

            Response.ContentType = "application/json";

            await Response.WriteAsync(JsonConvert.SerializeObject(response,
                new JsonSerializerSettings { Formatting = Formatting.Indented }));
        }

        [Authorize(Roles = "SuperAdmin, SuperManager, Manager, SuperManagerWithScript")]
        [HttpPost("/api/AddProjectInfo")]
        public async Task<IActionResult> AddProjectInfo()
        {
            dynamic projectInfo;

            using (StreamReader stream = new StreamReader(Request.Body))
            {
                using (JsonReader reader = new JsonTextReader(stream))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    projectInfo = serializer.Deserialize<dynamic>(reader);
                }
            }

            //Bad request if set id
            if (projectInfo.project.id != null)
            {
                int projectidFromJson = projectInfo.project.id;
                var projectInDb = await _veroVrContext.Projects
                    .SingleOrDefaultAsync(p => p.ProjectId == projectidFromJson);

                if (projectInDb != null)
                {
                    return BadRequest();
                }
            }

            string userID = User.FindFirst("UserId")?.Value;

            User user =
                await _veroVrContext
                .Users
                .Include(e => e.UserProjects)
                .SingleOrDefaultAsync(u => u.UserId == int.Parse(userID));

            if (user == null)
            {
                return NotFound();
            }

            IEnumerable<string> projectCodes =
                _veroVrContext
                .Projects
                .AsNoTracking()
                .Select(p => p.ProjectCode)
                .ToList();

            string generatedProjectCode = CodeGenerator.GetCode(length: 8);

            while (projectCodes.Contains(generatedProjectCode))
            {
                generatedProjectCode = CodeGenerator.GetCode(length: 8);
            }

            string youTubeVideoLink = projectInfo?.project?.videoUrl?.ToString();

            if (!String.IsNullOrEmpty(youTubeVideoLink))
            {
                bool isYouTubeVideoLink = _youTubeService.IsYoutubeLink(youTubeVideoLink);

                if (isYouTubeVideoLink == false)
                {
                    var response = new
                    {
                        succeeded = false,
                        error = "Youtube link is not a valid"
                    };

                    return new JsonResult(response)
                    {
                        StatusCode = 400
                    };
                }
            }

            var project = _veroVrContext.Projects.Add(new Project
            {
                ProjectName = projectInfo.project.name,
                ProjectDescription = projectInfo.project.description,
                ProjectCode = generatedProjectCode,
                Objects = new[]
                {
                    new ObjectYouTube
                    {
                        PathFileOnServer = youTubeVideoLink
                    }
                }
            });

            user.UserProjects.Add(
                    new UserProject
                    {
                        User = user,
                        Project = project.Entity
                    }
                );

            if (user.UserRole != UserRole.SuperAdmin)
            {
                var superAdmin =
                _veroVrContext
                .Users
                .Include(e => e.UserProjects)
                .SingleOrDefault(u => u.UserRole == UserRole.SuperAdmin);

                if (superAdmin == null)
                {
                    Response.ContentType = "application/json";

                    var errorObj = new
                    {
                        error = "Superadmin wasn't found."
                    };

                    await Response.WriteAsync(JsonConvert.SerializeObject(errorObj, new JsonSerializerSettings { Formatting = Formatting.Indented }));

                    return NotFound();
                }

                superAdmin.UserProjects.Add(new UserProject
                {
                    User = superAdmin,
                    Project = project.Entity
                });
            }

            //generate token for project
            string projectId = project.Entity.ProjectId.ToString();
            project.Entity.ProjectToken = Crypto.ActionEncrypt(projectId);

            await _veroVrContext.SaveChangesAsync();

            string miniImageUrl = projectInfo.project.miniImageUrl;

            if (!String.IsNullOrEmpty(miniImageUrl))
            {
                string search = "base64,";
                int pos = miniImageUrl.IndexOf(search);
                int count = pos + search.Length;
                projectInfo.project.miniImageUrl = miniImageUrl.Remove(0, count);
                string base64img = projectInfo.project.miniImageUrl;
                byte[] projectIcon = Convert.FromBase64String(base64img);

                await _fileManager.SaveFileForAddOperation(projectIcon,
               ObjectType.ProjectIcon,
               project.Entity.ProjectId.ToString());
            }

            string[] interiorsInfoForSave = projectInfo.project.interiorsInfo.ToObject<string[]>();
            string[] roomsInfoForSave = projectInfo.project.roomsInfo.ToObject<string[]>();
            string[] plansInfoForSave = projectInfo.project.plansInfo.ToObject<string[]>();
            string[] arObjectsInfoForSave = projectInfo.project.arObjectsInfo.ToObject<string[]>();

            return RedirectToAction("GetProject",
                new
                {
                    id = project.Entity.ProjectId,
                    interiorsInfo = interiorsInfoForSave,
                    roomsInfo = roomsInfoForSave,
                    plansInfo = plansInfoForSave,
                    arObjectsInfo = arObjectsInfoForSave,
                    isAddProject = true
                });
        }
        
        [Authorize(Roles = "SuperAdmin, SuperManager, Manager, SuperManagerWithScript")]
        [HttpPost("/api/AddProjectPdf")]
        public async Task AddProjectPdf(IFormFile pdf, int? projectId)
        {
            if (projectId == null)
            {
                Response.StatusCode = 400;

                var response = new
                {
                    succeeded = false,
                    error = "The parameter 'projectId' was not set.",
                };

                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonConvert.SerializeObject(response,
                    new JsonSerializerSettings { Formatting = Formatting.Indented }));

                return;
            }

            Project projectInDb =
                await _veroVrContext
                    .Projects
                    .SingleOrDefaultAsync(p => p.ProjectId == projectId);

            if (projectInDb == null)
            {
                Response.StatusCode = 400;

                var response = new
                {
                    succeeded = false,
                    error = $"The project with id = '{projectId}' was not founded.",
                };

                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonConvert.SerializeObject(response,
                    new JsonSerializerSettings { Formatting = Formatting.Indented }));

                return;
            }

            if (pdf == null)
            {
                string pdfFilePath = projectInDb.ProjectPdfUrl;

                if (!String.IsNullOrWhiteSpace(pdfFilePath))
                {
                    await _fileManager.DeleteFileAsync(projectInDb.ProjectPdfUrl);

                    projectInDb.ProjectPdfUrl = null;

                    await _veroVrContext.SaveChangesAsync();
                }

                Response.StatusCode = 200;

                var response = new
                {
                    succeeded = true,
                    error = string.Empty,
                };

                // сериализация ответа
                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonConvert.SerializeObject(response,
                    new JsonSerializerSettings { Formatting = Formatting.Indented }));
                return;
            }

            try
            {
                if (!String.IsNullOrWhiteSpace(projectInDb.ProjectPdfUrl))
                {
                    await _fileManager.DeleteFileAsync(projectInDb.ProjectPdfUrl);
                }

                string fileName = pdf?.FileName ?? projectInDb.ProjectName + ".pdf";
                char pathSep = Path.DirectorySeparatorChar;
                string relativePath = $"{pathSep}content{pathSep}{projectId}{pathSep}{(int)ObjectType.Pdf}{pathSep}";
                string relativeFilePathForAccess = (relativePath + fileName);

                string newFullFolderPath = _appEnvironment.WebRootPath + relativePath;
                string newFullFilePath = _appEnvironment.WebRootPath + relativeFilePathForAccess;

                if (!Directory.Exists(newFullFolderPath))
                {
                    Directory.CreateDirectory(newFullFolderPath);
                }

                // сохраняем файл в папку в каталоге wwwroot
                using (var fileStream = new FileStream(newFullFilePath, FileMode.Create))
                {
                    await pdf.CopyToAsync(fileStream);
                }

                projectInDb.ProjectPdfUrl = relativeFilePathForAccess;

                await _veroVrContext.SaveChangesAsync();

                Response.StatusCode = 200;

                var response = new
                {
                    succeeded = true,
                    error = string.Empty,
                };

                // сериализация ответа
                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonConvert.SerializeObject(response,
                    new JsonSerializerSettings { Formatting = Formatting.Indented }));
            }
            catch (Exception ex)
            {
                _logger.LogError((int)LoggingEvents.ERROR,
                    LogMessageFormatter.GetErrorMessage("An error occurred when calling /api/AddProjectPdf", ex));

                Response.StatusCode = 500;
                await Response.WriteAsync(ex.ToString());
            }
        }

        [Authorize(Roles = "SuperAdmin, SuperManager, Manager, SuperManagerWithScript")]
        [HttpPost("/api/UpdateProjectInfo")]
        public async Task<IActionResult> UpdateProjectInfo()
        {
            dynamic projectInfo;

            using (StreamReader stream = new StreamReader(Request.Body))
            {
                using (JsonReader reader = new JsonTextReader(stream))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    projectInfo = serializer.Deserialize<dynamic>(reader);
                }
            }

            Project projectInDb = null;

            if (projectInfo?.project?.id != null)
            {
                int projectidFromJson = projectInfo.project.id;

                projectInDb = await _veroVrContext
                 .Projects
                 .Include(e => e.RoomTypes)
                 .Include(e => e.PlanInfos)
                 .Include(e => e.Interiors)
                 .Include(e => e.ObjectArInfos)
                 .Include("Objects.RoomImages")
                 .SingleOrDefaultAsync(p => p.ProjectId == projectidFromJson);

                if (projectInDb == null)
                {
                    return BadRequest();
                }
            }
            else
            {
                return BadRequest();
            }

            string youTubeVideoLink = projectInfo.project.videoUrl?.ToString();

            if(!String.IsNullOrEmpty(youTubeVideoLink))
            {
                bool isYouTubeVideoLink = _youTubeService.IsYoutubeLink(youTubeVideoLink);

                if(isYouTubeVideoLink == false)
                {
                    var response = new
                    {
                        succeeded = false,
                        error = "Youtube link is not a valid"
                    };

                    return new JsonResult(response)
                    {
                        StatusCode = 400
                    };
                }
            }

            var objectYoutube =
                _veroVrContext
                .ObjectYouTubes
                .FirstOrDefault(o => o.ProjectId == projectInDb.ProjectId);

            if (objectYoutube != null)
            {
                objectYoutube.PathFileOnServer = projectInfo.project.videoUrl?.ToString();
            }
            else
            {
                projectInDb
                .Objects.Add(new ObjectYouTube
                {
                    PathFileOnServer = projectInfo.project.videoUrl?.ToString(),
                    Project = projectInDb,
                    ObjectType = ObjectType.VideoYouTube
                });
            }

            projectInDb.ProjectName = projectInfo?.project?.name;
            projectInDb.ProjectDescription = projectInfo?.project?.description;

            await _veroVrContext.SaveChangesAsync();

            var roomsTypesInDbIdsAndNames = projectInDb
              .Objects
              .SelectMany(o => o.RoomImages)
              .Select(o => new { o.ObjectId, o.RoomImageId, o.Object.ObjectName });

            //RoomTypes(Names of rooms)
            #region RoomTypes

            List<string> newRoomInfos = projectInfo?.project?.roomsInfo?
                                           .ToObject<List<string>>();

            #region  Remove info

            var idsForDelete = new List<int>();
            var preparedNewRoomInfos = newRoomInfos.Select(i => i.Trim());

            foreach (var roomtype in projectInDb.RoomTypes)
            {
                if (!preparedNewRoomInfos.Contains(roomtype.RoomTypeName.Trim())
                    && roomtype.RoomTypeName != InfoOptions.DefaultInfoOption)
                {
                    idsForDelete.Add(roomtype.RoomTypeId);
                }
            }

            var roomTypesForDelete = _veroVrContext
                            .RoomTypes
                            .Where(rt => rt.ProjectId == projectInDb.ProjectId)
                            .ToList()
                            .Where(rt => idsForDelete.Contains(rt.RoomTypeId));

            _veroVrContext.RoomTypes.RemoveRange(roomTypesForDelete);

            #endregion

            var listRoomImageIdsForSetDefaultValue = new List<int>();
            var listRoomObjectIdsForSetDefaultValue = new List<int>();

            if (newRoomInfos != null && newRoomInfos.Count() > 0)
            {

                foreach (var roomType in roomsTypesInDbIdsAndNames)
                {
                    // то чего нет в новых инфос
                    if (!newRoomInfos.Contains(roomType.ObjectName))
                    {
                        // запомним ид для дальнейших установок в дефолт
                        listRoomImageIdsForSetDefaultValue.Add(roomType.RoomImageId);
                        listRoomObjectIdsForSetDefaultValue.Add(roomType.ObjectId);
                        //найдем для удаления инфо объект
                        var forDelete = projectInDb.RoomTypes
                            .FirstOrDefault(r => r.RoomTypeName?.Trim() == roomType.ObjectName?.Trim() &&
                            r.RoomTypeName != InfoOptions.DefaultInfoOption);
                        //если не был удален ранее
                        if (forDelete != null)
                        {
                            //удаляем инфо объект
                            projectInDb.RoomTypes.Remove(forDelete);
                        }
                    }
                }

                await _veroVrContext.SaveChangesAsync();

                var currentInfosInDb = projectInDb.RoomTypes.Select(e => e.RoomTypeName);
                //проверка для добавления новых
                foreach (var newRoomType in newRoomInfos)
                {
                    if (!currentInfosInDb.Contains(newRoomType))
                    {
                        projectInDb.RoomTypes.Add(new RoomType
                        {
                            RoomTypeName = newRoomType,
                            Project = projectInDb
                        });
                    }
                }

                await _veroVrContext.SaveChangesAsync();

                //выберем roomImages для установления дефолта инфо
                var roomImagesForSetDefault = projectInDb
                            .Objects
                            .SelectMany(o => o.RoomImages)
                            .Where(o => listRoomImageIdsForSetDefaultValue
                            .Contains(o.RoomImageId));

                var defaultRoomType = projectInDb
                    .RoomTypes
                    .Single(e => e.RoomTypeName == InfoOptions.DefaultInfoOption);

                foreach (var roomImage in roomImagesForSetDefault)
                {
                    roomImage.RoomTypeId = defaultRoomType.RoomTypeId;
                }

                //выберем RoomObjects для установления дефолта инфо
                var roomObjectsForSetDefault = projectInDb
                            .Objects
                            .Where(o => listRoomObjectIdsForSetDefaultValue
                            .Contains(o.ObjectId));

                foreach (var roomObject in roomObjectsForSetDefault)
                {
                    roomObject.ObjectName = defaultRoomType.RoomTypeName;
                }
            }
            else
            {
                var deleteExceptDefault = projectInDb.RoomTypes
                    .Where(r => r.RoomTypeName != InfoOptions.DefaultInfoOption);

                _veroVrContext.RoomTypes.RemoveRange(deleteExceptDefault);

                var roomImagesForSetDefault = projectInDb
                                    .Objects
                                    .SelectMany(o => o.RoomImages);

                var defaultRoomType = projectInDb
                    .RoomTypes
                    .Single(e => e.RoomTypeName == InfoOptions.DefaultInfoOption);

                foreach (var roomImage in roomImagesForSetDefault)
                {
                    roomImage.RoomTypeId = defaultRoomType.RoomTypeId;
                }

                var roomObjectsForSetDefault = projectInDb
                                   .Objects
                                   .Where(o => o.ObjectType == ObjectType.Room);

                foreach (var roomObject in roomObjectsForSetDefault)
                {
                    roomObject.ObjectName = defaultRoomType.RoomTypeName;
                }
            }

            #endregion

            //InteriorInfos(Styles)
            #region InteriorInfos

            List<string> newInteriorInfos = projectInfo?.project?.interiorsInfo?
                                           .ToObject<List<string>>();

            #region Remove info

            var idsForInteriorsDelete = new List<int>();
            var preparedNewInteriors = newInteriorInfos.Select(i => i.Trim());

            foreach (var interiorInfo in projectInDb.Interiors)
            {
                if (!preparedNewInteriors.Contains(interiorInfo.InteriorName.Trim())
                    && interiorInfo.InteriorName != InfoOptions.DefaultInfoOption)
                {
                    idsForInteriorsDelete.Add(interiorInfo.InteriorId);
                }
            }

            var interiorsDelete = _veroVrContext
                            .Interiors
                            .Where(i => i.ProjectId == projectInDb.ProjectId)
                            .ToList()
                            .Where(i => idsForInteriorsDelete.Contains(i.InteriorId));

            _veroVrContext.Interiors.RemoveRange(interiorsDelete);

            #endregion

            var roomsTypesInDbIdsAndInteriorNames = projectInDb
                         .Objects
                         .Where(o => o.ObjectType == ObjectType.Room)
                         .SelectMany(o => o.RoomImages)
                         .Select(ri => new
                         {
                             ri.RoomImageId,
                             InteriorName = projectInDb
                             .Interiors
                             .Single(i => i.InteriorId == ri.InteriorId)
                             .InteriorName
                         })
                         .ToList();

            var listRoomImageIdsForSetDefaultValueForIntrior = new List<int>();

            if (newInteriorInfos != null && newInteriorInfos.Count() > 0)
            {
                foreach (var roomType in roomsTypesInDbIdsAndInteriorNames)
                {
                    // то чего нет в новых инфос
                    if (!newInteriorInfos.Contains(roomType.InteriorName))
                    {
                        // запомним ид для дальнейших установок в дефолт
                        listRoomImageIdsForSetDefaultValueForIntrior.Add(roomType.RoomImageId);
                        //найдем для удаления инфо объект
                        var forDelete = projectInDb
                            .Interiors
                            .FirstOrDefault(r => r.InteriorName?.Trim() == roomType.InteriorName?.Trim()
                            && r.InteriorName != InfoOptions.DefaultInfoOption);
                        //если не был удален ранее
                        if (forDelete != null)
                        {
                            //удаляем инфо объект
                            projectInDb.Interiors.Remove(forDelete);
                        }
                    }
                }

                var currentInfosInDb = projectInDb.Interiors.Select(e => e.InteriorName);
                //проверка для добавления новых
                foreach (var newInteriorName in newInteriorInfos)
                {
                    if (!currentInfosInDb.Contains(newInteriorName))
                    {
                        projectInDb.Interiors.Add(new Interior
                        {
                            InteriorName = newInteriorName,
                            Project = projectInDb
                        });
                    }
                }

                //выберем roomImages для установления дефолта инфо
                var roomImagesForSetDefault = projectInDb
                            .Objects
                            .SelectMany(o => o.RoomImages)
                            .Where(o => listRoomImageIdsForSetDefaultValueForIntrior
                            .Contains(o.RoomImageId));

                var defaultInteriorType = projectInDb
                    .Interiors
                    .Single(e => e.InteriorName == InfoOptions.DefaultInfoOption);

                foreach (var roomImage in roomImagesForSetDefault)
                {
                    roomImage.InteriorId = defaultInteriorType.InteriorId;
                }
            }
            else
            {
                var deleteExceptDefault = projectInDb.Interiors
                    .Where(r => r.InteriorName != InfoOptions.DefaultInfoOption);

                _veroVrContext.Interiors.RemoveRange(deleteExceptDefault);

                var roomImagesForSetDefault = projectInDb
                                    .Objects
                                    .SelectMany(o => o.RoomImages);

                var defaultRoomType = projectInDb
                    .Interiors
                    .Single(e => e.InteriorName == InfoOptions.DefaultInfoOption);

                foreach (var roomImage in roomImagesForSetDefault)
                {
                    roomImage.InteriorId = defaultRoomType.InteriorId;
                }
            }

            #endregion

            //ArInfos(AR models)
            #region ArInfos

            List<string> newArInfos = projectInfo?
                                .project?
                                .arObjectsInfo?
                                .ToObject<List<string>>();


            #region Remove info

            var idsForArDelete = new List<int>();
            var preparedNewArs = newArInfos.Select(i => i.Trim());

            foreach (var arInfo in projectInDb.ObjectArInfos)
            {
                if (!preparedNewArs.Contains(arInfo.ObjectArName.Trim())
                    && arInfo.ObjectArName != InfoOptions.DefaultInfoOption)
                {
                    idsForArDelete.Add(arInfo.ObjectArInfoId);
                }
            }

            var arsDelete = _veroVrContext
                            .ObjectArInfos
                            .Where(i => i.ProjectId == projectInDb.ProjectId)
                            .ToList()
                            .Where(i => idsForArDelete.Contains(i.ObjectArInfoId));

            _veroVrContext.ObjectArInfos.RemoveRange(arsDelete);

            #endregion

            var arObjectsInDbIdsAndNames = projectInDb
                         .Objects
                         .Where(o => o.ObjectType == ObjectType.Ar)
                         .Select(o => new
                         {
                             o.ObjectId,
                             o.ObjectName
                         });

            var listArObjectsIdsForSetDefaultValueForName = new List<int>();

            if (newArInfos != null && newArInfos.Count() > 0)
            {
                foreach (var arObjectsInDb in arObjectsInDbIdsAndNames)
                {
                    // то чего нет в новых инфос
                    if (!newArInfos.Contains(arObjectsInDb.ObjectName))
                    {
                        // запомним ид для дальнейших установок в дефолт
                        listArObjectsIdsForSetDefaultValueForName.Add(arObjectsInDb.ObjectId);
                        //найдем для удаления инфо объект
                        var forDelete = projectInDb
                            .ObjectArInfos
                            .FirstOrDefault(r => r.ObjectArName?.Trim() == arObjectsInDb.ObjectName?.Trim() &&
                            r.ObjectArName != InfoOptions.DefaultInfoOption);
                        //если не был удален ранее
                        if (forDelete != null)
                        {
                            //удаляем инфо объект
                            projectInDb.ObjectArInfos.Remove(forDelete);
                        }
                    }
                }

                var currentInfosInDb = projectInDb.ObjectArInfos.Select(e => e.ObjectArName);
                //проверка для добавления новых
                foreach (var newArInfoObjectName in newArInfos)
                {
                    if (!currentInfosInDb.Contains(newArInfoObjectName))
                    {
                        projectInDb.ObjectArInfos.Add(new ObjectArInfo
                        {
                            ObjectArName = newArInfoObjectName,
                            Project = projectInDb
                        });
                    }
                }

                //выберем ObjectAr для установления дефолта инфо
                var objectArsForSetDefault = projectInDb
                            .Objects
                            .Where(o => o.ObjectType == ObjectType.Ar &&
                            listArObjectsIdsForSetDefaultValueForName
                            .Contains(o.ObjectId));

                var defaultObjectType = projectInDb
                    .ObjectArInfos
                    .Single(e => e.ObjectArName == InfoOptions.DefaultInfoOption);

                foreach (var objectAr in objectArsForSetDefault)
                {
                    objectAr.ObjectName = defaultObjectType.ObjectArName;
                }
            }
            else
            {
                var deleteExceptDefault =
                    projectInDb
                    .ObjectArInfos
                    .Where(r => r.ObjectArName != InfoOptions.DefaultInfoOption);

                _veroVrContext.ObjectArInfos.RemoveRange(deleteExceptDefault);

                var arObjectsForSetDefault = projectInDb
                                    .Objects
                                    .Where(o => o.ObjectType == ObjectType.Ar);

                var defaultobjectArType = projectInDb
                    .ObjectArInfos
                    .Single(e => e.ObjectArName == InfoOptions.DefaultInfoOption);

                foreach (var objectAr in arObjectsForSetDefault)
                {
                    objectAr.ObjectName = defaultobjectArType.ObjectArName;
                }
            }

            #endregion

            //PlanInfos(Layout)
            #region PlanInfos

            List<string> newPlanInfos = projectInfo?
                                .project?
                                .plansInfo?
                                .ToObject<List<string>>();

            #region Remove info

            var idsForPlanInfoDelete = new List<int>();
            var preparedNewPlansInfo = newPlanInfos.Select(i => i.Trim());

            foreach (var planInfo in projectInDb.PlanInfos)
            {
                if (!preparedNewPlansInfo.Contains(planInfo.PlanName.Trim())
                    && planInfo.PlanName != InfoOptions.DefaultInfoOption)
                {
                    idsForPlanInfoDelete.Add(planInfo.PlanInfoId);
                }
            }

            var plansDelete = _veroVrContext
                            .PlanInfos
                            .Where(i => i.ProjectId == projectInDb.ProjectId)
                            .ToList()
                            .Where(i => idsForPlanInfoDelete.Contains(i.PlanInfoId));

            _veroVrContext.PlanInfos.RemoveRange(plansDelete);

            #endregion


            var planObjectsInDbIdsAndNames = projectInDb
                         .Objects
                         .Where(o => o.ObjectType == ObjectType.Plan)
                         .Select(o => new
                         {
                             o.ObjectId,
                             o.ObjectName
                         });

            var listPlanObjectsIdsForSetDefaultValueForName = new List<int>();

            if (newPlanInfos != null && newPlanInfos.Count() > 0)
            {
                foreach (var planObjectsInDb in planObjectsInDbIdsAndNames)
                {
                    // то чего нет в новых инфос
                    if (!newPlanInfos.Contains(planObjectsInDb.ObjectName))
                    {
                        // запомним ид для дальнейших установок в дефолт
                        listPlanObjectsIdsForSetDefaultValueForName.Add(planObjectsInDb.ObjectId);
                        //найдем для удаления инфо объект
                        var forDelete = projectInDb
                            .PlanInfos
                            .FirstOrDefault(r => r.PlanName?.Trim() == planObjectsInDb.ObjectName?.Trim() &&
                            r.PlanName != InfoOptions.DefaultInfoOption);
                        //если не был удален ранее
                        if (forDelete != null)
                        {
                            //удаляем инфо объект
                            projectInDb.PlanInfos.Remove(forDelete);
                        }
                    }
                }

                var currentInfosInDb = projectInDb.PlanInfos.Select(e => e.PlanName);
                //проверка для добавления новых
                foreach (var newPlanInfo in newPlanInfos)
                {
                    if (!currentInfosInDb.Contains(newPlanInfo))
                    {
                        projectInDb.PlanInfos.Add(new PlanInfo
                        {
                            PlanName = newPlanInfo,
                            Project = projectInDb
                        });
                    }
                }

                //выберем ObjectPlan для установления дефолта инфо
                var objectPlansForSetDefault = projectInDb
                            .Objects
                            .Where(o => o.ObjectType == ObjectType.Plan &&
                            listPlanObjectsIdsForSetDefaultValueForName
                            .Contains(o.ObjectId));

                var defaultObjectType = projectInDb
                    .PlanInfos
                    .Single(e => e.PlanName == InfoOptions.DefaultInfoOption);

                foreach (var objectPlan in objectPlansForSetDefault)
                {
                    objectPlan.ObjectName = defaultObjectType.PlanName;
                }
            }
            else
            {
                var deleteExceptDefault =
                    projectInDb
                    .PlanInfos
                    .Where(r => r.PlanName != InfoOptions.DefaultInfoOption);

                _veroVrContext.PlanInfos.RemoveRange(deleteExceptDefault);

                var planObjectsForSetDefault = projectInDb
                                    .Objects
                                    .Where(o => o.ObjectType == ObjectType.Plan);

                var defaultobjectArType = projectInDb
                    .PlanInfos
                    .Single(e => e.PlanName == InfoOptions.DefaultInfoOption);

                foreach (var planObject in planObjectsForSetDefault)
                {
                    planObject.ObjectName = defaultobjectArType.PlanName;
                }
            }

            await _veroVrContext.SaveChangesAsync();
            #endregion

            string miniImageUrl = projectInfo?.project?.miniImageUrl;
            //Logo project
            if (!String.IsNullOrEmpty(miniImageUrl))
            {
                bool isBase64 = miniImageUrl.Contains("base64");

                if (isBase64)
                {
                    // path to folder
                    bool isExistminiImage =
                        await _fileManager.IsFileExistsAsync(projectInDb.ProjectImageUrl);

                    if (isExistminiImage)
                    {
                        List<string> deletePaths = new List<string>();

                        var optimizeImagesPaths =
                            await _fileManager.GetAllSameFilesAsync(projectInDb.ProjectImageUrl);

                        if (optimizeImagesPaths.Count() > 0)
                        {
                            deletePaths.AddRange(optimizeImagesPaths);
                        }

                        //delete from file system
                        await _fileManager.DeleteFilesAsync(deletePaths);
                    }

                    string search = "base64,";
                    int pos = miniImageUrl.IndexOf(search);
                    int count = pos + search.Length;
                    projectInfo.project.miniImageUrl = miniImageUrl.Remove(0, count);
                    string base64Img = projectInfo.project.miniImageUrl;
                    byte[] projectIcon = Convert.FromBase64String(base64Img);

                    string userID = User.FindFirst("UserId")?.Value;

                    User user =
                           await _veroVrContext
                           .Users
                           .AsNoTracking()
                           .SingleAsync(u => u.UserId == int.Parse(userID));

                    await _fileManager.SaveFileForAddOperation(projectIcon,
                   ObjectType.ProjectIcon,
                   projectInDb.ProjectId.ToString());
                }
            }

            projectInDb.ProjectOriginalSizeBytes =
                        await _fileManager.GetFullSizeProjectByProjectIdAsync(projectInDb.ProjectId);

            await _veroVrContext.SaveChangesAsync();

            return RedirectToAction("GetProject",
                new { id = projectInDb.ProjectId });
        }

        [Authorize(Roles = "SuperAdmin, SuperManager, Manager, SuperManagerWithScript")]
        [HttpPost("/api/AddProjectPhotos")]
        public async Task AddProjectPhotos(IFormFileCollection photos, int? projectId)
        {
            if (projectId == null)
            {
                Response.StatusCode = 400;
                return;
            }

            try
            {
                await AddObject(photos, ObjectType.Photo, projectId);

                Response.StatusCode = 200;

                var response = new
                {
                    succeeded = true,
                    error = string.Empty,
                };

                // сериализация ответа
                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented }));
            }
            catch (Exception ex)
            {
                _logger.LogError((int)LoggingEvents.ERROR,
                       LogMessageFormatter.GetErrorMessage("An error occurred when calling /api/AddProjectPhotos", ex));

                Response.StatusCode = 500;
                await Response.WriteAsync(ex.ToString());
            }
            return;
        }

        [Authorize(Roles = "SuperAdmin, SuperManager, Manager, SuperManagerWithScript")]
        [HttpPut("/api/UpdateProjectPhotos")]
        public async Task UpdateProjectPhotos
            (IFormFileCollection photos,
            string[] deletedPhotosPaths, int? projectId)
        {
            if (projectId == null)
            {
                Response.StatusCode = 400;
                return;
            }

            try
            {
                List<string> deletePaths = new List<string>();

                if (deletedPhotosPaths.Length > 0)
                {
                    foreach (var path in deletedPhotosPaths)
                    {
                        var optimizeImagesPaths =
                            await _fileManager.GetAllSameFilesAsync(path);

                        if (optimizeImagesPaths.Count() > 0)
                        {
                            deletePaths.AddRange(optimizeImagesPaths);
                        }
                    }

                    await RemoveObjectsAndFilesByPath(deletePaths.ToArray(), projectId, ObjectType.Photo);
                }

                
                await AddObject(photos, ObjectType.Photo, projectId);

                Response.StatusCode = 200;

                var response = new
                {
                    succeeded = true,
                    error = string.Empty,
                };

                // сериализация ответа
                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented }));
            }
            catch (Exception ex)
            {
                _logger.LogError((int)LoggingEvents.ERROR,
                       LogMessageFormatter.GetErrorMessage("An error occurred when calling /api/UpdateProjectPhotos", ex));
                Response.StatusCode = 500;
                await Response.WriteAsync(ex.ToString());
            }
            return;
        }

        [Authorize(Roles = "SuperAdmin, SuperManager, Manager, SuperManagerWithScript")]
        [HttpDelete("/api/DeleteArObject")]
        public async Task DeleteArObject([FromQuery]int? id)
        {
            if (id == null)
            {
                Response.StatusCode = 400;
                return;
            }

            try
            {
                var arObject = await _veroVrContext
                    .ObjectArs
                    .AsNoTracking()
                    .SingleOrDefaultAsync(a => a.ObjectId == id);

                if (arObject == null)
                {
                    var responseEmptyAr = new
                    {
                        succeeded = false,
                        error = "Ar object wasn't found in database.",
                    };

                    // сериализация ответа
                    Response.ContentType = "application/json";
                    await Response.WriteAsync(JsonConvert.SerializeObject(responseEmptyAr, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                }

                ICollection<string> deletedArPaths = new List<string>();
                deletedArPaths.Add(arObject.PathFileOnServer);

                await RemoveObjectsAndFilesByPath(deletedArPaths.ToArray(), arObject.ProjectId, ObjectType.Ar);

                Response.StatusCode = 200;

                var arActivites = _statisticsContext
                    .UserActivities
                    .Where(ua => ua.UserActivityType == UserActivityType.ArModelView
                    && ua.Id == id)
                    .ToList();

                if (arActivites.Count > 0)
                {
                    _statisticsContext.UserActivities.RemoveRange(arActivites);
                    await _statisticsContext.SaveChangesAsync();
                }

                var response = new
                {
                    succeeded = true,
                    error = string.Empty,
                };

                // сериализация ответа
                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented }));
            }
            catch (Exception ex)
            {
                _logger.LogError((int)LoggingEvents.ERROR,
                LogMessageFormatter.GetErrorMessage("An error occurred when calling /api/DeleteArObject", ex));

                Response.StatusCode = 500;
                await Response.WriteAsync(ex.ToString());
            }

            return;
        }

        [Authorize(Roles = "SuperAdmin, SuperManager, Manager, SuperManagerWithScript")]
        [HttpPut("/api/UpdateProjectPlan")]
        public async Task UpdateProjectPlan
            (IFormFileCollection plans,
            int? projectId,
            int? planId,
            int? planInfoId,
            int? floorNumber)
        {
            if (projectId == null || planId == null
               || floorNumber == null
               || planInfoId == null)
            {
                Response.StatusCode = 400;
                var responseError = new
                {
                    error = "Missing input parameter(-s)"
                };

                // сериализация ответа
                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonConvert.SerializeObject(responseError, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                return;
            }

            if (plans.Count > 1)
            {
                Response.StatusCode = 400;
                var responseError = new
                {
                    error = "The number of plans should not be more than 1"
                };

                // сериализация ответа
                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonConvert.SerializeObject(responseError, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                return;
            }

            try
            {
                ObjectPlan planForUpdate =
                      await _veroVrContext
                      .ObjectPlans
                      .FirstOrDefaultAsync(p => p.ObjectId == planId);

                var planInfo = await _veroVrContext
                   .PlanInfos
                   .SingleOrDefaultAsync(p => p.PlanInfoId == planInfoId);

                if (planForUpdate == null)
                {

                    Response.StatusCode = 400;
                    var responseError = new
                    {
                        error = "The plan for update wasn't found."
                    };

                    // сериализация ответа
                    Response.ContentType = "application/json";
                    await Response.WriteAsync(JsonConvert.SerializeObject(responseError, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                    return;
                }

                if (planInfo == null)
                {
                    Response.StatusCode = 400;
                    var responseError = new
                    {
                        error = "The plan info wasn't found."
                    };

                    // сериализация ответа
                    Response.ContentType = "application/json";
                    await Response.WriteAsync(JsonConvert.SerializeObject(responseError, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                    return;
                }

                await UpdatePlanName(planForUpdate, planInfo.PlanName, floorNumber);

                if (plans.Count == 1)
                {
                    string userId = User.FindFirst("UserId")?.Value;

                    User user =
                           await _veroVrContext
                           .Users
                           .AsNoTracking()
                           .SingleAsync(u => u.UserId == int.Parse(userId));

                    await _fileManager.UpdatePlan(plans.First(),
                        projectId.Value.ToString(),
                        planForUpdate.ObjectId);
                }

                Response.StatusCode = 200;

                var response = new
                {
                    succeeded = true,
                    error = string.Empty,
                };

                // сериализация ответа
                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented }));
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                await Response.WriteAsync(ex.ToString());
            }
            return;
        }

        [Authorize(Roles = "SuperAdmin, SuperManager, Manager, SuperManagerWithScript")]
        [HttpPost("/api/AddRoom")]
        public async Task AddRoom(IFormFileCollection roomObjects,
               int? projectId,
               int? objectPlanId,
               int? interiorId,
               int? nameOfRoomId,
               int? dayTimeId,
               bool? defaultRoom)
        {
            if (projectId == null ||
                interiorId == null ||
                nameOfRoomId == null ||
                dayTimeId == null ||
                objectPlanId == null ||
                defaultRoom == null)

            {
                Response.StatusCode = 400;

                var responseError = new
                {
                    succeeded = false,
                    error = "Not all the parameters have been specified",
                };
                // сериализация ответа
                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonConvert.SerializeObject(responseError, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                return;
            }

            try
            {
                var interior = await _veroVrContext.Interiors
                    .AsNoTracking()
                    .SingleOrDefaultAsync(i => i.InteriorId == interiorId);

                var typeRoom = await _veroVrContext.RoomTypes
                    .AsNoTracking()
                    .SingleOrDefaultAsync(t => t.RoomTypeId == nameOfRoomId);

                var dayTime = await _veroVrContext.DayTimes
                    .AsNoTracking()
                 .SingleOrDefaultAsync(t => t.DayTimeId == dayTimeId);

                if (interior == null || typeRoom == null || dayTime == null)
                {
                    Response.StatusCode = 400;
                    return;
                }

                var objectPlan = _veroVrContext
                    .ObjectPlans
                    .AsNoTracking()
                    .FirstOrDefault(op => op.ObjectId == objectPlanId);

                var roomImage = new RoomImage
                {
                    InteriorId = interior.InteriorId,
                    DayTimeId = dayTime.DayTimeId,
                    RoomTypeId = typeRoom.RoomTypeId,
                    ObjectPlanId = objectPlan.ObjectId,
                    DefaultRoom = defaultRoom.Value ? DefaultRoom.Yes : DefaultRoom.No
                };

                await AddObject(roomObjects, ObjectType.Room, projectId, roomImage);

                Response.StatusCode = 200;

                var response = new
                {
                    succeeded = true,
                    error = string.Empty,
                };

                // сериализация ответа
                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented }));
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                await Response.WriteAsync(ex.ToString());
            }

            return;
        }

        [Authorize(Roles = "SuperAdmin, SuperManager, Manager, SuperManagerWithScript")]
        [HttpDelete("/api/DeleteProjectPlanWithRooms")]
        public async Task DeleteProjectPlanWithRooms([FromQuery] int? id)
        {
            if (id == null)
            {
                Response.StatusCode = 400;
                return;
            }

            try
            {
                ObjectPlan planForDelete =
                      await _veroVrContext
                      .ObjectPlans
                      .FirstOrDefaultAsync(p => p.ObjectId == id);

                if (planForDelete == null)
                {
                    Response.StatusCode = 400;
                    return;
                }

                //delete the plan and related rooms from the database
                var objectRooms = _veroVrContext
                    .RoomImages
                    .AsNoTracking()
                    .Where(ri => ri.ObjectPlanId == planForDelete.ObjectId)
                    .Select(ri => new { ri.Object, RoomImage = ri })
                    .ToList();

                if (objectRooms.Count() > 0)
                {
                    var objectRoomsForDelete = objectRooms
                        .Select(r => r.Object)
                        .Distinct();

                    _veroVrContext.Objects.RemoveRange(objectRoomsForDelete);

                    var roomImageIdsForDelete = objectRooms
                        .Select(r => r.RoomImage.RoomImageId)
                        .Distinct();

                    foreach (var roomImageId in roomImageIdsForDelete)
                    {
                        var roomsActivites = _statisticsContext
                          .UserRoomImageActivities
                          .Where(ua => ua.RoomImageId == roomImageId)
                          .ToList();

                        if (roomsActivites.Count > 0)
                        {
                            _statisticsContext.UserRoomImageActivities.RemoveRange(roomsActivites);
                            await _statisticsContext.SaveChangesAsync();
                        }
                    }
                }

                _veroVrContext.ObjectPlans.Remove(planForDelete);

                var plansActivites = _statisticsContext
                         .UserInPlansActivities
                         .Where(ua => ua.ObjectId == id)
                         .ToList();

                if (plansActivites.Count > 0)
                {
                    _statisticsContext.UserInPlansActivities.RemoveRange(plansActivites);
                    await _statisticsContext.SaveChangesAsync();
                }

                await _veroVrContext.SaveChangesAsync();

                IEnumerable<string> roomPaths = objectRooms.Select(e => e.RoomImage.ImageUrl);

                List<string> deletePaths = new List<string>();

                var optimizeImagesPaths =
                    await _fileManager.GetAllSameFilesAsync(planForDelete.PathFileOnServer);

                if(optimizeImagesPaths.Count() > 0)
                {
                    deletePaths.AddRange(optimizeImagesPaths);
                }

                foreach (var relativePath in roomPaths)
                {
                    var optimizeRoomImagesPaths =
                        await _fileManager.GetAllSameFilesAsync(relativePath);

                    if (optimizeRoomImagesPaths.Count() > 0)
                    {
                        deletePaths.AddRange(optimizeRoomImagesPaths);
                    }
                }

                //delete from file system
                await _fileManager.DeleteFilesAsync(deletePaths, deletePreview: true);

                Project project = await _veroVrContext.
                    Projects.
                    SingleOrDefaultAsync(p => p.ProjectId == planForDelete.ProjectId);

                if(project == null)
                {
                    var responseError = new
                    {
                        succeeded = false,
                        error = "Project was not found.",
                    };

                    Response.StatusCode = 404;
                    // сериализация ответа
                    Response.ContentType = "application/json";
                    await Response.WriteAsync(JsonConvert.SerializeObject(responseError, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                    return;
                }

                project.ProjectOriginalSizeBytes =
                        await _fileManager.GetFullSizeProjectByProjectIdAsync(project.ProjectId);

                await _veroVrContext.SaveChangesAsync();

                Response.StatusCode = 200;

                var response = new
                {
                    succeeded = true,
                    error = string.Empty,
                };

                // сериализация ответа
                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented }));
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                await Response.WriteAsync(ex.ToString());
            }
            return;
        }

        [Authorize(Roles = "SuperAdmin, SuperManager, Manager, SuperManagerWithScript")]
        [HttpDelete("/api/DeleteRoom")]
        public async Task DeleteRoom([FromQuery]int? imageRoomId)
        {
            if (imageRoomId == null)
            {
                Response.StatusCode = 400;
                return;
            }

            try
            {
                RoomImage roomForDelete = _veroVrContext
                     .RoomImages
                     .Include("Object.RoomImages")
                    .SingleOrDefault(ri => ri.RoomImageId == imageRoomId);

                if (roomForDelete == null)
                {
                    Response.StatusCode = 400;
                    return;
                }

                //delete the RoomImage (or) and related ObjectRoom from the database

                bool isLastRoomImageForObjectRoom = roomForDelete.Object.RoomImages.Count == 1;

                if (isLastRoomImageForObjectRoom)
                {
                    _veroVrContext.Objects.Remove(roomForDelete.Object);
                }
                else
                {
                    _veroVrContext.RoomImages.Remove(roomForDelete);
                }

                await _veroVrContext.SaveChangesAsync();

                var roomImageActivites = _statisticsContext
                       .UserRoomImageActivities
                       .Where(ua => ua.RoomImageId == imageRoomId)
                       .ToList();

                if (roomImageActivites.Count > 0)
                {
                    _statisticsContext
                        .UserRoomImageActivities
                        .RemoveRange(roomImageActivites);
                    await _statisticsContext.SaveChangesAsync();
                }

                List<string> deletePaths = new List<string>();

                var optimizeImagesPaths =
                    await _fileManager.GetAllSameFilesAsync(roomForDelete.ImageUrl);

                if (optimizeImagesPaths.Count() > 0)
                {
                    deletePaths.AddRange(optimizeImagesPaths);
                }

                //delete from file system
                await _fileManager.DeleteFilesAsync(deletePaths, deletePreview: true);

                Project project =
                    await _veroVrContext
                    .Projects
                    .SingleOrDefaultAsync(p => p.ProjectId == roomForDelete.Object.ProjectId);

                if (project == null)
                {
                    var responseError = new
                    {
                        succeeded = false,
                        error = "Project was not found.",
                    };

                    Response.StatusCode = 404;
                    // сериализация ответа
                    Response.ContentType = "application/json";
                    await Response.WriteAsync(JsonConvert.SerializeObject(responseError, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                    return;
                }

                project.ProjectOriginalSizeBytes =
                        await _fileManager.GetFullSizeProjectByProjectIdAsync(project.ProjectId);

                await _veroVrContext.SaveChangesAsync();

                Response.StatusCode = 200;

                var response = new
                {
                    succeeded = true,
                    error = string.Empty,
                };

                // сериализация ответа
                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented }));
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                await Response.WriteAsync(ex.ToString());
            }
            return;
        }


        private async Task UpdatePlanName(ObjectPlan planForUpdate, string planName, int? floorNumber)
        {
            planForUpdate.ObjectName = planName;
            planForUpdate.FloorNumber = floorNumber.Value;
            await _veroVrContext.SaveChangesAsync();
        }

        [Authorize(Roles = "SuperAdmin, SuperManager, Manager, SuperManagerWithScript")]
        [HttpPost("/api/AddProjectPlan")]
        public async Task AddProjectPlan(IFormFileCollection plans,
            int? projectId, int? planInfoId, int? floorNumber)
        {
            if (projectId == null || planInfoId == null
                || floorNumber == null)
            {
                Response.StatusCode = 400;
                return;
            }

            var projectInDb = await _veroVrContext
                .Projects
                .SingleOrDefaultAsync(p => p.ProjectId == projectId);

            if (projectInDb == null)
            {
                Response.StatusCode = 400;
                return;
            }

            try
            {
                await AddObject(plans, ObjectType.Plan, projectId, null, null, planInfoId, floorNumber);

                Response.StatusCode = 200;

                var response = new
                {
                    succeeded = true,
                    error = string.Empty,
                };

                // сериализация ответа
                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented }));
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                await Response.WriteAsync(ex.ToString());
            }
            return;
        }

        [Authorize(Roles = "SuperAdmin, SuperManager, Manager, SuperManagerWithScript")]
        [HttpPost("/api/AddProjectAr")]
        public async Task AddProjectAr(IFormFileCollection arObjects,
            IFormFileCollection arMapsObjects,
            int? arInfoId,
            int? projectId,
            double? size)
        {
            if (projectId == null || arInfoId == null || size == null)
            {
                Response.StatusCode = 400;
                return;
            }

            try
            {
                await AddObject(arObjects, ObjectType.Ar, projectId, null, arMapsObjects, null, null, arInfoId, size);

                Response.StatusCode = 200;

                var response = new
                {
                    succeeded = true,
                    error = string.Empty,
                };

                // сериализация ответа
                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented }));
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                await Response.WriteAsync(ex.ToString());
            }

            return;
        }

        [Authorize(Roles = "SuperAdmin, SuperManager, Manager, SuperManagerWithScript")]
        [HttpPut("/api/UpdateRoom")]
        public async Task UpdateRoom(IFormFileCollection roomObjects,
           int? projectId,
           int? imageRoomId,
           int? objectPlanId,
           int? interiorId,
           int? nameOfRoomId,
           int? dayTimeId,
           bool? defaultRoom)
        {
            if (projectId == null ||
                interiorId == null ||
                nameOfRoomId == null ||
                dayTimeId == null ||
                objectPlanId == null ||
                defaultRoom == null ||
                roomObjects.Count > 1)

            {
                Response.StatusCode = 400;

                var responseError = new
                {
                    succeeded = false,
                    error = "Not all the parameters have been specified",
                };
                // сериализация ответа
                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonConvert.SerializeObject(responseError, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                return;
            }

            try
            {
                var interior = await _veroVrContext.Interiors
                    .AsNoTracking()
                    .SingleOrDefaultAsync(i => i.InteriorId == interiorId);

                var typeRoom = await _veroVrContext.RoomTypes
                    .AsNoTracking()
                    .SingleOrDefaultAsync(t => t.RoomTypeId == nameOfRoomId);

                var dayTime = await _veroVrContext
                 .DayTimes
                 .SingleOrDefaultAsync(t => t.DayTimeId == dayTimeId);

                var project = await _veroVrContext
                  .Projects
                  .AsNoTracking()
                  .SingleOrDefaultAsync(p => p.ProjectId == projectId);

                if (interior == null || typeRoom == null
                  || dayTime == null || project == null)
                {
                    Response.StatusCode = 400;
                    var responseError = new
                    {
                        succeeded = false,
                        error = "Parameters have incorrect values",
                    };
                    // сериализация ответа
                    Response.ContentType = "application/json";
                    await Response.WriteAsync(JsonConvert.SerializeObject(responseError, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                    return;
                }

                RoomImage roomForUpdate = _veroVrContext
                     .RoomImages
                     .Include(e => e.Object)
                     .Include(e => e.DayTime)
                    .SingleOrDefault(ri => ri.RoomImageId == imageRoomId);

                if (roomForUpdate == null)
                {
                    Response.StatusCode = 400;
                    var responseError = new
                    {
                        succeeded = false,
                        error = "RoomImage was not found.",
                    };
                    // сериализация ответа
                    Response.ContentType = "application/json";
                    await Response.WriteAsync(JsonConvert.SerializeObject(responseError, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                    return;
                }

                var objectPlan = _veroVrContext
                  .ObjectPlans
                  .AsNoTracking()
                  .SingleOrDefault(o => o.ObjectId == objectPlanId);

                if (objectPlan != null)
                {
                    var roomImagesWithOneObjectRoom = _veroVrContext
                        .RoomImages
                        .Include(r => r.Object)
                        .Where(ri => ri.ObjectPlanId == roomForUpdate.ObjectPlanId
                        && ri.Object.ObjectName == roomForUpdate.Object.ObjectName);

                    foreach (var roomImage in roomImagesWithOneObjectRoom)
                    {
                        roomImage.ObjectPlanId = objectPlan.ObjectId;
                    }
                }

                roomForUpdate.DayTime = dayTime;
                roomForUpdate.InteriorId = interior.InteriorId;
                roomForUpdate.Object.ObjectName = typeRoom.RoomTypeName;
                roomForUpdate.DefaultRoom = defaultRoom.Value ? DefaultRoom.Yes : DefaultRoom.No;
                 
                await _veroVrContext.SaveChangesAsync();

                if (roomObjects.Count == 1)
                {
                    string userId = User.FindFirst("UserId")?.Value;

                    User user =
                           await _veroVrContext
                           .Users
                           .AsNoTracking()
                           .SingleAsync(u => u.UserId == int.Parse(userId));

                    await _fileManager.UpdateRoom(roomObjects.First(),
                        projectId.Value.ToString(),
                        roomForUpdate.RoomImageId);
                }

                Response.StatusCode = 200;

                var response = new
                {
                    succeeded = true,
                    error = string.Empty,
                };

                // сериализация ответа
                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented }));
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                await Response.WriteAsync(ex.ToString());
            }

            return;
        }


        [Authorize(Roles = "SuperAdmin, SuperManager, Manager, SuperManagerWithScript")]
        [HttpDelete("/api/DeleteProject")]
        public async Task DeleteProject([FromQuery] int? id)
        {
            if (id == null)
            {
                Response.StatusCode = 400;
                return;
            }

            try
            {
                var project = await _veroVrContext
               .Projects
               .Include(e => e.Objects)
               .SingleOrDefaultAsync(c => c.ProjectId == id);

                if (project == null)
                {
                    Response.StatusCode = 404;
                    return;
                }

                string projectPath = _fileManager.GetContentPath + project.ProjectId;

                if (!String.IsNullOrEmpty(projectPath))
                {
                    await _fileManager.DeleteDirectoryAsync(projectPath);
                }

                foreach (var @object in project.Objects)
                {
                    var objectActivites = _statisticsContext
                        .UserObjectActivities
                        .Where(ua => ua.Id == @object.ObjectId)
                        .ToList();

                    if (objectActivites.Count > 0)
                    {
                        _statisticsContext.UserObjectActivities.RemoveRange(objectActivites);
                    }
                }
                _veroVrContext.Projects.Remove(project);

                await _veroVrContext.SaveChangesAsync();

                var projectActivites = _statisticsContext
                       .UserProjectActivities
                       .Where(ua => ua.Id == id)
                       .ToList();

                if (projectActivites.Count > 0)
                {
                    _statisticsContext.UserProjectActivities
                        .RemoveRange(projectActivites);
                }

                await _statisticsContext.SaveChangesAsync();

                Response.StatusCode = 200;

                var response = new
                {
                    succeeded = true,
                    error = string.Empty,
                };

                // сериализация ответа
                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented }));
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                await Response.WriteAsync(ex.ToString());
            }
        }

        [AllowAnonymous]
        [HttpGet("/api/getProjectListIFrame")]
        public async Task getProjectListIFrame()
        {
            IEnumerable<Project> projects =
                _veroVrContext
                .Projects
                .Include(e => e.Objects)
                .AsNoTracking()
                .ToList();

            var response = new
            {
                projectList = GetProjectIFrameJson(projects),
                error = string.Empty
            };

            Response.ContentType = "application/json";
            await Response.WriteAsync(JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented }));
        }

        [AllowAnonymous]
        [HttpGet("/api/getProjectInfoIFrame")]
        public async Task getProjectInfoIFrame([FromQuery] string token)
        {
            if (token == null)
            {
                Response.StatusCode = 400;

                var responseError = new
                {
                    error = "The token is empty."
                };

                // сериализация ответа
                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonConvert.SerializeObject(responseError, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                return;
            }

            string decryptedToken = "";

            try
            {
                decryptedToken = Crypto.ActionDecrypt(token);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 400;

                var responseError = new
                {
                    error = "Invalid token format."
                };

                // сериализация ответа
                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonConvert.SerializeObject(responseError, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                return;
            }

            int decryptId;

            bool IsNumberToken = int.TryParse(decryptedToken, out decryptId);

            if (IsNumberToken == false)
            {
                Response.StatusCode = 400;

                var responseError = new
                {
                    error = "Invalid token content."
                };

                // сериализация ответа
                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonConvert.SerializeObject(responseError, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                return;
            }

            Project project =
                await _veroVrContext
                .Projects
                .Include(e => e.Objects)
                .AsNoTracking()
                .SingleOrDefaultAsync(p => p.ProjectId == decryptId);

            if (project == null)
            {
                Response.StatusCode = 404;

                var responseError = new
                {
                    error = "The project wasn't found."
                };

                // сериализация ответа
                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonConvert.SerializeObject(responseError, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                return;
            }

            var manager = project.Manager;

            var response = new
            {
                project = new
                {
                    id = project.ProjectId,
                    name = project.ProjectName,
                    description = project.ProjectDescription,
                    contactManagerName = manager.UserDisplayName,
                    contactManagerPersonalTitle = manager.UserAgeRange,
                    contactManagerEmail = manager.UserEmail,
                    contactManagerPhone = manager.UserPhoneNumber
                },
                error = string.Empty
            };

            Response.ContentType = "application/json";
            await Response.WriteAsync(JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented }));
        }

        [Authorize(Roles = "SuperAdmin, SuperManager, Manager, SuperManagerWithScript")]
        [HttpPost("api/UpdateProjectTransitionPoints")]
        public async Task UpdateProjectTransitionPoints()
        {
            dynamic projectInfo;

            using (StreamReader stream = new StreamReader(Request.Body))
            {
                using (JsonReader reader = new JsonTextReader(stream))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    projectInfo = serializer.Deserialize<dynamic>(reader);
                }
            }

            IDictionary<int, dynamic> projectFromJson = projectInfo
                .projectList
                .ToObject<IDictionary<int, dynamic>>();

            int projectIdFromJson = projectFromJson.Keys.Single();

            dynamic projectValue = projectFromJson.Values.SingleOrDefault();

            if (projectValue == null)
            {
                Response.StatusCode = 404;
                var responseError = new
                {
                    error = $"Project wasn't found in JSON"
                };
                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonConvert.SerializeObject(responseError, new JsonSerializerSettings { Formatting = Formatting.Indented }));

                return;
            }

            Project project = await _veroVrContext
                .Projects
                .Include("Objects.TransitionPoints")
                .SingleOrDefaultAsync(p => p.ProjectId == projectIdFromJson);

            if (project == null)
            {
                Response.StatusCode = 404;
                var responseError = new
                {
                    error = $"Project with id={projectIdFromJson} wasn't found"
                };
                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonConvert.SerializeObject(responseError, new JsonSerializerSettings { Formatting = Formatting.Indented }));

                return;
            }

            //Processing Plan
            IDictionary<int, dynamic> requestPlanDict =
                                projectValue
                                .planInfo
                                .ToObject<IDictionary<int, dynamic>>();

            var plansTp = project
                            .Objects
                            .Where(o => o.ObjectType == ObjectType.Plan)
                            .SelectMany(o => o.TransitionPoints);

            var roomTpsForDelete = project
                                .Objects
                                .Where(o => o.ObjectType == ObjectType.Room)
                                .SelectMany(o => o.TransitionPoints);

            //Remove transion points of plans
            _veroVrContext.TransitionPoints.RemoveRange(plansTp);
            //Remove transion points of rooms
            _veroVrContext.TransitionPoints.RemoveRange(roomTpsForDelete);

            foreach (var item in requestPlanDict)
            {
                //Add new points for plans
                int planId = item.Key;
                IEnumerable<dynamic> PlanTps = item.Value.transitionPoints;

                ICollection<TransitionPoint> tpsPlanForSave = new List<TransitionPoint>();
                BaseObject objPlan = project.Objects.Single(o => o.ObjectId == planId);

                foreach (var tp in PlanTps)
                {
                    tpsPlanForSave.Add(new TransitionPoint
                    {
                        PositionX = tp.positionX,
                        PositionY = tp.positionY,
                        PositionZ = tp.positionZ,
                        PercentageH = tp?.percentageH,
                        PercentageV = tp?.percentageV,
                        RoomId = tp.roomId,
                        Object = objPlan
                    });
                }

                await _veroVrContext.TransitionPoints.AddRangeAsync(tpsPlanForSave);

                ////Processing Rooms
                IDictionary<int, dynamic> rooms =
                    item.Value.rooms.ToObject<IDictionary<int, dynamic>>();

                ICollection<TransitionPoint> tpsRoomsForSave = new List<TransitionPoint>();

                foreach (var room in rooms)
                {
                    int roomId = room.Key;
                    IEnumerable<dynamic> Roomtps = room.Value.transitionPoints;
                    BaseObject objRoom = project.Objects.Single(o => o.ObjectId == roomId);

                    foreach (var roomTp in Roomtps)
                    {
                        tpsRoomsForSave.Add(new TransitionPoint
                        {
                            PositionX = roomTp.positionX,
                            PositionY = roomTp.positionY,
                            PositionZ = roomTp.positionZ,
                            PercentageH = roomTp?.percentageH,
                            PercentageV = roomTp?.percentageV,
                            RoomId = roomTp.roomId,
                            Object = objRoom
                        });
                    }

                    await _veroVrContext.TransitionPoints.AddRangeAsync(tpsRoomsForSave);
                }

                await _veroVrContext.SaveChangesAsync();
            }

            await _veroVrContext.SaveChangesAsync();

            Response.StatusCode = 200;

            var response = new
            {
                succeeded = true,
                error = string.Empty,
            };

            // сериализация ответа
            Response.ContentType = "application/json";
            await Response.WriteAsync(JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented }));
        }

        [Authorize(Roles = "SuperAdmin, SuperManager, Manager, SuperManagerWithScript")]
        [HttpPut("api/ChangeProjectCode")]
        public async Task ChangeProjectCode(int? projectId, string projectCode)
        {
            if (projectId == null)
            {
                var responseError = new
                {
                    error = "The projectId wasn't specified."
                };

                // сериализация ответа
                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonConvert.SerializeObject(responseError, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                return;
            }

            if (String.IsNullOrEmpty(projectCode))
            {
                var responseError = new
                {
                    error = "The projectCode wasn't specified."
                };

                // сериализация ответа
                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonConvert.SerializeObject(responseError, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                return;
            }

            if (projectCode.Length != 8)
            {
                var responseError = new
                {
                    error = "The new project code should consist of 8 characters."
                };

                // сериализация ответа
                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonConvert.SerializeObject(responseError, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                return;
            }

            bool isExistProjectWithSameCode = _veroVrContext.Projects.Any(p => p.ProjectCode == projectCode);

            if (isExistProjectWithSameCode)
            {
                var responseError = new
                {
                    error = "This project code has already been assigned to another project."
                };

                // сериализация ответа
                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonConvert.SerializeObject(responseError, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                return;
            }

            var project = await _veroVrContext
                            .Projects
                            .SingleOrDefaultAsync(p => p.ProjectId == projectId);

            if (project == null)
            {
                var responseError = new
                {
                    users = "The project wasn't found.",
                    error = string.Empty,
                };

                // сериализация ответа
                Response.ContentType = "application/json";
                await Response.WriteAsync(JsonConvert.SerializeObject(responseError, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                return;
            }

            project.ProjectCode = projectCode;
            await _veroVrContext.SaveChangesAsync();

            var response = new
            {
                succedded = true,
                error = string.Empty
            };

            // сериализация ответа
            Response.ContentType = "application/json";
            await Response.WriteAsync(JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented }));
        }

        #region Helper methods

        private async Task AddObject(IFormFileCollection objects,
            ObjectType objectType, int? projectId,
            RoomImage roomImage = null,
            IFormFileCollection arMaps = null,
            int? planInfoId = null,
            int? floorNumber = null,
            int? arObjectInfoId = null,
            double? sizeAr = null)
        {
            string userId = User.FindFirst("UserId")?.Value;

            User user =
               await _veroVrContext
               .Users
               .AsNoTracking()
               .SingleAsync(u => u.UserId == int.Parse(userId));

            await _fileManager.SaveFilesForAddOperation(objects,
                objectType,
                projectId.Value.ToString(),
                arMaps,
                roomImage,
                planInfoId,
                floorNumber,
                arObjectInfoId,
                sizeAr);
        }

        private object[] GetProjectJson(IEnumerable<Project> projects,
            string[] interiorsInfo = null, string[] roomsInfo = null,
            string[] plansInfo = null, string[] arObjectsInfo = null,
            bool? isAddProject = null)
        {
            ICollection<object> projectsForSend = new List<object>();

            foreach (Project project in projects)
            {
                //Photos
                var photos = GetPhotosUrls(project);

                //Youtube url
                string youTubeUrl = GetYouTubeUrl(project);

                //Ar
                ICollection<object> arObjectsJson = new List<object>();

                var arObjects = GetArObjects(project);

                foreach (var arObject in arObjects)
                {
                    arObjectsJson.Add(
                    new
                    {
                        id = arObject.ObjectId,
                        name = arObject?.ObjectName,
                        version = arObject?.Version,
                        url = arObject?.PathFileOnServer,
                        mtlUrl = arObject?.Additionals,
                        size = arObject?.Size,
                        transitionPoints = GetTransitionPoints(arObject)
                    });
                }

                //Plans
                var planObjects = GetPlansObjects(project);

                ICollection<object> plans = new List<object>();

                foreach (var plan in planObjects)
                {
                    plans.Add(new
                    {
                        id = plan?.ObjectId,
                        planName = plan?.ObjectName,
                        floorNumber = plan?.FloorNumber,
                        imageUrl = plan?.PathFileOnServer,
                        transitionPoints = GetTransitionPoints(plan),
                        rooms = GetRooms(project, plan)
                    });
                }

                ICollection<object> jsonIntriors = new List<object>();
                ICollection<object> jsonRooms = new List<object>();
                ICollection<object> jsonPlans = new List<object>();
                ICollection<object> jsonArObjects = new List<object>();

                if ((roomsInfo != null && roomsInfo.Length > 0) || (isAddProject.HasValue && isAddProject.Value))
                {
                    AddNamesOfRooms(roomsInfo, project, jsonRooms);
                }

                if ((plansInfo != null && plansInfo.Length > 0) || (isAddProject.HasValue && isAddProject.Value))
                {
                    AddNamesOfPlans(plansInfo, project, jsonPlans);
                }

                if ((interiorsInfo != null && interiorsInfo.Length > 0) || (isAddProject.HasValue && isAddProject.Value))
                {
                    AddNamesOfInteriors(interiorsInfo, project, jsonIntriors);
                }

                if ((arObjectsInfo != null && arObjectsInfo.Length > 0) || (isAddProject.HasValue && isAddProject.Value))
                {
                    AddNamesOfArObjects(arObjectsInfo, project, jsonArObjects);
                }

                var dayTimes = _veroVrContext.DayTimes
                    .AsNoTracking()
                    .ToList();

                ICollection<object> jsonDayTimes = new List<object>();

                foreach (var dayTime in dayTimes)
                {
                    jsonDayTimes.Add(new
                    {
                        id = dayTime.DayTimeId,
                        name = dayTime.DayTimeName
                    });
                }

                var arObjectsForJson = jsonArObjects.Count > 0
                   ? jsonArObjects :
                   new List<object>(project?
                   .ObjectArInfos
                   ?.Select(t => new { id = t.ObjectArInfoId, name = t?.ObjectArName }));

                var roomsForJson = jsonRooms.Count > 0
                    ? jsonRooms :
                    new List<object>(project?.RoomTypes
                    ?.Select(t => new { id = t.RoomTypeId, name = t?.RoomTypeName }));

                var plansForJson = jsonPlans.Count > 0
                    ? jsonPlans :
                    new List<object>(project?.PlanInfos
                    ?.Select(t => new { id = t.PlanInfoId, name = t?.PlanName }));

                var planExistsNames = new List<string>(project?
                    .Objects
                    .Where(o => o.ObjectType == ObjectType.Plan)
                    .Select(p => p.ObjectName));

                planExistsNames.Add(InfoOptions.DefaultInfoOption);

                var plansExistsForJson = new List<object>(project?.PlanInfos
                    ?.Where(p => planExistsNames.Contains(p.PlanName))
                    ?.Select(t => new { id = t.PlanInfoId,
                    planId = project.Objects.FirstOrDefault(pi => pi.ObjectType == ObjectType.Plan &&
                    String.Equals(t?.PlanName?.ToLower()?.Trim(), pi.ObjectName.ToLower().Trim()))?.ObjectId,
                        name = t?.PlanName }));

                var interiorsForJson = jsonIntriors.Count > 0 ? jsonIntriors :
                    new List<object>(project?.
                    Interiors
                    ?.Select(i =>
                    new
                    {
                        id = i.InteriorId,
                        name = i.InteriorName
                    })
                    );

                projectsForSend.Add(new
                {
                    id = project.ProjectId,
                    name = project.ProjectName,
                    description = project.ProjectDescription,
                    interiorsInfo = interiorsForJson,
                    roomsInfo = roomsForJson,
                    plansInfo = plansForJson,
                    plansExistsInfo = plansExistsForJson,
                    arObjectsInfo = arObjectsForJson,
                    allDayTimes = jsonDayTimes,
                    projectCode = project.ProjectCode,
                    users = GetUsers(project),
                    miniImageUrl = project.ProjectImageUrl,
                    pdfUrl = project.ProjectPdfUrl,
                    imageUrls = photos,
                    videoUrl = youTubeUrl,
                    arObjects = arObjectsJson,
                    plans = plans
                    //,
                    //defaults = new
                    //{
                    //    defaultRoomId = project.DefaultRoomId,
                    //    defaultInteriorId = project.DefaultInteriorId,
                    //    defaultDaytimeId = project.DefaultDayTimeId,
                    //    defaultPlanId = project.DefaultPlanInfoId
                    //}
                });

            }

            return projectsForSend.ToArray();
        }

        private object[] GetProjectIFrameJson(IEnumerable<Project> projects)
        {
            ICollection<object> projectsForSend = new List<object>();

            string currentDomainUrl = _httpContextAccessor.HttpContext?.Request?.GetDisplayUrl();
            Uri uri = new Uri(currentDomainUrl);
            string domainUrl = uri.Scheme + Uri.SchemeDelimiter + uri.Host;

            string shareLink = $"{domainUrl}/projects/token=";

            foreach (Project project in projects)
            {
                //360
                string urlRoom360 = GetFirst360Url(project);

                projectsForSend.Add(new
                {
                    name = project.ProjectName,
                    description = project.ProjectDescription,
                    link360 = domainUrl + urlRoom360,
                    shareLink = shareLink + project.ProjectToken
                });

            }

            return projectsForSend.ToArray();
        }

        private void AddNamesOfArObjects(string[] arObjectsInfo, Project project,
            ICollection<object> jsonArObjects)
        {
            ICollection<ObjectArInfo> objectArInfos = new List<ObjectArInfo>();

            var projectInDb = _veroVrContext.Projects
                        .SingleOrDefault(p => p.ProjectId == project.ProjectId);

            foreach (var arObjectInfo in arObjectsInfo)
            {
                string nameOfInteriorPrepare = arObjectInfo.Trim();

                var arObject = _veroVrContext
                    .ObjectArInfos.Add(
                    new ObjectArInfo
                    {
                        ObjectArName = nameOfInteriorPrepare,
                        Project = projectInDb
                    });

                _veroVrContext.SaveChanges();

                objectArInfos.Add(arObject.Entity);
            }

            var arObjectDefault = _veroVrContext
             .ObjectArInfos.Add(
             new ObjectArInfo
             {
                 ObjectArName = InfoOptions.DefaultInfoOption,
                 Project = projectInDb
             });

            _veroVrContext.SaveChanges();

            objectArInfos.Add(arObjectDefault.Entity);

            if (jsonArObjects != null)
            {
                foreach (var item in objectArInfos)
                {
                    jsonArObjects.Add(
                           new
                           {
                               id = item.ObjectArInfoId,
                               name = item.ObjectArName
                           });
                }
            }
        }

        private IDictionary<int, dynamic> GetProjectJsonOptimized(IEnumerable<Project> projects)
        {
            var projectsForSend = new Dictionary<int, dynamic>();

            foreach (Project project in projects)
            {
                //Photos
                var photos = GetPhotosUrls(project);

                //Youtube url
                string youTubeUrl = GetYouTubeUrl(project);

                //Ar
                IEnumerable<ObjectAr> arObjects = GetArObjects(project);

                IDictionary<int, dynamic> arObjectJson = new Dictionary<int, dynamic>();

                //Если необходимо скрыть элементы с "No select", то
                //необходимо расскоментировать код ниже
                foreach (var arObject in arObjects)
                {
                    //if(arObject?.ObjectName != InfoOptions.DefaultInfoOption)
                    //{
                    arObjectJson.Add(arObject.ObjectId, new
                    {
                        name = arObject?.ObjectName,
                        version = arObject?.Version,
                        url = arObject?.PathFileOnServer,
                        additionals = arObject?.Additionals,
                        size = arObject?.Size,
                        transitionPoints = GetTransitionPoints(arObject)
                    });
                    //}
                }

                //Plans
                var planObjects = GetPlansObjects(project);

                var plans = new Dictionary<int, dynamic>();

                //List<int> plansIdsForDelete = new List<int>();

                foreach (var plan in planObjects)
                {
                    var rooms = GetRoomsOptimized(project, plan);

                    //List<int> roomIdsForDelete = new List<int>();

                    //foreach (var room in rooms)
                    //{
                    //    //Удаляем из JSON интерьеры с именем InfoOptions.DefaultInfoOption
                    //    Dictionary<int, dynamic> interiorsOfRoom = room.Value.interiors;

                    //    var idsForDeleteInteriorsFromJson = new List<int>();

                    //    foreach (var item in interiorsOfRoom)
                    //    {
                    //        if(item.Value.name == InfoOptions.DefaultInfoOption)
                    //        {
                    //            idsForDeleteInteriorsFromJson.Add(item.Key);
                    //        }
                    //    }

                    //    foreach (int interiorId in idsForDeleteInteriorsFromJson)
                    //    {
                    //        interiorsOfRoom.Remove(interiorId);
                    //    }

                    //    //если все интерьеры удалены, удаляем комнату
                    //    if(interiorsOfRoom.Count == 0 ||
                    //        room.Value.name == InfoOptions.DefaultInfoOption)
                    //    {
                    //        roomIdsForDelete.Add(room.Key);
                    //    }
                    //}

                    //foreach (int roomId in roomIdsForDelete)
                    //{
                    //    rooms.Remove(roomId);
                    //}

                    ////если все комнаты удалены, удаляем план
                    //if (rooms.Count == 0 ||
                    //    plan.ObjectName == InfoOptions.DefaultInfoOption)
                    //{
                    //    plansIdsForDelete.Add(plan.ObjectId);
                    //}
                    string imgExtension = Path.GetExtension(plan?.PathFileOnServer);

                    string imagePreview = null;

                    if (imgExtension != null)
                    {
                        imagePreview = Path.ChangeExtension(plan?.PathFileOnServer, null) + InfoPreviewImage.PreviewPlanPostfix + imgExtension;
                    }

                    plans.Add(plan.ObjectId, new
                    {
                        planName = plan?.ObjectName,
                        floorNumber = plan?.FloorNumber,
                        imageUrl = plan?.PathFileOnServer,
                        imagePreview = imagePreview,
                        transitionPoints = GetTransitionPoints(plan),
                        rooms = rooms
                    });
                }

                //foreach (var plan in plansIdsForDelete)
                //{
                //    plans.Remove(plan);
                //}

                Dictionary<int, string> roomsShortInfo = new Dictionary<int, string>();

                foreach (var plan in plans)
                {
                    IDictionary<int, dynamic> roomsDic = plan.Value?.rooms;

                    foreach (var room in roomsDic)
                    {
                        roomsShortInfo.Add(room.Key, room.Value?.name);
                    }
                }

                var dayTimes = _veroVrContext.DayTimes
                    .AsNoTracking()
                    .ToList();

                var jsonDayTimes = new Dictionary<int, string>();

                foreach (var d in dayTimes)
                {
                    jsonDayTimes.Add(d.DayTimeId, d.DayTimeName);
                }

                //var roomsForJson = new Dictionary<int, string>();

                //foreach (var r in project?.RoomTypes)
                //{
                //    roomsForJson.Add(r.RoomTypeId, r.RoomTypeName);
                //}

                var planObjectsJson = new Dictionary<int, string>();

                foreach (var p in project?.PlanInfos)
                {
                    planObjectsJson.Add(p.PlanInfoId, p.PlanName);
                }

                var interiorsForJson = new Dictionary<int, string>();

                foreach (var p in project?.Interiors)
                {
                    interiorsForJson.Add(p.InteriorId, p.InteriorName);
                }

                var arObjectsForJson = new Dictionary<int, string>();

                foreach (var p in project?.ObjectArInfos)
                {
                    arObjectsForJson.Add(p.ObjectArInfoId, p.ObjectArName);
                }

                ulong projectSizeBytes;

                if (project.ProjectOriginalSizeBytes.HasValue &&
                    project.ProjectOriginalSizeBytes.Value != 0m)
                {
                    projectSizeBytes = (ulong)project.ProjectOriginalSizeBytes.Value;
                }
                else
                {
                    Project projectInDb = _veroVrContext
                        .Projects
                        .SingleOrDefault(p => p.ProjectId == project.ProjectId);

                    if(projectInDb != null)
                    {
                        ulong projectSize = _fileManager.GetFullSizeProjectByProjectId(project.ProjectId);

                        projectInDb.ProjectOriginalSizeBytes = projectSize;
                        _veroVrContext.SaveChanges();

                        projectSizeBytes = projectSize;
                    }
                    else
                    {
                        projectSizeBytes = 0UL;
                    }
                }

                projectsForSend.Add(project.ProjectId, new
                {
                    name = project.ProjectName,
                    description = project.ProjectDescription,
                    typeNames = new Dictionary<string, Dictionary<int, string>>
                    {
                        { "plans", planObjectsJson },
                        { "rooms", roomsShortInfo },
                        { "interiors", interiorsForJson },
                        { "dayTimes", jsonDayTimes },
                        { "arObjects", arObjectsForJson}
                    },
                    projectCode = project.ProjectCode,
                    projectSizeBytes = projectSizeBytes,
                    miniImageUrl = project.ProjectImageUrl,
                    imageUrls = photos,
                    videoUrl = youTubeUrl,
                    arObjectsInfo = arObjectJson,
                    planInfo = plans,
                    defaults = new
                    {
                        defaultRoomId = 0,
                        defaultInteriorId = 0,
                        defaultDaytimeId = 0,
                        defaultPlanId = 0
                    }
                });
            }

            return projectsForSend;
        }
       
        //It is necessary to optimize
        private void AddNamesOfRooms(string[] nameOfRooms, Project project,
            ICollection<object> jsonNameOfRooms = null)
        {
            ICollection<RoomType> nameOfRoomsProcessing = new List<RoomType>();

            var projectInDb = _veroVrContext.Projects
                        .SingleOrDefault(p => p.ProjectId == project.ProjectId);

            foreach (var nameOfRoom in nameOfRooms)
            {
                string nameOfRoomPrepare = nameOfRoom.Trim();

                var roomType = _veroVrContext
                    .RoomTypes.Add(
                    new RoomType
                    {
                        RoomTypeName = nameOfRoomPrepare,
                        Project = projectInDb
                    });

                _veroVrContext.SaveChanges();

                nameOfRoomsProcessing.Add(roomType.Entity);
            }

            var roomTypeDefault = _veroVrContext
                    .RoomTypes.Add(
                    new RoomType
                    {
                        RoomTypeName = InfoOptions.DefaultInfoOption,
                        Project = projectInDb
                    });

            _veroVrContext.SaveChanges();

            nameOfRoomsProcessing.Add(roomTypeDefault.Entity);

            _veroVrContext.SaveChanges();

            if (jsonNameOfRooms != null)
            {
                foreach (var item in nameOfRoomsProcessing)
                {
                    jsonNameOfRooms.Add(
                           new
                           {
                               id = item.RoomTypeId,
                               name = item.RoomTypeName
                           });
                }
            }
        }

        //It is necessary to optimize
        private void AddNamesOfPlans(string[] nameOfPlans, Project project,
           ICollection<object> jsonNameOfPlans = null)
        {
            ICollection<PlanInfo> namesOfPlans = new List<PlanInfo>();

            var projectInDb = _veroVrContext.Projects
                    .SingleOrDefault(p => p.ProjectId == project.ProjectId);

            foreach (var nameOfplan in nameOfPlans)
            {
                string nameOfPlanPrepare = nameOfplan.Trim();

                var plan = _veroVrContext
                    .PlanInfos.Add(
                    new PlanInfo
                    {
                        PlanName = nameOfPlanPrepare,
                        Project = projectInDb
                    });

                _veroVrContext.SaveChanges();

                namesOfPlans.Add(plan.Entity);
            }

            var planDefault = _veroVrContext
             .PlanInfos.Add(
             new PlanInfo
             {
                 PlanName = InfoOptions.DefaultInfoOption,
                 Project = projectInDb
             });

            _veroVrContext.SaveChanges();

            namesOfPlans.Add(planDefault.Entity);

            _veroVrContext.SaveChanges();

            if (jsonNameOfPlans != null)
            {
                foreach (var item in namesOfPlans)
                {
                    jsonNameOfPlans.Add(
                           new
                           {
                               id = item.PlanInfoId,
                               name = item.PlanName
                           });
                }
            }
        }

        private void AddNamesOfInteriors(string[] namesOfInteriors, Project project,
          ICollection<object> jsonIntriors = null)
        {
            ICollection<Interior> interiors = new List<Interior>();

            var projectInDb = _veroVrContext.Projects
                        .SingleOrDefault(p => p.ProjectId == project.ProjectId);

            foreach (var nameOfInterior in namesOfInteriors)
            {
                string nameOfInteriorPrepare = nameOfInterior.Trim();

                var interior = _veroVrContext
                    .Interiors.Add(
                    new Interior
                    {
                        InteriorName = nameOfInteriorPrepare,
                        Project = projectInDb
                    });

                _veroVrContext.SaveChanges();

                interiors.Add(interior.Entity);
            }

            var interiorDefault = _veroVrContext
                .Interiors.Add(
                new Interior
                {
                    InteriorName = InfoOptions.DefaultInfoOption,
                    Project = projectInDb
                });

            _veroVrContext.SaveChanges();

            interiors.Add(interiorDefault.Entity);

            if (jsonIntriors != null)
            {
                foreach (var item in interiors)
                {
                    jsonIntriors.Add(
                           new
                           {
                               id = item.InteriorId,
                               name = item.InteriorName
                           });
                }
            }
        }

        private IEnumerable<string> GetUsers(Project project)
        {
            return project
                .UserProjects
                .Select(up => up.User.UserEmail);
        }

        private IEnumerable<string> GetPhotosUrls(Project project)
        {
            //Photos (screenshots)
            return _veroVrContext
                .ObjectPhotos
                 .AsNoTracking()
                .Where(o => o.ProjectId == project.ProjectId)
                .Select(u => u.PathFileOnServer);
        }

        private string GetFirst360Url(Project project)
        {
            return project.Objects
                .FirstOrDefault(o => o.ObjectType == ObjectType.Room)
                ?.PathFileOnServer;
        }

        private string GetYouTubeUrl(Project project)
        {
            //YoutubeUrls
            return _veroVrContext
                .ObjectYouTubes
                 .AsNoTracking()
                .FirstOrDefault(o => o.ProjectId == project.ProjectId)
                ?.PathFileOnServer;
        }

        private IEnumerable<ObjectAr> GetArObjects(Project project)
        {
            //ARS
            return _veroVrContext
                .ObjectArs
                .Include(o => o.TransitionPoints)
                 .AsNoTracking()
                .Where(o => o.ProjectId == project.ProjectId);
        }

        private IEnumerable<ObjectPlan> GetPlansObjects(Project project)
        {
            //Plans
            return _veroVrContext
                  .ObjectPlans
                  .Include(o => o.TransitionPoints)
                  .Include(o => o.RoomImages)
                  .AsNoTracking()
                  .Where(o => o.ObjectType == ObjectType.Plan
                  && o.ProjectId == project.ProjectId);
        }

        private object[] GetTransitionPoints(ObjectTransitionPoint objectTransitionPoint)
        {
            ICollection<TransitionPoint> transitionpoints = objectTransitionPoint == null ?
                    new List<TransitionPoint>() : objectTransitionPoint.TransitionPoints;

            ICollection<object> tps = new List<object>();

            foreach (TransitionPoint point in transitionpoints)
            {
                object obj = null;

                if (objectTransitionPoint is ObjectAr)
                {
                    obj = new
                    {
                        positionX = point?.PositionX,
                        positionY = point?.PositionY,
                        positionZ = point?.PositionZ
                    };
                }
                else
                {
                    obj = new
                    {
                        positionX = point?.PositionX,
                        positionY = point?.PositionY,
                        positionZ = point?.PositionZ,
                        percentageH = point?.PercentageH,
                        percentageV = point?.PercentageV,
                        roomId = point?.RoomId
                    };
                }

                tps.Add(obj);
            }

            return tps.ToArray();
        }

        private ICollection<object> GetRooms(Project project,
            ObjectPlan objectPlan)
        {
            IEnumerable<BaseObject> rooms =
                project
                .Objects
                .Where(o => o.ObjectType == ObjectType.Room
                && o.RoomImages.All(r => r.ObjectPlanId == objectPlan.ObjectId));

            ICollection<object> roomsForSend = new List<object>();

            foreach (ObjectRoom room in rooms)
            {
                roomsForSend.Add(new
                {
                    id = room.ObjectId,
                    name = room.ObjectName,
                    transitionPoints = GetTransitionPoints(room),
                    interiors = Getinteriors(project, room)
                });
            }

            return roomsForSend;
        }

        private IDictionary<int, dynamic> GetRoomsOptimized(Project project,
           ObjectPlan objectPlan)
        {
            IEnumerable<BaseObject> rooms =
                project
                .Objects
                .Where(o => o.ObjectType == ObjectType.Room
                && o.RoomImages.All(r => r.ObjectPlanId == objectPlan.ObjectId));

            IDictionary<int, dynamic> roomsForSend = new Dictionary<int, dynamic>();

            foreach (ObjectRoom room in rooms)
            {
                roomsForSend.Add(room.ObjectId, new
                {
                    name = room.ObjectName,
                    transitionPoints = GetTransitionPoints(room),
                    interiors = GetinteriorsOptimized(project, room)
                });
            }

            return roomsForSend;
        }

        private object[] Getinteriors(Project project, ObjectRoom room)
        {
            ICollection<object> inters = new List<object>();

            var interiorIds = room.RoomImages.Select(r => r.InteriorId);

            foreach (var interior in project.Interiors)
            {
                if (interiorIds.Contains(interior.InteriorId))
                {
                    inters.Add(new
                    {
                        id = interior.InteriorId,
                        name = interior.InteriorName,
                        daytimes = GetDayTimes(room, interior.InteriorId)
                    });
                }
            }

            return inters.ToArray();
        }

        private IDictionary<int, dynamic> GetinteriorsOptimized(Project project, ObjectRoom room)
        {
            IDictionary<int, dynamic> inters = new Dictionary<int, dynamic>();

            var interiorIds = room.RoomImages.Select(r => r.InteriorId);

            foreach (var interior in project.Interiors)
            {
                if (interiorIds.Contains(interior.InteriorId))
                {
                    inters.Add(interior.InteriorId, new
                    {
                        id = interior.InteriorId,
                        name = interior.InteriorName,
                        daytimes = GetDayTimesOptimized(room, interior.InteriorId)
                    });
                }
            }

            return inters;
        }

        private object[] GetDayTimes(ObjectRoom room, int interiorId)
        {
            ICollection<RoomImage> roomImages = room?.RoomImages == null ?
                    new List<RoomImage>() :
                   room.RoomImages
              .Where(ri => ri.InteriorId == interiorId)
              .ToList();

            ICollection<object> inters = new List<object>();

            foreach (var roomImage in roomImages)
            {
                inters.Add(new
                {
                    id = roomImage?.DayTimeId,
                    @default = Convert.ToBoolean((int)roomImage.DefaultRoom),
                    imageRoomId = roomImage.RoomImageId,
                    imageUrl = roomImage.ImageUrl
                });
            }

            return inters.ToArray();
        }

        private IDictionary<int, dynamic> GetDayTimesOptimized(ObjectRoom room, int interiorId)
        {
            ICollection<RoomImage> roomImages = room?.RoomImages == null ?
                    new List<RoomImage>() :
                   room.RoomImages
              .Where(ri => ri.InteriorId == interiorId)
              .ToList();

            IDictionary<int, dynamic> dayTimes = new Dictionary<int, dynamic>();
            //Console.WriteLine($"==============================================");
            foreach (var roomImage in roomImages)
            {
                //Console.WriteLine($"roomImageId={roomImage.RoomImageId} daytimeId={roomImage.DayTimeId}");

                //try
                //{

                string imgExtension = Path.GetExtension(roomImage.ImageUrl);

                string imagePreview = null;

                if (imgExtension != null)
                {
                    imagePreview = Path.ChangeExtension(roomImage.ImageUrl, null) + InfoPreviewImage.PreviewRoomPostfix + imgExtension;
                }

                dayTimes.Add(roomImage.DayTimeId, new
                    {
                        id = roomImage?.RoomImageId,
                        @default = roomImage.DefaultRoom == DefaultRoom.Yes,
                        imageUrl = roomImage.ImageUrl,
                        imagePreview = imagePreview
                });
                //}
                //catch(Exception ex)
                //{
                //    Console.WriteLine($"The roomImageId={roomImage.RoomImageId}" +
                //        $" with daytimeId={roomImage.DayTimeId} already exists");
                //    Console.WriteLine(ex.InnerException);
                //    Console.WriteLine(ex.Message);
                //}
            }

            return dayTimes;
        }

        private async Task RemoveObjectsAndFilesByPath(string[] deletedPaths,
          int? projectId, ObjectType objectType)
        {
            if (deletedPaths != null && deletedPaths.Length > 0)
            {
                var project = _veroVrContext
                              .Projects
                              .Include(e => e.Objects)
                              .SingleOrDefault(p => p.ProjectId == projectId);

                if (objectType == ObjectType.Ar)
                {
                    IEnumerable<int> arObjectsIds =
                                    project
                                   .Objects
                                   .Where(o => o.ObjectType == ObjectType.Ar)
                                   .Select(o => o.ObjectId);
                    
                    var arObjects = new List<ObjectAr>();

                    foreach (var arObjectsId in arObjectsIds)
                    {
                        var objectAr = await _veroVrContext
                                     .ObjectArs
                                     .SingleOrDefaultAsync(o => o.ObjectId == arObjectsId);

                        if (objectAr != null)
                        {
                            arObjects.Add(objectAr);
                        }
                    }

                    ObjectAr deletedObject =
                                arObjects
                               .FirstOrDefault(o => deletedPaths.Contains(o.PathFileOnServer));

                    //delete object with mtl and obj from database
                    _veroVrContext.Objects.Remove(deletedObject);

                    string objectArPath = deletedPaths.First();
                    string objectArDirectoryPath = Path.GetDirectoryName(objectArPath);

                    //delete from file system
                    await _fileManager.DeleteDirectoryAsync(objectArDirectoryPath);
                }
                else if (objectType == ObjectType.Photo)
                {
                    var delPaths = deletedPaths
                           .Distinct()
                           .Select(p => Path.GetFullPath(p).TrimEnd('\\'));

                    IEnumerable<BaseObject> deletedObjects =
                           project
                           .Objects
                           .Where(o => o.ObjectType == objectType &&
                           delPaths.Contains(Path.GetFullPath(o.PathFileOnServer).TrimEnd('\\')));

                    //delete from database
                    _veroVrContext.Objects.RemoveRange(deletedObjects);

                    //delete from file system
                    await _fileManager.DeleteFilesAsync(deletedPaths);
                }

                project.ProjectOriginalSizeBytes =
                        await _fileManager.GetFullSizeProjectByProjectIdAsync(project.ProjectId);

                await _veroVrContext.SaveChangesAsync();
            }
        }

        #endregion
    }
}