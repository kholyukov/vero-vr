﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Primitives;
using VeroVrCore.Common.Models;
using VeroVrCore.Common.Security;
using VeroVrCore.DataLayer.EntityFramework;
using VeroVrCore.DataLayer.EntityFramework.VeroVrCore.DataLayer.EntityFramework;
using VeroVrCore.WebApi.Model;

namespace VeroVrCore.WebApi.Controllers
{
    public class SharingController : Controller
    {
        private VeroVrContext _veroVrContext;
        private IHttpContextAccessor _httpContextAccessor;
        private VeroVrStatisticsContext _statisticsContext;

        public SharingController(VeroVrContext veroVrContext,
            VeroVrStatisticsContext statisticsContext,
            IHttpContextAccessor httpContextAccessor)
        {
            _veroVrContext = veroVrContext;
            _httpContextAccessor = httpContextAccessor;
            _statisticsContext = statisticsContext;
        }

        [Route("/view/sharing")]
        public IActionResult Sharing()
        {
            StringValues project;
            bool isExistProjectParam = HttpContext.Request.Query.TryGetValue("project", out project);

            StringValues user;
            bool isExistUserParam = HttpContext.Request.Query.TryGetValue("user", out user);

            if (!isExistProjectParam)
            {
                return NotFound();
            }

            SharingModel model = new SharingModel();

            try
            {
                if (String.IsNullOrEmpty(project))
                {
                    return NotFound();
                }

                int projectId = int.Parse(Crypto.ActionDecrypt(project));

                var projectInDb = _veroVrContext
                    .Projects
                    .AsNoTracking()
                    .SingleOrDefault(e => e.ProjectId == projectId);

                if (projectInDb == null)
                {
                    return NotFound();
                }

                model.ProjectName = projectInDb.ProjectName;
                string currentDomainUrl = _httpContextAccessor.HttpContext?.Request?.GetDisplayUrl();
                Uri uri = new Uri(currentDomainUrl);
                string domainUrl = uri.Scheme + Uri.SchemeDelimiter + uri.Host;

                model.ProjectImageUrl = domainUrl + projectInDb.ProjectImageUrl;
                model.ProjectDescription = projectInDb.ProjectDescription;
                model.RedirectUrl = $"{domainUrl}/sharing?project={project}&user={user}";

                if (isExistUserParam && !String.IsNullOrEmpty(user))
                {
                    try
                    {
                        int userId = int.Parse(Crypto.ActionDecrypt(user));

                        _statisticsContext
                     .UserShareProjectActivities
                     .Add(new UserShareProjectActivity
                     {
                         EventDateStamp = DateTime.Now,
                         UserId = userId,
                         ProjectId = projectId
                     });

                        _statisticsContext.SaveChanges();
                    }
                    catch (Exception ex)
                    {

                    }
                }
            }
            catch (Exception ex)
            {
                return NotFound();
            }

            return View("sharing", model);
        }

        [Route("/view/sharing-pdf")]
        public IActionResult SharingPdf()
        {
            StringValues project;
            bool isExistProjectParam = HttpContext.Request.Query.TryGetValue("project", out project);

            StringValues user;
            bool isExistUserParam = HttpContext.Request.Query.TryGetValue("user", out user);

            if (!isExistProjectParam)
            {
                return NotFound();
            }

            SharingModel model = new SharingModel();

            try
            {
                if (String.IsNullOrEmpty(project))
                {
                    return NotFound();
                }

                int projectId = int.Parse(Crypto.ActionDecrypt(project));

                var projectInDb = _veroVrContext
                    .Projects
                    .AsNoTracking()
                    .SingleOrDefault(e => e.ProjectId == projectId);

                if (projectInDb == null)
                {
                    return NotFound();
                }

                model.ProjectName = projectInDb.ProjectName;
                string currentDomainUrl = _httpContextAccessor.HttpContext?.Request?.GetDisplayUrl();
                Uri uri = new Uri(currentDomainUrl);
                string domainUrl = uri.Scheme + Uri.SchemeDelimiter + uri.Host;

                model.ProjectImageUrl = domainUrl + projectInDb.ProjectImageUrl;
                model.ProjectDescription = projectInDb.ProjectDescription;

                string pathPdf = projectInDb.ProjectPdfUrl ?? "/not-found";

                model.RedirectUrl = $"{domainUrl}{pathPdf}";

                if (isExistUserParam && !String.IsNullOrEmpty(user))
                {
                    try
                    {
                        int userId = int.Parse(Crypto.ActionDecrypt(user));

                        _statisticsContext
                     .UserShareProjectActivities
                     .Add(new UserShareProjectActivity
                     {
                         EventDateStamp = DateTime.Now,
                         UserId = userId,
                         ProjectId = projectId
                     });

                        _statisticsContext.SaveChanges();
                    }
                    catch (Exception ex)
                    {

                    }
                }
            }
            catch (Exception ex)
            {
                return NotFound();
            }

            return View("sharing", model);
        }
    }
}