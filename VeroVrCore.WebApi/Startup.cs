﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System;
using System.IO;
using VeroVrCore.DataLayer.EntityFramework;
using VeroVrCore.DataLayer.EntityFramework.VeroVrCore.DataLayer.EntityFramework;
using VeroVrCore.WebApi.LogProvider;
using VeroVrCore.WebApi.LogProviders.LogFileProvider;
using VeroVrCore.WebApi.Middlewares;
using VeroVrCore.WebApi.Services;

namespace VeroVrCore.WebApi
{
    public class Startup
    {
        //#if DEBUG
        string jsPath = Path.Combine(Environment.CurrentDirectory, "wwwroot", "main.bundle.js");
        //#endif

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                    .AddJwtBearer(options =>
                    {
                        options.RequireHttpsMetadata = false;
                        options.TokenValidationParameters = new TokenValidationParameters
                        {
                            // укзывает, будет ли валидироваться издатель при валидации токена
                            ValidateIssuer = true,
                            // строка, представляющая издателя
                            ValidIssuer = AuthOptions.ISSUER,

                            // будет ли валидироваться потребитель токена
                            ValidateAudience = true,
                            // установка потребителя токена
                            ValidAudience = AuthOptions.AUDIENCE,
                            // будет ли валидироваться время существования
                            ValidateLifetime = true,

                            // установка ключа безопасности
                            IssuerSigningKey = AuthOptions.GetSymmetricSecurityKey(),
                            // валидация ключа безопасности
                            ValidateIssuerSigningKey = true,
                        };
                    });

            string dbTypeParam = Configuration["DbTypeSettings:DbType"].ToString();

            string connectionVeroVrString = GetVeroVrConnectionString(dbTypeParam);
            string connectionVeroVrStatisticsString = GetVeroVrStatisticsConnectionString(dbTypeParam);

            var dbVeroVrOptions = GetDbOptions(dbTypeParam, connectionVeroVrString);
            services.AddDbContext<VeroVrContext>(dbVeroVrOptions);

            var dbVeroVrStatisticsOptions = GetDbOptions(dbTypeParam, connectionVeroVrStatisticsString);
            services.AddDbContext<VeroVrStatisticsContext>(dbVeroVrStatisticsOptions);

            services.AddMvc().AddMvcOptions(mvcOptions =>
            {
                mvcOptions.CacheProfiles.Add("NoCache", new CacheProfile
                {
                    NoStore = true,
                    Duration = 0
                });
            });

            services.AddMemoryCache();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddTransient<FileManagerService>();
            services.AddTransient<EmailService>();
            services.AddTransient<NotificationService>();
            services.AddTransient<ImageService>();
            services.AddTransient<YouTubeService>();
        }

        private Action<DbContextOptionsBuilder> GetDbOptions(string dbTypeParam, string connectionString)
        {
            DbType dbType = Enum.Parse<DbType>(dbTypeParam.Trim(), ignoreCase: true);

            switch (dbType)
            {
                case DbType.MsSql:
                    return options => options.UseSqlServer(connectionString);
                case DbType.MySql:
                    return options => options.UseMySql(connectionString);
                default:
                    throw new NotSupportedException("This type of database wasn't define.");
            }
        }

        private string GetVeroVrConnectionString(string dbTypeParam)
        {
            DbType dbType = Enum.Parse<DbType>(dbTypeParam.Trim(), ignoreCase: true);

            switch (dbType)
            {
                case DbType.MsSql:
                    return Configuration.GetConnectionString("VeroVrMsSqlDbConnection");
                case DbType.MySql:
                    return Configuration.GetConnectionString("VeroVrMySqlDbConnection");
                default:
                    throw new NotImplementedException("This type of database wasn't define.");
            }
        }

        private string GetVeroVrStatisticsConnectionString(string dbTypeParam)
        {
            DbType dbType = Enum.Parse<DbType>(dbTypeParam.Trim(), ignoreCase: true);

            switch (dbType)
            {
                case DbType.MsSql:
                    return Configuration.GetConnectionString("VeroVrStatisticsMsSqlDbConnection");
                case DbType.MySql:
                    return Configuration.GetConnectionString("VeroVrStatisticsMySqlDbConnection");
                default:
                    throw new NotImplementedException("This type of database wasn't define.");
            }
        }

        private static void StartPage(IApplicationBuilder app)
        {
            app.Run(async context =>
            {
                context.Response.Redirect("/");
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env,
            ILoggerFactory loggerFactory)
        {
            app.UseImageProcessing();

            //#if DEBUG_CLIENT
            app.Use(async (context, next) =>
            {
                bool changed = false;
                try
                {
                    if (context.Request.Path.Value.Contains("main.bundle.js") && File.Exists(jsPath))
                    {
                        string domainName = Configuration["DomainSettings:DomainName"]?.ToString();
                        if (!String.IsNullOrEmpty(domainName))
                        {
                            string newContent = await File.ReadAllTextAsync(jsPath);
                            newContent = newContent.Replace(domainName, context.Request.Host.Value);
                            //newContent = newContent.Replace("https://" + domainName, "http://" + context.Request.Host.Value);
                            await File.WriteAllTextAsync(jsPath, newContent);
                            changed = true;
                            await next();
                            newContent = newContent.Replace(context.Request.Host.Value, domainName);
                            await File.WriteAllTextAsync(jsPath, newContent);
                        }
                    }
                }
                finally
                {
                    app.Map("/start-page", StartPage);

                    if (!changed)
                        await next();

                    if (context.Response.StatusCode == 401)
                    {
                        var errorObj = new
                        {
                            error = "Requires authorization to execute the request."
                        };
                        await context.Response.WriteAsync(JsonConvert.SerializeObject(errorObj, new JsonSerializerSettings { Formatting = Formatting.Indented }));
                    }
                }
            });
            //#endif

            app.UseCors(builder =>
            {
                builder.AllowAnyOrigin();
                builder.AllowAnyHeader();
                builder.AllowAnyMethod();
                builder.AllowCredentials();
            });
            //app.UseOptions();

            app.UseDeveloperExceptionPage();

            //if (env.IsDevelopment())
            //{
            //    app.UseDeveloperExceptionPage();
            //}

            app.UseDefaultFiles();

            app.UseStaticFiles(
                new StaticFileOptions
                {
                    ServeUnknownFileTypes = true
                });

            app.UseAuthentication();
            
            app.UseMvc();

            app.Use(async (context, next) =>
            {
                await next();

                context.Response.Headers.Add("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
                context.Response.Headers.Add("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,PATCH,OPTIONS");

                if (!context.Response.Headers.ContainsKey("Access-Control-Allow-Origin"))
                {
                    context.Response.Headers["Access-Control-Allow-Origin"] = "*";
                }

                if (context.Request.Method?.ToUpperInvariant() == "OPTIONS")
                {
                    context.Response.StatusCode = 200;
                    await context.Response.WriteAsync("");
                }
                else
                if (context.Response.StatusCode == 404 &&
                    !Path.HasExtension(context.Request.Path.Value) &&
                    !context.Request.Path.Value.StartsWith("/api/"))
                {
                    context.Response.StatusCode = 200;
                    context.Response.ContentType = "text/html";
                    await context.Response.SendFileAsync(Path.Combine(env.WebRootPath, "index.html"));
                    //context.Request.Path = "/index.html";
                }
            });

            //app.Run(async (context) => { });
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            string dbTypeParam = Configuration["DbTypeSettings:DbType"].ToString();
            string connectionString = GetVeroVrStatisticsConnectionString(dbTypeParam);
            DbType dbType = Enum.Parse<DbType>(dbTypeParam.Trim(), ignoreCase: true);

            loggerFactory.AddContext(LogLevel.Error, dbType, connectionString);
            string pathToWriteLog = Path.Combine(Directory.GetCurrentDirectory(), "LogEvent.log");
            loggerFactory.AddFile(LogLevel.Error, pathToWriteLog);
        }
    }
}
