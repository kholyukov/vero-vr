﻿using VeroVrCore.Common.Infrastructure;

namespace VeroVrCore.WebApi.Model
{
    public class SharingModel
    {
        private string _description;

        public string ProjectName { get; set; }
        public string ProjectDescription
        {
            get { return LinkParser.GerParsedDescription(_description); }
            set { _description = value; }
        }

        public string ProjectImageUrl { get; set; }
        public string RedirectUrl { get; set; }
    }
}
